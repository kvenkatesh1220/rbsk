package ejbs;


import java.util.ArrayList;

import javax.ejb.Stateless;

import dao.ChildBirthDAO;
import dao.UpdateBabyDetailsDao;
import util.beans.AddressBean;
import util.beans.AntenataldetailsBean;
import util.beans.BabyTotalDataBean;
import util.beans.CongenitalanomaliesBean;
import util.beans.DefectsBean;
import util.beans.DeliverydetailsBean;
import util.beans.DiagnosisdetailsBean;
import util.beans.DropDownBean;
import util.beans.UserMappingBean;
import util.beans.InvestigationdetailsBean;
import util.beans.ReportingdetailsBean;



@Stateless
public class ChildBirthBean implements ChildBirthBeanRemote  {

    public ChildBirthBean() {
    }


    public boolean insertAddress(AddressBean address)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertAddress EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertAddress(address);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public boolean insertAntenataldetails(AntenataldetailsBean antenataldetails)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertAntenataldetails EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertAntenataldetails(antenataldetails);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }

    
    public boolean insertCongenitalanomalies(CongenitalanomaliesBean congenitalanomalies)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertCongenitalanomalies EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertCongenitalanomalies(congenitalanomalies);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public boolean insertDefects(ArrayList<DefectsBean> defectslist)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertDefects EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	//DefectsBean defects=new DefectsBean();
    	//ArrayList<Defects> defectlist=new ArrayList<Defects>();
    	return childbirthdao.insertDefects(defectslist);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public boolean insertDeliverydetails(DeliverydetailsBean deliverydetails)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertDeliverydetails EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertDeliverydetails(deliverydetails);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public boolean insertDiagnosisdetails(DiagnosisdetailsBean diagnosisdetails)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertDiagnosisdetails EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertDiagnosisdetails(diagnosisdetails);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public boolean insertInvestigationdetails(InvestigationdetailsBean investigationdetails)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertInvestigationdetails EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertInvestigationdetails(investigationdetails);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public boolean insertReportingdetails(ReportingdetailsBean reportingdetails)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertReportingdetails EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertReportingdetails(reportingdetails);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
   
    public ArrayList<UserMappingBean> getUserMappings(String username){
    	System.out.println("ChildBirthBean GetDropDownValues EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.getUserMappings(username);
    	
    }
    public boolean updateBabyTotalDetailsExceptDefects(BabyTotalDataBean  babytotdetbeanobj)
    {
    	try
    	{
    	System.out.println("ChildBirthBean updateBabyTotalDetailsExceptDefects EJB  method has been called");
    	UpdateBabyDetailsDao updatebabydetails = new UpdateBabyDetailsDao();
    	return updatebabydetails.updateBabyTotalDetailsExceptDefects(babytotdetbeanobj);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public boolean deleteFromDefects(String bid)
    {
    	try
    	{
    	System.out.println("ChildBirthBean deleteFromDefects EJB  method has been called");
    	UpdateBabyDetailsDao updatebabydetails = new UpdateBabyDetailsDao();
    	return updatebabydetails.deleteFromDefects(bid);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}

    }
    
    public boolean insertIntoAudit(String bid,String username,String action)
    {
    	try
    	{
    	System.out.println("ChildBirthBean insertIntoAudit EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.insertIntoAudit( bid, username, action);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    
    public int selectFromAudit(String username,String action, String bid)
    {
    	try
    	{
    	System.out.println("ChildBirthBean selectFromAudit EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.selectFromAudit( username, action,  bid);
    	}
    	catch(Exception e)
    	{
    	return -1;
    	}
    }
    
    public ArrayList<DropDownBean> selectFromApplicationProperties()
    {
    	try
    	{
    	System.out.println("ChildBirthBean selectFromApplicationProperties  EJB  method has been called");
    	ChildBirthDAO childbirthdao = new ChildBirthDAO();
    	return childbirthdao.selectFromApplicationProperties();
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
    }
   	
}
