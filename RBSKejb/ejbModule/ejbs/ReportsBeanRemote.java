package ejbs;

import java.util.ArrayList;

import javax.ejb.Remote;

import util.beans.AuditReportBean;
import util.beans.BabyTotalDataBean;

import util.beans.HospitalBean;
import util.beans.ReportBean;
import util.beans.SelectDropDownBean;


@Remote
public interface ReportsBeanRemote {

	public ArrayList<SelectDropDownBean> loadDistricts();
	public ArrayList<SelectDropDownBean> loadClusters(String district);
	public ArrayList<SelectDropDownBean> loadSource(String district , String cluster );
	public ArrayList<SelectDropDownBean> loadHospitalNames(String district , String cluster );
	public ArrayList<SelectDropDownBean> loadDefectsCategory();
	public ArrayList<SelectDropDownBean> loadSubDefectsCategory(String defectcategory );
	public ArrayList<ReportBean> getReport(String query );
	public ArrayList<BabyTotalDataBean> getBabyTotalData(String bid,String file_server_url,String defect_images_path ,String hospital_logs_path);
	public ArrayList<AuditReportBean> getAuditReport(String query,String from );
	public boolean addHospital(HospitalBean hospitalbean);
	public boolean updateHospitalDetails(HospitalBean hospitalbean);
}
