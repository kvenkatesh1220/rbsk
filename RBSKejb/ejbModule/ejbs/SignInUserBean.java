package ejbs;


import javax.ejb.Stateless;

import dao.ChildBirthDAO;
import dao.LoginUsersDAO;
import util.beans.UserMappingBean;



@Stateless
public class SignInUserBean implements SignInUserBeanRemote {

    public SignInUserBean() {
    }

    public boolean isValidUser(String username, String password)
    {
    	try
    	{
    	System.out.println("SignInUserBean isValidUser  method has been called");
    	LoginUsersDAO loginuserdaoobj = new LoginUsersDAO();
    	return loginuserdaoobj.isValidUser(username, password);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    //isValidUser method ended here
    public boolean insertIntoLoginDetails(String custid)
    {
    	try
    	{
    	System.out.println("SignInUserBean insertIntoLoginDetails  method has been called");
    	LoginUsersDAO loginuserdaoobj = new LoginUsersDAO();
    	return loginuserdaoobj.insertIntoLoginDetails(custid);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
    }
    // changePassword method started here
    public int changePassword(String username, String currpassword , String newpassword)
    { 	
    	try
        	{
        	System.out.println("SignInUserBean changePassword  method has been called");
        	LoginUsersDAO loginuserdaoobj = new LoginUsersDAO();
        	return loginuserdaoobj.changePassword(username, currpassword, newpassword);
        	}
        	catch(Exception e)
        	{
        	return -1 ;
        	}
        }
    // changePassword method ended here
    public UserMappingBean  getUserMappingsObj(String username)
    { 	
    try
	{
	System.out.println("SignInUserBean getUserMappingsObj  method has been called");
	ChildBirthDAO childbirthdao = new ChildBirthDAO();
	return childbirthdao.getUserMappingsObj(username);
	}
	catch(Exception e)
	{
	return null ;
	}
    }
    public int selectFromLoginDetails(String custid)
    {
    	 try
    		{
    		System.out.println("SignInUserBean selectFromLoginDetails  method has been called");
    		LoginUsersDAO childbirthdao = new LoginUsersDAO();
    		return childbirthdao.selectFromLoginDetails(custid);
    		}
    		catch(Exception e)
    		{
    		return -1 ;
    		}	
    }
    public boolean userLogOut(String cust_id)
    {
    	try
    	{
    	System.out.println("SingUpBean EJB userLogOut method has been called");	
    	System.out.println("cust_id : "+cust_id);
    	LoginUsersDAO sud = new LoginUsersDAO();
    	boolean ret_val=sud.userLogOut(cust_id);
    	return ret_val;
    	}
    	catch(Exception e)
    	{
    	 System.out.println("Exception occoured in userLogOut");	
    	 return false;
    	}
    	
    }
}
