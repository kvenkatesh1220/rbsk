package ejbs;

import java.util.ArrayList;

import javax.ejb.Stateless;

import dao.AdminDAO;
import dao.ReportsDAO;
import util.beans.AuditReportBean;
import util.beans.BabyTotalDataBean;
import util.beans.HospitalBean;
import util.beans.ReportBean;
import util.beans.SelectDropDownBean;

@Stateless
public class ReportsBean implements ReportsBeanRemote {

    public ReportsBean() {
    }

	public ArrayList<SelectDropDownBean> loadDistricts()
	{
    	try
    	{
    	System.out.println("ReportsBean  loadDistricts  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.loadDistricts();
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
    }
	// loadDistricts ended
	
	public ArrayList<SelectDropDownBean> loadClusters(String district)
	{
    	try
    	{
    	System.out.println("ReportsBean  loadClusters  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.loadClusters(district);
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
    }
	//  loadClusters ended
	
	public ArrayList<SelectDropDownBean> loadSource(String district , String cluster )
	{
    	try
    	{
    	System.out.println("ReportsBean  loadSource  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.loadSource(district,cluster);
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
	}
	// loadSource ended.
	
	public ArrayList<SelectDropDownBean> loadHospitalNames(String district , String cluster )
	{
    	try
    	{
    	System.out.println("ReportsBean  loadHospitalNames  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.loadHospitalNames(district,cluster);
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
	}
	// loadHospitalNames ended
	public ArrayList<SelectDropDownBean> loadDefectsCategory()
	{
		try
    	{
    	System.out.println("ReportsBean  loadDefectsCategory  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.loadDefectsCategory();
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
	}
	// loadDefectsCategory ended
	public ArrayList<SelectDropDownBean> loadSubDefectsCategory(String defectcategory )
	{
		try
    	{
    	System.out.println("ReportsBean  loadSubDefectsCategory  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.loadSubDefectsCategory(defectcategory);
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
	}
    // loadSubDefectsCategory ended

	
	public ArrayList<ReportBean> getReport(String query )
	{
		try
    	{
    	System.out.println("ReportsBean  getReport  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.getReport(query);
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
	}
	// Get Report ended 
	public ArrayList<AuditReportBean> getAuditReport(String query,String from )
	{
		try
    	{
    	System.out.println("ReportsBean  getAuditReport  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.getAuditReport(query,from);
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
	}
	// Get Audit Report ended 
	public ArrayList<BabyTotalDataBean> getBabyTotalData(String bid,String file_server_url,String defect_images_path,String hospital_logs_path )
	{
		try
    	{
    	System.out.println("ReportsBean  getBabyTotalData  EJB  method has been called");
    	ReportsDAO  childbirthdao = new ReportsDAO();
    	return childbirthdao.getBabyTotalData(bid,file_server_url,defect_images_path,hospital_logs_path);
    	}
    	catch(Exception e)
    	{
    	return null;
    	}
	}
	public boolean addHospital(HospitalBean hospitalbean)
	{
		try
    	{
    	System.out.println("ReportsBean  addHospital  EJB  method has been called");
    	AdminDAO  admindao = new AdminDAO();
    	return admindao.addHospital(hospitalbean);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
	}
	public boolean updateHospitalDetails(HospitalBean hospitalbean)
	{
		try
    	{
    	System.out.println("ReportsBean  updateHospitalDetails  EJB  method has been called");
    	AdminDAO  admindao = new AdminDAO();
    	return admindao.updateHospitalDetails(hospitalbean);
    	}
    	catch(Exception e)
    	{
    	return false;
    	}
	}
}
