package ejbs;


import javax.ejb.Remote;

import util.beans.UserMappingBean;






@Remote
public interface SignInUserBeanRemote {

	public boolean isValidUser(String username, String password);
	public int changePassword(String username, String currpassword , String newpassword);
	public UserMappingBean  getUserMappingsObj(String username);
	 public boolean userLogOut(String cust_id);
	 public boolean insertIntoLoginDetails(String custid);
	 public int selectFromLoginDetails(String custid);
}
