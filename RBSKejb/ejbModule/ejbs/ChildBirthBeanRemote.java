package ejbs;


import java.util.ArrayList;

import javax.ejb.Remote;

import util.beans.AddressBean;
import util.beans.AntenataldetailsBean;
import util.beans.BabyTotalDataBean;
import util.beans.CongenitalanomaliesBean;
import util.beans.DefectsBean;
import util.beans.DeliverydetailsBean;
import util.beans.DiagnosisdetailsBean;
import util.beans.DropDownBean;
import util.beans.UserMappingBean;
import util.beans.InvestigationdetailsBean;
import util.beans.ReportingdetailsBean;






@Remote
public interface ChildBirthBeanRemote {

	public boolean insertAddress(AddressBean address);
	public boolean insertAntenataldetails(AntenataldetailsBean antenataldetails);
	public boolean insertCongenitalanomalies(CongenitalanomaliesBean congenitalanomalies);
	public boolean insertDefects(ArrayList<DefectsBean> defectslist);
	public boolean insertDeliverydetails(DeliverydetailsBean deliverydetails);
	public boolean insertDiagnosisdetails(DiagnosisdetailsBean diagnosisdetails);
	public boolean insertInvestigationdetails(InvestigationdetailsBean investigationdetails);
	public boolean insertReportingdetails(ReportingdetailsBean reportingdetails);
	public ArrayList<UserMappingBean> getUserMappings(String username);
	public boolean updateBabyTotalDetailsExceptDefects(BabyTotalDataBean  babytotdetbeanobj);
	public boolean deleteFromDefects(String bid);
	public boolean insertIntoAudit(String bid,String username,String action);
	public int selectFromAudit(String username,String action, String bid);
	public ArrayList<DropDownBean> selectFromApplicationProperties();
	
}
