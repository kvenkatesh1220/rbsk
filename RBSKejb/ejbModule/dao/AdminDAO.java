package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import util.DBUtil;
 
import util.beans.HospitalBean;
import util.beans.UserMappingBean;
 

public class AdminDAO {
	
	static Logger logger = Logger.getLogger(AdminDAO.class.getName());
	public int createUser(String  username ,	String  password ,	String  districts ,	String  clusters ,	String  hospitalid ,	String  hospitaltype ,	String  isadmin )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    PreparedStatement preparedStmt1;
	    DBUtil dbutil = new DBUtil();
	    try
	    {
	    	conn = dbutil.getConnection("AdminDAO_createUser");
	    	String query="select count(*) as count from  loginusers where username='"+username+"' ";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("AdminDAO_createUser  query executed");
	    	int count=0;
	    	int insertcount=0;
	    	while(rs.next())
	    	{
	    		count=rs.getInt("count");
	    		logger.info("Record Found :: count is ::"+count);
	    	}
	    	if(count>0)
	    	{
	    		return -1; // means userfound
	    	}
	    	else
	    	{
	    		String insertquery="insert into loginusers(username	,password	,district	,cluster 	,hospitalid	,hospitaltype	,usertype)"
	    				+ " values('"+username+"'	,'"+password+"'	,'"+districts+"','"+clusters+"' , '"+hospitalid+"','"+hospitaltype+"','"+isadmin+"') ";
	    		preparedStmt1=conn.prepareStatement(insertquery);
	    		insertcount=preparedStmt1.executeUpdate(insertquery);
	    	}
	    	if(insertcount>0)
	    	{
	    		return insertcount;
	    	}
	    	else
	    	{
	    		return 0; // means not inserted
	    	}

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return -2;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "AdminDAO_createUser");
	    }
		
	}// createUser ended
	
	public ArrayList<UserMappingBean> getUserDetails(String username)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<UserMappingBean> usermappingsbeanlist = new ArrayList<UserMappingBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_getUserDetails");
	    	//String query="select username , district,cluster,hospitalname,hospitaltype,usertype  from  loginusers where username='"+username+"' ";  
	    	String query =" select lu.username,lu.district,lu.cluster,hs.hospital_name,lu.hospitaltype,lu.usertype,hs.hospitalid  from loginusers  lu "
					+ " 	join hospitaldetails hs on "
					+ " lu.hospitalid=hs.hospitalid where username='"+username+"' ";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_getUserDetails  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	UserMappingBean usermappingbeanobj = new UserMappingBean();
	    	usermappingbeanobj.setUser_id(rs.getString("username"));
	    	usermappingbeanobj.setDistrict(rs.getString("district"));
	    	usermappingbeanobj.setCluster(rs.getString("cluster"));
	    	usermappingbeanobj.setHospitalname(rs.getString("hospital_name"));
	    	usermappingbeanobj.setHospitaltype(rs.getString("hospitaltype"));
	    	usermappingbeanobj.setUsertype(rs.getString("usertype"));
	    	usermappingbeanobj.setHospitalid(rs.getString("hospitalid"));
	    	usermappingsbeanlist.add(usermappingbeanobj);
	    	}
	    	return usermappingsbeanlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_getUserDetails");
	    }
	}
	// getUserDeatails ended.
	
	public int updateUser(String  username ,String  districts ,	String  clusters ,	String  hospitalname ,	String  hospitaltype ,	String  isadmin )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    
	    DBUtil dbutil = new DBUtil();
	    try
	    {
	    	conn = dbutil.getConnection("AdminDAO_updateUser");
	    	String query=" update loginusers set district='"+districts+"',cluster='"+clusters+"',hospitalid='"+hospitalname+"' ,hospitaltype='"+hospitaltype+"' ,usertype='"+isadmin+"'  where username ='"+username+"' ";
	    	int count=0;
	    	preparedStmt = conn.prepareStatement(query);
	    	count = preparedStmt.executeUpdate();
	    	
	    	logger.info("AdminDAO_updateUser  query executed");
	    	
	    	if(count>0)
	    	{
	    		return count;
	    	}
	    	else
	    	{
	    		return count; // means not inserted
	    	}

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return -1;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "AdminDAO_createUser");
	    }
		
	}
	// updateuserdetails ended
	
	
	public int ResetPasswordInAdmin(String  username ,String  password )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    
	    DBUtil dbutil = new DBUtil();
	    try
	    {
	    	conn = dbutil.getConnection("AdminDAO_ResetPasswordInAdmin");
	    	String query=" update loginusers set password='"+password+"'   where username ='"+username+"' ";
	    	int count=0;
	    	preparedStmt = conn.prepareStatement(query);
	    	count = preparedStmt.executeUpdate();
	    	
	    	logger.info("AdminDAO_ResetPasswordInAdmin  query executed");
	    	
	    	if(count>0)
	    	{
	    		return count;
	    	}
	    	else
	    	{
	    		return count; // means not inserted
	    	}

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return -1;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "AdminDAO_ResetPasswordInAdmin");
	    }
		
	}
	// updatepwdinAdmin ended
	
	
	// Hospital creation
	
public boolean addHospital(HospitalBean hospitalbean)
{
    Connection conn=null;
    
    PreparedStatement preparedStmt1;
    DBUtil dbutil = new DBUtil();
    try
    {
    	conn = dbutil.getConnection("AdminDAO_addHospital");
    	logger.info("AdminDAO_addHospital  query executed");
    	
    		String insertquery="insert into hospitaldetails (hospitalid,	hospital_name,	regnum,	phonenum,	logo,	address,	country,	state,	district,	latitude,	longitude,	loccation)"
    				+ " values('"+hospitalbean.getHospitalid()+"'	,'"+hospitalbean.getHospitalname()+"'	,'"+hospitalbean.getRegnum()+"','"
    				+hospitalbean.getPhnum()+"' , '"+hospitalbean.getLogo()+"','"+hospitalbean.getAddress()+"','"+hospitalbean.getCountry()+"','"+hospitalbean.getState()+"','"+hospitalbean.getDistrict()+"','"+hospitalbean.getLatitude()+"'"
    						+ ",'"+hospitalbean.getLongitude()+"','"+hospitalbean.getLoccation()+"') ";
    		
    		preparedStmt1=conn.prepareStatement(insertquery);
    		int insertcount=preparedStmt1.executeUpdate(insertquery);
    	
    	if(insertcount>0)
    	{
    		return true;
    	}
    	else
    	{
    		return false; 
    	}

    }
    catch(Exception e)
    {
    	e.printStackTrace();
    	return false;
    }
    finally
    {
    	dbutil.closeConnection(conn, "AdminDAO_addHospital");
    }
} 
// hospital addition
public ArrayList<HospitalBean> getAutoCompleteHospitalDetails(String district  )
{

    Connection conn=null;
    PreparedStatement preparedStmt;
    DBUtil dbutil = new DBUtil();
    ArrayList<HospitalBean> hospitalbeanlist = new ArrayList<HospitalBean>();
    
    try
    {
    	String query="";
    	conn = dbutil.getConnection("ReportsDAO_getAutoCompleteHospitalDetails");
    	
    		query=query+"select * from hospitaldetails where district='"+district+"' ";
    	
    	
    	logger.info("query from AdminDao of getAutoCompleteHospitalDetails() ::"+query);
    	preparedStmt = conn.prepareStatement(query);
    	ResultSet rs = preparedStmt.executeQuery();
    	
    	logger.info("ReportsDAO_getAutoCompleteHospitalDetails  query executed");
    	
    	while(rs.next())
    	{
    	logger.info("Record Found");
    	HospitalBean hspbeanobj=new HospitalBean();
    	hspbeanobj.setHospitalid(rs.getString("hospitalid"));
    	String hspname=rs.getString("hospital_name").trim();
    	// String dist=rs.getString("district");
    	String address=rs.getString("address");
    	String totaddress=hspname;
    	if(address!=null && !"".equals(address))
    	{
    		totaddress=totaddress+"::"+address;
    	}
    	hspbeanobj.setAddress(totaddress);
    /*	hspbeanobj.setRegnum(rs.getString("regnum"));
    	hspbeanobj.setPhnum(rs.getString("phonenum"));
    	hspbeanobj.setLogo(rs.getString("logo"));
    	hspbeanobj.setAddress(rs.getString("address"));
    	hspbeanobj.setCountry(rs.getString("country"));
    	hspbeanobj.setState(rs.getString("state"));
    	hspbeanobj.setDistrict(rs.getString("district"));
    	hspbeanobj.setLatitude(rs.getString("latitude"));
    	hspbeanobj.setLongitude(rs.getString("longitude"));
    	hspbeanobj.setLoccation(rs.getString("loccation"));*/
    	
    	hospitalbeanlist.add(hspbeanobj);
    	}
    	return hospitalbeanlist;

    }
    catch(Exception e)
    {
    	e.printStackTrace();
    	return null;
    }
    finally
    {
    	dbutil.closeConnection(conn, "ReportsDAO_getAutoCompleteHospitalDetails");
    }


}
public ArrayList<HospitalBean> getHospitalDetails(String district , String hospitalname , String hspid)
{
    Connection conn=null;
    PreparedStatement preparedStmt;
    DBUtil dbutil = new DBUtil();
    ArrayList<HospitalBean> hospitalbeanlist = new ArrayList<HospitalBean>();
    
    try
    {
    	String query="";
    	conn = dbutil.getConnection("ReportsDAO_getHospitalDetails");
    	if(hspid!= null && !"".equals(hspid.trim()))
    	{
    		query=query+"select * from hospitaldetails where hospitalid='"+hspid+"' ";
    	}
    	else{
    	 query=query+"select * from hospitaldetails where district='"+district+"' ";
    	}
    	 if(hospitalname!=null && !"".equals(hospitalname.trim()))
    		 {
    		 query=query+"and hospital_name like '%"+hospitalname+"%'";
    		 }
    	logger.info("query from AdminDao of getHospitalDetails() ::"+query);
    	preparedStmt = conn.prepareStatement(query);
    	ResultSet rs = preparedStmt.executeQuery();
    	
    	logger.info("ReportsDAO_getHospitalDetails  query executed");
    	
    	while(rs.next())
    	{
    	logger.info("Record Found");
    	HospitalBean hspbeanobj = new HospitalBean();
    	hspbeanobj.setHospitalid(rs.getString("hospitalid"));
    	hspbeanobj.setHospitalname(rs.getString("hospital_name"));
    	hspbeanobj.setRegnum(rs.getString("regnum"));
    	hspbeanobj.setPhnum(rs.getString("phonenum"));
    	hspbeanobj.setLogo(rs.getString("logo"));
    	hspbeanobj.setAddress(rs.getString("address"));
    	hspbeanobj.setCountry(rs.getString("country"));
    	hspbeanobj.setState(rs.getString("state"));
    	hspbeanobj.setDistrict(rs.getString("district"));
    	hspbeanobj.setLatitude(rs.getString("latitude"));
    	hspbeanobj.setLongitude(rs.getString("longitude"));
    	hspbeanobj.setLoccation(rs.getString("loccation"));
    	
    	hospitalbeanlist.add(hspbeanobj);
    	}
    	return hospitalbeanlist;

    }
    catch(Exception e)
    {
    	e.printStackTrace();
    	return null;
    }
    finally
    {
    	dbutil.closeConnection(conn, "ReportsDAO_getHospitalDetails");
    }

}
//  get Hospital details
	
// update Hospital

public boolean updateHospitalDetails(HospitalBean hospitalbean)
{
    Connection conn=null;
    
    PreparedStatement preparedStmt1;
    DBUtil dbutil = new DBUtil();
    try
    {
    	conn = dbutil.getConnection("AdminDAO_updateHospitalDetails");
    	logger.info("AdminDAO_updateHospitalDetails  query executed");
    	
    		String updatequery=" update hospitaldetails set  "
    				+" hospital_name = '"+hospitalbean.getHospitalname()+"' , "
    				+" regnum ='"+hospitalbean.getRegnum()+"', "
    				+" phonenum ='"+hospitalbean.getPhnum()+"' , "
    				+" logo ='"+hospitalbean.getLogo()+"', "
    				+" address = '"+hospitalbean.getAddress()+"', "
    				+" country = '"+hospitalbean.getCountry()+"', "
    				+" state = '"+hospitalbean.getState()+"', "
    				+" district = '"+hospitalbean.getDistrict()+"', "
    				+" latitude = '"+hospitalbean.getLatitude()+"',"
    				+" longitude = '"+hospitalbean.getLongitude()+"', "
    				+" loccation = '"+hospitalbean.getLoccation()+"' "
    				+" where hospitalid ='"+hospitalbean.getHospitalid()+"' " ;
    		
    		logger.info("Query in AdminDAO_updateHospitalDetails ::"+ updatequery);
    		preparedStmt1=conn.prepareStatement(updatequery);
    		int updatecount=preparedStmt1.executeUpdate(updatequery);
    	
    	if(updatecount>0)
    	{
    		return true;
    	}
    	else
    	{
    		return false; 
    	}

    }
    catch(Exception e)
    {
    	e.printStackTrace();
    	return false;
    }
    finally
    {
    	dbutil.closeConnection(conn, "AdminDAO_updateHospitalDetails");
    }

}
}
