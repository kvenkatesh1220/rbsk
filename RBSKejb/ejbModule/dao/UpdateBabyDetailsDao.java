package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;

import util.DBUtil;
//import util.MyDateUtil;
import util.beans.BabyTotalDataBean;

public class UpdateBabyDetailsDao {

	static Logger logger = Logger.getLogger(UpdateBabyDetailsDao.class.getName());
	public boolean updateBabyTotalDetailsExceptDefects(BabyTotalDataBean  babytotdetails)
	{

	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertDeliverydetails");
	    	logger.info("***************************************in UPDATE DAO******************************************************************");
			logger.info("babytotdetails.getBhouseno()  :: "+babytotdetails.getBhouseno()+"babytotdetails.getFathername() :: "+babytotdetails.getFathername());
			logger.info("babytotdetails.getBstreetname()  :: "+babytotdetails.getBstreetname());
			logger.info("babytotdetails.getBareaname()  :: "+babytotdetails.getBareaname());
			logger.info("babytotdetails.getBpostoffice()  :: "+babytotdetails.getBpostoffice());
			logger.info("babytotdetails.getBdistrict()  :: "+babytotdetails.getBdistrict());
			logger.info("babytotdetails.getBstate()  :: "+babytotdetails.getBstate());
			logger.info("babytotdetails.getBthouseno()  :: "+babytotdetails.getBthouseno());
			logger.info("babytotdetails.getBtstreetname()  :: "+babytotdetails.getBtstreetname());
			logger.info("babytotdetails.getBtareaname()  :: "+babytotdetails.getBtareaname());
			logger.info("babytotdetails.getBtpostoffice()  :: "+babytotdetails.getBtpostoffice());
			logger.info("babytotdetails.getBtstate()  :: "+babytotdetails.getBtstate());
			logger.info("babytotdetails.getBtdistrict()  :: "+babytotdetails.getBtdistrict());
			logger.info("babytotdetails.getFolicaciddetails()  :: "+babytotdetails.getFolicaciddetails());
			logger.info("babytotdetails.getHoseriousmetirialillness()  :: "+babytotdetails.getHoseriousmetirialillness());
			logger.info("babytotdetails.getHoradiationexposure()  :: "+babytotdetails.getHoradiationexposure());
			logger.info("babytotdetails.getHosubstancceabuse()  :: "+babytotdetails.getHosubstancceabuse());
			logger.info("babytotdetails.getParentalconsanguinity()  :: "+babytotdetails.getParentalconsanguinity());
			logger.info("babytotdetails.getAssistedconception()  :: "+babytotdetails.getAssistedconception());
			logger.info("babytotdetails.getImmunisationhistory()  :: "+babytotdetails.getImmunisationhistory());
			logger.info("babytotdetails.getMaternaldrugs()  :: "+babytotdetails.getMaternaldrugs());
			logger.info("babytotdetails.getHistoryofanomalies()  :: "+babytotdetails.getHistoryofanomalies());
			logger.info("babytotdetails.getNoofpreviousabortion()  :: "+babytotdetails.getNoofpreviousabortion());
			logger.info("babytotdetails.getNofofpreviousstillbirth()  :: "+babytotdetails.getNofofpreviousstillbirth());
			logger.info("babytotdetails.getMaternaldrugsdesc()  :: "+babytotdetails.getMaternaldrugsdesc());
			logger.info("babytotdetails.getHeadcircumference()  :: "+babytotdetails.getHeadcircumference());
			logger.info("babytotdetails.getName()  :: "+babytotdetails.getName());
			logger.info("babytotdetails.getDescription()  :: "+babytotdetails.getDescription());
			logger.info("babytotdetails.getAgeat()  :: "+babytotdetails.getAgeat());
			logger.info("babytotdetails.getCode()  :: "+babytotdetails.getCode());
			logger.info("babytotdetails.getConfirmmed()  :: "+babytotdetails.getConfirmmed());
			logger.info("babytotdetails.getDateofbirth()  :: "+babytotdetails.getDateofbirth());
			logger.info("babytotdetails.getBirthweightingrms()  :: "+babytotdetails.getBirthweightingrms());
			logger.info("babytotdetails.getBirthorder()  :: "+babytotdetails.getBirthorder());
			logger.info("babytotdetails.getBabydeliveryas()  :: "+babytotdetails.getBabydeliveryas());
			logger.info("babytotdetails.getSex()  :: "+babytotdetails.getSex());
			logger.info("babytotdetails.getGestationalageinweeks()  :: "+babytotdetails.getGestationalageinweeks());
			logger.info("babytotdetails.getLastmensuralperiod()  :: "+babytotdetails.getLastmensuralperiod());
			logger.info("babytotdetails.getBirthasphyxia()  :: "+babytotdetails.getBirthasphyxia());
			logger.info("babytotdetails.getAutopsyshowbirth()  :: "+babytotdetails.getAutopsyshowbirth());
			logger.info("babytotdetails.getModeofdelivery()  :: "+babytotdetails.getModeofdelivery());
			logger.info("babytotdetails.getStatusofinduction()  :: "+babytotdetails.getStatusofinduction());
			logger.info("babytotdetails.getBirthstate()  :: "+babytotdetails.getBirthstate());
			logger.info("babytotdetails.getBirthdistict()  :: "+babytotdetails.getBirthdistict());
			logger.info("babytotdetails.getBirthblock()  :: "+babytotdetails.getBirthblock());
			logger.info("babytotdetails.getBirthminicipality()  :: "+babytotdetails.getBirthminicipality());
			logger.info("babytotdetails.getBirthplace()  :: "+babytotdetails.getBirthplace());
			logger.info("babytotdetails.getMctsno()  :: "+babytotdetails.getMctsno());
			logger.info("babytotdetails.getAdharno()  :: "+babytotdetails.getAdharno());
			logger.info("babytotdetails.getChildname()  :: "+babytotdetails.getChildname());
			logger.info("babytotdetails.getMothersname()  :: "+babytotdetails.getMothersname());
			logger.info("babytotdetails.getMothersage()  :: "+babytotdetails.getMothersage());
			logger.info("babytotdetails.getFathersage()  :: "+babytotdetails.getFathersage());
			logger.info("babytotdetails.getMobilenumber()  :: "+babytotdetails.getMobilenumber());
			logger.info("babytotdetails.getCaste()  :: "+babytotdetails.getCaste());
			logger.info("babytotdetails.getNoofphotos()  :: "+babytotdetails.getNoofphotos());
			logger.info("babytotdetails.getNoofattachments()  :: "+babytotdetails.getNoofattachments());
			logger.info("babytotdetails.getPhoto1()  :: "+babytotdetails.getPhoto1());
			logger.info("babytotdetails.getPhoto2()  :: "+babytotdetails.getPhoto2());
			logger.info("babytotdetails.getReportattachment1()  :: "+babytotdetails.getReportattachment1());
			logger.info("babytotdetails.getReportattachment2()  :: "+babytotdetails.getReportattachment2());
			logger.info("babytotdetails.getReportattachment3()  :: "+babytotdetails.getReportattachment3());
			logger.info("babytotdetails.getReportattachment4()  :: "+babytotdetails.getReportattachment4());
			logger.info("babytotdetails.getReportattachment5()  :: "+babytotdetails.getReportattachment5());
			logger.info("babytotdetails.getReportattachment6()  :: "+babytotdetails.getReportattachment6());
			logger.info("babytotdetails.getNoofbabies()  :: "+babytotdetails.getNoofbabies());
			logger.info("babytotdetails.getTypeofdelivery()  :: "+babytotdetails.getTypeofdelivery());
			logger.info("babytotdetails.getBirthtype()  :: "+babytotdetails.getBirthtype());
			logger.info("babytotdetails.getAnomaly()  :: "+babytotdetails.getAnomaly());
			logger.info("babytotdetails.getSyndrome()  :: "+babytotdetails.getSyndrome());
			logger.info("babytotdetails.getProvisional()  :: "+babytotdetails.getProvisional());
			logger.info("babytotdetails.getComplediagnosis()  :: "+babytotdetails.getComplediagnosis());
			logger.info("babytotdetails.getNotifyingperson()  :: "+babytotdetails.getNotifyingperson());
			logger.info("babytotdetails.getDesignationofcontact()  :: "+babytotdetails.getDesignationofcontact());
			logger.info("babytotdetails.getFacilityreferred()  :: "+babytotdetails.getFacilityreferred());
			logger.info("babytotdetails.getProvisionaldiagstat()  :: "+babytotdetails.getProvisionaldiagstat());
			logger.info("babytotdetails.getKarotype()  :: "+babytotdetails.getKarotype());
			logger.info("babytotdetails.getKarotypefindings()  :: "+babytotdetails.getKarotypefindings());
			logger.info("babytotdetails.getBloodtestforch()  :: "+babytotdetails.getBloodtestforch());
			logger.info("babytotdetails.getBloodtestforcah()  :: "+babytotdetails.getBloodtestforcah());
			logger.info("babytotdetails.getBloodtestforg6Pd()  :: "+babytotdetails.getBloodtestforg6pd());
			logger.info("babytotdetails.getBloodtestforscd()  :: "+babytotdetails.getBloodtestforscd());
			logger.info("babytotdetails.getBloodtestothers()  :: "+babytotdetails.getBloodtestothers());
			logger.info("babytotdetails.getBloodtestfindings()  :: "+babytotdetails.getBloodtestfindings());
			logger.info("babytotdetails.getBera()  :: "+babytotdetails.getBera());
			logger.info("babytotdetails.getBerafindings()  :: "+babytotdetails.getBerafindings());
			logger.info("babytotdetails.getState()  :: "+babytotdetails.getState());
			logger.info("babytotdetails.getDistrict()  :: "+babytotdetails.getDistrict());
			logger.info("babytotdetails.getCluster()  :: "+babytotdetails.getCluster());
			logger.info("babytotdetails.getSource()  :: "+babytotdetails.getSource());
			logger.info("babytotdetails.getReportingdate()  :: "+babytotdetails.getReportingdate());
			logger.info("babytotdetails.getDateofidentification()  :: "+babytotdetails.getDateofidentification());
			logger.info("babytotdetails.getAgeofidentification()  :: "+babytotdetails.getAgeofidentification());
			logger.info("babytotdetails.getHospitalname()  :: "+babytotdetails.getHospitalname());
logger.info("***************************************in UPDATE DAO******************************************************************");

			String query2 ="UPDATE address address "
					+"JOIN Antenataldetails antenataldetails ON address.bid= antenataldetails.bid "
					+"JOIN Congenitalanomalies congenitalanomalies ON address.bid= congenitalanomalies.bid "
					+"JOIN Defects defects ON address.bid= defects.bid "
					+"JOIN Deliverydetails deliverydetails ON address.bid= deliverydetails.bid "
					+"JOIN Diagnosisdetails diagnosisdetails ON address.bid= diagnosisdetails.bid "
					+"JOIN investigationdetails investigationdetails ON address.bid= investigationdetails.bid "
					+"JOIN Reportingdetails reportingdetails ON address.bid= reportingdetails.bid "
					+"SET address.bhouseno= ?, "
					+"    address.bstreetname= ?, "
					+"    address.bareaname= ?, "
					+"    address.bpostoffice= ?, "
					+"    address.bdistrict= ?, "
					+"    address.bstate= ?, "
					+"    address.bthouseno= ?, "
					+"    address.btstreetname= ?, "
					+"    address.btareaname= ?, "
					+"    address.btpostoffice= ?, "
					+"    address.btstate= ?, "
					+"    address.btdistrict= ?, "
					+"    antenataldetails.folicaciddetails= ?, "
					+"    antenataldetails.hoseriousmetirialillness= ?, "
					+"    antenataldetails.horadiationexposure= ?, "
					+"    antenataldetails.hosubstancceabuse= ?, "
					+"    antenataldetails.parentalconsanguinity= ?, "
					+"    antenataldetails.assistedconception= ?, "
					+"    antenataldetails.immunisationhistory= ?, "
					+"    antenataldetails.maternaldrugs= ?, "
					+"    antenataldetails.historyofanomalies= ?, "
					+"    antenataldetails.noofpreviousabortion= ?, "
					+"    antenataldetails.nofofpreviousstillbirth= ?, "
					+"    antenataldetails.maternaldrugsdesc= ?, "
					+"    antenataldetails.headcircumference= ?, "
					+"    congenitalanomalies.name= ?, "
					+"    congenitalanomalies.description= ?, "
					+"    congenitalanomalies.ageat= ?, "
					+"    congenitalanomalies.code= ?, "
					+"    congenitalanomalies.confirmmed= ?, "
				
					+"    deliverydetails.dateofbirth= IFNULL(STR_TO_DATE(?, '%d-%b-%Y %T'),null) , "
					+"    deliverydetails.birthweightingrms= ?, "
					+"    deliverydetails.birthorder= ?, "
					+"    deliverydetails.babydeliveryas= ?, "
					+"    deliverydetails.sex= ?, "
					+"    deliverydetails.gestationalageinweeks= ?, "
					+"    deliverydetails.lastmensuralperiod= IFNULL(STR_TO_DATE(? , '%Y-%m-%d '),null) , "
					+"    deliverydetails.birthasphyxia= ?, "
					+"    deliverydetails.autopsyshowbirth= ?, "
					+"    deliverydetails.modeofdelivery= ?, "
					+"    deliverydetails.statusofinduction= ?, "
					+"    deliverydetails.birthstate= ?, "
					+"    deliverydetails.birthdistict= ?, "
					+"    deliverydetails.birthblock= ?, "
					+"    deliverydetails.birthminicipality= ?, "
					+"    deliverydetails.birthplace= ?, "
					+"    deliverydetails.mctsno= ?, "
					+"    deliverydetails.adharno= ?, "
					+"    deliverydetails.childname= ?, "
					+"    deliverydetails.mothersname= ?, "
					+"    deliverydetails.mothersage= ?, "
					+"    deliverydetails.fathersage= ?, "
					+"    deliverydetails.mobilenumber= ?, "
					+"    deliverydetails.caste= ?, "
					+"    deliverydetails.noofphotos= ?, "
					+"    deliverydetails.noofattachments= ?, "
					+"    deliverydetails.photo1= ?, "
					+"    deliverydetails.photo2= ?, "
					+"    deliverydetails.reportattachment1= ?, "
					+"    deliverydetails.reportattachment2= ?, "
					+"    deliverydetails.reportattachment3= ?, "
					+"    deliverydetails.reportattachment4= ?, "
					+"    deliverydetails.reportattachment5= ?, "
					+"    deliverydetails.reportattachment6= ?, "
					+"    deliverydetails.noofbabies= ?, "
					+"    deliverydetails.typeofdelivery= ?, "
					+"    deliverydetails.birthtype= ?, "
					+"    diagnosisdetails.anomaly= ?, "
					+"    diagnosisdetails.syndrome= ?, "
					+"    diagnosisdetails.provisional= ?, "
					+"    diagnosisdetails.complediagnosis= ?, "
					+"    diagnosisdetails.notifyingperson= ?, "
					+"    diagnosisdetails.designationofcontact= ?, "
					+"    diagnosisdetails.facilityreferred= ?, "
					+"    diagnosisdetails.provisionaldiagstat= ?, "
					+"    investigationdetails.karotype= ?, "
					+"    investigationdetails.karotypefindings= ?, "
					+"    investigationdetails.bloodtestforch= ?, "
					+"    investigationdetails.bloodtestforcah= ?, "
					+"    investigationdetails.bloodtestforg6pd= ?, "
					+"    investigationdetails.bloodtestforscd= ?, "
					+"    investigationdetails.bloodtestothers= ?, "
					+"    investigationdetails.bloodtestfindings= ?, "
					+"    investigationdetails.bera= ?, "
					+"    investigationdetails.berafindings= ?, "
					+"    reportingdetails.state= ?, "
					+"    reportingdetails.district= ?, "
					+"    reportingdetails.cluster= ?, "
					+"    reportingdetails.source= ?, "
					+"    reportingdetails.reportingdate= IFNULL(STR_TO_DATE(? , '%Y-%m-%d '),null) , "
					+"    reportingdetails.dateofidentification=IFNULL(STR_TO_DATE(? , '%Y-%m-%d '),null) , "
					+"    reportingdetails.ageofidentification= ?, "
					+"    reportingdetails.hospitalname= ? ,"
					+"    deliverydetails.fathername= ?  , " 
					+"    deliverydetails.resonforcesarean=? "
					+"WHERE address.bid=?";
			preparedStmt = conn.prepareStatement(query2);
			
			preparedStmt.setString(1 , babytotdetails.getBhouseno());
			preparedStmt.setString(2 , babytotdetails.getBstreetname());
			preparedStmt.setString(3 , babytotdetails.getBareaname());
			preparedStmt.setString(4 , babytotdetails.getBpostoffice());
			preparedStmt.setString(5 , babytotdetails.getBdistrict());
			preparedStmt.setString(6 , babytotdetails.getBstate());
			preparedStmt.setString(7 , babytotdetails.getBthouseno());
			preparedStmt.setString(8 , babytotdetails.getBtstreetname());
			preparedStmt.setString(9 , babytotdetails.getBtareaname());
			preparedStmt.setString(10 , babytotdetails.getBtpostoffice());
			preparedStmt.setString(11 , babytotdetails.getBtstate());
			preparedStmt.setString(12 , babytotdetails.getBtdistrict());
			preparedStmt.setString(13 , babytotdetails.getFolicaciddetails());
			preparedStmt.setString(14 , babytotdetails.getHoseriousmetirialillness());
			preparedStmt.setString(15 , babytotdetails.getHoradiationexposure());
			preparedStmt.setString(16 , babytotdetails.getHosubstancceabuse());
			preparedStmt.setString(17 , babytotdetails.getParentalconsanguinity());
			preparedStmt.setString(18 , babytotdetails.getAssistedconception());
			preparedStmt.setString(19 , babytotdetails.getImmunisationhistory());
			preparedStmt.setString(20 , babytotdetails.getMaternaldrugs());
			preparedStmt.setString(21 , babytotdetails.getHistoryofanomalies());
			preparedStmt.setString(22 , babytotdetails.getNoofpreviousabortion());
			preparedStmt.setString(23 , babytotdetails.getNofofpreviousstillbirth());
			preparedStmt.setString(24 , babytotdetails.getMaternaldrugsdesc());
			preparedStmt.setString(25 , babytotdetails.getHeadcircumference());
			preparedStmt.setString(26 , babytotdetails.getName());
			preparedStmt.setString(27 , babytotdetails.getDescription());
			preparedStmt.setString(28 , babytotdetails.getAgeat());
			preparedStmt.setString(29 , babytotdetails.getCode());
			preparedStmt.setString(30 , babytotdetails.getConfirmmed());
			preparedStmt.setString(31 , babytotdetails.getDateofbirth());
			preparedStmt.setString(32 , babytotdetails.getBirthweightingrms());
			preparedStmt.setString(33 , babytotdetails.getBirthorder());
			preparedStmt.setString(34 , babytotdetails.getBabydeliveryas());
			preparedStmt.setString(35 , babytotdetails.getSex());
			preparedStmt.setString(36 , babytotdetails.getGestationalageinweeks());
			preparedStmt.setString(37 , babytotdetails.getLastmensuralperiod());
			preparedStmt.setString(38 , babytotdetails.getBirthasphyxia());
			preparedStmt.setString(39 , babytotdetails.getAutopsyshowbirth());
			preparedStmt.setString(40 , babytotdetails.getModeofdelivery());
			preparedStmt.setString(41 , babytotdetails.getStatusofinduction());
			preparedStmt.setString(42 , babytotdetails.getBirthstate());
			preparedStmt.setString(43 , babytotdetails.getBirthdistict());
			preparedStmt.setString(44 , babytotdetails.getBirthblock());
			preparedStmt.setString(45 , babytotdetails.getBirthminicipality());
			preparedStmt.setString(46 , babytotdetails.getBirthplace());
			preparedStmt.setString(47 , babytotdetails.getMctsno());
			preparedStmt.setString(48 , babytotdetails.getAdharno());
			preparedStmt.setString(49 , babytotdetails.getChildname());
			preparedStmt.setString(50 , babytotdetails.getMothersname());
			preparedStmt.setString(51 , babytotdetails.getMothersage());
			preparedStmt.setString(52 , babytotdetails.getFathersage());
			preparedStmt.setString(53 , babytotdetails.getMobilenumber());
			preparedStmt.setString(54 , babytotdetails.getCaste());
			preparedStmt.setString(55 , babytotdetails.getNoofphotos());
			preparedStmt.setString(56 , babytotdetails.getNoofattachments());
			preparedStmt.setString(57 , babytotdetails.getPhoto1());
			preparedStmt.setString(58 , babytotdetails.getPhoto2());
			preparedStmt.setString(59 , babytotdetails.getReportattachment1());
			preparedStmt.setString(60 , babytotdetails.getReportattachment2());
			preparedStmt.setString(61 , babytotdetails.getReportattachment3());
			preparedStmt.setString(62 , babytotdetails.getReportattachment4());
			preparedStmt.setString(63 , babytotdetails.getReportattachment5());
			preparedStmt.setString(64 , babytotdetails.getReportattachment6());
			preparedStmt.setString(65 , babytotdetails.getNoofbabies());
			preparedStmt.setString(66 , babytotdetails.getTypeofdelivery());
			preparedStmt.setString(67 , babytotdetails.getBirthtype());
			preparedStmt.setString(68 , babytotdetails.getAnomaly());
			preparedStmt.setString(69 , babytotdetails.getSyndrome());
			preparedStmt.setString(70 , babytotdetails.getProvisional());
			preparedStmt.setString(71 , babytotdetails.getComplediagnosis());
			preparedStmt.setString(72 , babytotdetails.getNotifyingperson());
			preparedStmt.setString(73 , babytotdetails.getDesignationofcontact());
			preparedStmt.setString(74 , babytotdetails.getFacilityreferred());
			preparedStmt.setString(75 , babytotdetails.getProvisionaldiagstat());
			preparedStmt.setString(76 , babytotdetails.getKarotype());
			preparedStmt.setString(77 , babytotdetails.getKarotypefindings());
			preparedStmt.setString(78 , babytotdetails.getBloodtestforch());
			preparedStmt.setString(79 , babytotdetails.getBloodtestforcah());
			preparedStmt.setString(80 , babytotdetails.getBloodtestforg6pd());
			preparedStmt.setString(81 , babytotdetails.getBloodtestforscd());
			preparedStmt.setString(82 , babytotdetails.getBloodtestothers());
			preparedStmt.setString(83 , babytotdetails.getBloodtestfindings());
			preparedStmt.setString(84 , babytotdetails.getBera());
			preparedStmt.setString(85 , babytotdetails.getBerafindings());
			preparedStmt.setString(86 , babytotdetails.getState());
			preparedStmt.setString(87 , babytotdetails.getDistrict());
			preparedStmt.setString(88 , babytotdetails.getCluster());
			preparedStmt.setString(89 , babytotdetails.getSource());
			preparedStmt.setString(90 , babytotdetails.getReportingdate());
			preparedStmt.setString(91 , babytotdetails.getDateofidentification());
			preparedStmt.setString(92 , babytotdetails.getAgeofidentification());
			preparedStmt.setString(93 , babytotdetails.getHospitalname());
			preparedStmt.setString(94 , babytotdetails.getFathername());
			preparedStmt.setString(95 , babytotdetails.getResonforcesarean());
			preparedStmt.setString(96 , babytotdetails.getBid());
	    	//
			
			
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data Updated successfully in UpdateBabyDetailsDao_updateBabyTotalDetailsExceptDefects");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in UpdateBabyDetailsDao_updateBabyTotalDetailsExceptDefects");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "UpdateBabyDetailsDao_updateBabyTotalDetailsExceptDefects");
		}
	}
	public boolean deleteFromDefects(String bid)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("UpdateBabyDetailsDao_deleteFromDefects");
			String query2 = "delete from defects where bid='"+bid+"' ";
	    	preparedStmt = conn.prepareStatement(query2);
	    	
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data deleted successfully UpdateBabyDetailsDao_deleteFromDefects");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in UpdateBabyDetailsDao_deleteFromDefects");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "UpdateBabyDetailsDao_deleteFromDefects");
		}
		
	
	}
	
	
	
}
