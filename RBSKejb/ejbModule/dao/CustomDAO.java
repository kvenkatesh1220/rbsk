package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;



import org.apache.log4j.Logger;

import util.CustomColPojo;
import util.CustomRowPojo;
import util.DBUtil;

public class CustomDAO {
	static Logger logger = Logger.getLogger(CustomDAO.class.getName());
	/* This method is to execute queries sent from servlet and returns list of row objects */
	public ArrayList<CustomRowPojo> executeSelectQuery(String query)
	{
		logger.info("CustomDAO executeSelectQuery started here");
		Connection conn=null;
	    PreparedStatement preparedStmt=null;
	    ResultSet rs = null;
	    DBUtil dbutil = new DBUtil();
	    int noofrows=0;
	    int numberOfColumns=0;
	    ArrayList<CustomRowPojo> mycustrowsobjlist = new ArrayList<CustomRowPojo>();
		try
		{
	    	conn = dbutil.getConnection("CustomDAO_executeSelectQuery");
	    	preparedStmt = conn.prepareStatement(query);	
	    	rs = preparedStmt.executeQuery();
	    	
	    	logger.info("CustomDAO_executeSelectQuery: query executed");
	    	
	    	/* reading result set started here */
	    	logger.info("CustomDAO_executeSelectQuery: Reading result set started here");
	    	while(rs.next())
	    	{
	    		noofrows=noofrows+1;
	    		ResultSetMetaData rsMetaData = rs.getMetaData();
	    		numberOfColumns = rsMetaData.getColumnCount();
	    		ArrayList<CustomColPojo> customcurrowobjlist = new ArrayList<CustomColPojo>();
	    		
	    		for(int i=1;i<=numberOfColumns;i++)
	    		{
	    		    logger.info("Column Type of :"+rsMetaData.getColumnType(i));
	    		    customcurrowobjlist.add(new CustomColPojo(rsMetaData.getColumnName(i),rs.getString(i)));
	    		}
	    		if(customcurrowobjlist.size()>0)
	    		{
	    		CustomRowPojo customrowpojoobj = new CustomRowPojo();
	    		customrowpojoobj.setCustomrowlistobj(customcurrowobjlist);
	    		mycustrowsobjlist.add(customrowpojoobj);
	    		}
	    	}
	    	logger.info("CustomDAO_executeSelectQuery: noofrows:"+noofrows);
	    	return mycustrowsobjlist;
		}// try ended here
	    catch(Exception e)
	    {
	    	logger.info("CustomDAO_checkinputQuery: Error occured in CustomDAO_executeSelectQuery");
	    	e.printStackTrace();
	    	return mycustrowsobjlist;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "CustomDAO_checkinputQuery");
	    	try
	    	{
	    	    if(preparedStmt!=null)
	    	    {
	    		if(preparedStmt!=null){preparedStmt.close();};
	    		if(rs!=null){rs.close();};
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    	logger.info("Unable close prepared statement of CustomDAO_checkinputQuery");
	    	}
	    }
	}
	public int executeInsertQuery(ArrayList<CustomColPojo> customcurrowobjlist,String tablename)
	{
		int ret_val=0;
		try
		{
			int i=0;
			String insertquery="";
			if(customcurrowobjlist.size()>0)
			{
				String prepart =  "insert into "+tablename+"(";
				String colpart ="";
				String valuepart="values(";
				for(CustomColPojo customcolpojoobj : customcurrowobjlist)
				{
					logger.info("parametername : "+customcolpojoobj.getKey()+" :: value"+customcolpojoobj.getValue());
					if(i==customcurrowobjlist.size()-1)
					{
					colpart=colpart+customcolpojoobj.getKey();
					valuepart=valuepart+"'"+customcolpojoobj.getValue()+"'";
					}
					colpart=colpart+customcolpojoobj.getKey()+",";
					valuepart=valuepart+"'"+customcolpojoobj.getValue()+"',";
				}
				valuepart=valuepart+")";
				colpart=colpart+")";
				insertquery=insertquery+prepart+colpart+valuepart;
				if(insertquery.trim().length()>0)
				{
					Connection conn=null;
					PreparedStatement preparedStmt=null;
				    ResultSet rs = null;
				    DBUtil dbutil = new DBUtil();
				    conn = dbutil.getConnection("CustomDAO_executeInsertQuery");
				    preparedStmt = conn.prepareStatement(insertquery);
				    ret_val = preparedStmt.executeUpdate();
				    logger.info("No of Rows inserted : "+ret_val);
				}
			}
			return ret_val;
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in preparing insert statement ");
			return ret_val;
		}
	}//
	
	public int executeUpdateQuery(ArrayList<CustomColPojo> customcurrowobjlist,String tablename, String wherestatement)
	{
		int ret_val=0;
		try
		{
			int i=0;
			String updatequery="";
			String wherepart="";
			if(wherestatement.length()>0)
			{
				wherepart=wherestatement;
			}
			if(customcurrowobjlist.size()>0)
			{
				String prepart =  "update "+tablename+" set";
				String colpart ="";
				for(CustomColPojo customcolpojoobj : customcurrowobjlist)
				{
					logger.info("parametername : "+customcolpojoobj.getKey()+" :: value"+customcolpojoobj.getValue());
					if(i==customcurrowobjlist.size()-1)
					{
					colpart=colpart+customcolpojoobj.getKey()+"='"+customcolpojoobj.getValue();
					}
					colpart=colpart+customcolpojoobj.getKey()+"='"+customcolpojoobj.getValue()+",";
				}
				updatequery=updatequery+prepart+colpart+wherepart;
				if(updatequery.trim().length()>0)
				{
					Connection conn=null;
					PreparedStatement preparedStmt=null;
				    ResultSet rs = null;
				    DBUtil dbutil = new DBUtil();
				    conn = dbutil.getConnection("CustomDAO_updatequery");
				    preparedStmt = conn.prepareStatement(updatequery);
				    ret_val = preparedStmt.executeUpdate();
				    logger.info("No of Rows updated : "+ret_val);
				}
			}
			return ret_val;
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in preparing update statement ");
			return ret_val;
		}
	}//
	
}
	

