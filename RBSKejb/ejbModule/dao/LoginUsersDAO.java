package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import util.DBUtil;

public class LoginUsersDAO {
	static logger logger = logger.getlogger(LoginUsersDAO.class.getName());
	//isValidUser method started here
	public boolean isValidUser(String username, String password)
	{
		Connection conn=null;
		PreparedStatement preparedStmt;
		DBUtil dbutil = new DBUtil();
		try
		{
		 logger.info("LoginUsersDAO : isValidUser started here");
		 conn = dbutil.getConnection("LoginUsersDAO_isValidUser");
		 String query = "select count(*) from loginusers  where username=? and password=?";
		 preparedStmt = conn.prepareStatement(query);
		 preparedStmt.setString(1, username);
	     preparedStmt.setString(2, password);
		 ResultSet rs = preparedStmt.executeQuery();
		 int count=0;
		 while(rs.next())
		 {
			 count = rs.getInt(1);
	      logger.info("noofrecords found in db for user :"+username+" ::: are :"+count);
	      
		 }
		 
		 if(count>0)
		 {
			 return true;
		 }
		 
		 return false;
		 
		}
		catch(Exception e)
		{
			e.printStackTrace();
			 return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "LoginUsersDAO_isValidUser");
			logger.info("LoginUsersDAO : isValidUser ended here");
		}
	}
	//isValidUser method ended here
	
	public boolean insertIntoLoginDetails(String custid)
	{
		Connection conn=null;
		DBUtil dbutil = new DBUtil();
		try
		{
		 logger.info("LoginUsersDAO_insertIntoLoginDetails  started here");
		 conn = dbutil.getConnection("LoginUsersDAO_insertIntoLoginDetails");
		 PreparedStatement preparedStmt2;
		 String query2 = "insert into LOGIN_DETAILS(CUST_ID,LOGIN_DT) values(?,now())";
	    	
	    	preparedStmt2=conn.prepareStatement(query2);
	    	preparedStmt2.setString(1,custid);
	    	int recUpdated = preparedStmt2.executeUpdate();
	    	logger.info("No of records updated in Login Details is "+recUpdated);
	    	if(recUpdated>0)
	    	{
	    		return true;
	    	}
	    	else
	    	{
	    		return false;
	    	}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			 return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "LoginUsersDAO_insertIntoLoginDetails");
			logger.info("LoginUsersDAO_insertIntoLoginDetails ended here");
		}
	}

	public int selectFromLoginDetails(String custid)
	{
		Connection conn=null;
		PreparedStatement preparedStmt;
		DBUtil dbutil = new DBUtil();
		try
		{
		 logger.info("LoginUsersDAO : selectFromLoginDetails started here");
		 conn = dbutil.getConnection("LoginUsersDAO_selectFromLoginDetails");
		 String query = "select count(*) from login_details where CUST_ID=? ";
		 preparedStmt = conn.prepareStatement(query);
		 preparedStmt.setString(1, custid);
	     
		 ResultSet rs = preparedStmt.executeQuery();
		 int count=0;
		 while(rs.next())
		 {
			 count = rs.getInt(1);
	      logger.info("noofrecords found in db for count"+count);
	      
		 }
		return count;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			 return -1;
		}
		finally
		{
			dbutil.closeConnection(conn, "LoginUsersDAO_isValidUser");
			logger.info("LoginUsersDAO : isValidUser ended here");
		}
	
	}
	
	//changePassword method started here
		public int changePassword(String username, String currpassword , String newpassword)
		{
			Connection conn=null;
			PreparedStatement preparedStmt;
			PreparedStatement preparedStmtupdate;
			DBUtil dbutil = new DBUtil();
			try
			{
			 logger.info("LoginUsersDAO : changePassword started here");
			 conn = dbutil.getConnection("LoginUsersDAO_changePassword");
			 String query = "select count(*) from loginusers where username=? and password=?";
			 preparedStmt = conn.prepareStatement(query);
			 preparedStmt.setString(1, username);
		     preparedStmt.setString(2, currpassword);
			 ResultSet rs = preparedStmt.executeQuery();
			 int count=0;
			 while(rs.next())
			 {
				 count = rs.getInt(1);
		      logger.info("noofrecords found in db for user :"+username+" ::: are :"+count);
		      
			 }
			 
			 if(count>0)
			 {
				 String updatequery= "update loginusers set password=? where username=? and password=? ";
				 preparedStmtupdate = conn.prepareStatement(updatequery);
				 preparedStmtupdate.setString(1, newpassword);
				 preparedStmtupdate.setString(2, username);
				 preparedStmtupdate.setString(3, currpassword);
				 int updatecount=preparedStmtupdate.executeUpdate();
				 
				 if(updatecount>0)
				 {
					 return updatecount ;
				 }
				 else
				 {
					 return -1 ; // -1 means unable to update
				 }
			 }
			 
			 return -2 ; // -2 means password mismatch
			 
			}
			catch(Exception e)
			{
				e.printStackTrace();
				 return -1 ; // in exception
			}
			finally
			{
				dbutil.closeConnection(conn, "LoginUsersDAO_changePassword");
				logger.info("LoginUsersDAO : changePassword ended here");
			}
		}
		//isValidUser method ended here

		 public boolean userLogOut(String cust_id)
		    {
		    	logger.info("Entered into LoginUsersDAO: userLogOut method ");
		    	Connection conn=null;
		    	PreparedStatement ps=null;
		    	logger.info("Customer id is :"+cust_id);
		    	DBUtil dbutil = new DBUtil();
		    	try
		    	{
		    	conn=dbutil.getConnection("LoginUsersDAO:userLogOut");
		    	if(conn==null)
		    	{
			    logger.info("LoginUsersDAO  userLogOut method has been ended with connection error");
			    return false; 	
		    	}
		    	String query2 = "update LOGIN_DETAILS set LOGOUT_DT=now() where CUST_ID=? and LOGOUT_DT is null";
		    	// Calendar calendar = Calendar.getInstance();
		    //	java.sql.Date todayDate = new java.sql.Date(calendar.getTime().getTime());
		    	ps=conn.prepareStatement(query2);
		    	// ps.setDate(1,todayDate);
		    	ps.setString(1,cust_id);
		    	int recUpdated = ps.executeUpdate();
		    	logger.info("No of records updated in Login Details is "+recUpdated);
			    logger.info("LoginUsersDAO  userLogOut method has been ended properly");
		    	return true;
		    	}
		    	catch(Exception e)
		    	{
		    		logger.info("Exception Occoured in LoginUsersDAO : userLogOut : " + e);	
		    		e.printStackTrace();
		    		return false;
		    	}
		    	finally
		    	{
		    		try
		    		{
		    			if(conn!=null)
		    			conn.close();
		    		}
		    		catch(Exception e)
		    		{
		    			e.printStackTrace();
		    		}
		    	}
		    }
	
}
