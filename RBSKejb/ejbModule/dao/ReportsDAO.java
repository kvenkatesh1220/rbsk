package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import util.DBUtil;
import util.beans.AuditReportBean;
import util.beans.BabyTotalDataBean;
import util.beans.ReportBean;
import util.beans.SelectDropDownBean;

public class ReportsDAO {
	static Logger logger = Logger.getLogger(ReportsDAO.class.getName());
	public ArrayList<SelectDropDownBean> loadDistricts()
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<SelectDropDownBean> dropdownlist = new ArrayList<SelectDropDownBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_loadDistricts");
	    	// String query="select distinct district from  loginusers where district is not null";
	    	String query="select name as district from tg_districts order by name";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_loadDistricts  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	SelectDropDownBean dropdownobj = new SelectDropDownBean();
	    	String district=rs.getString("district");
	    	dropdownobj.setOption(district);
	    	dropdownobj.setValue(district);
	    	
	    	dropdownlist.add(dropdownobj);
	    	}
	    	return dropdownlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_loadDistricts");
	    }
		
	}
// Load districts method end
	
	// Load cluster started here
	
	public ArrayList<SelectDropDownBean> loadClusters(String district)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<SelectDropDownBean> dropdownlist = new ArrayList<SelectDropDownBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_loadClusters");
	    	String query="select distinct cluster from  loginusers where cluster is not null and district='"+district+"' ";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_loadClusters  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	SelectDropDownBean dropdownobj = new SelectDropDownBean();
	    	String cluster=rs.getString("cluster");
	    	dropdownobj.setOption(cluster);
	    	dropdownobj.setValue(cluster);
	    	
	    	dropdownlist.add(dropdownobj);
	    	}
	    	return dropdownlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_loadClusters");
	    }
		
	}

	// Load cluster ended here
	
	// Load Source started here
	public ArrayList<SelectDropDownBean> loadSource(String district , String cluster )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<SelectDropDownBean> dropdownlist = new ArrayList<SelectDropDownBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_loadSource");
	    	String query="select distinct source from  reportingdetails where source is not null and district='"+district+"' and cluster='"+cluster+"'  ";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_loadSource  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	SelectDropDownBean dropdownobj = new SelectDropDownBean();
	    	String source=rs.getString("source");
	    	dropdownobj.setOption(source);
	    	dropdownobj.setValue(source);
	    	
	    	dropdownlist.add(dropdownobj);
	    	}
	    	return dropdownlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_loadSource");
	    }
		
	}
	// Load Source ended here
	
	
	// Load HospitalNames Started here
	public ArrayList<SelectDropDownBean> loadHospitalNames(String district , String cluster )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<SelectDropDownBean> dropdownlist = new ArrayList<SelectDropDownBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_loadHospitalNames");
	    	String query="select distinct hospital_name from  loginusers lu "
	    			+ "     			 join hospitaldetails hp"
	    			+ "       on lu.hospitalid=hp.hospitalid where hospital_name is not null  and lu.district='"+district+"' and lu.cluster='"+cluster+"'  ";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_loadHospitalNames  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	SelectDropDownBean dropdownobj = new SelectDropDownBean();
	    	String hospitalname=rs.getString("hospital_name");
	    	dropdownobj.setOption(hospitalname);
	    	dropdownobj.setValue(hospitalname);
	    	
	    	dropdownlist.add(dropdownobj);
	    	}
	    	return dropdownlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_loadHospitalNames");
	    }
		
	}
	// Load HospitalName ended here
	
	//  Load Defects category started here
	public ArrayList<SelectDropDownBean> loadDefectsCategory()
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<SelectDropDownBean> dropdownlist = new ArrayList<SelectDropDownBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_loadDefectsCategory");
	    	String query="select distinct defectcategory from defects where defectcategory is not null";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_loadDefectsCategory  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	SelectDropDownBean dropdownobj = new SelectDropDownBean();
	    	String defectcategory=rs.getString("defectcategory");
	    	dropdownobj.setOption(defectcategory);
	    	dropdownobj.setValue(defectcategory);
	    	
	    	dropdownlist.add(dropdownobj);
	    	}
	    	return dropdownlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_loadDefectsCategory");
	    }
		
	}
	//  Load Defects category ended here

	// Load Sub Defects category startd here
	
	public ArrayList<SelectDropDownBean> loadSubDefectsCategory(String defectcategory )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<SelectDropDownBean> dropdownlist = new ArrayList<SelectDropDownBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_loadSubDefectsCategory");
	    	String query=" select distinct defectsubcategory from defects where defectsubcategory is not null and defectcategory='"+defectcategory+"' ";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_loadSubDefectsCategory  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	SelectDropDownBean dropdownobj = new SelectDropDownBean();
	    	String defectsubcategory=rs.getString("defectsubcategory");
	    	dropdownobj.setOption(defectsubcategory);
	    	dropdownobj.setValue(defectsubcategory);
	    	
	    	dropdownlist.add(dropdownobj);
	    	}
	    	return dropdownlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_loadSubDefectsCategory");
	    }
		
	}
	// Load Sub Defects category ended here
	
	// GetReport
	
	public ArrayList<ReportBean> getReport(String query )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<ReportBean> reportbeanlist = new ArrayList<ReportBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_getReport");
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_getReport  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	ReportBean reportbeanobj = new ReportBean();
	    	reportbeanobj.setBid(rs.getString("bid"));
	    	String source=rs.getString("source");
	    	if(source.contains("_"))
	    	{
	    		source=source.replaceAll("_", " ");
	    	}
	    	reportbeanobj.setSource(source);  
	    	reportbeanobj.setDefectcategory(rs.getString("defectcategory"));
	    	// reportbeanobj.setDefectsubcategory(rs.getString("defectsubcategory"));
	    	reportbeanobj.setCluster(rs.getString("cluster"));
	    	reportbeanobj.setDistrict(rs.getString("district"));
	    	reportbeanobj.setSex(rs.getString("sex"));
	    	String modeofdelivery=rs.getString("modeofdelivery");
	    	if(modeofdelivery!=null)
	    	{
	    		if(modeofdelivery.contains("_") )
	    		modeofdelivery=modeofdelivery.replaceAll("_", " ");
	    	}
	    	reportbeanobj.setModeofdelivery(modeofdelivery);
	    	reportbeanobj.setBirthdistict(rs.getString("birthdistict"));
	    	reportbeanobj.setMctsno(rs.getString("mctsno"));
	    	reportbeanobj.setAdharno(rs.getString("adharno"));
	    	reportbeanobj.setMothersname(rs.getString("mothersname"));
	    	reportbeanobj.setTypeofdelivery(rs.getString("typeofdelivery"));
	    	reportbeanobj.setBirthtype(rs.getString("birthtype"));
	    	String ageofidentification=rs.getString("ageofidentification");
	    	if(ageofidentification!=null)
	    	{if(ageofidentification.contains("_") )
	    		ageofidentification=ageofidentification.replaceAll("_", " ");
	    	}
	    	reportbeanobj.setAgeofidentification(ageofidentification);
	    	reportbeanobj.setHospitalname(rs.getString("hospitalname"));								
	    	
	    	reportbeanlist.add(reportbeanobj);
	    	}
	    	return reportbeanlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_getReport");
	    }
		
	}
	
	// GetReport
	
	public ArrayList<AuditReportBean> getAuditReport(String query,String from )
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<AuditReportBean> reportbeanlist = new ArrayList<AuditReportBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_getAuditReport");
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_getAuditReport  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	AuditReportBean reportbeanobj = new AuditReportBean();
	    	reportbeanobj.setUsername(rs.getString("username"));
	    	if(from.equals("Login_Details"))
	    	{
	    		reportbeanobj.setLogindate(rs.getString("date"));
		    	reportbeanobj.setLogoutdate(rs.getString("LOGOUT_DT"));
	    	}
	    	else
	    	{
	    		reportbeanobj.setAction(rs.getString("action"));
		    	reportbeanobj.setAudit_date(rs.getString("date"));
		    	reportbeanobj.setBid(rs.getString("bid"));
		    	
	    	}
	    	reportbeanobj.setDistrict(rs.getString("district"));
	    	reportbeanobj.setCluster(rs.getString("cluster"));
	    
	    					
	    	
	    	reportbeanlist.add(reportbeanobj);
	    	}
	    	return reportbeanlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_getAuditReport");
	    }
		
	}
	

	
	// Get Audit Report
	public ArrayList<BabyTotalDataBean> getBabyTotalData(String bid,String file_server_url,String defect_images_path ,String hospital_logs_path )
	{

	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<BabyTotalDataBean> babytotdatalist = new ArrayList<BabyTotalDataBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ReportsDAO_getBabyTotalData");
	    	String query="Select  * , IFNULL(DATE_FORMAT(del.dateofbirth,'%d-%b-%Y %T'),null) as dob , rep.source as src , rep.reportingdate as repdate,rep.ageofidentification as ageofiden  from address ad  "
	    			+ " join  "
	    			+ " Antenataldetails ant "
	    			+ " on ad.bid= ant.bid "
	    			+ " join "
	    			+ "  Congenitalanomalies con "
	    			+ " on ad.bid= con.bid "
	    			+ " join "
	    			+ " Defects def "
	    			+ " on ad.bid= def.bid "
	    			+ " join "
	    			+ " Deliverydetails del "
	    			+ " on ad.bid= del.bid "
	    			+ " join "
	    			+ " Diagnosisdetails dia  "
	    			+ "on ad.bid= dia.bid "
	    			+ " join "
	    			+ " investigationdetails inv "
	    			+ " on ad.bid= inv.bid "
	    			+ " join "
	    			+ " Reportingdetails rep "
	    			+ " on ad.bid= rep.bid "
	    			+ " join loginusers lu "
	    			+ " on lu.username= rep.loggedby "
                   + " join hospitaldetails hd "
                    + " on hd.hospitalid=lu.hospitalid " 
	    			+ " where ad.bid='"+bid+"' " ; 
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ReportsDAO_getBabyTotalData  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	BabyTotalDataBean babytotdetails = new BabyTotalDataBean();
	    
	    	babytotdetails.setBid(rs.getString("bid"));
	    	babytotdetails.setBhouseno(rs.getString("bhouseno"));
	    	babytotdetails.setBstreetname(rs.getString("bstreetname"));
	    	babytotdetails.setBareaname(rs.getString("bareaname"));
	    	babytotdetails.setBpostoffice(rs.getString("bpostoffice"));
	    	babytotdetails.setBdistrict(rs.getString("bdistrict"));
	    	babytotdetails.setBstate(rs.getString("bstate"));
	    	babytotdetails.setBthouseno(rs.getString("bthouseno"));
	    	babytotdetails.setBtstreetname(rs.getString("btstreetname"));
	    	babytotdetails.setBtareaname(rs.getString("btareaname"));
	    	babytotdetails.setBtpostoffice(rs.getString("btpostoffice"));
	    	babytotdetails.setBtstate(rs.getString("btstate"));
	    	babytotdetails.setBtdistrict(rs.getString("btdistrict"));
	    	babytotdetails.setFolicaciddetails(rs.getString("folicaciddetails"));
	    	babytotdetails.setHoseriousmetirialillness(rs.getString("hoseriousmetirialillness"));
	    	babytotdetails.setHoradiationexposure(rs.getString("horadiationexposure"));
	    	babytotdetails.setHosubstancceabuse(rs.getString("hosubstancceabuse"));
	    	babytotdetails.setParentalconsanguinity(rs.getString("parentalconsanguinity"));
	    	babytotdetails.setAssistedconception(rs.getString("assistedconception"));
	    	babytotdetails.setImmunisationhistory(rs.getString("immunisationhistory"));
	    	babytotdetails.setMaternaldrugs(rs.getString("maternaldrugs"));
	    	babytotdetails.setHistoryofanomalies(rs.getString("historyofanomalies"));
	    	babytotdetails.setNoofpreviousabortion(rs.getString("noofpreviousabortion"));
	    	babytotdetails.setNofofpreviousstillbirth(rs.getString("nofofpreviousstillbirth"));
	    	babytotdetails.setMaternaldrugsdesc(rs.getString("maternaldrugsdesc"));
	    	babytotdetails.setHeadcircumference(rs.getString("headcircumference"));
	    	babytotdetails.setName(rs.getString("name"));
	    	babytotdetails.setDescription(rs.getString("description"));
	    	babytotdetails.setAgeat(rs.getString("ageat"));
	    	babytotdetails.setCode(rs.getString("code"));
	    	babytotdetails.setConfirmmed(rs.getString("confirmmed"));
	    	/*babytotdetails.setSource(rs.getString("source"));
	    	babytotdetails.setAgeofidentification(rs.getString("ageofidentification"));
	    	babytotdetails.setReportingdate(rs.getString("reportingdate"));*/
	    	babytotdetails.setDefectcategory(rs.getString("defectcategory"));
	    	babytotdetails.setDefectsubcategory(rs.getString("defectsubcategory"));
	    	babytotdetails.setDateofbirth(rs.getString("dateofbirth"));
	    	babytotdetails.setBirthweightingrms(rs.getString("birthweightingrms"));
	    	babytotdetails.setBirthorder(rs.getString("birthorder"));
	    	babytotdetails.setBabydeliveryas(rs.getString("babydeliveryas"));
	    	babytotdetails.setSex(rs.getString("sex"));
	    	babytotdetails.setGestationalageinweeks(rs.getString("gestationalageinweeks"));
	    	babytotdetails.setLastmensuralperiod(rs.getString("lastmensuralperiod"));
	    	babytotdetails.setBirthasphyxia(rs.getString("birthasphyxia"));
	    	babytotdetails.setAutopsyshowbirth(rs.getString("autopsyshowbirth"));
	    	babytotdetails.setModeofdelivery(rs.getString("modeofdelivery"));
	    	babytotdetails.setStatusofinduction(rs.getString("statusofinduction"));
	    	babytotdetails.setBirthstate(rs.getString("birthstate"));
	    	babytotdetails.setBirthdistict(rs.getString("birthdistict"));
	    	babytotdetails.setBirthblock(rs.getString("birthblock"));
	    	babytotdetails.setBirthminicipality(rs.getString("birthminicipality"));
	    	babytotdetails.setBirthplace(rs.getString("birthplace"));
	    	babytotdetails.setMctsno(rs.getString("mctsno"));
	    	babytotdetails.setAdharno(rs.getString("adharno"));
	    	babytotdetails.setChildname(rs.getString("childname"));
	    	babytotdetails.setMothersname(rs.getString("mothersname"));
	    	babytotdetails.setMothersage(rs.getString("mothersage"));
	    	babytotdetails.setFathersage(rs.getString("fathersage"));
	    	babytotdetails.setMobilenumber(rs.getString("mobilenumber"));
	    	babytotdetails.setCaste(rs.getString("caste"));
	    	babytotdetails.setNoofphotos(rs.getString("noofphotos"));
	    	babytotdetails.setNoofattachments(rs.getString("noofattachments"));
	    	String photo1="";
	    	if(rs.getString("photo1")!=null &&!"".equals(rs.getString("photo1")))
	    	{
	    		photo1=file_server_url+defect_images_path+bid+"/"+rs.getString("photo1");
	    	}
	    	babytotdetails.setPhoto1(photo1);
	    	String photo2="";
	    	if(rs.getString("photo2")!=null && !"".equals(rs.getString("photo2")))
	    	{
	    		photo2=file_server_url+defect_images_path+bid+"/"+rs.getString("photo2");
	    	}
	    	babytotdetails.setPhoto2(photo2);
	    	String reportattachment1="";
	    	if(rs.getString("reportattachment1")!=null && !"".equals(rs.getString("reportattachment1")))
	    	{
	    		reportattachment1=file_server_url+defect_images_path+bid+"/"+rs.getString("reportattachment1");
	    	}
	    	babytotdetails.setReportattachment1(reportattachment1);
	    	babytotdetails.setReportattachment2(rs.getString("reportattachment2"));
	    	babytotdetails.setReportattachment3(rs.getString("reportattachment3"));
	    	babytotdetails.setReportattachment4(rs.getString("reportattachment4"));
	    	babytotdetails.setReportattachment5(rs.getString("reportattachment5"));
	    	babytotdetails.setReportattachment6(rs.getString("reportattachment6"));
	    	babytotdetails.setNoofbabies(rs.getString("noofbabies"));
	    	babytotdetails.setTypeofdelivery(rs.getString("typeofdelivery"));
	    	babytotdetails.setResonforcesarean(rs.getString("resonforcesarean"));  
	    	babytotdetails.setFathername(rs.getString("fathername"));  
	    	babytotdetails.setBirthtype(rs.getString("birthtype"));
	    	babytotdetails.setAnomaly(rs.getString("anomaly"));
	    	babytotdetails.setSyndrome(rs.getString("syndrome"));
	    	babytotdetails.setProvisional(rs.getString("provisional"));
	    	babytotdetails.setComplediagnosis(rs.getString("complediagnosis"));
	    	babytotdetails.setNotifyingperson(rs.getString("notifyingperson"));
	    	babytotdetails.setDesignationofcontact(rs.getString("designationofcontact"));
	    	babytotdetails.setFacilityreferred(rs.getString("facilityreferred"));
	    	babytotdetails.setProvisionaldiagstat(rs.getString("provisionaldiagstat"));
	    	babytotdetails.setKarotype(rs.getString("karotype"));
	    	babytotdetails.setKarotypefindings(rs.getString("karotypefindings"));
	    	babytotdetails.setBloodtestforch(rs.getString("bloodtestforch"));
	    	babytotdetails.setBloodtestforcah(rs.getString("bloodtestforcah"));
	    	babytotdetails.setBloodtestforg6pd(rs.getString("bloodtestforg6pd"));
	    	babytotdetails.setBloodtestforscd(rs.getString("bloodtestforscd"));
	    	babytotdetails.setBloodtestothers(rs.getString("bloodtestothers"));
	    	babytotdetails.setBloodtestfindings(rs.getString("bloodtestfindings"));
	    	babytotdetails.setBera(rs.getString("bera"));
	    	babytotdetails.setBerafindings(rs.getString("berafindings"));
	    	babytotdetails.setState(rs.getString("state"));
	    	babytotdetails.setDistrict(rs.getString("district"));
	    	babytotdetails.setCluster(rs.getString("cluster"));
// 	    	babytotdetails.setSource(rs.getString("source"));
	    	babytotdetails.setSource(rs.getString("src"));
	    	// babytotdetails.setReportingdate(rs.getString("reportingdate"));
	    	babytotdetails.setReportingdate(rs.getString("repdate"));
	    	babytotdetails.setDateofidentification(rs.getString("dateofidentification"));
	    	// babytotdetails.setAgeofidentification(rs.getString("ageofidentification")); // 
	    	babytotdetails.setAgeofidentification(rs.getString("ageofiden"));
	    	babytotdetails.setHospitalname(rs.getString("hospitalname"));
	    	babytotdetails.setDateofbirth(rs.getString("dob"));  // it is overriding the babytotdetails.setDateofbirth(rs.getString("dateofbirth"));
	    	String logo="";
	    	if(rs.getString("logo")!=null && !"".equals(rs.getString("logo")))
	    	{
	    		logo=file_server_url+hospital_logs_path+rs.getString("hospitalid")+"/"+rs.getString("logo");
	    	}
	    	babytotdetails.setLogo(logo);
	    	
	    	babytotdetails.setPhonenum(rs.getString("phonenum"));
	    	babytotdetails.setHaddress(rs.getString("address"));
	    	babytotdetails.setHdist(rs.getString("district"));
	    	babytotdetails.setHstate(rs.getString("state"));
	    

	    	logger.info(rs.getString("source")+"::"+rs.getString("reportingdate")+"::"+rs.getString("district")+"::"+rs.getString("cluster"));
	    	babytotdatalist.add(babytotdetails);
	    	}
	    	return babytotdatalist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ReportsDAO_getBabyTotalData");
	    }
		
	
	}
// Get Baby total data	
	
	
}
