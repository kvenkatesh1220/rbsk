package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;





import org.apache.log4j.Logger;

import util.DBUtil;
import util.beans.MonthwiseReportBean;

public class ChildBirthReportDAO {
	static Logger logger = Logger.getLogger(ChildBirthReportDAO.class.getName());
	
	public int noofdefectscount(String reporttype, String fromdate,String todate, String month, String year)
	{
		logger.info("noofdefectscount method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		int count=0;
		String query="";
		if("Date".equals(reporttype))
		{
			query="select count(distinct bid) from defects where reportingdate='"+fromdate+"'";
		}
		if("Month".equals(reporttype))
		{
			query="select count(distinct bid) from defects where month(reportingdate)='"+month+"' and year(reportingdate)='"+year+"'";
		}
		if("Year".equals(reporttype))
		{
			query="select count(distinct bid) from defects where year(reportingdate)='"+year+"'";	
		}
		if("Date Range".equals(reporttype))
		{
			query="select count(distinct bid) from defects where reportingdate between ('"+fromdate+"') and ('"+todate+"')";
		}
		try
		{
			conn1 = dbutil.getConnection("ChildBirthReportDAO_noofdefectscount");
			PreparedStatement preparedStatement=conn1.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
 	         count = rs.getInt(1);
 	         logger.info("Count of defects are :"+count);
 	        }
			
			return count;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return count;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthReportDAO_noofdefectscount");
			logger.info("noofdefectscount method ended here");
		}
	}
	//
	public int noofbirthscount(String reporttype, String fromdate,String todate, String month, String year)
	{
		logger.info("noofbirthscount method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		int count=0;
		String query="";
		
		if("Date".equals(reporttype))
		{
			query="select count(distinct bid) from reportingdetails where reportingdate='"+fromdate+"'";
		}
		if("Month".equals(reporttype))
		{
			query="select count(distinct bid) from reportingdetails where month(reportingdate)='"+month+"' and year(reportingdate)='"+year+"'";
		}
		if("Year".equals(reporttype))
		{
			query="select count(distinct bid) from reportingdetails where year(reportingdate)='"+year+"'";	
		}
		if("Date Range".equals(reporttype))
		{
			query="select count(distinct bid) from reportingdetails where reportingdate between ('"+fromdate+"') and ('"+todate+"')";
		}
		logger.info("Query :"+query);
		try
		{
			conn1 = dbutil.getConnection("ChildBirthReportDAO_noofbirthscount");
			PreparedStatement preparedStatement=conn1.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
 	         count = rs.getInt(1);
 	         logger.info("Count of births are :"+count);
 	        }
			
			return count;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return count;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthReportDAO_noofbirthscount");
			logger.info("noofdefectscount method ended here");
		}
	}
	//
	public ArrayList<MonthwiseReportBean> noofbirthsmonwisecount()
	{  
		
		logger.info("noofbirthsmonwisecount method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		
		ArrayList<MonthwiseReportBean> monwisereportlist = new ArrayList<MonthwiseReportBean>(); 
		try
		{
			conn1 = dbutil.getConnection("ChildBirthReportDAO_noofbirthsmonwisecount");
			PreparedStatement preparedStatement=conn1.prepareStatement("select reportingmonth,reportingyear,count(*) from reportingdetails group by reportingmonth,reportingyear");
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
			 MonthwiseReportBean monwiserpt = new MonthwiseReportBean();
			 monwiserpt.setDefectname(rs.getString("reportingmonth"));
			 monwiserpt.setYear(rs.getString("reportingyear"));
			 monwiserpt.setCount(""+rs.getInt(3));
 	         logger.info(rs.getString("reportingmonth")+"Count of births are :"+rs.getInt(3));
 	        }
			
			return monwisereportlist;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return monwisereportlist;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthReportDAO_noofbirthsmonwisecount");
			logger.info("noofbirthsmonwisecount method ended here");
		}
	}
	public ArrayList<MonthwiseReportBean> defectwisecount(String reporttype, String fromdate,String todate, String month, String year)
	{  
		
		logger.info("defectwisecount method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		String query="";
		ArrayList<MonthwiseReportBean> monwisereportlist = new ArrayList<MonthwiseReportBean>(); 
		
		if("Date".equals(reporttype))
		{
			query="select defectcategory,count(distinct(bid)) from defects where reportingdate='"+fromdate+"' group by defectcategory";
		}
		if("Month".equals(reporttype))
		{
			query="select defectcategory,count(distinct(bid)) from defects where month(reportingdate)='"+month+"' and year(reportingdate)='"+year+"' group by defectcategory";
		}
		if("Year".equals(reporttype))
		{
			query="select defectcategory,count(distinct(bid)) from defects where year(reportingdate)='"+year+"' and year(reportingdate)='"+year+"' group by defectcategory";	
		}
		if("Date Range".equals(reporttype))
		{
			query="select defectcategory,count(distinct(bid)) from defects where reportingdate between ('"+fromdate+"') and ('"+todate+"') group by defectcategory";
		}
		try
		{
			conn1 = dbutil.getConnection("ChildBirthReportDAO_noofbirthsmonwisecount");
			PreparedStatement preparedStatement=conn1.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
			 MonthwiseReportBean monwiserpt = new MonthwiseReportBean();
			 if(rs.getString("defectcategory")!=null)
			 {
			 monwiserpt.setDefectname(rs.getString("defectcategory"));
			 monwiserpt.setCount(""+rs.getInt(2));
 	         logger.info(rs.getString("defectcategory")+"Count of births in defect :"+rs.getInt(2));
 	         monwisereportlist.add(monwiserpt);
			 }
 	        }
			
			return monwisereportlist;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return monwisereportlist;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthReportDAO_defectwisecount");
			logger.info("defectwisecount method ended here");
		}
	}
	public ArrayList<MonthwiseReportBean> noofsubdefectsmonwisecount()
	{  
		
		logger.info("noofdefectsmonwisecount method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		
		ArrayList<MonthwiseReportBean> monwisereportlist = new ArrayList<MonthwiseReportBean>(); 
		try
		{
			conn1 = dbutil.getConnection("ChildBirthReportDAO_noofsubdefectsmonwisecount");
			PreparedStatement preparedStatement=conn1.prepareStatement("select reportingmonth,reportingyear,defectcategory,count(*) from defects group by reportingmonth,reportingyear,defectsubcategory");
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
			 MonthwiseReportBean monwiserpt = new MonthwiseReportBean();
			 monwiserpt.setDefectname(rs.getString("reportingmonth"));
			 monwiserpt.setYear(rs.getString("reportingyear"));
			 monwiserpt.setDefectname(rs.getString("defectcategory"));
			 monwiserpt.setCount(""+rs.getInt(2));
 	         logger.info(rs.getString("reportingmonth")+"Count of births are :"+rs.getInt(2));
 	        monwisereportlist.add(monwiserpt);
 	        }
			
			return monwisereportlist;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return monwisereportlist;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthReportDAO_noofsubdefectsmonwisecount");
			logger.info("noofsubdefectsmonwisecount method ended here");
		}
	}
	//
	public ArrayList<MonthwiseReportBean> sourcewisecount(String reporttype, String fromdate,String todate, String month, String year)
	{  
		
		logger.info("sourcewisecount method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		ArrayList<MonthwiseReportBean> monwisereportlist = new ArrayList<MonthwiseReportBean>();
		String query="";
		if("Date".equals(reporttype))
		{
			query="select source,count(distinct(bid)) from defects where reportingdate='"+fromdate+"' group by source";
		}
		if("Month".equals(reporttype))
		{
			query="select source,count(distinct(bid)) from defects where month(reportingdate)='"+month+"' and year(reportingdate)='"+year+"' group by source";
		}
		if("Year".equals(reporttype))
		{
			query="select source,count(distinct(bid)) from defects where year(reportingdate)='"+year+"'  group by source";	
		}
		if("Date Range".equals(reporttype))
		{
			query="select source,count(distinct(bid)) from defects where reportingdate between ('"+fromdate+"') and ('"+todate+"') group by source";
		}
		
		try
		{
			conn1 = dbutil.getConnection("ChildBirthReportDAO_sourcewisecount");
			PreparedStatement preparedStatement=conn1.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
			 MonthwiseReportBean monwiserpt = new MonthwiseReportBean();
			 if(rs.getString("source")!=null)
			 {
				 String source= rs.getString("source");
				 if(rs.getString("source").contains("_"))
					 source=source.replaceAll("_", " ");
			 monwiserpt.setSource(source);
			 monwiserpt.setCount(""+rs.getInt(2));
 	         logger.info(rs.getString("source")+"Count of births are :"+rs.getInt(2));
 	        monwisereportlist.add(monwiserpt);
			 }
 	        }
			
			return monwisereportlist;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return monwisereportlist;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthReportDAO_sourcewisecount");
			logger.info("sourcewisecount method ended here");
		}
	}
	//
	public ArrayList<MonthwiseReportBean> agewisecount(String reporttype, String fromdate,String todate, String month, String year)
	{  
		
		logger.info("agewisecount method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		ArrayList<MonthwiseReportBean> monwisereportlist = new ArrayList<MonthwiseReportBean>(); 
		String query="";
		if("Date".equals(reporttype))
		{
			query="select ageofidentification,count(distinct(bid)) from defects where reportingdate='"+fromdate+"' group by ageofidentification";
		}
		if("Month".equals(reporttype))
		{
			query="select ageofidentification,count(distinct(bid)) from defects where month(reportingdate)='"+month+"' and year(reportingdate)='"+year+"' group by ageofidentification";
		}
		if("Year".equals(reporttype))
		{
			query="select ageofidentification,count(distinct(bid)) from defects where year(reportingdate)='"+year+"' and year(reportingdate)='"+year+"' group by ageofidentification";	
		}
		if("Date Range".equals(reporttype))
		{
			query="select ageofidentification,count(distinct(bid)) from defects where reportingdate between ('"+fromdate+"') and ('"+todate+"') group by ageofidentification";
		}
		
		try
		{
			conn1 = dbutil.getConnection("ChildBirthReportDAO_agewisecount");
			PreparedStatement preparedStatement=conn1.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
			 MonthwiseReportBean monwiserpt = new MonthwiseReportBean();
			 if(rs.getString("ageofidentification")!=null)
			 {
				 String ageofidentification= rs.getString("ageofidentification");
				 if(ageofidentification!=null)
				 {
					 if(ageofidentification.contains("_"))
						 ageofidentification=ageofidentification.replaceAll("_", " ");
				 }
			 monwiserpt.setAge(ageofidentification);
			 monwiserpt.setCount(""+rs.getInt(2));
			 
 	         logger.info(rs.getString("ageofidentification")+"Count of births are :"+rs.getInt(2));
 	        monwisereportlist.add(monwiserpt);
			 }
 	        }
			
			return monwisereportlist;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return monwisereportlist;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthReportDAO_agewisecount");
			logger.info("agewisecount method ended here");
		}
	}
	//
	
	
	
	
}
