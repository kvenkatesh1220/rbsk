package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Row;

import util.CustIDGeneratorUtil;
import util.DBUtil;

public class BulkUploadDAO {
	static Logger logger = Logger.getLogger(LoginUsersDAO.class.getName());
	
	public boolean insertIntoBulkUploads(String bulkuploadfolderid, String username, String uploadfilename)
	{
		Connection conn=null;
		DBUtil dbutil = new DBUtil();
		try
		{
		System.out.println("BulkUploadDAO_insertIntoBulkUploads  started here");
		 conn = dbutil.getConnection("BulkUploadDAO_insertIntoBulkUploads");
		 PreparedStatement preparedStmt2;
		 String query2 = "insert into bulkupload (foldername,username,uploadfilename) values (?,?,?)";
	    	
	    	preparedStmt2=conn.prepareStatement(query2);
	    	preparedStmt2.setString(1,bulkuploadfolderid);
	    	preparedStmt2.setString(2,username);
	    	preparedStmt2.setString(3,uploadfilename);
	    	int recUpdated = preparedStmt2.executeUpdate();
	    	logger.info("No of records inserted  inbulkupload  is "+recUpdated);
	    	if(recUpdated>0)
	    	{
	    		return true;
	    	}
	    	else
	    	{
	    		return false;
	    	}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			 return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "BulkUploadDAO_insertIntoBulkUploads");
			logger.info("BulkUploadDAO_insertIntoBulkUploads ended here");
		}
	
	}
	
	public boolean insertIntoLoginUsersPre( HSSFSheet sheet ,String state, String  country,String username)
	{
		Connection conn=null;
		DBUtil dbutil = new DBUtil();
		PreparedStatement preparedStmt2=null;
		try
		{
		System.out.println("BulkUploadDAO_insertIntoLoginUsersPre  started here");
		 conn = dbutil.getConnection("BulkUploadDAO_insertIntoLoginUsersPre");
		 CustIDGeneratorUtil randomStringGen = new CustIDGeneratorUtil();
			
		 String query="select count(*) as count from loginusers ";
		 preparedStmt2 = conn.prepareStatement(query);
		 ResultSet rs = preparedStmt2.executeQuery();
		 int count=0;
		 while(rs.next())
		 {
			 count = rs.getInt(1);
	      logger.info("noofrecords found in db for count :"+count);
	 	 }
	
		 int recUpdated =0;
		/* FileInputStream input = new FileInputStream(filepath);
         POIFSFileSystem fs = new POIFSFileSystem( input );
         HSSFWorkbook wb = new HSSFWorkbook(fs);
         HSSFSheet sheet = wb.getSheetAt(0);*/
         Row row;
         
		 System.out.println("Number of Rows in Sheet ::"+sheet.getLastRowNum());
         for(int i=2; i<=sheet.getLastRowNum(); i++){
             row = sheet.getRow(i);
             //String Sno =  ""; //row.getCell(0).getStringCellValue();
             String distname = row.getCell(1).getStringCellValue();
             String Cluster = row.getCell(2).getStringCellValue();
				
				String HSPname = row.getCell(3).getStringCellValue();
				String hsptype = row.getCell(4).getStringCellValue();
				String phnum = row.getCell(5).getStringCellValue();
				String regnum = row.getCell(6).getStringCellValue();
				String address = row.getCell(7).getStringCellValue();
				
				String password=randomStringGen.generateRandomString(); // +randomStringGen.generateRandomString();
				
		 String query2 = "insert into loginusers_pre (isvalidrec,remarks,cluster,hospitaltype,"
		 		+ "usertype,hospitalid,hospital_name,regnum,phonenum,address,country,state,district,byuser) "
		 		+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 /* String username="";
	   	if(hsptype.equals("Pvt. Hospital"))
	    	{
	    		username="pvt"+distname.substring(0, 3)+Cluster.substring(0, 3);
	    	}
	    	else
	    	{
	    		username="gov"+distname.substring(0, 3)+Cluster.substring(0, 3);
	    	}*/
		 String remarks="";
		 String isvalidrec="0";
		 if(distname==null || distname.trim()=="") 
		 {
			 remarks=remarks+"  District Name Required .";
			 isvalidrec="1";
		 }
		 if( Cluster==null || Cluster.trim()=="")
		 {
			 remarks=remarks+"  Cluster Name Required ."; isvalidrec="1";
		 }
		if( HSPname==null || HSPname.trim()=="" )
		{
			remarks=remarks+"  Hospital  Name Required ."; isvalidrec="1";
		}
		if( hsptype==null || hsptype.trim()=="" || !("GOVT".equals(hsptype) ||  "Pvt. Hospital".equals(hsptype)) )
		{
			remarks=remarks+"  Hospital  Type  Required  and it should GOVT or Pvt. Hospital ."; isvalidrec="1";
		}
		if( address==null || address.trim()=="")
		{
			remarks=remarks+"   address is   Required ."; isvalidrec="1";
		}
	    	preparedStmt2=conn.prepareStatement(query2);
	    	preparedStmt2.setString(1,isvalidrec);
	    	preparedStmt2.setString(2,remarks);
	    	preparedStmt2.setString(3,Cluster);
	    	
	    	preparedStmt2.setString(4,hsptype);
	    	preparedStmt2.setString(5,"USER");
	    	preparedStmt2.setString(6,password);
	    	
	    	preparedStmt2.setString(7,HSPname);
	    	preparedStmt2.setString(8,regnum);
	    	preparedStmt2.setString(9,phnum);
	    	
	    	preparedStmt2.setString(10,address);
	    	preparedStmt2.setString(11,country);
	    	preparedStmt2.setString(12,state);
	    	preparedStmt2.setString(13,distname);
	    	preparedStmt2.setString(14, username);
	    	
	    	 recUpdated = preparedStmt2.executeUpdate();
	    	 System.out.println("password ::"+password+" hospital id ::"+password+" :: Name of the District ::"+distname+":: Cluster ::"+Cluster+":: HSPname"+HSPname+" :: hsptype::"+hsptype+":: phnum :: "+phnum+"::  regnum ::"+regnum+" :: address ::"+address);
             System.out.println("Import rows "+i);
		}
         
		if(recUpdated>0)
		{
			 preparedStmt2.close();
	         conn.close();
	         
			return true;
		}
		else
		{
			return false;
		}
	}
		catch(SQLException ex){
	        System.out.println(ex);
	        return false;
	    }
		catch(Exception e)
		{
			e.printStackTrace();
			 return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "BulkUploadDAO_insertIntoLoginUsersPre");
			logger.info("BulkUploadDAO_insertIntoLoginUsersPre ended here");
		}
	}
	
}
