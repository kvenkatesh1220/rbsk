package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import util.DBUtil;
import util.MyDateUtil;
import util.beans.AddressBean;
import util.beans.AntenataldetailsBean;
import util.beans.CongenitalanomaliesBean;
import util.beans.DefectsBean;
import util.beans.DeliverydetailsBean;
import util.beans.DiagnosisdetailsBean;
import util.beans.DropDownBean;
import util.beans.UserMappingBean;
import util.beans.InvestigationdetailsBean;
import util.beans.ReportingdetailsBean;



public class ChildBirthDAO {
	static Logger logger = Logger.getLogger(ChildBirthDAO.class.getName());
	
	public boolean insertAddress(AddressBean address)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertAddress");
			String query2 = "insert into address (bid,bhouseno,bstreetname,bareaname,bpostoffice,bdistrict,bstate,bthouseno, btstreetname, btareaname, btpostoffice, btdistrict ,btstate) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, address.getBid());
	    	preparedStmt.setString(2, address.getBhouseno());
	    	preparedStmt.setString(3, address.getBstreetname());
	    	preparedStmt.setString(4, address.getBareaname());
	    	preparedStmt.setString(5,address.getBpostoffice());
	    	preparedStmt.setString(6,address.getBdistrict());
	    	preparedStmt.setString(7,address.getBstate());
	    	
	    	preparedStmt.setString(8, address.getBthouseno());
	    	preparedStmt.setString(9, address.getBtstreetname());
	    	preparedStmt.setString(10, address.getBtareaname());
	    	preparedStmt.setString(11,address.getBtpostoffice());
	    	preparedStmt.setString(12,address.getBtdistrict());
	    	preparedStmt.setString(13,address.getBtstate());
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully insertAddress");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertAddress");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertAddress");
		}
		
	}
	
	
	
	public boolean insertAntenataldetails(AntenataldetailsBean antenataldetails)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertAntenataldetails");
			String query2 = "insert into Antenataldetails (bid,folicaciddetails,hoseriousmetirialillness,"
					+ "horadiationexposure,hosubstancceabuse,parentalconsanguinity,"
					+ "assistedconception,immunisationhistory,maternaldrugs,historyofanomalies,"
					+ "noofpreviousabortion,nofofpreviousstillbirth,maternaldrugsdesc,headcircumference) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, antenataldetails.getBid());
	    	preparedStmt.setString(2, antenataldetails.getFolicaciddetails());
	    	preparedStmt.setString(3, antenataldetails.getHoseriousmetirialillness());
	    	preparedStmt.setString(4, antenataldetails.getHoradiationexposure());
	    	preparedStmt.setString(5,antenataldetails.getHosubstancceabuse());
	    	preparedStmt.setString(6,antenataldetails.getParentalconsanguinity());
	    	preparedStmt.setString(7,antenataldetails.getAssistedconception());
	    	preparedStmt.setString(8,antenataldetails.getImmunisationhistory());
	    	preparedStmt.setString(9,antenataldetails.getMaternaldrugs());
	    	preparedStmt.setString(10,antenataldetails.getHistoryofanomalies());
	    	preparedStmt.setString(11,antenataldetails.getNoofpreviousabortion());
	    	preparedStmt.setString(12,antenataldetails.getNofofpreviousstillbirth());
	    	preparedStmt.setString(13,antenataldetails.getMaternaldrugsdesc());
	    	preparedStmt.setString(14,antenataldetails.getHeadcircumference());
	    	
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully in insertAntenataldetails");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertAntenataldetails");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertAntenataldetails");
		}
		
	}

	
	
	public boolean insertCongenitalanomalies(CongenitalanomaliesBean congenitalanomalies)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertAntenataldetails");
			String query2 = "insert into Congenitalanomalies (bid,name,description,ageat,code,confirmmed) values(?,?,?,?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, congenitalanomalies.getBid());
	    	preparedStmt.setString(2, congenitalanomalies.getName());
	    	preparedStmt.setString(3, congenitalanomalies.getDescription());
	    	preparedStmt.setString(4, congenitalanomalies.getAgeat());
	    	preparedStmt.setString(5,congenitalanomalies.getCode());
	    	preparedStmt.setString(6,congenitalanomalies.getConfirmmed());
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully in insertCongenitalanomalies");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertCongenitalanomalies");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertCongenitalanomalies");
		}
		
	}
	
	
	public boolean insertDefects(ArrayList<DefectsBean> defectslist)
	{
		logger.info("defectslist ::"+defectslist);
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    MyDateUtil mydateutil = new MyDateUtil();
	    int count=0;
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertAntenataldetails");
			String query2 = "insert into Defects (bid,source,ageofidentification,reportingdate,defectcategory,defectsubcategory) values(?,?,?,?,?,?)";
	    	for(DefectsBean dl : defectslist){
			
			preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, dl.getBid());
	    	preparedStmt.setString(2, dl.getSource());
	    	preparedStmt.setString(3, dl.getAgeofidentification());
	    	
	    	if(dl.getReportingdate()!=null && !"".equals(dl.getReportingdate().trim())){
	    		java.sql.Date reportingdate = mydateutil.convertStringddmmyytomysqldate(dl.getReportingdate());
	    		logger.info("ChildBirth DAO dateofidentification : "+reportingdate);
	    		preparedStmt.setDate(4, reportingdate);  
	    	} 
	    	else{
	    		preparedStmt.setDate(4, null);
	    	}
	    	
	    	
	    	preparedStmt.setString(5, dl.getDefectcategory());
	    	preparedStmt.setString(6, dl.getDefectsubcategory());
	    	
	    	//
	    	 count = preparedStmt.executeUpdate();
	    	}
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully in insertDefects");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
	    	
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertDefects");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertDefects");
		}
		
	}
	
	
	
	public boolean insertDeliverydetails(DeliverydetailsBean deliverydetails)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    MyDateUtil mydateutil = new MyDateUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertDeliverydetails");
			String query2 = "insert into Deliverydetails (bid,dateofbirth,birthweightingrms,birthorder,"
					+ "babydeliveryas,sex,gestationalageinweeks,lastmensuralperiod,birthasphyxia,"
					+ "autopsyshowbirth,modeofdelivery,statusofinduction,birthstate,birthdistict,"
					+ "birthblock,birthminicipality,birthplace,mctsno,adharno,childname,mothersname,"
					+ "mothersage,fathersage,mobilenumber,caste,noofphotos,noofattachments,photo1,photo2,"
					+ "reportattachment1,reportattachment2,reportattachment3,reportattachment4,reportattachment5,"
					+ "reportattachment6,noofbabies,typeofdelivery,birthtype,fathername,resonforcesarean) "
					+ "values(?,IFNULL(STR_TO_DATE(?, '%d-%b-%Y %T'),null) , ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);


	    	preparedStmt.setString(1, deliverydetails.getBid());
	    	logger.info("ChildBirth DAO dateofbirth : "+deliverydetails.getDateofbirth());
	    	/*if(deliverydetails.getDateofbirth()!=null && !"".equals(deliverydetails.getDateofbirth().trim())){
	    		//java.sql.Date dateofbirth = mydateutil.convertddMonyyyy2mysqldate(deliverydetails.getDateofbirth()); 
	    		logger.info("ChildBirth DAO dateofbirth : "+deliverydetails.getDateofbirth());
	    		 DateFormat df2 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	    		 Date dateofbirth = (Date) df2.parse(deliverydetails.getDateofbirth());
	    		preparedStmt.setDate(2, dateofbirth);  
	    		logger.info("ChildBirth DAO Final dateofbirth : "+dateofbirth);
	    	} 
	    	else{
	    		preparedStmt.setDate(2, null);
	    	}*/
	    	preparedStmt.setString(2, deliverydetails.getDateofbirth());
	    	//preparedStmt.setString(2, deliverydetails.getDateofbirth());
	    	preparedStmt.setString(3, deliverydetails.getBirthweightingrms());
	    	preparedStmt.setString(4, deliverydetails.getBirthorder());
	    	preparedStmt.setString(5, deliverydetails.getBabydeliveryas());
	    	preparedStmt.setString(6, deliverydetails.getSex());
	    	preparedStmt.setString(7, deliverydetails.getGestationalageinweeks());
	    	
	    	if(deliverydetails.getLastmensuralperiod()!=null && !"".equals(deliverydetails.getLastmensuralperiod().trim())){
	    		java.sql.Date lastmensuralperiod = mydateutil.convertStringddmmyytomysqldate(deliverydetails.getLastmensuralperiod());
	    		logger.info("ChildBirth DAO dateofbirth : "+lastmensuralperiod);
	    		preparedStmt.setDate(8, lastmensuralperiod);  
	    	} 
	    	else{
	    		preparedStmt.setDate(8, null);
	    	}
	    	//preparedStmt.setString(8, deliverydetails.getLastmensuralperiod());
	    	preparedStmt.setString(9, deliverydetails.getBirthasphyxia());
	    	preparedStmt.setString(10, deliverydetails.getAutopsyshowbirth());
	    	preparedStmt.setString(11, deliverydetails.getModeofdelivery());
	    	preparedStmt.setString(12, deliverydetails.getStatusofinduction());
	    	preparedStmt.setString(13, deliverydetails.getBirthstate());
	    	preparedStmt.setString(14, deliverydetails.getBirthdistict());
	    	preparedStmt.setString(15, deliverydetails.getBirthblock());
	    	preparedStmt.setString(16, deliverydetails.getBirthminicipality());
	    	preparedStmt.setString(17, deliverydetails.getBirthplace());
	    	preparedStmt.setString(18, deliverydetails.getMctsno());
	    	preparedStmt.setString(19, deliverydetails.getAdharno());
	    	preparedStmt.setString(20, deliverydetails.getChildname());
	    	preparedStmt.setString(21, deliverydetails.getMothersname());
	    	preparedStmt.setString(22, deliverydetails.getMothersage());
	    	preparedStmt.setString(23, deliverydetails.getFathersage());
	    	preparedStmt.setString(24, deliverydetails.getMobilenumber());
	    	preparedStmt.setString(25, deliverydetails.getCaste());
	    	preparedStmt.setString(26, deliverydetails.getNoofphotos());
	    	preparedStmt.setString(27, deliverydetails.getNoofattachments());
	    	preparedStmt.setString(28, deliverydetails.getPhoto1());
	    	preparedStmt.setString(29, deliverydetails.getPhoto2());
	    	preparedStmt.setString(30, deliverydetails.getReportattachment1());
	    	preparedStmt.setString(31, deliverydetails.getReportattachment2());
	    	preparedStmt.setString(32, deliverydetails.getReportattachment3());
	    	preparedStmt.setString(33, deliverydetails.getReportattachment4());
	    	preparedStmt.setString(34, deliverydetails.getReportattachment5());
	    	preparedStmt.setString(35, deliverydetails.getReportattachment6());
	    	preparedStmt.setString(36, deliverydetails.getNoofbabies());
	    	preparedStmt.setString(37, deliverydetails.getTypeofdelivery());
	    	preparedStmt.setString(38, deliverydetails.getBirthtype());
	    	preparedStmt.setString(39, deliverydetails.getFathername());
	    	preparedStmt.setString(40, deliverydetails.getResonforcesarean());
	    	//
	    	logger.info("deliverydetails.getFathername() ::"+deliverydetails.getFathername());
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully in insertDeliverydetails");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertDeliverydetails");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertDeliverydetails");
		}
		
	}

	public boolean insertDiagnosisdetails(DiagnosisdetailsBean diagnosisdetails)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertDiagnosisdetails");
			String query2 = "insert into Diagnosisdetails (bid,anomaly,syndrome,provisional,complediagnosis,notifyingperson,designationofcontact,facilityreferred,provisionaldiagstat) values(?,?,?,?,?,?,?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, diagnosisdetails.getBid());
	    	preparedStmt.setString(2, diagnosisdetails.getAnomaly());
	    	preparedStmt.setString(3, diagnosisdetails.getSyndrome());
	    	preparedStmt.setString(4, diagnosisdetails.getProvisional());
	    	preparedStmt.setString(5,diagnosisdetails.getComplediagnosis());
	    	preparedStmt.setString(6,diagnosisdetails.getNotifyingperson());
	    	preparedStmt.setString(7,diagnosisdetails.getDesignationofcontact());
	    	preparedStmt.setString(8,diagnosisdetails.getFacilityreferred());
	    	preparedStmt.setString(9,diagnosisdetails.getProvisionaldiagstat());
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully insertDiagnosisdetails");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertDiagnosisdetails");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertDiagnosisdetails");
		}
		
	}
	
	
	public boolean insertInvestigationdetails(InvestigationdetailsBean investigationdetails)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertInvestigationdetails");
			String query2 = "insert into investigationdetails (bid,karotype,karotypefindings,bloodtestforch,bloodtestforcah,bloodtestforg6pd,bloodtestforscd,bloodtestothers,bloodtestfindings,bera,berafindings) values(?,?,?,?,?,?,?,?,?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, investigationdetails.getBid());
	    	preparedStmt.setString(2, investigationdetails.getKarotype());
	    	preparedStmt.setString(3, investigationdetails.getKarotypefindings());
	    	preparedStmt.setString(4, investigationdetails.getBlodtestforch());
	    	preparedStmt.setString(5, investigationdetails.getBloodtestforcah());
	    	preparedStmt.setString(6, investigationdetails.getBloodtestforg6pd());
	    	preparedStmt.setString(7, investigationdetails.getBloodtestforscd());
	    	preparedStmt.setString(8, investigationdetails.getBloodtestothers());
	    	preparedStmt.setString(9, investigationdetails.getBloodtestfindings());
	    	preparedStmt.setString(10, investigationdetails.getBera());
	    	preparedStmt.setString(11, investigationdetails.getBerafindings());
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully insertInvestigationdetails");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertInvestigationdetails");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertInvestigationdetails");
		}
		
	}
	
	
	public boolean insertReportingdetails(ReportingdetailsBean reportingdetails)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    MyDateUtil mydateutil = new MyDateUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertReportingdetails");
			String query2 = "insert into Reportingdetails (bid,state,district,cluster,source,reportingdate,dateofidentification,ageofidentification,hospitalname,loggedby) values(?,?,?,?,?,?,?,?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, reportingdetails.getBid());
	    	preparedStmt.setString(2, reportingdetails.getState());
	    	preparedStmt.setString(3, reportingdetails.getDistrict());
	    	preparedStmt.setString(4, reportingdetails.getCluster());
	    	preparedStmt.setString(5, reportingdetails.getSource());

	    	
	    	if(reportingdetails.getReportingdate()!=null && !"".equals(reportingdetails.getReportingdate().trim())){
	    		java.sql.Date reportingdate = mydateutil.convertStringddmmyytomysqldate(reportingdetails.getReportingdate());
	    		logger.info("ChildBirth DAO dateofidentification : "+reportingdate);
	    		preparedStmt.setDate(6, reportingdate);  
	    	} 
	    	else{
	    		preparedStmt.setDate(6, null);
	    	}
	    	
	    	
	    	if(reportingdetails.getDateofidentification()!=null && !"".equals(reportingdetails.getDateofidentification().trim())){
	    		java.sql.Date dateofidentification = mydateutil.convertStringddmmyytomysqldate(reportingdetails.getDateofidentification());
	    		logger.info("ChildBirth DAO dateofidentification : "+dateofidentification);
	    		preparedStmt.setDate(7, dateofidentification);  
	    	} 
	    	else{
	    		preparedStmt.setDate(7, null);
	    	}
	    	
	    	
	    	preparedStmt.setString(8, reportingdetails.getAgeofidentification());
	    	preparedStmt.setString(9, reportingdetails.getHospitalname());
	    	preparedStmt.setString(10, reportingdetails.getUsername());
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully insertReportingdetails");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertReportingdetails");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertReportingdetails");
		}
		
	}
	
	
	//getting values for drop downs
	
	
	public ArrayList<UserMappingBean> getUserMappings(String username)
	{  
		
		logger.info("ChildBirthDAO_GetDropDownValues method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		
		ArrayList<UserMappingBean> dropdownbeeanlist = new ArrayList<UserMappingBean>(); 
		try
		{
			conn1 = dbutil.getConnection("ChildBirthDAO_getUserMappings");
			
			//String query ="select username,district,cluster,hospitalname,hospitaltype,usertype from loginusers where username='"+username+"' ";
			// String query ="select username,district,cluster,hospitalname,hospitaltype,usertype from loginusers_bkp  where username='"+username+"' ";
						String query =" select lu.username,lu.district,lu.cluster,hs.hospital_name,lu.hospitaltype,lu.usertype from loginusers  lu "
								+ " 	join hospitaldetails hs on "
								+ " lu.hospitalid=hs.hospitalid where username='"+username+"' ";
			PreparedStatement preparedStatement=conn1.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next())
 	        {
				UserMappingBean usermappingbeanobj = new UserMappingBean();
				usermappingbeanobj.setUser_id(rs.getString("username"));
				
				usermappingbeanobj.setDistrict(rs.getString("district"));
				usermappingbeanobj.setCluster(rs.getString("cluster"));
				usermappingbeanobj.setHospitalname(rs.getString("hospital_name"));
				usermappingbeanobj.setHospitaltype(rs.getString("hospitaltype"));
				usermappingbeanobj.setUsertype(rs.getString("usertype"));
				
				dropdownbeeanlist.add(usermappingbeanobj);
 	         logger.info("From getUserMappings of ChildBirth DAO :: "+rs.getString(1)+"::"+rs.getString(2)+"::"+rs.getString(3)+"::"+rs.getString(4)+"::"+rs.getString(5));
 	        }
			
			return dropdownbeeanlist;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return dropdownbeeanlist;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthDAO_getUserMappings");
			logger.info("ChildBirthDAO_getUserMappings method ended here");
		}
	}
	
	public UserMappingBean  getUserMappingsObj(String username)
	{  
		
		logger.info("ChildBirthDAO_getUserMappingsObj method started here");
		DBUtil dbutil = new DBUtil();
		Connection conn1=null;
		try
		{
			conn1 = dbutil.getConnection("ChildBirthDAO_getUserMappingsObj");
			
			// String query ="select username,district,cluster,hospitalname,hospitaltype,usertype from loginusers_bkp  where username='"+username+"' ";
			String query =" select lu.username,lu.district,lu.cluster,hs.hospital_name,lu.hospitaltype,lu.usertype from loginusers  lu "
					+ " 	join hospitaldetails hs on "
					+ " lu.hospitalid=hs.hospitalid where username='"+username+"' ";
			
			PreparedStatement preparedStatement=conn1.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			UserMappingBean usermappingbeanobj = new UserMappingBean();
			while(rs.next())
 	        {
				usermappingbeanobj.setUser_id(rs.getString("username"));
				usermappingbeanobj.setDistrict(rs.getString("district"));
				usermappingbeanobj.setCluster(rs.getString("cluster"));
				usermappingbeanobj.setHospitalname(rs.getString("hospital_name"));
				usermappingbeanobj.setHospitaltype(rs.getString("hospitaltype"));
				usermappingbeanobj.setUsertype(rs.getString("usertype"));
 	         logger.info("From getUserMappingsObj of ChildBirth DAO :: "+rs.getString(1)+"::"+rs.getString(2)+"::"+rs.getString(3)+"::"+rs.getString(4)+"::"+rs.getString(5));
 	        }
			
			return usermappingbeanobj;
		}
		catch(Exception e)
		{
		 e.printStackTrace();
		 return null;
		}
		finally
		{
			dbutil.closeConnection(conn1, "ChildBirthDAO_getUserMappingsObj");
			logger.info("ChildBirthDAO_getUserMappingsObj method ended here");
		}
	}
	
	public boolean insertIntoAudit(String bid,String username,String action)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertIntoAudit");
			String query2 = "insert into audit (bid,action,user_name) values(?,?,?)";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, bid);
	    	preparedStmt.setString(2, action);
	    	preparedStmt.setString(3, username);
	    	
	    	//
	    	int count = preparedStmt.executeUpdate();
	    	if(count>0)
	    	{
	    	logger.info("data entered successfully in insertIntoAudit");
	    	return true;
	    	}
	    	else
	    	{
	    	return false;	
	    	}
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertIntoAudit");
			e.printStackTrace();
			return false;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertIntoAudit");
		}
	}
	
	public int selectFromAudit(String username,String action, String bid)
	{
	    Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	   	try
		{
	    	conn = dbutil.getConnection("ChildBirthDAO_insertIntoAudit");
			String query2 = "select count(*) as count from audit where user_name=? and action=? and bid=? ";
	    	preparedStmt = conn.prepareStatement(query2);
	    	preparedStmt.setString(1, username);
	    	preparedStmt.setString(2,action );
	    	preparedStmt.setString(3, bid);
	    	
	    	//
	    	ResultSet rs = preparedStmt.executeQuery();
	    	int count=0;
	    	while(rs.next())
 	        {
	    		count=rs.getInt(1);
	    		logger.info("Number of records returned from ChildBirthDAO ::"+count);
 	        }
	    	return count;
		}
		catch(Exception e)
		{
			logger.info("Exception occourred in ChildBirthDAO_insertIntoAudit");
			e.printStackTrace();
			return -1;
		}
		finally
		{
			dbutil.closeConnection(conn, "ChildBirthDAO_insertIntoAudit");
		}
	}
	
	public ArrayList<DropDownBean> selectFromApplicationProperties()
	{
		Connection conn=null;
	    PreparedStatement preparedStmt;
	    DBUtil dbutil = new DBUtil();
	    ArrayList<DropDownBean> dropdownlist = new ArrayList<DropDownBean>();
	    
	    try
	    {
	    	conn = dbutil.getConnection("ChildBirthDAO_selectFromApplicationProperties");
	    	String query="select *   from  application_properties ";
	    	
	    	preparedStmt = conn.prepareStatement(query);
	    	ResultSet rs = preparedStmt.executeQuery();
	    	
	    	logger.info("ChildBirthDAO_selectFromApplicationProperties  query executed");
	    	
	    	while(rs.next())
	    	{
	    	logger.info("Record Found");
	    	DropDownBean dropdownobj = new DropDownBean();
	    	dropdownobj.setName(rs.getString("prop_name"));
	    	dropdownobj.setValue(rs.getString("prop_value"));
	    	
	    	dropdownlist.add(dropdownobj);
	    	}
	    	return dropdownlist;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	    finally
	    {
	    	dbutil.closeConnection(conn, "ChildBirthDAO_selectFromApplicationProperties");
	    }
		
	
		
	}
}
