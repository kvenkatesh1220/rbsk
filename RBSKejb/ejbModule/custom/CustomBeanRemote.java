package custom;

import java.util.ArrayList;
import java.util.HashMap;

import javax.ejb.Remote;

import util.CustomColPojo;
import util.CustomPojo;
import util.CustomRowPojo;

@Remote
public interface CustomBeanRemote {
	public ArrayList<CustomRowPojo> executeSelectQuery(String query);
	public int executeInsertQuery(ArrayList<CustomColPojo> customcurrowobjlist,String tablename);
	public int executeUpdateQuery(ArrayList<CustomColPojo> customcurrowobjlist,String tablename, String wherestatement);
	
}
