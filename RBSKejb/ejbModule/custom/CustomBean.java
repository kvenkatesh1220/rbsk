package custom;

import java.util.ArrayList;
import java.util.HashMap;

import javax.ejb.Stateless;

import util.CustomColPojo;
import util.CustomPojo;
import util.CustomRowPojo;
import dao.CustomDAO;




/**
 * Session Bean implementation class SingUpBean
 */
@Stateless

public class CustomBean implements CustomBeanRemote {

    /**
     * Default constructor. 
     */
    public CustomBean() {
        // TODO Auto-generated constructor stub
    }
    public ArrayList<CustomRowPojo> executeSelectQuery(String query)
    {
    	CustomDAO customdaoobj = new CustomDAO();
    	return customdaoobj.executeSelectQuery(query);
    }
    public int executeInsertQuery(ArrayList<CustomColPojo> customcurrowobjlist,String tablename)
    {
    	CustomDAO customdaoobj = new CustomDAO();
    	return customdaoobj.executeInsertQuery(customcurrowobjlist,tablename);
    }
    public int executeUpdateQuery(ArrayList<CustomColPojo> customcurrowobjlist,String tablename, String wherestatement)
    {
    	CustomDAO customdaoobj = new CustomDAO();
    	return customdaoobj.executeUpdateQuery(customcurrowobjlist,tablename,wherestatement);
    }

}
