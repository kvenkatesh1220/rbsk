package util;

import java.util.ArrayList;

import org.apache.log4j.Logger;

public class CustomRowPojo {
	static Logger logger = Logger.getLogger(CustomRowPojo.class.getName());
	ArrayList<CustomColPojo> customrowlistobj;
	
	String jsonrowstring;

	public ArrayList<CustomColPojo> getCustomrowlistobj() {
		return customrowlistobj;
	}

	public void setCustomrowlistobj(ArrayList<CustomColPojo> customrowlistobj) {
		this.customrowlistobj = customrowlistobj;
		
		/* creating json string for each row started here */
		if(customrowlistobj.size()>0)
		{
		this.jsonrowstring="{";
		String rowpart="";
		for(int i=0;i<customrowlistobj.size();i++)
		{
			CustomColPojo customcolobj = customrowlistobj.get(i);
			if(i<customrowlistobj.size()-1)
			{
				rowpart=rowpart+"\""+customcolobj.getKey()+"\":\""+customcolobj.getValue()+"\",";
			}
			else
			{
				rowpart=rowpart+"\""+customcolobj.getKey()+"\":\""+customcolobj.getValue()+"\"";
			}
			logger.info("jsonrowstring ::"+jsonrowstring);
		}
		this.jsonrowstring=this.jsonrowstring+rowpart+"}";
		}
		/* creating json string for each row ended here */
	}

	public String getJsonrowstring() {
		return jsonrowstring;
	}
	
	public String getValueofColumn(String inputcolname) {
		
		try
		{
		if(this.customrowlistobj.size()>0 && inputcolname!=null)
		{
		inputcolname=inputcolname.trim();
		for(int i=0;i<this.customrowlistobj.size();i++)
		{
			CustomColPojo customcolobj = customrowlistobj.get(i);
			String colname = ((String)customcolobj.getKey()).trim();
			if(colname.equalsIgnoreCase(inputcolname))
			{
				return (String)customcolobj.getValue();
			}
		}
		}
		else
		{
		 return "";
		}
		return "";
		}
		catch(Exception e)
		{
		return "";
		}
	}

}
