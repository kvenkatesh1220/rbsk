package util.beans;

public class DefectsBean {
	String bid;

	String source;
	String ageofidentification;
	
	String reportingdate;
	String defectcategory;
	String defectsubcategory;
	
	
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getAgeofidentification() {
		return ageofidentification;
	}
	public void setAgeofidentification(String ageofidentification) {
		this.ageofidentification = ageofidentification;
	}
	
	public String getReportingdate() {
		return reportingdate;
	}
	public void setReportingdate(String reportingdate) {
		this.reportingdate = reportingdate;
	}
	public String getDefectcategory() {
		return defectcategory;
	}
	public void setDefectcategory(String defectcategory) {
		this.defectcategory = defectcategory;
	}
	public String getDefectsubcategory() {
		return defectsubcategory;
	}
	public void setDefectsubcategory(String defectsubcategory) {
		this.defectsubcategory = defectsubcategory;
	}
}
