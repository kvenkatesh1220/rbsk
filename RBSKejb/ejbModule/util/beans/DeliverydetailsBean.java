package util.beans;

public class DeliverydetailsBean {
	String bid ;
	String dateofbirth ;
	String birthweightingrms ;
	String birthorder ;
	String babydeliveryas ;
	String sex ;
	String gestationalageinweeks ;
	String lastmensuralperiod ;
	String birthasphyxia ;
	String autopsyshowbirth ;
	String modeofdelivery ;
	String statusofinduction ;
	String birthstate ;
	String birthdistict ;
	String birthblock ;
	String birthminicipality ;
	String birthplace ;
	String mctsno ;
	String adharno ;
	String childname ;
	String mothersname ;
	String mothersage ; 
	String fathersage ;
	String mobilenumber ;
	String caste ;
	String noofphotos ;
	String noofattachments ;
	String photo1 ;
	String photo2 ;
	String reportattachment1 ;
	String reportattachment2 ;
	String reportattachment3 ;
	String reportattachment4 ;
	String reportattachment5 ;
	String reportattachment6 ;
	String noofbabies;
	String typeofdelivery;
	String timeofbirth;
	String birthtype;
	String fathername;
	String resonforcesarean;
	
	
	public String getResonforcesarean() {
		return resonforcesarean;
	}
	public void setResonforcesarean(String resonforcesarean) {
		this.resonforcesarean = resonforcesarean;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	public String getNoofbabies() {
		return noofbabies;
	}
	public void setNoofbabies(String noofbabies) {
		this.noofbabies = noofbabies;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getBirthweightingrms() {
		return birthweightingrms;
	}
	public void setBirthweightingrms(String birthweightingrms) {
		this.birthweightingrms = birthweightingrms;
	}
	public String getBirthorder() {
		return birthorder;
	}
	public void setBirthorder(String birthorder) {
		this.birthorder = birthorder;
	}
	public String getBabydeliveryas() {
		return babydeliveryas;
	}
	public void setBabydeliveryas(String babydeliveryas) {
		this.babydeliveryas = babydeliveryas;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getGestationalageinweeks() {
		return gestationalageinweeks;
	}
	public void setGestationalageinweeks(String gestationalageinweeks) {
		this.gestationalageinweeks = gestationalageinweeks;
	}
	public String getLastmensuralperiod() {
		return lastmensuralperiod;
	}
	public void setLastmensuralperiod(String lastmensuralperiod) {
		this.lastmensuralperiod = lastmensuralperiod;
	}
	public String getBirthasphyxia() {
		return birthasphyxia;
	}
	public void setBirthasphyxia(String birthasphyxia) {
		this.birthasphyxia = birthasphyxia;
	}
	public String getAutopsyshowbirth() {
		return autopsyshowbirth;
	}
	public void setAutopsyshowbirth(String autopsyshowbirth) {
		this.autopsyshowbirth = autopsyshowbirth;
	}
	public String getModeofdelivery() {
		return modeofdelivery;
	}
	public void setModeofdelivery(String modeofdelivery) {
		this.modeofdelivery = modeofdelivery;
	}
	public String getStatusofinduction() {
		return statusofinduction;
	}
	public void setStatusofinduction(String statusofinduction) {
		this.statusofinduction = statusofinduction;
	}
	public String getBirthstate() {
		return birthstate;
	}
	public void setBirthstate(String birthstate) {
		this.birthstate = birthstate;
	}
	public String getBirthdistict() {
		return birthdistict;
	}
	public void setBirthdistict(String birthdistict) {
		this.birthdistict = birthdistict;
	}
	public String getBirthblock() {
		return birthblock;
	}
	public void setBirthblock(String birthblock) {
		this.birthblock = birthblock;
	}
	public String getBirthminicipality() {
		return birthminicipality;
	}
	public void setBirthminicipality(String birthminicipality) {
		this.birthminicipality = birthminicipality;
	}
	public String getBirthplace() {
		return birthplace;
	}
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}
	public String getMctsno() {
		return mctsno;
	}
	public void setMctsno(String mctsno) {
		this.mctsno = mctsno;
	}
	public String getAdharno() {
		return adharno;
	}
	public void setAdharno(String adharno) {
		this.adharno = adharno;
	}
	public String getChildname() {
		return childname;
	}
	public void setChildname(String childname) {
		this.childname = childname;
	}
	public String getMothersname() {
		return mothersname;
	}
	public void setMothersname(String mothersname) {
		this.mothersname = mothersname;
	}
	public String getMothersage() {
		return mothersage;
	}
	public void setMothersage(String mothersage) {
		this.mothersage = mothersage;
	}
	public String getFathersage() {
		return fathersage;
	}
	public void setFathersage(String fathersage) {
		this.fathersage = fathersage;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getNoofphotos() {
		return noofphotos;
	}
	public void setNoofphotos(String noofphotos) {
		this.noofphotos = noofphotos;
	}
	public String getNoofattachments() {
		return noofattachments;
	}
	public void setNoofattachments(String noofattachments) {
		this.noofattachments = noofattachments;
	}
	public String getPhoto1() {
		return photo1;
	}
	public void setPhoto1(String photo1) {
		this.photo1 = photo1;
	}
	public String getPhoto2() {
		return photo2;
	}
	public void setPhoto2(String photo2) {
		this.photo2 = photo2;
	}
	public String getReportattachment1() {
		return reportattachment1;
	}
	public void setReportattachment1(String reportattachment1) {
		this.reportattachment1 = reportattachment1;
	}
	public String getReportattachment2() {
		return reportattachment2;
	}
	public void setReportattachment2(String reportattachment2) {
		this.reportattachment2 = reportattachment2;
	}
	public String getReportattachment3() {
		return reportattachment3;
	}
	public void setReportattachment3(String reportattachment3) {
		this.reportattachment3 = reportattachment3;
	}
	public String getReportattachment4() {
		return reportattachment4;
	}
	public void setReportattachment4(String reportattachment4) {
		this.reportattachment4 = reportattachment4;
	}
	public String getReportattachment5() {
		return reportattachment5;
	}
	public void setReportattachment5(String reportattachment5) {
		this.reportattachment5 = reportattachment5;
	}
	public String getReportattachment6() {
		return reportattachment6;
	}
	public void setReportattachment6(String reportattachment6) {
		this.reportattachment6 = reportattachment6;
	}
	public String getTypeofdelivery() {
		return typeofdelivery;
	}
	public void setTypeofdelivery(String typeofdelivery) {
		this.typeofdelivery = typeofdelivery;
	}
	public String getTimeofbirth() {
		return timeofbirth;
	}
	public void setTimeofbirth(String timeofbirth) {
		this.timeofbirth = timeofbirth;
	}
	public String getBirthtype() {
		return birthtype;
	}
	public void setBirthtype(String birthtype) {
		this.birthtype = birthtype;
	}
	
	
	
	
}
