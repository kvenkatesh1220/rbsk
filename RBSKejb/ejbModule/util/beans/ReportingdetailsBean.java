package util.beans;

public class ReportingdetailsBean {
	String bid ;
	String state ;
	String district ;
	String cluster ;
	String source ;
	String reportingdate ;
	String dateofidentification ;
	String ageofidentification ;
	String hospitalname;
	String username;
	
	
	public String getHospitalname() {
		return hospitalname;
	}
	public void setHospitalname(String hospitalname) {
		this.hospitalname = hospitalname;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getCluster() {
		return cluster;
	}
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getReportingdate() {
		return reportingdate;
	}
	public void setReportingdate(String reportingdate) {
		this.reportingdate = reportingdate;
	}
	public String getDateofidentification() {
		return dateofidentification;
	}
	public void setDateofidentification(String dateofidentification) {
		this.dateofidentification = dateofidentification;
	}
	public String getAgeofidentification() {
		return ageofidentification;
	}
	public void setAgeofidentification(String ageofidentification) {
		this.ageofidentification = ageofidentification;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

}
