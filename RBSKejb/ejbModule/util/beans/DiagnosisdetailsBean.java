package util.beans;

public class DiagnosisdetailsBean {

	String bid ;
	String anomaly ;
	String syndrome ;
	String provisional ;
	String complediagnosis ;
	String notifyingperson ;
	String designationofcontact ;
	String facilityreferred ;
	String provisionaldiagstat ;
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getAnomaly() {
		return anomaly;
	}
	public void setAnomaly(String anomaly) {
		this.anomaly = anomaly;
	}
	public String getSyndrome() {
		return syndrome;
	}
	public void setSyndrome(String syndrome) {
		this.syndrome = syndrome;
	}
	public String getProvisional() {
		return provisional;
	}
	public void setProvisional(String provisional) {
		this.provisional = provisional;
	}
	public String getComplediagnosis() {
		return complediagnosis;
	}
	public void setComplediagnosis(String complediagnosis) {
		this.complediagnosis = complediagnosis;
	}
	public String getNotifyingperson() {
		return notifyingperson;
	}
	public void setNotifyingperson(String notifyingperson) {
		this.notifyingperson = notifyingperson;
	}
	public String getDesignationofcontact() {
		return designationofcontact;
	}
	public void setDesignationofcontact(String designationofcontact) {
		this.designationofcontact = designationofcontact;
	}
	public String getFacilityreferred() {
		return facilityreferred;
	}
	public void setFacilityreferred(String facilityreferred) {
		this.facilityreferred = facilityreferred;
	}
	public String getProvisionaldiagstat() {
		return provisionaldiagstat;
	}
	public void setProvisionaldiagstat(String provisionaldiagstat) {
		this.provisionaldiagstat = provisionaldiagstat;
	}
	
	
}
