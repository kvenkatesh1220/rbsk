package util.beans;

public class AddressBean {
	
	String bid ;
	String bhouseno ;
	String bstreetname ;
	String bareaname ;
	String bpostoffice ; 
	String bdistrict ; 
	String bstate ;
	String bthouseno;
	String btstreetname;
	String btareaname;
	String btpostoffice;
	String btstate;
	String btdistrict;


	public String getBthouseno() {
		return bthouseno;
	}
	public void setBthouseno(String bthouseno) {
		this.bthouseno = bthouseno;
	}
	public String getBtstreetname() {
		return btstreetname;
	}
	public void setBtstreetname(String btstreetname) {
		this.btstreetname = btstreetname;
	}
	public String getBtareaname() {
		return btareaname;
	}
	public void setBtareaname(String btareaname) {
		this.btareaname = btareaname;
	}
	public String getBtpostoffice() {
		return btpostoffice;
	}
	public void setBtpostoffice(String btpostoffice) {
		this.btpostoffice = btpostoffice;
	}
	public String getBtstate() {
		return btstate;
	}
	public void setBtstate(String btstate) {
		this.btstate = btstate;
	}
	public String getBtdistrict() {
		return btdistrict;
	}
	public void setBtdistrict(String btdistrict) {
		this.btdistrict = btdistrict;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getBhouseno() {
		return bhouseno;
	}
	public void setBhouseno(String bhouseno) {
		this.bhouseno = bhouseno;
	}
	public String getBstreetname() {
		return bstreetname;
	}
	public void setBstreetname(String bstreetname) {
		this.bstreetname = bstreetname;
	}
	public String getBareaname() {
		return bareaname;
	}
	public void setBareaname(String bareaname) {
		this.bareaname = bareaname;
	}
	public String getBpostoffice() {
		return bpostoffice;
	}
	public void setBpostoffice(String bpostoffice) {
		this.bpostoffice = bpostoffice;
	}
	public String getBdistrict() {
		return bdistrict;
	}
	public void setBdistrict(String bdistrict) {
		this.bdistrict = bdistrict;
	}
	public String getBstate() {
		return bstate;
	}
	public void setBstate(String bstate) {
		this.bstate = bstate;
	}
	
	
	
}
