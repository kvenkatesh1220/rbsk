package util.beans;

public class BabyTotalDataBean {

	String bid;
	String bhouseno;
	String bstreetname;
	String bareaname;
	String bpostoffice;
	String bdistrict;
	String bstate;
	String bthouseno;
	String btstreetname;
	String btareaname;
	String btpostoffice;
	String btstate;
	String btdistrict;
	String folicaciddetails;
	String hoseriousmetirialillness;
	String horadiationexposure;
	String hosubstancceabuse;
	String parentalconsanguinity;
	String assistedconception;
	String immunisationhistory;
	String maternaldrugs;
	String historyofanomalies;
	String noofpreviousabortion;
	String nofofpreviousstillbirth;
	String maternaldrugsdesc;
	String headcircumference;
	String name;
	String description;
	String ageat;
	String code;
	String confirmmed;
	String defectcategory;
	String defectsubcategory;
	String dateofbirth;
	String birthweightingrms;
	String birthorder;
	String babydeliveryas;
	String sex;
	String gestationalageinweeks;
	String lastmensuralperiod;
	String birthasphyxia;
	String autopsyshowbirth;
	String modeofdelivery;
	String statusofinduction;
	String birthstate;
	String birthdistict;
	String birthblock;
	String birthminicipality;
	String birthplace;
	String mctsno;
	String adharno;
	String childname;
	String mothersname;
	String mothersage;
	String fathersage;
	String mobilenumber;
	String caste;
	String noofphotos;
	String noofattachments;
	String photo1;
	String photo2;
	String reportattachment1;
	String reportattachment2;
	String reportattachment3;
	String reportattachment4;
	String reportattachment5;
	String reportattachment6;
	String noofbabies;
	String typeofdelivery;
	String timeofbirth;
	String birthtype;
	String anomaly;
	String syndrome;
	String provisional;
	String complediagnosis;
	String notifyingperson;
	String designationofcontact;
	String facilityreferred;
	String provisionaldiagstat;
	String karotype;
	String karotypefindings;
	String bloodtestforch;
	String bloodtestforcah;
	String bloodtestforg6pd;
	String bloodtestforscd;
	String bloodtestothers;
	String bloodtestfindings;
	String bera;
	String berafindings;
	String state;
	String district;
	String cluster;
	String source;
	String reportingdate;
	String dateofidentification;
	String ageofidentification;
	String hospitalname;
	String fathername;
	String resonforcesarean;
	String logo;
	String phonenum;
	String haddress;
	String hdist;
	String hstate;
	
	
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getPhonenum() {
		return phonenum;
	}
	public void setPhonenum(String phonenum) {
		this.phonenum = phonenum;
	}
	public String getHaddress() {
		return haddress;
	}
	public void setHaddress(String haddress) {
		this.haddress = haddress;
	}
	public String getHdist() {
		return hdist;
	}
	public void setHdist(String hdist) {
		this.hdist = hdist;
	}
	public String getHstate() {
		return hstate;
	}
	public void setHstate(String hstate) {
		this.hstate = hstate;
	}
	public String getResonforcesarean() {
		return resonforcesarean;
	}
	public void setResonforcesarean(String resonforcesarean) {
		this.resonforcesarean = resonforcesarean;
	}
	public String getBhouseno() {
		return bhouseno;
	}
	public void setBhouseno(String bhouseno) {
		this.bhouseno = bhouseno;
	}
	public String getBstreetname() {
		return bstreetname;
	}
	public void setBstreetname(String bstreetname) {
		this.bstreetname = bstreetname;
	}
	public String getBareaname() {
		return bareaname;
	}
	public void setBareaname(String bareaname) {
		this.bareaname = bareaname;
	}
	public String getBpostoffice() {
		return bpostoffice;
	}
	public void setBpostoffice(String bpostoffice) {
		this.bpostoffice = bpostoffice;
	}
	public String getBdistrict() {
		return bdistrict;
	}
	public void setBdistrict(String bdistrict) {
		this.bdistrict = bdistrict;
	}
	public String getBstate() {
		return bstate;
	}
	public void setBstate(String bstate) {
		this.bstate = bstate;
	}
	public String getBthouseno() {
		return bthouseno;
	}
	public void setBthouseno(String bthouseno) {
		this.bthouseno = bthouseno;
	}
	public String getBtstreetname() {
		return btstreetname;
	}
	public void setBtstreetname(String btstreetname) {
		this.btstreetname = btstreetname;
	}
	public String getBtareaname() {
		return btareaname;
	}
	public void setBtareaname(String btareaname) {
		this.btareaname = btareaname;
	}
	public String getBtpostoffice() {
		return btpostoffice;
	}
	public void setBtpostoffice(String btpostoffice) {
		this.btpostoffice = btpostoffice;
	}
	public String getBtstate() {
		return btstate;
	}
	public void setBtstate(String btstate) {
		this.btstate = btstate;
	}
	public String getBtdistrict() {
		return btdistrict;
	}
	public void setBtdistrict(String btdistrict) {
		this.btdistrict = btdistrict;
	}
	public String getFolicaciddetails() {
		return folicaciddetails;
	}
	public void setFolicaciddetails(String folicaciddetails) {
		this.folicaciddetails = folicaciddetails;
	}
	public String getHoseriousmetirialillness() {
		return hoseriousmetirialillness;
	}
	public void setHoseriousmetirialillness(String hoseriousmetirialillness) {
		this.hoseriousmetirialillness = hoseriousmetirialillness;
	}
	public String getHoradiationexposure() {
		return horadiationexposure;
	}
	public void setHoradiationexposure(String horadiationexposure) {
		this.horadiationexposure = horadiationexposure;
	}
	public String getHosubstancceabuse() {
		return hosubstancceabuse;
	}
	public void setHosubstancceabuse(String hosubstancceabuse) {
		this.hosubstancceabuse = hosubstancceabuse;
	}
	public String getParentalconsanguinity() {
		return parentalconsanguinity;
	}
	public void setParentalconsanguinity(String parentalconsanguinity) {
		this.parentalconsanguinity = parentalconsanguinity;
	}
	public String getAssistedconception() {
		return assistedconception;
	}
	public void setAssistedconception(String assistedconception) {
		this.assistedconception = assistedconception;
	}
	public String getImmunisationhistory() {
		return immunisationhistory;
	}
	public void setImmunisationhistory(String immunisationhistory) {
		this.immunisationhistory = immunisationhistory;
	}
	public String getMaternaldrugs() {
		return maternaldrugs;
	}
	public void setMaternaldrugs(String maternaldrugs) {
		this.maternaldrugs = maternaldrugs;
	}
	public String getHistoryofanomalies() {
		return historyofanomalies;
	}
	public void setHistoryofanomalies(String historyofanomalies) {
		this.historyofanomalies = historyofanomalies;
	}
	public String getNoofpreviousabortion() {
		return noofpreviousabortion;
	}
	public void setNoofpreviousabortion(String noofpreviousabortion) {
		this.noofpreviousabortion = noofpreviousabortion;
	}
	public String getNofofpreviousstillbirth() {
		return nofofpreviousstillbirth;
	}
	public void setNofofpreviousstillbirth(String nofofpreviousstillbirth) {
		this.nofofpreviousstillbirth = nofofpreviousstillbirth;
	}
	public String getMaternaldrugsdesc() {
		return maternaldrugsdesc;
	}
	public void setMaternaldrugsdesc(String maternaldrugsdesc) {
		this.maternaldrugsdesc = maternaldrugsdesc;
	}
	public String getHeadcircumference() {
		return headcircumference;
	}
	public void setHeadcircumference(String headcircumference) {
		this.headcircumference = headcircumference;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAgeat() {
		return ageat;
	}
	public void setAgeat(String ageat) {
		this.ageat = ageat;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getConfirmmed() {
		return confirmmed;
	}
	public void setConfirmmed(String confirmmed) {
		this.confirmmed = confirmmed;
	}
	public String getDefectcategory() {
		return defectcategory;
	}
	public void setDefectcategory(String defectcategory) {
		this.defectcategory = defectcategory;
	}
	public String getDefectsubcategory() {
		return defectsubcategory;
	}
	public void setDefectsubcategory(String defectsubcategory) {
		this.defectsubcategory = defectsubcategory;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getBirthweightingrms() {
		return birthweightingrms;
	}
	public void setBirthweightingrms(String birthweightingrms) {
		this.birthweightingrms = birthweightingrms;
	}
	public String getBirthorder() {
		return birthorder;
	}
	public void setBirthorder(String birthorder) {
		this.birthorder = birthorder;
	}
	public String getBabydeliveryas() {
		return babydeliveryas;
	}
	public void setBabydeliveryas(String babydeliveryas) {
		this.babydeliveryas = babydeliveryas;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getGestationalageinweeks() {
		return gestationalageinweeks;
	}
	public void setGestationalageinweeks(String gestationalageinweeks) {
		this.gestationalageinweeks = gestationalageinweeks;
	}
	public String getLastmensuralperiod() {
		return lastmensuralperiod;
	}
	public void setLastmensuralperiod(String lastmensuralperiod) {
		this.lastmensuralperiod = lastmensuralperiod;
	}
	public String getBirthasphyxia() {
		return birthasphyxia;
	}
	public void setBirthasphyxia(String birthasphyxia) {
		this.birthasphyxia = birthasphyxia;
	}
	public String getAutopsyshowbirth() {
		return autopsyshowbirth;
	}
	public void setAutopsyshowbirth(String autopsyshowbirth) {
		this.autopsyshowbirth = autopsyshowbirth;
	}
	public String getModeofdelivery() {
		return modeofdelivery;
	}
	public void setModeofdelivery(String modeofdelivery) {
		this.modeofdelivery = modeofdelivery;
	}
	public String getStatusofinduction() {
		return statusofinduction;
	}
	public void setStatusofinduction(String statusofinduction) {
		this.statusofinduction = statusofinduction;
	}
	public String getBirthstate() {
		return birthstate;
	}
	public void setBirthstate(String birthstate) {
		this.birthstate = birthstate;
	}
	public String getBirthdistict() {
		return birthdistict;
	}
	public void setBirthdistict(String birthdistict) {
		this.birthdistict = birthdistict;
	}
	public String getBirthblock() {
		return birthblock;
	}
	public void setBirthblock(String birthblock) {
		this.birthblock = birthblock;
	}
	public String getBirthminicipality() {
		return birthminicipality;
	}
	public void setBirthminicipality(String birthminicipality) {
		this.birthminicipality = birthminicipality;
	}
	public String getBirthplace() {
		return birthplace;
	}
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}
	public String getMctsno() {
		return mctsno;
	}
	public void setMctsno(String mctsno) {
		this.mctsno = mctsno;
	}
	public String getAdharno() {
		return adharno;
	}
	public void setAdharno(String adharno) {
		this.adharno = adharno;
	}
	public String getChildname() {
		return childname;
	}
	public void setChildname(String childname) {
		this.childname = childname;
	}
	public String getMothersname() {
		return mothersname;
	}
	public void setMothersname(String mothersname) {
		this.mothersname = mothersname;
	}
	public String getMothersage() {
		return mothersage;
	}
	public void setMothersage(String mothersage) {
		this.mothersage = mothersage;
	}
	public String getFathersage() {
		return fathersage;
	}
	public void setFathersage(String fathersage) {
		this.fathersage = fathersage;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getNoofphotos() {
		return noofphotos;
	}
	public void setNoofphotos(String noofphotos) {
		this.noofphotos = noofphotos;
	}
	public String getNoofattachments() {
		return noofattachments;
	}
	public void setNoofattachments(String noofattachments) {
		this.noofattachments = noofattachments;
	}
	public String getPhoto1() {
		return photo1;
	}
	public void setPhoto1(String photo1) {
		this.photo1 = photo1;
	}
	public String getPhoto2() {
		return photo2;
	}
	public void setPhoto2(String photo2) {
		this.photo2 = photo2;
	}
	public String getReportattachment1() {
		return reportattachment1;
	}
	public void setReportattachment1(String reportattachment1) {
		this.reportattachment1 = reportattachment1;
	}
	public String getReportattachment2() {
		return reportattachment2;
	}
	public void setReportattachment2(String reportattachment2) {
		this.reportattachment2 = reportattachment2;
	}
	public String getReportattachment3() {
		return reportattachment3;
	}
	public void setReportattachment3(String reportattachment3) {
		this.reportattachment3 = reportattachment3;
	}
	public String getReportattachment4() {
		return reportattachment4;
	}
	public void setReportattachment4(String reportattachment4) {
		this.reportattachment4 = reportattachment4;
	}
	public String getReportattachment5() {
		return reportattachment5;
	}
	public void setReportattachment5(String reportattachment5) {
		this.reportattachment5 = reportattachment5;
	}
	public String getReportattachment6() {
		return reportattachment6;
	}
	public void setReportattachment6(String reportattachment6) {
		this.reportattachment6 = reportattachment6;
	}
	public String getNoofbabies() {
		return noofbabies;
	}
	public void setNoofbabies(String noofbabies) {
		this.noofbabies = noofbabies;
	}
	public String getTypeofdelivery() {
		return typeofdelivery;
	}
	public void setTypeofdelivery(String typeofdelivery) {
		this.typeofdelivery = typeofdelivery;
	}
	public String getTimeofbirth() {
		return timeofbirth;
	}
	public void setTimeofbirth(String timeofbirth) {
		this.timeofbirth = timeofbirth;
	}
	public String getBirthtype() {
		return birthtype;
	}
	public void setBirthtype(String birthtype) {
		this.birthtype = birthtype;
	}
	public String getAnomaly() {
		return anomaly;
	}
	public void setAnomaly(String anomaly) {
		this.anomaly = anomaly;
	}
	public String getSyndrome() {
		return syndrome;
	}
	public void setSyndrome(String syndrome) {
		this.syndrome = syndrome;
	}
	public String getProvisional() {
		return provisional;
	}
	public void setProvisional(String provisional) {
		this.provisional = provisional;
	}
	public String getComplediagnosis() {
		return complediagnosis;
	}
	public void setComplediagnosis(String complediagnosis) {
		this.complediagnosis = complediagnosis;
	}
	public String getNotifyingperson() {
		return notifyingperson;
	}
	public void setNotifyingperson(String notifyingperson) {
		this.notifyingperson = notifyingperson;
	}
	public String getDesignationofcontact() {
		return designationofcontact;
	}
	public void setDesignationofcontact(String designationofcontact) {
		this.designationofcontact = designationofcontact;
	}
	public String getFacilityreferred() {
		return facilityreferred;
	}
	public void setFacilityreferred(String facilityreferred) {
		this.facilityreferred = facilityreferred;
	}
	public String getProvisionaldiagstat() {
		return provisionaldiagstat;
	}
	public void setProvisionaldiagstat(String provisionaldiagstat) {
		this.provisionaldiagstat = provisionaldiagstat;
	}
	public String getKarotype() {
		return karotype;
	}
	public void setKarotype(String karotype) {
		this.karotype = karotype;
	}
	public String getKarotypefindings() {
		return karotypefindings;
	}
	public void setKarotypefindings(String karotypefindings) {
		this.karotypefindings = karotypefindings;
	}
	public String getBloodtestforch() {
		return bloodtestforch;
	}
	public void setBloodtestforch(String bloodtestforch) {
		this.bloodtestforch = bloodtestforch;
	}
	public String getBloodtestforcah() {
		return bloodtestforcah;
	}
	public void setBloodtestforcah(String bloodtestforcah) {
		this.bloodtestforcah = bloodtestforcah;
	}
	public String getBloodtestforg6pd() {
		return bloodtestforg6pd;
	}
	public void setBloodtestforg6pd(String bloodtestforg6pd) {
		this.bloodtestforg6pd = bloodtestforg6pd;
	}
	public String getBloodtestforscd() {
		return bloodtestforscd;
	}
	public void setBloodtestforscd(String bloodtestforscd) {
		this.bloodtestforscd = bloodtestforscd;
	}
	public String getBloodtestothers() {
		return bloodtestothers;
	}
	public void setBloodtestothers(String bloodtestothers) {
		this.bloodtestothers = bloodtestothers;
	}
	public String getBloodtestfindings() {
		return bloodtestfindings;
	}
	public void setBloodtestfindings(String bloodtestfindings) {
		this.bloodtestfindings = bloodtestfindings;
	}
	public String getBera() {
		return bera;
	}
	public void setBera(String bera) {
		this.bera = bera;
	}
	public String getBerafindings() {
		return berafindings;
	}
	public void setBerafindings(String berafindings) {
		this.berafindings = berafindings;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCluster() {
		return cluster;
	}
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getReportingdate() {
		return reportingdate;
	}
	public void setReportingdate(String reportingdate) {
		this.reportingdate = reportingdate;
	}
	public String getDateofidentification() {
		return dateofidentification;
	}
	public void setDateofidentification(String dateofidentification) {
		this.dateofidentification = dateofidentification;
	}
	public String getAgeofidentification() {
		return ageofidentification;
	}
	public void setAgeofidentification(String ageofidentification) {
		this.ageofidentification = ageofidentification;
	}
	public String getHospitalname() {
		return hospitalname;
	}
	public void setHospitalname(String hospitalname) {
		this.hospitalname = hospitalname;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	

	
	
	
}
