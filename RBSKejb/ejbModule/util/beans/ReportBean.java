package util.beans;

public class ReportBean {
	String bid; 
	String source; 
	String defectcategory; 
	String defectsubcategory; 
	String cluster; 
	String district; 
	String sex; 
	String modeofdelivery; 
	String birthdistict; 
	String mctsno; 
	String adharno; 
	String mothersname; 
	String typeofdelivery; 
	String birthtype; 
	String ageofidentification; 
	String hospitalname;
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDefectcategory() {
		return defectcategory;
	}
	public void setDefectcategory(String defectcategory) {
		this.defectcategory = defectcategory;
	}
	public String getDefectsubcategory() {
		return defectsubcategory;
	}
	public void setDefectsubcategory(String defectsubcategory) {
		this.defectsubcategory = defectsubcategory;
	}
	public String getCluster() {
		return cluster;
	}
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getModeofdelivery() {
		return modeofdelivery;
	}
	public void setModeofdelivery(String modeofdelivery) {
		this.modeofdelivery = modeofdelivery;
	}
	public String getBirthdistict() {
		return birthdistict;
	}
	public void setBirthdistict(String birthdistict) {
		this.birthdistict = birthdistict;
	}
	public String getMctsno() {
		return mctsno;
	}
	public void setMctsno(String mctsno) {
		this.mctsno = mctsno;
	}
	public String getAdharno() {
		return adharno;
	}
	public void setAdharno(String adharno) {
		this.adharno = adharno;
	}
	public String getMothersname() {
		return mothersname;
	}
	public void setMothersname(String mothersname) {
		this.mothersname = mothersname;
	}
	public String getTypeofdelivery() {
		return typeofdelivery;
	}
	public void setTypeofdelivery(String typeofdelivery) {
		this.typeofdelivery = typeofdelivery;
	}
	public String getBirthtype() {
		return birthtype;
	}
	public void setBirthtype(String birthtype) {
		this.birthtype = birthtype;
	}
	public String getAgeofidentification() {
		return ageofidentification;
	}
	public void setAgeofidentification(String ageofidentification) {
		this.ageofidentification = ageofidentification;
	}
	public String getHospitalname() {
		return hospitalname;
	}
	public void setHospitalname(String hospitalname) {
		this.hospitalname = hospitalname;
	} 								
	
	
}
