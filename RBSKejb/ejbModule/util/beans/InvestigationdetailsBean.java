package util.beans;

public class InvestigationdetailsBean {

	String bid ; 
	String karotype ;
	String karotypefindings ;
	String bloodtestforch ;
	String bloodtestforcah ;
	String bloodtestforg6pd ;
	String bloodtestforscd ;
	String bloodtestothers ;
	String bloodtestfindings ;
	String bera ;
	String berafindings ;
	
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getKarotype() {
		return karotype;
	}
	public void setKarotype(String karotype) {
		this.karotype = karotype;
	}
	public String getKarotypefindings() {
		return karotypefindings;
	}
	public void setKarotypefindings(String karotypefindings) {
		this.karotypefindings = karotypefindings;
	}
	public String getBlodtestforch() {
		return bloodtestforch;
	}
	public void setBlodtestforch(String blodtestforch) {
		this.bloodtestforch = blodtestforch;
	}
	public String getBloodtestforcah() {
		return bloodtestforcah;
	}
	public void setBloodtestforcah(String bloodtestforcah) {
		this.bloodtestforcah = bloodtestforcah;
	}
	public String getBloodtestforg6pd() {
		return bloodtestforg6pd;
	}
	public void setBloodtestforg6pd(String bloodtestforg6pd) {
		this.bloodtestforg6pd = bloodtestforg6pd;
	}
	public String getBloodtestforscd() {
		return bloodtestforscd;
	}
	public void setBloodtestforscd(String bloodtestforscd) {
		this.bloodtestforscd = bloodtestforscd;
	}
	public String getBloodtestothers() {
		return bloodtestothers;
	}
	public void setBloodtestothers(String bloodtestothers) {
		this.bloodtestothers = bloodtestothers;
	}
	public String getBloodtestfindings() {
		return bloodtestfindings;
	}
	public void setBloodtestfindings(String bloodtestfindings) {
		this.bloodtestfindings = bloodtestfindings;
	}
	public String getBera() {
		return bera;
	}
	public void setBera(String bera) {
		this.bera = bera;
	}
	public String getBerafindings() {
		return berafindings;
	}
	public void setBerafindings(String berafindings) {
		this.berafindings = berafindings;
	}
	
	
}
