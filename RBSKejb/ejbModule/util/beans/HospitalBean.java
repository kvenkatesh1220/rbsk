package util.beans;

public class HospitalBean {
String hospitalname;
String hospitalid;
String regnum;
String phnum;
String logo;
String address;
String country;
String state;
String district;
String latitude;
String longitude;
String loccation;

public String getLoccation() {
	return loccation;
}
public void setLoccation(String loccation) {
	this.loccation = loccation;
}
public String getHospitalname() {
	return hospitalname;
}
public void setHospitalname(String hospitalname) {
	this.hospitalname = hospitalname;
}
public String getHospitalid() {
	return hospitalid;
}
public void setHospitalid(String hospitalid) {
	this.hospitalid = hospitalid;
}
public String getRegnum() {
	return regnum;
}
public void setRegnum(String regnum) {
	this.regnum = regnum;
}
public String getPhnum() {
	return phnum;
}
public void setPhnum(String phnum) {
	this.phnum = phnum;
}
public String getLogo() {
	return logo;
}
public void setLogo(String logo) {
	this.logo = logo;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getDistrict() {
	return district;
}
public void setDistrict(String district) {
	this.district = district;
}
public String getLatitude() {
	return latitude;
}
public void setLatitude(String latitude) {
	this.latitude = latitude;
}
public String getLongitude() {
	return longitude;
}
public void setLongitude(String longitude) {
	this.longitude = longitude;
}


}
