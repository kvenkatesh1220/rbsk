package util.beans;

public class AntenataldetailsBean {

	String bid ;
	String folicaciddetails ;
	String hoseriousmetirialillness ;
	String horadiationexposure ;
	String hosubstancceabuse ;
	String parentalconsanguinity ;
	String assistedconception ;
	String immunisationhistory ;
	String maternaldrugs ;
	String historyofanomalies ;
	String noofpreviousabortion ;
	String nofofpreviousstillbirth ;
	String maternaldrugsdesc;
	String headcircumference;
	
	public String getHeadcircumference() {
		return headcircumference;
	}
	public void setHeadcircumference(String headcircumference) {
		this.headcircumference = headcircumference;
	}
	public String getMaternaldrugsdesc() {
		return maternaldrugsdesc;
	}
	public void setMaternaldrugsdesc(String maternaldrugsdesc) {
		this.maternaldrugsdesc = maternaldrugsdesc;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getFolicaciddetails() {
		return folicaciddetails;
	}
	public void setFolicaciddetails(String folicaciddetails) {
		this.folicaciddetails = folicaciddetails;
	}
	public String getHoseriousmetirialillness() {
		return hoseriousmetirialillness;
	}
	public void setHoseriousmetirialillness(String hoseriousmetirialillness) {
		this.hoseriousmetirialillness = hoseriousmetirialillness;
	}
	public String getHoradiationexposure() {
		return horadiationexposure;
	}
	public void setHoradiationexposure(String horadiationexposure) {
		this.horadiationexposure = horadiationexposure;
	}
	public String getHosubstancceabuse() {
		return hosubstancceabuse;
	}
	public void setHosubstancceabuse(String hosubstancceabuse) {
		this.hosubstancceabuse = hosubstancceabuse;
	}
	public String getParentalconsanguinity() {
		return parentalconsanguinity;
	}
	public void setParentalconsanguinity(String parentalconsanguinity) {
		this.parentalconsanguinity = parentalconsanguinity;
	}
	public String getAssistedconception() {
		return assistedconception;
	}
	public void setAssistedconception(String assistedconception) {
		this.assistedconception = assistedconception;
	}
	public String getImmunisationhistory() {
		return immunisationhistory;
	}
	public void setImmunisationhistory(String immunisationhistory) {
		this.immunisationhistory = immunisationhistory;
	}
	public String getMaternaldrugs() {
		return maternaldrugs;
	}
	public void setMaternaldrugs(String maternaldrugs) {
		this.maternaldrugs = maternaldrugs;
	}
	public String getHistoryofanomalies() {
		return historyofanomalies;
	}
	public void setHistoryofanomalies(String historyofanomalies) {
		this.historyofanomalies = historyofanomalies;
	}
	public String getNoofpreviousabortion() {
		return noofpreviousabortion;
	}
	public void setNoofpreviousabortion(String noofpreviousabortion) {
		this.noofpreviousabortion = noofpreviousabortion;
	}
	public String getNofofpreviousstillbirth() {
		return nofofpreviousstillbirth;
	}
	public void setNofofpreviousstillbirth(String nofofpreviousstillbirth) {
		this.nofofpreviousstillbirth = nofofpreviousstillbirth;
	}
	
}
