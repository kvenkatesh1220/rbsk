package util.beans;

public class CongenitalanomaliesBean {

	String bid ;
	String name ;
	String description ;
	String ageat ;
	String code ;
	String confirmmed ;
	
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAgeat() {
		return ageat;
	}
	public void setAgeat(String ageat) {
		this.ageat = ageat;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getConfirmmed() {
		return confirmmed;
	}
	public void setConfirmmed(String confirmmed) {
		this.confirmmed = confirmmed;
	}
	
	
}
