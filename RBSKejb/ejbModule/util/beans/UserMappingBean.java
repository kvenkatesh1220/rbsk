package util.beans;

public class UserMappingBean {
String user_id;
String source;
String district;
String cluster;
String municipality;
String hospitalname;
String hospitaltype;
String usertype;
String hospitalid;

public String getHospitalid() {
	return hospitalid;
}
public void setHospitalid(String hospitalid) {
	this.hospitalid = hospitalid;
}
public String getUsertype() {
	return usertype;
}
public void setUsertype(String usertype) {
	this.usertype = usertype;
}
public String getHospitalname() {
	return hospitalname;
}
public void setHospitalname(String hospitalname) {
	this.hospitalname = hospitalname;
}
public String getHospitaltype() {
	return hospitaltype;
}
public void setHospitaltype(String hospitaltype) {
	this.hospitaltype = hospitaltype;
}
public String getUser_id() {
	return user_id;
}
public void setUser_id(String user_id) {
	this.user_id = user_id;
}
public String getSource() {
	return source;
}
public void setSource(String source) {
	this.source = source;
}
public String getDistrict() {
	return district;
}
public void setDistrict(String district) {
	this.district = district;
}
public String getCluster() {
	return cluster;
}
public void setCluster(String cluster) {
	this.cluster = cluster;
}
public String getMunicipality() {
	return municipality;
}
public void setMunicipality(String municipality) {
	this.municipality = municipality;
}






	
}
