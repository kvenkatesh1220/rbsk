package util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

public class MyDateUtil {
	static Logger logger = Logger.getLogger(MyDateUtil.class.getName());
	public java.sql.Date convertStringddmmyytomysqldate(String dateinstrig)
	{
		try
		{
			logger.info("Source Date String :"+dateinstrig);
			DateFormat sourceformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date sourceDate = sourceformat.parse(dateinstrig);
			DateFormat destformat = new SimpleDateFormat("yyyy-MM-dd");
			dateinstrig = destformat.format(sourceDate);
			logger.info("Source Util Date :"+sourceDate);
			logger.info("Destination Date String :"+dateinstrig);
			java.util.Date destdate = destformat.parse(dateinstrig);
			
			java.sql.Date sqlDate = new java.sql.Date(destdate.getTime());
			logger.info("Converted sql Date :"+sqlDate);
			return sqlDate;
		}
		catch(Exception e)
		{
			logger.info("Unable to convert String "+dateinstrig+" to mysql date");
			e.printStackTrace();
			return null;
		}
	}

	public java.sql.Date convertString2mysqldate(String dateinstrig)
	{
		try
		{
			logger.info("Source Date String :"+dateinstrig);
			DateFormat sourceformat = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date sourceDate = sourceformat.parse(dateinstrig);
			DateFormat destformat = new SimpleDateFormat("yyyy-MM-dd");
			dateinstrig = destformat.format(sourceDate);
			logger.info("Source Util Date :"+sourceDate);
			logger.info("Destination Date String :"+dateinstrig);
			java.util.Date destdate = destformat.parse(dateinstrig);
			
			java.sql.Date sqlDate = new java.sql.Date(destdate.getTime());
			logger.info("Converted sql Date :"+sqlDate);
			return sqlDate;
		}
		catch(Exception e)
		{
			logger.info("Unable to convert String "+dateinstrig+" to mysql date");
			e.printStackTrace();
			return null;
		}
	}
	public java.sql.Date convertddmonyyyy2mysqldate(String dateinstrig)
	{
		try
		{
			logger.info("Source Date String :"+dateinstrig);
			DateFormat sourceformat = new SimpleDateFormat("dd-MMM-yyyy");
			java.util.Date sourceDate = sourceformat.parse(dateinstrig);
			DateFormat destformat = new SimpleDateFormat("yyyy-MM-dd");
			dateinstrig = destformat.format(sourceDate);
			logger.info("Source Util Date :"+sourceDate);
			logger.info("Destination Date String :"+dateinstrig);
			java.util.Date destdate = destformat.parse(dateinstrig);
			
			java.sql.Date sqlDate = new java.sql.Date(destdate.getTime());
			logger.info("Converted sql Date :"+sqlDate);
			return sqlDate;
		}
		catch(Exception e)
		{
			logger.info("Unable to convert String "+dateinstrig+" to mysql date");
			e.printStackTrace();
			return null;
		}
	}

	public java.sql.Date convertddMonyyyy2mysqldate(String dateinstrig)
	{
		try
		{
			logger.info("Source Date String :"+dateinstrig);
			DateFormat sourceformat = new SimpleDateFormat("dd-MMM-yyyy");
			java.util.Date sourceDate = sourceformat.parse(dateinstrig);
			DateFormat destformat = new SimpleDateFormat("yyyy-MM-dd");
			dateinstrig = destformat.format(sourceDate);
			logger.info("Source Util Date :"+sourceDate);
			logger.info("Destination Date String :"+dateinstrig);
			java.util.Date destdate = destformat.parse(dateinstrig);
			
			java.sql.Date sqlDate = new java.sql.Date(destdate.getTime());
			logger.info("Converted sql Date :"+sqlDate);
			return sqlDate;
		}
		catch(Exception e)
		{
			logger.info("Unable to convert String "+dateinstrig+" to mysql date");
			e.printStackTrace();
			return null;
		}
	}
	public String convertmysqlDatetoString(java.sql.Date mysqldate)
	{
		try
		{
			logger.info("Source mysql date :"+mysqldate);
			java.util.Date utilDate = new java.util.Date(mysqldate.getTime());
			DateFormat sourceformat = new SimpleDateFormat("dd-MMM-yyyy");
			String dateinstrig = sourceformat.format(utilDate);
			logger.info("String of given mysql date :"+dateinstrig);
			return dateinstrig;
		}
		catch(Exception e)
		{
			logger.info("Unable to mysqldate "+mysqldate+" to String");
			e.printStackTrace();
			return null;
			
		}
	}
	public String convertmysqlDatetoddmmyyyyString(java.sql.Date mysqldate)
	{
		try
		{
			logger.info("Source mysql date :"+mysqldate);
			java.util.Date utilDate = new java.util.Date(mysqldate.getTime());
			DateFormat sourceformat = new SimpleDateFormat("yyyy-MM-dd");
			String dateinstrig = sourceformat.format(utilDate);
			logger.info("String of given mysql date :"+dateinstrig);
			return dateinstrig;
		}
		catch(Exception e)
		{
			logger.info("Unable to mysqldate "+mysqldate+" to String");
			e.printStackTrace();
			return null;
			
		}
	}
	public java.util.Date convertstringddmmyyyytoutildate(String stringdate)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		
		try {

			java.util.Date date = formatter.parse(stringdate);
			logger.info(date);
			return date;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public String convertddmmmyyyytosqlformat(String stringdate)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		
		try {

			java.util.Date date = formatter.parse(stringdate);
			logger.info(date);
			DateFormat sourceformat = new SimpleDateFormat("yyyy-MM-dd");
			String dateinstrig = sourceformat.format(date);
			logger.info("String of given mysql date :"+dateinstrig);
			return dateinstrig;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
