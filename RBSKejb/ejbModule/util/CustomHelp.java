package util;

import java.util.ArrayList;

import org.apache.log4j.Logger;

public class CustomHelp {
	static Logger logger = Logger.getLogger(CustomHelp.class.getName());
	public String getJsonArrayString(ArrayList<CustomRowPojo> mycustrowsobjlist)
	{
		String jsonarraystring="";
		String prepart="[";
		String postpart="]";
		String bodypart="";
		try
		{
			for(int i=0;i<mycustrowsobjlist.size();i++)
			{
				CustomRowPojo customrowpojoobj = mycustrowsobjlist.get(i);
				if(i==mycustrowsobjlist.size()-1)
				{
					bodypart=bodypart+customrowpojoobj.getJsonrowstring();
				}
				else
				{
					bodypart=bodypart+customrowpojoobj.getJsonrowstring()+",";
				}
			
			logger.info("bodypart ::"+bodypart);
			}
			jsonarraystring=prepart+bodypart+postpart;
			
			return jsonarraystring;
		}
		catch(Exception e)
		{
			logger.info("Exception occoured in getJsonArrayString");
			return jsonarraystring;
		}
	}

}
