package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class CustIDGeneratorUtil {
 
    private static final String CHAR_LIST ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static final int RANDOM_STRING_LENGTH = 9;
     
    /**
     * This method generates random string
     * @return
     */
    public String generateRandomString(){
         
   	 Calendar cal = Calendar.getInstance();
	 cal.add(Calendar.DATE, 0);
	 SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
	 System.out.println(cal.getTime());
	 // Output "Wed Sep 26 14:23:28 EST 2012"

	 String formatteddate = format1.format(cal.getTime());
	 System.out.println("Date format in Bulkupload DAO ::"+formatteddate);
	 
        StringBuffer randStr = new StringBuffer();
        for(int i=0; i<RANDOM_STRING_LENGTH; i++){
            int number = getRandomNumber();
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString()+"_"+formatteddate;
    }
     
    /**
     * This method generates random numbers
     * @return int
     */
    private int getRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }
    public String generateRandomStringForCaptcha(){
        
      	   StringBuffer randStr = new StringBuffer();
           for(int i=0; i<5; i++){
               int number = getRandomNumber();
               char ch = CHAR_LIST.charAt(number);
               randStr.append(ch);
           }
           return randStr.toString();
       }
    
}
