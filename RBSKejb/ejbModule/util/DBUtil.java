package util;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class DBUtil {

	/* Database properties */
	String dbUser = "rbsk";
	String dbPwd = "wm]*QATA&(Bf";
	String connectionUrl = "jdbc:mysql://94.228.219.195:3306/rbsk";
	static Logger logger = Logger.getLogger(DBUtil.class.getName());
	/* get Database Connection */  
	public Connection getConnection(String className) 
	{
		Connection connection=null;
		try
		{
    	//Class.forName("com.mysql.jdbc.Driver");
    	//conn = DriverManager.getConnection(connectionUrl, dbUser, dbPwd);
    	//logger.info("Connection has been created for className "+className);
    	
    	///
    	InitialContext ctx = new InitialContext();
    	DataSource ds = (DataSource) ctx.lookup("jdbc/rbsk");
    	connection = ds.getConnection();
    		logger.info("Connection has establishing connection using  jndi!"+connection);
    	if (connection == null)
        {
            logger.info("Error establishing connection!");
        }
    	///
    	}
    	catch(Exception e)
    	{
    	e.printStackTrace();
    	return null;
    	}
    		return connection;
    	
	}
	
	/* close Database Connection */
	public boolean closeConnection(Connection dbConnection, String className) 
	{
		
		try 
		{
		dbConnection.close();
		logger.info("Database connection closed for class "+className);
		return true;
		}
		catch (SQLException e1) 
		{
		logger.info("Could not close database connection for class"+className);
		e1.printStackTrace();
		return false;
		}

	}
}
