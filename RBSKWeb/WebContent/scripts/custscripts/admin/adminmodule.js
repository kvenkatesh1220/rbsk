/**
 * BY KV
 */
var usertype="";
var user_id="";
var district="";
var cluster="";
var hospitalname="";
var hospitaltype=""; 

$(document).ready(function(e){
	GetUserMappings(e);
	// loadDistricts(e);
	$('#updateuserbtn').hide();
	$('#updatehspdetbtn').hide();
	 
	
});


$("#createuserbtn").click(function(e) {
	createUser(e);
	});
$("#getdetailsbtn").click(function(e) { 
	getUserDetails(e);
	});
$("#updateuserbtn").click(function(e) {
	updateUser(e);
	});
$("#resetpwdbtn").click(function(e) {
	resetPwd(e);
	});

$("#addhspbtn").click(function(e) {
	createHospital(e);
});

$("#gethspdetbtn").click(function(e) {
	getHospitalDet(e);
});

$("#updatehspdetbtn").click(function(e) {
	updateHospitalDet(e);
});

$("#getmodelhspdetbtn").click(function(e) {
	searchHsp(e);
});


function createUser(e)
{
	if( $('#username').val()!=="" && $('#password').val()!=="" &&  $('#districts :selected').val()!=="" && $('#clusters :selected').val()!=="" && $('#hospitalname :selected').val()!=="" &&  $('#hospitaltype :selected').val()!=="" ){
	 // getting values for Districts drop down
	
	if(!checkalphanumericwithsymbols($('#username').val().trim())){
		toastr.error('Special characters are not allowed in User Name.');
		return false;
	}
	
	
	
	
	 $.ajax({
		 	url: "CreateUsers", 
			type: 'POST',
			 data: {
				 username : $('#username').val(),
				 password : $('#password').val(),
				 districts : $('#districts :selected').val(),
				 clusters : $('#clusters :selected').val(),
				 hospitalid : $('#hospitalname :selected').val(), 
				 hospitaltype : $('#hospitaltype :selected').val(),
				 isadmin : $('input:radio[name=isadmin]:checked').val()},
			success: function (data) {
				if(data.indexOf('Success!') !== -1)
 				{
					$("#usercreateform").trigger('reset'); 
					
					swal({
		                title: "",
		                text: data
		            });
 				
 				}
				if(data.indexOf('Error!') !== -1)
 				{
					toastr.error(data);
 				}
				
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in CreateUsers servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	}
	else
		{
		toastr.error("Error ! '*' Fields are mandatory");
		}
}

function updateUser(e)
{
if($('#update_username').val()!=="" && $('#update_districts :selected').val()!=="" && $('#update_clusters :selected').val()!=="" && $('#update_hospitalname').val()!=="" && $('#update_hospitaltype :selected').val()!=="" &&  $('input:radio[name=update_isadmin]:checked').val()!=="" ){

	if(!checkalphanumericwithsymbols($('#update_username').val().trim())){
		toastr.error('Special characters are not allowed in User Name.');
		return false;
	}
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "UpdateUser", 
			type: 'POST',
			 data: {
				 username : $('#update_username').val(),
				 districts : $('#update_districts :selected').val(),
				 clusters : $('#update_clusters :selected').val(),
				 hospitalname : $('#update_hospitalname :selected').val(),  
				 hospitaltype : $('#update_hospitaltype :selected').val(),
				 isadmin : $('input:radio[name=update_isadmin]:checked').val()},
			success: function (data) {
				if(data.indexOf('Success!') !== -1)
				{
					$("#userupdateform").trigger('reset'); 
					$('#update_username').val('');
					swal({
		                title: "",
		                text: data
		            });
				
				}
				if(data.indexOf('Error!') !== -1)
				{
					toastr.error(data);
				}
				
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in UpdateUser servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
}
else{
	if($('#update_username').val()==="" ){
		toastr.error('Error! UserName should not empty.');
	} if( $('#update_districts :selected').val()==="" ) { 
		toastr.error('Error!  Please select District.');
	} if( $('#update_clusters :selected').val()==="" ) { 
		toastr.error('Error!  Please select Cluster.');
	} if( $('#update_hospitalname').val()==="" ) { 
		toastr.error('Error! HospitalName should not empty.');
	} if( $('#update_hospitaltype :selected').val()==="" ) { 
		toastr.error('Error!  Please select Hospital Type .');
	} if(  $('input:radio[name=update_isadmin]:checked').val()==="" ){
		toastr.error('Error! Please select isAdmin.');
	}
	
}
}


function resetPwd(e)
{
	//ResetPasswordInAdmin

	if($('#reset_username').val()!=="" && $('#reset_pwd').val()!==""){

		if(!checkalphanumericwithsymbols($('#reset_username').val().trim())){
			toastr.error('Special characters are not allowed in User Name.');
			return false;
		}
		 // getting values for Districts drop down
		 $.ajax({
			 	url: "ResetPasswordInAdmin", 
				type: 'POST',
				 data: {
					 username : $('#reset_username').val(),
					 password : $('#reset_pwd').val()
					},
				success: function (data) {
					if(data.indexOf('Success!') !== -1)
					{
						$('#reset_pwd').val(''); 
						$('#reset_username').val('');
						swal({
			                title: "",
			                text: data
			            });
					
					}
					if(data.indexOf('Error!') !== -1)
					{
						toastr.error(data);
					}
					
				},//success ended here 
				
				error: function(request, status, message) { // error started here
					console.log("error occourred in UpdateUser servlet call :");
				    console.log(message);
				   } //error ended here
			});// ajax call ended here
	}
	else{
		if($('#reset_username').val()==="" ){
			toastr.error('Error! Username should not empty.');
		} if($('#reset_pwd').val()==="" ){
			toastr.error('Error! Password should not empty.');
		} 
	}

}

function getUserDetails(e)
{
	if($('#update_username').val()!==""){
		
		if(!checkalphanumericwithsymbols($('#update_username').val().trim())){
			toastr.error('Special characters are not allowed in User Name.');
			return false;
		}
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "GetUserDetails", 
			type: 'POST',
			 data: {username : $('#update_username').val() },
			success: function (data) { // success started here
				var i=0;
				 $.each(data, function(key,value) {
					 
					 $('input:radio[name="update_isadmin"][value="'+value['usertype']+'"]').prop('checked', true); 
					 $("#update_hospitaltype").val(value['hospitaltype']);
					 $("#update_districts").val(value['district']);
					 
					 loadClusters(value['district'],e);
					 loadHospitals(value['district'],e);
					 var j=0;
					 if(j==0)
						 {
					 setInterval(function(){ j=j+1;  }, 300);
						 }
					//  $("#update_clusters").val(value['cluster']);  
					 $('#update_clusters option[value="'+value['cluster']+'"]').attr('selected','selected');
					 
					 
					// $("select#update_hospitalname").val(value['hospitalname']).attr('selected', true);
					//  $("#update_hospitalname").val(value['hospitalid']);   // load hospitals  
					 $("#update_hospitalname").select2().select2('val',value['hospitalid']);  
					 
					 i++;
				 }); // loop ended here 
				 
				  if(i==0)
					  {
					  toastr.error('Error! Not Found Such UserName .');	  
					  }
				  if(i!==0)
					  {
					  $('#updateuserbtn').show();
					  }
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in GetUser details servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	}
	else{
		toastr.error('Error! Username should not empty.');	
	}
	
}


function loadDistricts(e)
{
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "LoadDistricts", 
			type: 'POST',
			// data: {tablename : 'tg_source' },
			success: function (data) { // success started here
				$('#districts').empty();
				$('#update_districts').empty(); // empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select District</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				// console.log(listtoappendstr);
				 $('#districts').append(listtoappendstr); // append  districts 
				 $('#update_districts').append(listtoappendstr);
				 $('#update_hsp_districts').append(listtoappendstr);
				 $('#add_hsp_districts').append(listtoappendstr);
				 $('#update_hsp_districts2').append(listtoappendstr);
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadDistricts servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	
}
$("#districts").change(function(e) {
	 
	if($('#districts :selected').val()!=="")
	{
		loadClusters($('#districts :selected').val(),e); 
		 loadHospitals($('#districts :selected').val(),e);
	}
	else
		{
		$("#clusters").empty();
		$("#clusters").append("<option value=\"\">Select Cluster</option>"); 
		 
		}
});
$("#update_districts").change(function(e) {
	 
	if($('#update_districts :selected').val()!=="")
	{
		loadClusters($('#update_districts :selected').val(),e); 
		loadHospitals($('#update_districts :selected').val(),e); 
		
	}
	else
		{
		$("#update_clusters").empty();
		$("#update_clusters").append("<option value=\"\">Select Cluster</option>"); 
		 
		}
});

function loadClusters(district,e)
{
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "LoadClusters", 
			type: 'POST',
			data: {district : district },
			success: function (data) { // success started here
				$('#clusters').empty();
				$('#update_clusters').empty();// empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select Cluster</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				 if(listtoappendstr=="")
					 {
					 listtoappendstr=listtoappendstr+"<option value=\"\">Select Cluster</option>" ;
					 }
				 //console.log(listtoappendstr);
				 $('#clusters').append(listtoappendstr); // append  districts 
				 $('#update_clusters').append(listtoappendstr);
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadClusters servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
}

function createHospital(e)
{
	var isValid = true; 
	if($('#address').val().trim()!=="" && $('#hspname').val()!=="" && $('#address').val().trim()!==null  && $('#add_hsp_districts :selected').val().trim()===district && district!=="")     
	{
		if(!checkalphanumericwithsymbols($('#hspname').val().trim())){
			toastr.error('Special characters are not allowed in Hospital Name.');
			isValid = false;
		}
		if(!checkalphanumericwithsymbols($('#address').val().trim())){
			toastr.error('Special characters are not allowed in address.');
			isValid = false;
		}
		if(!checkalphanumericwithsymbols($('#regnum').val().trim())){
			toastr.error('Special characters are not allowed in registration number.');
			isValid = false;
		}
		if(!checkalphanumericwithsymbols($('#det_location').val().trim())){
			toastr.error('Special characters are not allowed in Location.');
			isValid = false;
		}
		if( $("#hsplogo")[0].files[0]){
			if(!validateextension('hsplogo')){
				toastr.error("Photo 2 - We support .jpg,.gif,.png,.jpeg. ");
				isValid=false; 
			}else if($("#hsplogo")[0].files[0].size > 10485760){
				toastr.error("Photo 2 - Max file upload size is 10MB. ");
				isValid=false;
			}
		}
		//hsplogo
		
		if(!isValid){
			return;
		}
	$("#addhspbtn").attr('disabled',true);
	var addform = document.getElementById('addhspform');
	var formdata = new FormData(addform);
	  // ajax call started here
	  $.ajax({
	   url: "/AddHospital",
	   type: 'POST',
	   data: formdata,
       contentType: false,
	   processData: false,
	   success: function (data) { 
		
		   if(data.indexOf('Success!')!== -1){
    			
    			$("#addhspform").trigger('reset');
 			   $('#addhspbtn').removeAttr('disabled');
 			  
    		swal({
                title: "",
                text:data
                
            });
    		}
    		
    		if(data.indexOf('Error!') !== -1)
    			{
    			toastr.error(data);
	    		}
	   }, // success ended here
	   error: function(request, status, message) { // error started here
		    console.log("error occourred in AddHospital servlet call :");
		    console.log(message);
		    toastr.error('Error - Unable to Connect Keansa Solutions Server');
		    $('#addhspbtn').removeAttr('disabled');
		    } // error ended here
		  });
		}
	else{
		if($('#address').val()==="" || $('#address').val().trim()==="" || $('#address').val()===null) 
			{
			 toastr.error('Error - Please Provide Address.');
			}
		if($('#hspname').val()==="") 
		{
		 toastr.error('Error - Please Provide HospitalName.');
		}
	if($('#add_hsp_districts :selected').val().trim()!==district) 
		{
		toastr.error('Error - You don\'t have permission to Create Hospital in other districts '); 
		}
	}
}

function getHospitalDet(e)
{
	if($('#update_hsp_districts :selected').val()!="")
		{
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "GetHospitalDetails", 
			type: 'POST',
			 data: {districts :  $('#update_hsp_districts :selected').val(),  
				 hspname : $('#update_hsp_name').val() },
			success: function (data) { // success started here
				 // success started here 
			//	$("#update_hsp_districts").val("");
				
				$('#reporttable').empty(); // empty districts before load

				var tablemetadataatstart = "<div class=\"table-responsive\"><table class=\"table table-hover table-bordered table-striped\"><tbody>";

			    var tableheading = "<tr>" +
			    "<td><strong> Hospital Name</strong></td>"+ 
			    "<td><strong> Registration Number</strong></td>"+ 
			    "<td><strong> Phone </strong></td>"+ 
			    "<td><strong> Location</strong></td>"+ 
			    "<td><strong> Address</strong></td>"+ 
			    "<td><strong> Action</strong></td>"+
			      "</tr>";
				  var tablemetadataatend = "</tbody></table>"; 
				  
				  var tablerow="";
				  
				 // loop started here
				var i=0;
				 $.each(data, function(key,value) { 
					 var hospitalname =convertHtmlEntity(value['hospitalname']); 
					 var hospitalid =convertHtmlEntity(value['hospitalid']); 
					 var regnum =(value['regnum']); 
					 var phnum =convertHtmlEntity(value['phnum']); 
					 var address =convertHtmlEntity(value['address']); 
					 var loccation =convertHtmlEntity(value['loccation']); 
					 
					 if(typeof value['hospitalname']==='undefined' || value['hospitalname']===null || value['hospitalname'].trim()=="")  {  hospitalname="-";  }
					 if(typeof value['hospitalid']==='undefined' ||  value['hospitalid']===null || value['hospitalid'].trim()=="")  {  hospitalid="-";  }
					 if(typeof value['regnum']==='undefined' || value['regnum']===null || value['regnum'].trim()=="" ||  value['regnum']==='null')  {  regnum="-";  }
					 if(typeof value['phnum']==='undefined' || value['phnum']===null || value['phnum'].trim()=="" ||  value['phnum']==='null')  {  phnum="-";  }
					 if(typeof value['address']==='undefined' || value['address']===null || value['address'].trim()=="" ||  value['address']==='null')  {  address="-";  }
					 if(typeof value['loccation']==='undefined' || value['loccation']===null || value['loccation'].trim()=="" ||  value['loccation']==='null')  {  loccation="-";  } 
					 
				//	 alert(hospitalname);
					 tablerow = tablerow + "<tr>" +
					 "<td>"+hospitalname+"</td>" + 
					 "<td>"+regnum+"</td>" + 
					 "<td>"+phnum+"</td>" + 
					 "<td>"+loccation+"</td>" + 
					 "<td>"+address+"</td>" + 
					 "<td class=\"text-right\">" +
					"<button class=\"btn btn-info btn-xs\" " +
					 "onclick=\"getHospitalDetwithhid('"+hospitalid+"','"+e+"') \"> Edit</button>" +
					 "</tr>" ;
					 i=i+1;
					 
				 }); // loop ended here
				 console.log(tablemetadataatstart+tableheading+tablerow+tablemetadataatend);
				 $('#reporttable').empty();
				 $('#reporttable').append("<p> Total Number of Records ::  <b>"+i+"</b></P>");
				 if(i!=0)
					 {
				 $('#reporttable').append(tablemetadataatstart+tableheading+tablerow+tablemetadataatend); // append  districts
					 }
				  if(i===0)
					  {
					  toastr.error('Error - Can not found Hospital Name .');
					  }
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in GetHospitalDetails servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
		}
	else
		{
		toastr.error('Error - Please Select  District  .');
		}
	}

function getHospitalDetwithhid(hspid,e) 
{
	$('#updatehspdetbtn').show();
	
	setHospitaldata(hspid,e);
	
}

function setHospitaldata(hspid,e){
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "GetHospitalDetails", 
			type: 'POST',
			 data: { hospitalid : hspid },
			success: function (data) { // success started here
				 // success started here 
				 $.each(data, function(key,value) {
					 var hospitalname =value['hospitalname']; 
					 var hospitalid =value['hospitalid']; 
					 var regnum =value['regnum']; 
					 var phnum =value['phnum']; 
					 var address =value['address']; 
					 var loccation =value['loccation']; 
					 
					 var logo=value['logo'];
					 var country=value['country'];
					 var state=value['state'];
					 var district=value['district'];
					 var latitude=value['latitude'];
					 var longitude=value['longitude'];
					 
					 if(typeof value['hospitalname']==='undefined' || value['hospitalname']===null || value['hospitalname'].trim()==""  || value['hospitalname']==='null')  {  hospitalname="";  }
					 if(typeof value['hospitalid']==='undefined' ||  value['hospitalid']===null || value['hospitalid'].trim()=="" || value['hospitalid']==='null')  {  hospitalid="";  }
					 if(typeof value['regnum']==='undefined' || value['regnum']===null || value['regnum'].trim()==""   || value['regnum']==='null')  {  regnum="";  }
					 if(typeof value['phnum']==='undefined' || value['phnum']===null || value['phnum'].trim()==""  || value['phnum']==='null')  {  phnum="";  }
					 if(typeof value['address']==='undefined' || value['address']===null || value['address'].trim()==""  || value['address']==='null')  {  address="";  }
					 if(typeof value['loccation']==='undefined' || value['loccation']===null || value['loccation'].trim()==""  || value['loccation']==='null')  {  loccation="";  }
				
					 if(typeof value['logo']==='undefined' || value['logo']===null || value['logo'].trim()==""  || value['logo']==='null')  {  logo="";  }
					 if(typeof value['country']==='undefined' ||  value['country']===null || value['country'].trim()==""  || value['country']==='null')  {  country="";  }
					 if(typeof value['state']==='undefined' || value['state']===null || value['state'].trim()==""  || value['state']==='null')  {  state="";  }
					 if(typeof value['district']==='undefined' || value['district']===null || value['district'].trim()==""  || value['district']==='null')  {  district="";  }
					 if(typeof value['latitude']==='undefined' || value['latitude']===null || value['latitude'].trim()==""  || value['latitude']==='null')  {  latitude="";  }
					 if(typeof value['longitude']==='undefined' || value['longitude']===null || value['longitude'].trim()==""  || value['longitude']==='null')  {  longitude="";  }
					   
					 $('#update_hospitalid').val(hospitalid);
					 $('#update_hspname').val(hospitalname);
					 $('#update_phnum').val(phnum);
					 $('#logo_original').val(logo); 
					 $('#update_address').val(address);
					 $('#update_regnum').val(regnum);
					 $('#update_det_location').val(loccation);
					 $('#update_det_country').val(country);
					 $('#update_det_state').val(state);
					 $('#update_det_city').val(district);
					 $('#update_hsp_districts2').val(district); 
					 $('#update_det_latitude').val(latitude);
					 $('#update_det_longitude').val(longitude);
					 
				 }); // loop ended here
				
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in GetHospitalDetails servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	
	
}

function updateHospitalDet(e)
{
	if($('#logo_original').val()==$('#update_hsplogo').val() && $('#update_hsplogo').val() !=="" && $('#logo_modify').val()==="")
	{
	$('#logo_modify').val($('#logo_original').val());
	}
if($('#update_hsplogo').val()==="") 
	{
	$('#logo_modify').val($('#logo_original').val());  
	}

if($('#update_address').val().trim()!=="" && $('#update_hspname').val()!=="" && $('#update_address').val().trim()!==null)  
{
	var isValid =true;
	if(!checkalphanumericwithsymbols($('#update_hspname').val().trim())){
		toastr.error('Special characters are not allowed in Hospital Name.');
		isValid = false;
	}
	if(!checkalphanumericwithsymbols($('#update_address').val().trim())){
		toastr.error('Special characters are not allowed in address.');
		isValid = false;
	}
	if(!checkalphanumericwithsymbols($('#update_regnum').val().trim())){
		toastr.error('Special characters are not allowed in registration number.');
		isValid = false;
	}
	if(!checkalphanumericwithsymbols($('#update_det_location').val().trim())){
		toastr.error('Special characters are not allowed in Location.');
		isValid = false;
	}
	if( $("#update_hsplogo")[0].files[0]){
		if(!validateextension('update_hsplogo')){
			toastr.error("Photo 2 - We support .jpg,.gif,.png,.jpeg. ");
			isValid=false; 
		}else if($("#update_hsplogo")[0].files[0].size > 10485760){
			toastr.error("Photo 2 - Max file upload size is 10MB. ");
			isValid=false;
		}
	}
	
	if(!isValid){
		return;
	}
	
	$("#updatehspdetbtn").attr('disabled',true);
	var addform = document.getElementById('updatehspform');
	var formdata = new FormData(addform);
	  // ajax call started here
	  $.ajax({
	   url: "/UpdateHospitalDet",
	   type: 'POST',
	   data: formdata,
       contentType: false,
	   processData: false,
	   success: function (data) { 
		
		   if(data.indexOf('Success!')!== -1){
    			
    			$("#updatehspform").trigger('reset');
 			   $('#updatehspdetbtn').removeAttr('disabled');
 			  $('#updatehspdetbtn').hide();
    		swal({
                title: "",
                text:data
                
            });
    		}
    		
    		if(data.indexOf('Error!') !== -1)
    			{
    			toastr.error(data);
	    		}
	   }, // success ended here
	   error: function(request, status, message) { // error started here
		    console.log("error occourred in AddHospital servlet call :");
		    console.log(message);
		    toastr.error('Error - Unable to Connect Keansa Solutions Server');
		    $('#addhspbtn').removeAttr('disabled');
		    } // error ended here
		  });
}
else{
	if($('#update_address').val()==="" || $('#update_address').val().trim()==="" || $('#update_address').val()===null) 
		{
		 toastr.error('Error - Please Provide Address.');
		}
	if($('#update_hspname').val()==="") 
	{
	 toastr.error('Error - Please Provide HospitalName.');
	}
}
}

$('#update_hsplogo').on('change', function(event) { 
	if(typeof $('#update_hsplogo').val()!=='undefined' && $('#update_hsplogo').val()!=="" && $('#update_hsplogo').val()!==null)
		{
	var img=$('#update_hsplogo').val().split("\\"); 
	$('#logo_modify').val(img[img.length-1]);
		}
});

function searchHsp(e)
{
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "GetHospitalDetails", 
			type: 'POST',
			 data: {districts :  $('#model_hsp_districts :selected').val(),  
				 hspname : $('#model_hsp_name').val() },
			success: function (data) { // success started here
				 // success started here 
				$("#update_hsp_districts").val("");
				
				$('#reporttable').empty(); // empty districts before load

				var tablemetadataatstart = "<div class=\"table-responsive\"><table class=\"table table-hover table-bordered table-striped\"><tbody>";

			    var tableheading = "<tr>" +
			    "<td><strong> Hospital Name</strong></td>"+ 
			    "<td><strong> Registration Number</strong></td>"+ 
			    "<td><strong> Phone </strong></td>"+ 
			    "<td><strong> Location</strong></td>"+ 
			    "<td><strong> Address</strong></td>"+ 
			    "<td><strong> Action</strong></td>"+
			      "</tr>";
				  var tablemetadataatend = "</tbody></table>"; 
				  
				  var tablerow="";
				  
				 // loop started here
				var i=0;
				 $.each(data, function(key,value) {
					 var hospitalname =convertHtmlEntity(value['hospitalname']); 
					//  var hospitalid =value['hospitalid'];  
					 var regnum =convertHtmlEntity(value['regnum']); 
					 var phnum =convertHtmlEntity(value['phnum']); 
					 var address =convertHtmlEntity(value['address']); 
					 var loccation =convertHtmlEntity(value['loccation']); 
					 
					 if(typeof value['hospitalname']==='undefined' || value['hospitalname']===null || value['hospitalname'].trim()=="")  {  hospitalname="";  }
					 if(typeof value['hospitalid']==='undefined' ||  value['hospitalid']===null || value['hospitalid'].trim()=="")  {  hospitalid="";  }
					 if(typeof value['regnum']==='undefined' || value['regnum']===null || value['regnum'].trim()=="")  {  regnum="";  }
					 if(typeof value['phnum']==='undefined' || value['phnum']===null || value['phnum'].trim()=="")  {  phnum="";  }
					 if(typeof value['address']==='undefined' || value['address']===null || value['address'].trim()=="")  {  address="";  }
					 if(typeof value['loccation']==='undefined' || value['loccation']===null || value['loccation'].trim()=="")  {  loccation="";  }
					 
					 // alert(convertHtmlEntity(hospitalname));
					 tablerow = tablerow + "<tr>" +
					 "<td>"+convertHtmlEntity(hospitalname)+"</td>" + 
					 "<td>"+regnum+"</td>" + 
					 "<td>"+phnum+"</td>" + 
					 "<td>"+loccation+"</td>" + 
					 "<td>"+address+"</td>" + 
					 "<td class=\"text-right\">" +
					"<button class=\"btn btn-info btn-xs\" " +
					 "onclick=\"populatefields('"+hospitalname+"','"+address+"','"+$('#model_hsp_districts :selected').val()+"') \"> Select</button>" +
					 "</tr>" ;
					 i=i+1;
					 
				 }); // loop ended here
				 console.log(tablemetadataatstart+tableheading+tablerow+tablemetadataatend);
				 $('#modelrept').empty();
				 $('#modelrept').append("<p> Total Number of Records ::  <b>"+i+"</b></P>");
				 if(i!=0)
					 {
				 $('#modelrept').append(tablemetadataatstart+tableheading+tablerow+tablemetadataatend); // append  districts
					 }
 			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in GetHospitalDetails servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	
}
function populatefields(hspname,add,dist)
{
	var totadd=hspname;
	if(add!==null && add!=="")
		{
		totadd=totadd+","+add;
		}
	$('#myModal').modal('toggle'); 
	$('#hospitalname').val(totadd); 
	$('#districts').val(dist); 
	
	 loadClusters(dist,'e');
}


function loadHospitals(dist,e)
{
    $.ajax({
           	url : "AutoComplete_GetHospitalDetails",
           	type : "GET",
           	data : { 
           		districts :dist  },
           	dataType : "json",
           	success : function(data) 
           	{
           		var options= "";	
				var i=1;
    		    $.each(data, function(key,value) {
    		  		  
    					var locationId=value.hospitalid;
    				 	var location=value.address;
    				 	if(i==1)
    				   
    				   {
    				 		options = options + " <option value=\"\">Select Hospital</option>";
    				   }
    				 	options = options + " <option value=\""+locationId +"\">"+location+"</option>";
    				 	i=i+1;
    		    }); // each loop ended here
    		    $("#hospitalname").empty(); 
		    	$("#hospitalname").append( options );
		    	
		    	$("#update_hospitalname").empty(); 
		    	$("#update_hospitalname").append( options );
    	   	},
           	error: function(request, status, message) {
				console.log("error occourred in GetHospitalDetails servlet call :");
				console.log(message);
				console.log(request.responseText);
				toastr.error('Error - Unable to process your request at this time.');
			}	
           	}); // ajax ended here
}  



$('.onlynumber').keydown(function (e){
	allowonlynum(e); 
	 });
 
function allowonlynum(e)
{
		if (e.shiftKey || e.ctrlKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) )) {
		e.preventDefault();
		}
		}
}

function GetUserMappings(e)  
{
	 // getting values for Source drop down
	 $.ajax({
		 	url: "GetUserMappingsServlet", 
			type: 'POST',
			// data: {tablename : 'tg_source' },
			success: function (data) { // success started here
				
				
				 $.each(data, function(key,value) {
					 user_id=value['user_id'];
					 district=value['district'];
					 cluster=value['cluster'];
					 hospitalname=value['hospitalname'];
					 hospitaltype=value['hospitaltype']; 
					 usertype=value['usertype'];
						
				 }); // loop ended here
				 /*jQuery("input[value='"+district+"']").attr('checked', true);
				 $('#cluster').val(cluster);
				 //$("#cluster").attr("disabled", "disabled");
				 $('#cluster').attr('readonly', true);
				 $('#birthblock').val(cluster);
				 $('#hospitalname').val(hospitalname);*/
				 
				 if(usertype=='ADMIN' &&  usertype.trim()!=="")
					 { 
				loadDistricts(e);
					 }
				//loadDefectCategory(e);
				 if(usertype=='SUPERUSER' &&  usertype.trim()!=="")  
					 {
					 $('#admindivid').hide();  
					 $('#updateadmindivid').hide(); 
					 $('#districts').append("<option value=\""+district+"\" selected>"+district+"</option>") ;
					 $('#districts').attr('readonly', true);
					 
					 $('#update_districts').append("<option value=\""+district+"\" selected>"+district+"</option>") ;
					 $('#update_districts').attr('readonly', true);
					 
					 $('#update_hsp_districts').append("<option value=\""+district+"\" selected>"+district+"</option>" );
					 $('#update_hsp_districts').attr('readonly', true);
					 
					 $('#add_hsp_districts').append("<option value=\""+district+"\" selected>"+district+"</option>" );
					 $('#add_hsp_districts').attr('readonly', true);
					 
					 $('#update_hsp_districts2').append("<option value=\""+district+"\" selected>"+district+"</option>" );
					 $('#update_hsp_districts2').attr('readonly', true);
					  
					 
					 loadClusters(district,e); 
					 loadHospitals(district,e);
					  }
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				
			    console.log("error occourred in GetUserMappingsServlet servlet call :");
			    console.log(message);
			    
			   } //error ended here
		});// ajax call ended here
	 
}




