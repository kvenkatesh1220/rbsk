/**
 *  BY KV
 */
$(document).ready(function(e){
	// date picker started here
/*	
	$('#auditdatefrom').on('change', function () {
	    $('.datepicker').hide();
	});

	$('#auditdateto').on('change', function () {
    $('.datepicker').hide();
	});*/

	$('#auditdatefrom').datetimepicker({
		// language: 'pt-BR' ,
		format: 'DD-MMM-YYYY HH:mm:ss'
	}); 
	$('#auditdateto').datetimepicker({
		// language: 'pt-BR' ,
		format: 'DD-MMM-YYYY HH:mm:ss'
	}); 
	//date picker ended here
	
});
	

$("#getauditreport").click(function(e) {
	
	if($('#auditdatefrom').val()!=="" && $('#auditdateto').val()!=="")
		{

		var auditfromdate = convertfieldstrtobrowserstrdatetime($('#auditdatefrom').val());
		console.log("browseraudittodate"+auditfromdate);
		
		var audittodate = convertfieldstrtobrowserstrdatetime($('#auditdateto').val());
		console.log("browseraudittodate"+audittodate);
		
		 // getting values for Districts drop down
		 $.ajax({
			 	url: "GetAuditReportsServlet",  
				type: 'POST',
				data: {
					auditdatefrom : $('#auditdatefrom').val(), 
					auditdateto : $('#auditdateto').val(),
					districts : $('#districts :selected').val(),
					clusters : $('#clusters :selected').val(),
					audit  : $('input:radio[name=audit]:checked').val() 
					},
				success: function (data) { // success started here 
					$('#reporttable').empty(); // empty districts before load

					var tablemetadataatstart = "<div class=\"table-responsive\"><table class=\"table table-hover table-bordered table-striped\" id=\"audittbl\"><tbody>";

				    var tableheading = "<tr>" +
				    "<td><strong> User Name</strong></td>";
				    if($('input:radio[name=audit]:checked').val()=='Login_Details')
				    	{
				    	tableheading=tableheading+ "<td><strong> Login Date</strong></td>"+ 
						    "<td><strong> LogOut Date</strong></td>";
				    	}
				    if($('input:radio[name=audit]:checked').val()!=='Login_Details')
			    	{
			    	tableheading=tableheading+ "<td><strong>Action By User</strong></td>"+ 
					    "<td><strong> Date</strong></td>";
			    	} 
				    tableheading=tableheading+"<td><strong> District</strong></td>"+ 
				    "<td><strong> Cluster</strong></td>";
				    if($('input:radio[name=audit]:checked').val()!=='Login_Details')
			    	{
				    	 tableheading=tableheading+ "<td class=\"auditcls\"><strong> Action</strong></td>"; 
			    	}
				      "</tr>";
					  var tablemetadataatend = "</tbody></table>"; 
					  
					  var tablerow="";
					  
					 // loop started here
					var i=0;
					 $.each(data, function(key,value) {
						 var logindate =value['logindate']; 
						 var username =value['username']; 
						 var cluster =value['cluster']; 
						 var district =value['district']; 
						 var audit_date =value['audit_date']; 
						 var action =value['action']; 
						 var logoutdate = value['logoutdate'];
						 var bid=value['bid'];
						
						 if(typeof value['logindate']==='undefined' ||  value['logindate']===null || value['logindate'].trim()=="")  {  logindate="";  }
						 if(typeof value['logoutdate']==='undefined' || value['logoutdate']===null || value['logoutdate'].trim()=="")  {  logoutdate="-";  }
						 if(typeof value['username']==='undefined' || value['username']===null || value['username'].trim()=="")  {  username="";  }
						 if(typeof value['cluster']==='undefined' || value['cluster']===null || value['cluster'].trim()=="")  {  cluster="";  }
						 if(typeof value['district']==='undefined' || value['district']===null || value['district'].trim()=="")  {  district="";  }
						 if(typeof value['audit_date']==='undefined' || value['audit_date']===null || value['audit_date'].trim()=="")  {  audit_date="";  }
						 if(typeof value['action']==='undefined' || value['action']===null || value['action'].trim()=="")  { action ="";  }
						 if(typeof value['bid']==='undefined' || value['bid']===null || value['bid'].trim()=="")  { bid ="";  }
						 
						 console.log("logindate:"+logindate);
						 if(logindate!="")
						 {
						 logindate= dbdatetimetobrowsertime(logindate);
						 logindate= logindate.toString().split("GMT")[0];
						 
						 console.log("logindate in browsertime:"+logindate);
						 
					     }
						 console.log("audit_date :"+audit_date);
						 if(audit_date!="")
						 {
						 audit_date=dbdatetimetobrowsertime(audit_date);
						 audit_date= audit_date.toString().split("GMT")[0];
						 console.log("audit date in browsertime :"+audit_date);
						 }
						 
						 tablerow = tablerow + "<tr>" +
						 "<td>"+username+"</td>" ;
						 if($('input:radio[name=audit]:checked').val()=='Login_Details')
					    	{
							 tablerow=tablerow+ "<td>"+logindate+"</td>" + 
							 "<td>"+logoutdate+"</td>" ; 
					    	}
						 if($('input:radio[name=audit]:checked').val()!=='Login_Details')
					    	{
							 tablerow=tablerow+ "<td>"+action+"</td>" + 
							 "<td>"+audit_date+"</td>" ; 
					    	}
						 tablerow=tablerow+"<td>"+district+"</td>" + 
						 "<td>"+cluster+"</td>" ; 
						
						 
						 if($('input:radio[name=audit]:checked').val()!=='Login_Details')
					    	{
							 tablerow=tablerow+"<td class=\"text-right auditcls\"> <button class=\"btn btn-info btn-xs\" " +
						    "onclick=\"updateBirthReport('"+bid+"','"+e+"') \"> Edit</button>" ;
					    	}
						 "</tr>" ;
						 i=i+1;
						 
					 }); // loop ended here
					 console.log(tablemetadataatstart+tableheading+tablerow+tablemetadataatend);
					 
						var audit=$('input:radio[name=audit]:checked').val() ;
						var fileheading="";
						var colspan="6";
						if(audit=='Insert'){fileheading="  - Records Enter By" ; }
						if(audit=='Update'){fileheading="  - Records Update By" ; }
						if(audit=='BirthReportPrinted'){fileheading="  - Records Prented By" ; }
						 if(audit=='Login_Details'){fileheading="  - Login Details"; colspan="5"; }
						 // <center>Audit Trail Report '+fileheading+' </center> <br>  colspan="2"
						 var tblcaption='<caption><center>Audit Trail Report '+fileheading+' </center></caption>';
						 var numberofrecs='<td  colspan="'+colspan+'">Total Number of Records ::  <b>'+i+'</b></td>  '; 
						 if(i==0){
						 $('#reporttable').empty(); 
					 $('#reporttable').append('<p> Total Number of Records ::  <b>'+i+'</b></P>');
						 } 
					 if(i!=0)
						 {
					 $('#reporttable').append(tablemetadataatstart+tblcaption+numberofrecs+tableheading+tablerow+tablemetadataatend); // append  result
						 }
					  
				},//success ended here 
				
				error: function(request, status, message) { // error started here
					console.log("error occourred in GetReportsServlet servlet call :");
				    console.log(message);
				   } //error ended here
			});// ajax call ended here
		
		}
	else
	{
	if($('#auditdatefrom').val()==="" || $('#auditdateto').val()==="")
		{
		toastr.error('Error - From date , To date should not empty');
		}
	}
});

function printauditreportPDF()
{
	 if($('input:radio[name=audit]:checked').val()!=='Login_Details')
 	{
	$(".auditcls").hide();
 	} 
	var printWindow = window.open('', '', 'height=1500,width=1800');
	 printWindow.document.write('<html>');
	 printWindow.document.write('<body >');
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/fontawesome/css/font-awesome.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/metisMenu/dist/metisMenu.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/animate.css/animate.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/bootstrap/dist/css/bootstrap.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"fonts/pe-icon-7-stroke/css/helper.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"styles/style.css\" type=\"text/css\" media=\"print\"/>" );
	 var divToPrint = document.getElementById('reporttable'); 
	 printWindow.document.write(divToPrint.innerHTML);  
	 printWindow.document.write('</body></html>');
	 printWindow.document.close();
	 printWindow.print(); 
	 if($('input:radio[name=audit]:checked').val()!=='Login_Details')
	 	{
		$(".auditcls").show();
	 	} 
}

function printauditinEXL() 
{
	 if($('input:radio[name=audit]:checked').val()!=='Login_Details')
	 	{
		$(".auditcls").hide();
	 	} 
	// window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=reporttable]').html()));
	var filename=$('input:radio[name=audit]:checked').val()+"_"+$('#auditdatefrom').val()+"_"+ $('#auditdateto').val(); 
	
	if($('#districts :selected').val()!="")
		{
		filename=filename+"_"+$('#districts :selected').val();
		}
	if( $('#clusters :selected').val()!="")
		{
		filename=filename+"_"+$('#clusters :selected').val(); 
		}
	 //getting values of current time for generating the file name
  /*  var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours(); 
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;*/
    //creating a temporary HTML link element (they support setting file names)
    var a = document.createElement('a');
    //getting data from our div that contains the HTML table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('reporttable');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    a.href = data_type + ', ' + table_html;
    //setting the file name
   // a.download = 'exported_table_' + postfix + '.xls';
    a.download = filename + '.xls'; 
    //triggering the function
    a.click();
    //just in case, prevent default behaviour
    if($('input:radio[name=audit]:checked').val()!=='Login_Details')
 	{
	$(".auditcls").show(); 
 	}  
    e.preventDefault();
   
}

function convertfieldstrtobrowserstrdatetime(fieldstrdatetime)
{
	var temparr = fieldstrdatetime.split(" ");
	var temparr1 = temparr[0].split("-");
	var month="";
	switch(temparr1[1])
	{
	case "Jan" : 	month="01"; break;
	case "Feb" : 	month="02"; break;
	case "Mar" : 	month="03"; break;
	case "Apr" : 	month="04"; break;
	case "May" : 	month="05"; break;
	case "Jun" : 	month="06"; break;
	case "Jul" : 	month="07"; break;
	case "Aug" : 	month="08"; break;
	case "Sep" : 	month="09"; break;
	case "Oct" : 	month="10"; break;
	case "Nov" : 	month="11"; break;
	case "Dec" : 	month="12"; break;
	}
	var browserdattimestr= temparr1[2]+"-"+month+"-"+temparr1[0]+" "+temparr[1];
	return browserdattimestr;
}