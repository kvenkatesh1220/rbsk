/**
 *  BY KV
 */
$(document).ready(function()
{ 
	 
});

  
$('#bulkupload').on('click', function (e) {
	if($('#uploadfilename').val()!=="" && $('#isexcel').val()==='YES' ) {   
	 // Birth Form submit started here
	$("#bulkupload").attr('disabled',true);
	var addform = document.getElementById('bulkuploadform');
	var formdata = new FormData(addform);
	  // ajax call started here
	  $.ajax({
	   url: "/BulkUpload",
	   type: 'POST',
	   data: formdata,
       contentType: false,
	   processData: false,
	   success: function (data) { 
    		if(data.indexOf('Success')!== -1){
    			$("#bulkuploadform").trigger('reset');
    			$('#bulkupload').removeAttr('disabled');
 			   swal({
                title: "",
                text: "File  Uploaded successfully ."
            });
    		}
    		if(data.indexOf('Error') !== -1)
    			{
    			toastr.error(data);
	    		}
	   }, // success ended here
	   error: function(request, status, message) { // error started here
		    console.log("error occourred in BulkUpload servlet call :");
		    console.log(message);
		    toastr.error('Error - Unable to Connect Keansa Solutions Server');
		    $('#bulkupload').removeAttr('disabled');
		   } // error ended here
		  });
	}
	else{
		if($('#uploadfilename').val()==="")
		toastr.error('Error - Please select the file.'); 
		if($('#isexcel').val()==='NO' )
			toastr.error('Error - Please upload valid excel file .xlsx, .xlsm, .xls only.');  
	}
	});
  
	
$('#uploadfilename').on('change', function () {

	var file=$('#uploadfilename').val();
	
	if (!(/\.(xlsx|xls|xlsm)$/i).test(file)) {
		$('#isexcel').val('NO');
		 toastr.error('Please upload valid excel file .xlsx, .xlsm, .xls only.');
		$(file).val('');
		}
		else{
			$('#isexcel').val('YES');
		}

	
	});
	    


