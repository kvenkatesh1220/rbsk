/**
 * 
 */
$(document).ready(function(){
// date picker started here
$("#reportingyear").datepicker({
       	format: "yyyy-mm" ,
       	todayBtn: "linked" ,
       	endDate: '0',
});
$('#reportingyear').on('change', function () {
    $('.datepicker').hide();
});
//date picker ended here

$('#reportsdiv').hide();
$('#formdiv').show();

//form validate started here
$("#birthreport").validate({ //Member Addition Form validation started here       

	rules: {
		reportingyear: {
            required: true,
        },
		
    },
    
    submitHandler: function(form) {
    	$('#formdiv').hide();
    	$('#reportsdiv').show();
    	
    	// calling GetGenericdata servlet started here
    	 $.ajax({
    		   url: "/GetGenericdataServlet",
    		   type: 'POST',
    		   data:{reportingyear:$('#reportingyear').val()},
    		   success: function (data) {
    			   var i=0;
    			   $.each(data, function(key,value) {
    				   console.log("value ::"+value);
    				   if(i==0)
    				   {
    				    $('#reportmonthtd').empty();
    				    $('#reportmonthtd').append(value);
    				   }
    				   if(i==1)
    				   {
    				    $('#reportyeartd').empty();
    				    $('#reportyeartd').append(value);
    				   }
    				   if(i==2)
    				   {
    				    $('#noofbirthstd').empty();
    				    $('#noofbirthstd').append(value);
    				   }
    				   if(i==3)
    				   {
    				    $('#noofdefectstd').empty();
    				    $('#noofdefectstd').append(value);
    				   }
    				   i=i+1;
    				   
    			   });
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetGenericdata servlet ended here
    	
    	 // calling GetDefectsReport servlet started here
    	 $.ajax({
    		   url: "/GetDefectsReportServlet",
    		   type: 'POST',
    		   data:{reportingyear:$('#reportingyear').val()},
    		   success: function (data) {
    			   $('#defectlist'). empty();
    			   var i=0;
    			   var noofrec=0;
    			   $.each(data, function(key,value) {
    				   $('#defectlist').append("<tr><td>"+value.defectname+"</td><td>"+value.count+"</td></tr>");
    				   noofrec=noofrec+1;
    				   i=i+1;
    			   });
    			   if(noofrec==0)
    			   {
    			      
    				   $('#defectlist').append("<tr><td>No Records Found</td><td></td></tr>");
    			   }
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetDefectsReport servlet ended here
    	// calling GetAgeWiseReport servlet started here
    	 $.ajax({
    		   url: "/GetAgeWiseReportServlet",
    		   type: 'POST',
    		   data:{reportingyear:$('#reportingyear').val()},
    		   success: function (data) {
    			   $('#agewiselist'). empty();
    			   var i=0;
    			   var noofrec=0;
    			   $.each(data, function(key,value) {
    				   $('#agewiselist').append("<tr><td>"+value.age+"</td><td>"+value.count+"</td></tr>");
    				   noofrec=noofrec+1;
    				   i=i+1;
    			   });
    			   if(noofrec==0)
    			   {
    			      
    				   $('#agewiselist').append("<tr><td>No Records Found</td><td></td></tr>");
    			   }
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetAgeWiseReport servlet ended here 
    	 
    	// calling GetSourceWiseReport servlet started here
    	 $.ajax({
    		   url: "/GetSourceWiseReportServlet",
    		   type: 'POST',
    		   data:{reportingyear:$('#reportingyear').val()},
    		   success: function (data) {
    			   $('#sourcewiselist'). empty();
    			   var i=0;
    			   var noofrec=0;
    			   $.each(data, function(key,value) {
    				   $('#sourcewiselist').append("<tr><td>"+value.source+"</td><td>"+value.count+"</td></tr>");
    				   noofrec=noofrec+1;
    				   i=i+1;
    			   });
    			   if(noofrec==0)
    			   {
    			      
    				   $('#sourcewiselist').append("<tr><td>No Records Found</td><td></td></tr>");
    			   }
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetSourceWiseReport servlet ended here 
    }
	
	
 });// form validate ended here

});

