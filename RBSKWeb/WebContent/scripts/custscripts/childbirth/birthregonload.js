/**
 *  
 */
$(document).ready(function(){
	
	  
	  hideerrmsgsonload(); 

	 //loadsource method is to load source list from db started here.
	  

	  
	 //loadsource method is to load source list from db ended here.
	 $('#sourcediv').on('click','.mysource', function(event) { 
			//$("#otherstextbox").show(); 
		 	  $("#otherstextbox").hide();
		});
	 $('#typeofdeliverydiv').on('click', function(event) { 
			//$("#otherstextbox").show(); 
		 	  $("#typeofdeliverrmsg").hide(); 
		 	  $("#typeofdeliverrmsgcesa").hide(); 
		 	 $('[href=#step2]').removeClass('btn-danger');  // removing default blue of step1 tab
			$('[href=#step2]').addClass('btn-info');   // adding red color to step1 tab 
		});
	 $('#typeofdeliverydiv').on('click', function(event) { 
		 
		 if($("input[name='typeofdelivery']:checked").val()==='Cesareandelivery')
			{
			 $("#resonforcesarean").show();  
			}
	 });
	 $('.deltype').on('click', function(event) { 
		  $(".typeofdelive").hide(); 
		  $(".typeofdeliveothr").hide(); 
		   $("#typeofdeliverytxtbox").hide(); 
		   $('input[name=typeofdeliveryassisted]').prop('checked',false) ; 
		   $('#typeofdeliverytxtbox').val(''); 
	}); 
$('.typeofdelive').on('click', function(event) {  
	 $("#typeofdeliverytxtbox").hide(); 
	   $('input[name=typeofdeliveryother][value=typeofdeliveryother]').prop('checked',false) ;
	   $('#typeofdeliverytxtbox').val(''); 
	  
}); 

	 $('#sourcediv').on('click','#sourceOthers', function(event) { 
			//$("#otherstextbox").show();   
             $("#otherstextbox").show();			
		});
	 
	 // hiding all custom error message div's on page load started here
	 function hideerrmsgsonload()
	 {
		 $('#disterrmsg').hide();  // hiding district of reporting location options in step1 error message.
		 $('#sourceerrmsg').hide(); // hiding source of reporting location options in step1 error message.
		 $('#clusterrmsg').hide(); // hiding cluster of reporting location options in step1 error message.
		 $('#rptdterrmsg').hide(); // hiding report date of reporting location options in step1 error message.
		 $('#doberrmsg').hide(); // hiding report date of reporting location options in step1 error message.
		 $('#adharerrmsg').hide(); // hiding report date of reporting location options in step1 error message.
		 $('#noofbabiesc').hide(); // hiding noofbabiescount text field
		 $('#noofbabieserrmsg').hide();
		 $('#sexnonerrmsg').hide();
		 $('#antenataldetailsdiv').hide();
		 $('#residentialdiv').hide();
		 $('#birthweightingrmserrmsg').hide();
		 $('#gestationalageinweekserrmsg').hide();
		 $('#adharerrmsg').hide();
		 $('#motherageerrmsg').hide();
		 $('#fatherageerrmsg').hide();
		 $('#mobileerrmsg').hide();
		 $('#headcircumferenceerrmsg').hide();
		 $('#typeofdeliverytxtbox').hide();
		 $('.typeofdelive').hide(); 
		 $('#typeofdeliverrmsg').hide(); 
		  $('#typeofdeliverrmsgcesa').hide(); 
		 $(".typeofdeliveothr").hide(); 
		  $("#resonforcesarean").hide(); 
	 }
	// hiding all custom error message div's on page load ended here
	 
	 $('#typeofdeliveryassisted').click(function(){
		  $('input[name=typeofdelivery]').prop('checked',false) ; 
		  $('input[name=typeofdeliveryother]').prop('checked',false) ; 
		  $('#typeofdeliverytxtbox').hide(); 
		  $('#typeofdeliverytxtbox').val(''); 
		  $('.typeofdelive').show();  
		  $(".typeofdeliveothr").show(); 
	 });
	 
	 $('#typeofdeliveryother').click(function(){
		  $('input[name=typeofdelivery]').prop('checked',false) ; 
		  $('input[name=typeofdeliveryassisted]').prop('checked',false) ;  
		  $('#typeofdeliverytxtbox').show();  
	 });
	 

    // hiding district of reporting location options in step1 error message on option click started here	 
	$('.rptdist').change(function(){
		 $('#disterrmsg').hide();  // hiding district of reporting location options in step1 error message.
		 $('[href=#step1]').removeClass('btn-danger'); // removing red color of step1 tab
		 $('[href=#step1]').addClass('btn-info');  // adding blue color to step1 tab.
	});
	// hiding district of reporting location options in step1 error message on option click ended here
	
	// hiding source of reporting location options in step1 error message on option click started here
	$('#sourcediv').on('change','.mysource', function(event) { 
		$('#sourceerrmsg').hide();
		 $('[href=#step1]').removeClass('btn-danger'); // removing red color of step1 tab
		 $('[href=#step1]').addClass('btn-info');  // adding blue color to step1 tab.
	});
	$('#sourcediv').on('change','.mysourceo', function(event) { 
		$('#sourceerrmsg').hide();
		 $('[href=#step1]').removeClass('btn-danger'); // removing red color of step1 tab
		 $('[href=#step1]').addClass('btn-info');  // adding blue color to step1 tab.
	});
	// hiding source of reporting location options in step1 error message on option click ended here	 
	// hiding cluster of reporting location options in step1 error message on option click started here	 
	$('#cluster').change(function(){
		 $('#clusterrmsg').hide();  // hiding cluster of reporting location options in step1 error message.
		 $('[href=#step1]').removeClass('btn-danger'); // removing red color of step1 tab
		 $('[href=#step1]').addClass('btn-info');  // adding blue color to step1 tab.
	});
	// hiding cluster of reporting location options in step1 error message on option click ended here
	// hiding report date of reporting location options in step1 error message on option click started here	 
	$('#reportingdate').change(function(){
		 $('#rptdterrmsg').hide();  // hiding cluster of reporting location options in step1 error message.
		 $('[href=#step1]').removeClass('btn-danger'); // removing red color of step1 tab
		 $('[href=#step1]').addClass('btn-info');  // adding blue color to step1 tab.
	});
	// hiding report date of reporting location options in step1 error message on option click ended here
	// hiding date of birth of step2 error message on option change started here	 
	$('#dateofbirth').change(function(){
		 $('#doberrmsg').hide();  // hiding cluster of reporting location options in step1 error message.
		 $('[href=#step2]').removeClass('btn-danger'); // removing red color of step1 tab
		 $('[href=#step2]').addClass('btn-info');  // adding blue color to step1 tab.
	});
	// hiding date of birth of step2 error message on option change ended here
	// hiding ahdar card of step2 error message on option change started here	 
	$('#adharno').change(function(){
		 $('#adharerrmsg').hide();  // hiding cluster of reporting location options in step1 error message.
		 $('[href=#step2]').removeClass('btn-danger'); // removing red color of step1 tab
		 $('[href=#step2]').addClass('btn-info');  // adding blue color to step1 tab.
	});
	// hiding ahdar card of step2 error message on option change ended here
	
	// hiding sex none of delivery outcome details in step2 error message on option click started here	 
	$('.sexclass').change(function(){
		 $('#sexnonerrmsg').hide();  // hiding sex of delivery outcome details options in step2 error message.
		 $('[href=#step2]').removeClass('btn-danger'); // removing red color of step2 tab
		 $('[href=#step2]').addClass('btn-info');  // adding blue color to step2 tab.
	});
	// hiding sex none of delivery outcome details in step2 error message on option click ended here
	
	// hiding sex none of delivery outcome details in step2 error message on option click started here	 
	$('#birthweightingrms').change(function(){
		 $('#birthweightingrmserrmsg').hide();  // hiding sex of delivery outcome details options in step2 error message.
		 $('[href=#step2]').removeClass('btn-danger'); // removing red color of step2 tab
		 $('[href=#step2]').addClass('btn-info');  // adding blue color to step2 tab.
	});
	// hiding sex none of delivery outcome details in step2 error message on option click ended here
	
	/*****Added by Ravindar ******/

	
	 $('#mothersname').change(function(){
		$('[href=#step2]').removeClass('btn-danger');  
		$('[href=#step2]').addClass('btn-info');   
	 
   });
	 $('#fathername').change(function(){
		$('[href=#step2]').removeClass('btn-danger');  
		$('[href=#step2]').addClass('btn-info');   
		 
   });
	$('#bhouseno').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#bstreetname').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#bareaname').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#bpostoffice').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#bthouseno').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#btstreetname').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#btareaname').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#btpostoffice').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#btdistrict').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#btstate').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#birthblock').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#birthminicipality').change(function(){
		$('[href=#step3]').removeClass('btn-danger');  
		$('[href=#step3]').addClass('btn-info');   
		 
   });
	$('#description').change(function(){
		$('[href=#step6]').removeClass('btn-danger');  
		$('[href=#step6]').addClass('btn-info');   
		 
   });
	$('#ageat').change(function(){
		$('[href=#step6]').removeClass('btn-danger');  
		$('[href=#step6]').addClass('btn-info');   
		 
   });
	$('#code').change(function(){
		$('[href=#step6]').removeClass('btn-danger');  
		$('[href=#step6]').addClass('btn-info');   
		 
   });
	$('#karotypefindings').change(function(){
		$('[href=#step7]').removeClass('btn-danger');  
		$('[href=#step7]').addClass('btn-info');   
		 
   });
	$('#berafindings').change(function(){
		$('[href=#step7]').removeClass('btn-danger');  
		$('[href=#step7]').addClass('btn-info');   
		 
   });
	$('#bloodtestfindings').change(function(){
		$('[href=#step7]').removeClass('btn-danger');  
		$('[href=#step7]').addClass('btn-info');   
		 
   });
	$('#anomaly').change(function(){
		$('[href=#step8]').removeClass('btn-danger');  
		$('[href=#step8]').addClass('btn-info');   
		 
   });
	$('#syndrome').change(function(){
		$('[href=#step8]').removeClass('btn-danger');  
		$('[href=#step8]').addClass('btn-info');   
		 
   });
	$('#provisional').change(function(){
		$('[href=#step8]').removeClass('btn-danger');  
		$('[href=#step8]').addClass('btn-info');   
		 
   });
	$('#complediagnosis').change(function(){
		$('[href=#step8]').removeClass('btn-danger');  
		$('[href=#step8]').addClass('btn-info');   
		 
   });
	$('#notifyingperson').change(function(){
		$('[href=#step8]').removeClass('btn-danger');  
		$('[href=#step8]').addClass('btn-info');   
		 
   });
	$('#designationofcontact').change(function(){
		$('[href=#step8]').removeClass('btn-danger');  
		$('[href=#step8]').addClass('btn-info');   
		 
   });
	$('#facilityreferred').change(function(){
		$('[href=#step8]').removeClass('btn-danger');  
		$('[href=#step8]').addClass('btn-info');   
		 
   });
	
	/*****End*****/
	
	
	
	
});//end of document.ready function
$('#birthweightingrms').keyup(function()
{   
	
			if(CheckDecimal($('#birthweightingrms').val()))
			{
				$('#birthweightingrmserrmsg').hide();
			}
			else{
				$('#birthweightingrmserrmsg').show();	
			}
});

$('#gestationalageinweeks').keyup(function()
{   
			
					if(CheckDecimal($('#gestationalageinweeks').val()))
					{
						$('#gestationalageinweekserrmsg').hide();
					}
					else{
						$('#gestationalageinweekserrmsg').show();	
					}
});

$('#adharno').keyup(function()
{   
					
							if(CheckNumber($('#adharno').val()) && $('#adharno').val().length == 12)
							{
								$('#adharerrmsg').hide();
							}
							else{
								$('#adharerrmsg').show();	
							}
});

$('#mothersage').keyup(function()
{   
							if(CheckNumber($('#mothersage').val()) && $('#mothersage').val().length == 2)
							{
							$('#motherageerrmsg').hide();
							}
							else{
							$('#motherageerrmsg').show();	
							}
});

$('#fathersage').keyup(function()
{   
									if(CheckNumber($('#fathersage').val()) && $('#fathersage').val().length == 2)
									{
									$('#fatherageerrmsg').hide();
									}
									else{
									$('#fatherageerrmsg').show();	
									}
});
$('#mobileno').keyup(function()
		{   
											if(CheckNumber($('#mobileno').val()) && $('#mobileno').val().length == 10)
											{
											$('#mobileerrmsg').hide();
											}
											else{
											$('#mobileerrmsg').show();	
											}
		});
$('#antenataldetailsdiv').on('keyup','#headcircumference', function(event)
		{   
					
							if(CheckDecimal($('#headcircumference').val()))
							{
								$('#headcircumferenceerrmsg').hide();
							}
							else{
								$('#headcircumferenceerrmsg').show();	
							}
		});
function CheckDecimal(inputtxt)   
{   
var decimal=  /((\d+)((\.\d{1,2})?))$/;   
if(decimal.test(inputtxt))   
{   
return true;  
}  
else  
{   
return false;  
}  
} 

function CheckNumber(inputtxt)   
{   
var decimal=  /^\d+$/;   
if(decimal.test(inputtxt))   
{   
return true;  
}  
else  
{   
return false;  
}  
}
	 
