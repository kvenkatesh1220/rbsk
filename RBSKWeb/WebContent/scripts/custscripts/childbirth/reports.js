/**
 * By KV 
 */
var usertype="";
var user_id="";
var district="";
var cluster="";
var hospitalname="";
var hospitaltype=""; 

$(document).ready(function(e){
	
	GetUserMappings(e);
	// date picker started here
	$("#datefrom").datepicker({
	       	format: "dd-M-yyyy" ,
	       	todayBtn: "linked" ,
	        	endDate: '0',
	});
	$('#datefrom').on('change', function () {
	    $('.datepicker').hide();
	});
	
	$("#dateto").datepicker({
       	format: "dd-M-yyyy" ,
       	todayBtn: "linked" ,
        	endDate: '0',
});
	$('#dateto').on('change', function () {
    $('.datepicker').hide();
	});

	//date picker ended here
	
});
function GetUserMappings(e)  
{
	 // getting values for Source drop down
	 $.ajax({
		 	url: "GetUserMappingsServlet", 
			type: 'POST',
			// data: {tablename : 'tg_source' },
			success: function (data) { // success started here
				
				
				 $.each(data, function(key,value) {
					 user_id=value['user_id'];
					 district=value['district'];
					 cluster=value['cluster'];
					 hospitalname=value['hospitalname'];
					 hospitaltype=value['hospitaltype']; 
					 usertype=value['usertype'];
						
				 }); // loop ended here
				 /*jQuery("input[value='"+district+"']").attr('checked', true);
				 $('#cluster').val(cluster);
				 //$("#cluster").attr("disabled", "disabled");
				 $('#cluster').attr('readonly', true);
				 $('#birthblock').val(cluster);
				 $('#hospitalname').val(hospitalname);*/
				loadDistricts(e);
				loadDefectCategory(e);
					
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				
			    console.log("error occourred in GetUserMappingsServlet servlet call :");
			    console.log(message);
			    
			   } //error ended here
		});// ajax call ended here
	 
}


function loadDistricts(e)
{
	if(usertype=='ADMIN'&& usertype.trim()!==""){
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "LoadDistricts", 
			type: 'POST',
			// data: {tablename : 'tg_source' },
			success: function (data) { // success started here
				$('#districts').empty(); // empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select District</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				 //console.log(listtoappendstr);
				 $('#districts').append(listtoappendstr); // append  districts 
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadDistricts servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	}
	else
	{
		$('#districts').empty();
		$('#districts').append("<option value=\""+district+"\">"+district+"</option>");
		$('#districts').attr("disabled", true); 
		if(usertype=='USER'){ 
		$('#hospitals').empty(); 
		$('#hospitals').append("<option value=\""+hospitalname+"\">"+hospitalname+"</option>"); 
		$('#hospitals').attr("disabled", true);  
	}
		if(hospitaltype=="Pvt. Hospital" ) 
			{
			$('#sources').empty();
			$('#sources').append("<option value=\"\">Select Source</option>" +
					" <option value=\"Private Medical College\">Private Medical College</option>" +
					" 	<option value=\"Private Nursing Home/Hospital/ tertiary centre\">Private Nursing Home/Hospital/ tertiary centre</option>"); 
			
			}
		loadClusters(district,e);
	}
}



function loadClusters(district,e)
{
	if(usertype=='ADMIN' || usertype=='SUPERUSER' && usertype.trim()!==""){  
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "LoadClusters", 
			type: 'POST',
			data: {district : district },
			success: function (data) { // success started here
				$('#clusters').empty(); // empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select Cluster</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				 //console.log(listtoappendstr);
				 $('#clusters').append(listtoappendstr); // append  districts 
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadClusters servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	}
	else
	{
		$('#clusters').empty();
		$('#clusters').append("<option value=\""+cluster+"\">"+cluster+"</option>");
		$('#clusters').attr("disabled", true);  
		// loadSource(district,cluster,e); 
	}
}
/*
function  loadSource(district,cluster,e)  // as per changes it is configured as static. this function is not using.
{ 	
	// getting values for Districts drop down
	 $.ajax({
		 	url: "LoadSource", 
			type: 'POST',
			data: {district : district ,
				cluster : cluster},
			success: function (data) { // success started here
				$('#sources').empty(); // empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select Source</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				 //console.log(listtoappendstr);
				 $('#sources').append(listtoappendstr); // append  districts 
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadSource servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
}
*/
function  loadHospitals(district,cluster,e)
{
	if(usertype=='ADMIN' || usertype=='SUPERUSER' && usertype.trim()!==""){
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "LoadHospitals", 
			type: 'POST',
			data: {district : district ,
				cluster : cluster},
			success: function (data) { // success started here
				$('#hospitals').empty(); // empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select Hospital</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				 //console.log(listtoappendstr);
				 $('#hospitals').append(listtoappendstr); // append  districts 
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadHospitals servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	}
	else
	{
		$('#hospitals').empty();
		$('#hospitals').append("<option value=\""+hospitalname+"\">"+hospitalname+"</option>");
		$('#hospitals').attr("disabled", true);  
		 
	}
	
}

function loadDefectCategory(e)
{
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "LoadDefectCategory", 
			type: 'POST',
			// data: {tablename : 'tg_source' },
			success: function (data) { // success started here
				$('#defectcategory').empty(); // empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select Defect Category</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				 //console.log(listtoappendstr);
				 $('#defectcategory').append(listtoappendstr); // append  districts 
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadDefectCategory servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here

}

function loadDefectSubCategory(defectcategory,e)
{
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "LoadDefectsSubCategory", 
			type: 'POST',
			data: {defectcategory : defectcategory },
			success: function (data) { // success started here
				$('#defectsubcategory').empty(); // empty districts before load
				 var listtoappendstr = "";
				 // loop started here
				 var i=1;
				 $.each(data, function(key,value) { 
					 var values=value['value'];
					 var options=value['option'];
					 
					 if(value['option']===undefined || value['option']==null)
					 {
						 options="";    
					 }
					 if(value['value']===undefined || value['value']==null)
					 {
						 values="";    
					 }
					 
					 if(i==1)
						 {
						 listtoappendstr=listtoappendstr+"<option value=\"\">Select Sub Category</option>" ;
						 }
					 listtoappendstr=listtoappendstr+"<option value=\""+values+"\">"+options+"</option>" ;
	 				
					 i=i+1;
				 }); // loop ended here
				 //console.log(listtoappendstr);
				 $('#defectsubcategory').append(listtoappendstr); // append  districts 
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadDefectsSubCategory servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here

	
}

// on change 


$("#districts").change(function(e) {
	$("#clusters").empty();
	$("#clusters").append("<option value=\"\">Select Cluster</option>");

	$("#hospitals").empty(); 
	$("#hospitals").append("<option value=\"\">Select Hospital</option>"); 
	
	if($('#districts :selected').val()!=="")
	{
	 loadClusters($('#districts :selected').val(),e); 
	}
	else
		{
		$("#clusters").empty();
		$("#clusters").append("<option value=\"\">Select Cluster</option>");
		/*$("#sources").empty();
		$("#sources").append("<option value=\"\">Select Source</option>");*/
		$("#hospitals").empty(); 
		$("#hospitals").append("<option value=\"\">Select Hospital</option>"); 
		}
});

/*$("#clusters").change(function(e) {
	 
	if($('#districts :selected').val()!=="" && $('#clusters :selected').val()!=="")
	{
	 loadSource($('#districts :selected').val(),$('#clusters :selected').val(),e); 
	}
	else
		{
		$("#sources").empty();
		$("#sources").append("<option value=\"\">Select Source</option>");
		$("#hospitals").empty(); 
		$("#hospitals").append("<option value=\"\">Select Hospital</option>"); 
		}
});*/

$("#clusters").change(function(e) {
	$("#hospitals").empty(); 
	$("#hospitals").append("<option value=\"\">Select Hospital</option>");
	
	if($('#districts :selected').val()!=="" && $('#clusters :selected').val()!=="") 
	{
	 loadHospitals($('#districts :selected').val(),$('#clusters :selected').val(),e); 
	}
	else
		{
		$("#hospitals").empty(); 
		$("#hospitals").append("<option value=\"\">Select Hospital</option>");
		}
});

$("#defectcategory").change(function(e) {
	 
	if($('#defectcategory :selected').val()!=="")
	{
		loadDefectSubCategory($('#defectcategory :selected').val(),e); 
	}
	else
		{
		$("#defectsubcategory").empty();
		$("#defectsubcategory").append("<option value=\"\">Select Sub-Category</option>");
		}
});
 

// get report started here.

$("#getreport").click(function(e) { 
if(($('#datefrom').val()!=="" && $('#dateto').val()!=="") ||  $('#aadharnum').val()!=="" ) 
	{
	$( "#loctablediv" ).mousemove();
	 // getting values for Districts drop down
	 $.ajax({
		 	url: "GetReportsServlet", 
			type: 'POST',
			data: {
				datefrom : $('#datefrom').val(),
				dateto : $('#dateto').val(),
				districts : $('#districts :selected').val(),
				clusters : $('#clusters :selected').val(),
				sources : $('#sources :selected').val(),
				hospitals : $('#hospitals :selected').val(),
				defectcategory : $('#defectcategory :selected').val(),
				defectsubcategory : $('#defectsubcategory :selected').val(),
				ageofidentification : $('#ageofidentification :selected').val(),
				typeofdelivery : $('#typeofdelivery :selected').val(),
				birthtype : $('#birthtype :selected').val(),
				aadharnum : $('#aadharnum').val() 
				},
			success: function (data) { // success started here 
				$('#reporttable').empty(); // empty districts before load

				var tablemetadataatstart = "<div class=\"table-responsive\"><table class=\"table table-hover table-bordered table-striped\"><tbody>";

			    var tableheading = "<tr>" +
			    "<td><strong> Source</strong></td>"+ 
			    "<td width=\"500%\" ><strong> Defect Category :: Sub-Category</strong></td>"+ 
			   //  "<td><strong> defectsubcategory</strong></td>"+  
			    "<td><strong> cluster</strong></td>"+ 
			    "<td><strong> district</strong></td>"+ 
			    "<td><strong> sex</strong></td>"+ 
			    "<td><strong> modeofdelivery</strong></td>"+ 
			    "<td><strong> birthdistict</strong></td>"+ 
			    "<td><strong> mctsno</strong></td>"+ 
			    "<td><strong> adharno</strong></td>"+ 
			    "<td><strong> mothersname</strong></td>"+ 
			    "<td><strong> typeofdelivery</strong></td>"+ 
			    "<td><strong> birthtype</strong></td>"+ 
			    "<td><strong> ageofidentification</strong></td>"+ 
			    "<td><strong> hospitalname</strong></td>"+
			    "<td><strong> Action</strong></td>"+
			      "</tr>";
				  var tablemetadataatend = "</tbody></table>"; 
				  
				  var tablerow="";
				  
				 // loop started here
				var i=0;
				 $.each(data, function(key,value) {
					 var bid =convertHtmlEntity(value['bid']);
					 var source =convertHtmlEntity(value['source']);  
					 var defectcategory =convertHtmlEntity(value['defectcategory']); 
					 // var defectsubcategory =value['defectsubcategory']; 
					 var cluster =convertHtmlEntity(value['cluster']); 
					 var district =convertHtmlEntity(value['district']); 
					 var sex =convertHtmlEntity(value['sex']); 
					 var modeofdelivery =convertHtmlEntity(value['modeofdelivery']); 
					 var birthdistict =convertHtmlEntity(value['birthdistict']); 
					 var mctsno =convertHtmlEntity(value['mctsno']); 
					 var adharno =convertHtmlEntity(value['adharno']); 
					 var mothersname =convertHtmlEntity(value['mothersname']); 
					 var typeofdelivery =convertHtmlEntity(value['typeofdelivery']); 
					 var birthtype =convertHtmlEntity(value['birthtype']); 
					 var ageofidentification =convertHtmlEntity(value['ageofidentification']);
					 var hospitalname =convertHtmlEntity(value['hospitalname']); 
					  
					 
					 
					 if(typeof value['defectcategory']==='undefined' ||  value['defectcategory']===null || value['defectcategory'].trim()=="")  {  defectcategory="";  }
					 if(typeof value['bid']==='undefined' || value['bid']===null || value['bid'].trim()=="")  {  bid="";  }
					 if(typeof value['source']==='undefined' || value['source']===null || value['source'].trim()=="")  {  source="";  }
					 if(typeof value['defectsubcategory']==='undefined' || value['defectsubcategory']===null || value['defectsubcategory'].trim()=="")  {  defectsubcategory="";  }
					 if(typeof value['cluster']==='undefined' || value['cluster']===null || value['cluster'].trim()=="")  {  cluster="";  }
					 if(typeof value['district']==='undefined' || value['district']===null || value['district'].trim()=="")  {  district="";  }
					 if(typeof value['sex']==='undefined' || value['sex']===null || value['sex'].trim()=="")  {  sex="";  }
					 if(typeof value['modeofdelivery']==='undefined' || value['modeofdelivery']===null || value['modeofdelivery'].trim()=="")  { modeofdelivery ="";  }
					 if(typeof value['birthdistict']==='undefined' || value['birthdistict']===null || value['birthdistict'].trim()=="")  { birthdistict ="";  }
					 if(typeof value['mctsno']==='undefined' || value['mctsno']===null || value['mctsno'].trim()=="")  {  mctsno="";  }
					 if(typeof value['adharno']==='undefined' || value['adharno']===null || value['adharno'].trim()=="")  {  adharno="";  }
					 if(typeof value['mothersname']==='undefined' || value['mothersname']===null || value['mothersname'].trim()=="")  { mothersname ="";  }
					 if(typeof value['typeofdelivery']==='undefined' || value['typeofdelivery']===null || value['typeofdelivery'].trim()=="")  { typeofdelivery ="";  }
					 if(typeof value['birthtype']==='undefined' || value['birthtype']===null || value['birthtype'].trim()=="")  {  birthtype="";  }
					 if(typeof value['ageofidentification']==='undefined' || value['ageofidentification']===null || value['ageofidentification'].trim()=="")  { ageofidentification ="";  }
					 if(typeof value['hospitalname']==='undefined' || value['hospitalname']===null || value['hospitalname'].trim()=="")  {  hospitalname="";  }
					 
					 tablerow = tablerow + "<tr>" +
					 "<td>"+source+"</td>" + 
					 "<td  width=\"500%\"  >"+defectcategory.replace(",",", <br>")+"</td>" +  
					//  "<td>"+defectsubcategory+"</td>" +  
					 "<td>"+cluster+"</td>" + 
					 "<td>"+district+"</td>" + 
					 "<td>"+sex+"</td>" + 
					 "<td>"+modeofdelivery.replace("_", " ")+"</td>" +
					 "<td>"+birthdistict+"</td>" + 
					 "<td>"+mctsno+"</td>" + 
					 "<td>"+adharno+"</td>" + 
					 "<td>"+mothersname+"</td>" + 
					 "<td>"+typeofdelivery+"</td>" + 
					 "<td>"+birthtype+"</td>" + 
					 "<td>"+ageofidentification+"</td>" + 
					 "<td>"+hospitalname+"</td>" +
					"<td class=\"text-right\">" +
					"<button class=\"btn btn-info btn-xs\" " +
					 // "data-toggle=\"modal\" data-target=\"#myModal\" " + 
					 "onclick=\"updateBirthReport('"+bid+"','"+e+"') \"> Edit</button>" +
					 "<button class=\"btn btn-info btn-xs\" onclick=\"printBirthInfo('"+bid+"','"+e+"') \"> Print Birth Info</button>" + 
					 "</tr>" ;
					 i=i+1;
					 
				 }); // loop ended here
				 console.log(tablemetadataatstart+tableheading+tablerow+tablemetadataatend);
				/* $('#noofrec').empty();
				 $('#noofrec').append("<p> Total Number of Records ::  <b>"+i+"</b></P>");
				 */
				 var tblcaption='<caption><center>Birth Reports</center></caption>';
				 var numberofrecs='<td  colspan="15">Total Number of Records ::  <b>'+i+'</b></td>  ';  
				 
				 if(i!=0)
					 {
				 $('#reporttable').append(tablemetadataatstart+tblcaption+numberofrecs+tableheading+tablerow+tablemetadataatend); // append  districts
					 }
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in GetReportsServlet servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	}
else
	{
	if($('#datefrom').val()==="" || $('#dateto').val()==="")
		{
		toastr.error('Error - From date , To date or Aadhar Number should not empty');
		}
	}
});

function updateBirthReport(bid,e) 
{
	 // 
	 $.ajax({
		 	url: "CheckUpdateLimit", 
			type: 'POST',
			data: { bid : bid ,
				action : 'Update'},
			success: function (data) {
				
				if(data.indexOf('Success!')!== -1){
					document.location.href="/updatebirthreg.jsp?bid="+bid+"";
				}
				if(data.indexOf('Error!')!== -1){
					toastr.error(data); 
				}
				
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in GetReportsServlet servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
	
	 
	
}

function printBirthInfo(bid,e)
{
	document.location.href="/printbirthform.jsp?bid="+bid+"";
}



$('.onlynumber').keydown(function (e){
	allowonlynum(e); 
	 });
 
function allowonlynum(e)
{
		if (e.shiftKey || e.ctrlKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) )) {
		e.preventDefault();
		}
		}
}