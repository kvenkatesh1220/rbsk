/**
 * 
 */


$(document).ready(function()
{

	$("#defectsdiv").hide();
	$("#investigationdiv").hide();
	$("#congenitalanomaliesdiv").hide();
	
	$("#birthform").validate({ //Member Addition Form validation started here       

		rules: {
			
	    },
	    
	    submitHandler: function(form) {
	 // Member Addition Form submit started here
		$("#submitcustfeedback").attr('disabled',true);
		var addform = document.getElementById('birthform');
		var formdata = new FormData(addform);
		  // ajax call started here
		  $.ajax({
		   url: "/BirthManagementServlet",
		   type: 'POST',
		   data: formdata,
	       contentType: false,
		   processData: false,
		   success: function (data) { 
			   $("#birthform").trigger('reset');
			   $('#submitcustfeedback').removeAttr('disabled');
	    		
	    		if(data.indexOf('Success')!== -1){
	    		swal({
	                title: "",
	                text: "Data has been successfully saved."
	            });
	    		}
	    		
	    		if(data.indexOf('Error') !== -1)
	    			{
	    			//toastr.error("");
		    		}
		   
			   
		   }, // success ended here
		   error: function(request, status, message) { // error started here
			    console.log("error occourred in BirthManagement servlet call :");
			    console.log(message);
			    toastr.error('Error - Unable to Connect Health Pinata Server');
			    $('#submitcustfeedback').removeAttr('disabled');
			    
			   } // error ended here
			  });
		  return false; 
	    }
	});

$('#birthdefectchoice').on('change', function(){
	
	if($('#birthdefectchoice').val()=="yes")
		{
		console.log($('#birthdefectchoice').val());
		$("#defectsdiv").show();
		}
	if($('#birthdefectchoice').val()=="no")
	{
	console.log($('#birthdefectchoice').val());
	$("#defectsdiv").hide();
	}
});



//congenitalanomalieschoice started here 
$('#congenitalanomalieschoice').change(function() {

	
	if($('#congenitalanomalieschoice').val()=="yes")
		{
		console.log($('#congenitalanomalieschoice').val());
		$("#congenitalanomaliesdiv").show();
		}
	if($('#congenitalanomalieschoice').val()=="no")
	{
	console.log($('#congenitalanomalieschoice').val());
	$("#congenitalanomaliesdiv").hide();
	}
});

//congenitalanomalieschoice ended here

$('#investigationchoice').on('change', function(){
	
	if($('#investigationchoice').val()=="yes")
		{
		console.log($('#investigationchoice').val());
		$("#investigationdiv").show();
		}
	if($('#investigationchoice').val()=="no")
	{
	console.log($('#investigationchoice').val());
	$("#investigationdiv").hide();
	}
});


//dateofidentification started here
$("#dateofidentification").datepicker({
       	format: "yyyy-mm-dd" ,
       	todayBtn: "linked" ,
       	endDate: '0',
});
$('#dateofidentification').on('change', function () {
    $('.datepicker').hide();
});

// dateofbirth started here
$("#dateofbirth").datepicker({
   	format: "yyyy-mm-dd" ,
   	todayBtn: "linked" ,
   	endDate: '0',
});
$('#dateofbirth').on('change', function () {
$('.datepicker').hide();
});

//lastmensuralperiod  started here 
$("#lastmensuralperiod").datepicker({
   	format: "yyyy-mm-dd" ,
   	todayBtn: "linked" ,
   	endDate: '0',
});
$('#lastmensuralperiod').on('change', function () {
$('.datepicker').hide();
});
});

