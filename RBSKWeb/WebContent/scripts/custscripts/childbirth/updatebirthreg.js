/**
 *  By KV
 */

$(document).ready(function(e){
	getBabyData($('#bid').val(),e);
});

function printfunction()
{
	document.location.href="/printbirthform.jsp?bid="+$('#bid').val()+""; 
}


function getBabyData(bid,e)
{
	 $.ajax({
		 	url: "GetBabyTotalDetails",  
			type: 'POST',
			 data: {bid : $('#bid').val() },
			success: function (data) { // success started here
				var reportattachment1="";
				var photo1="";
				var photo2="";
				var sourcearray=['ASHA','UHC','PHC','CHC','Government_Medical_College','District_Hospital','Private_Medical_College','Private_Nursing_Home/Hospital/tertiary_centre','Mobile_Health_Team','ANM','UPHC','AH'];
				 $.each(data, function(key,value) { 
				
					// ***********************************Step 1-   Location Of Reporting Started Here ***********************************//
					 if(typeof value['state']!=='undefined' && value['state']!==null && value['state']!=="") 
					  {  $('input[name=state][value=' + value['state'] + ']').prop('checked',true) ;
					  } 
					 if(typeof value['district']!=='undefined' && value['district']!==null && value['district']!=="") 
					  {  $('input[name=district][value=' + value['district'] + ']').prop('checked',true) ;
					  } 
					 if(typeof value['source']!=='undefined' && value['source']!==null && value['source']!=="") 
					  {  $('input[name=source][value='+ value['source'] + ']').prop('checked',true) ; 
					  } 
					 if( sourcearray.indexOf(value['source'])=== -1)
						 {
						 $('input[name=source][value=Others]').prop('checked',true) ;
						 $('#otherstextbox').show(); 
						 $('#others').val(value['source']); 
						 }
						 
						
					 
					 
					 if(typeof value['cluster']!=='undefined' && value['cluster']!==null && value['cluster']!=="") 
					  { $('#cluster').val(value['cluster']);
					  } 
					 if(typeof value['reportingdate']!=='undefined' && value['reportingdate']!==null && value['reportingdate']!=="") 
					  { $('#reportingdate').val(value['reportingdate']);
					  } 
						// ***********************************Step 1-   Location Of Reporting Ended Here ***********************************//
					 
						// ***********************************Step 2-   Delivery Outcome Details Started Here ***********************************//
					 
					 if(typeof value['dateofidentification']!=='undefined' && value['dateofidentification']!==null && value['dateofidentification']!=="") 
					  { $('#dateofidentification').val(value['dateofidentification']);
					  }
					 
					 if(typeof value['ageofidentification']!=='undefined' && value['ageofidentification']!==null && value['ageofidentification']!=="") 
					  {  $('input[name=ageofidentification][value=' + value['ageofidentification'] + ']').prop('checked',true) ;
					  } 
					 
					 if(typeof value['hospitalname']!=='undefined' && value['hospitalname']!==null && value['hospitalname']!=="") 
					  { $('#hospitalname').val(value['hospitalname']);
					  }
					
					 if(typeof value['dateofbirth']!=='undefined' && value['dateofbirth']!==null && value['dateofbirth']!=="") 
					  { $('#dateofbirth').val(value['dateofbirth']);
					  }
					 if(typeof value['birthweightingrms']!=='undefined' && value['birthweightingrms']!==null && value['birthweightingrms']!=="") 
					  { $('#birthweightingrms').val(value['birthweightingrms']);
					  }
					 
					 if(typeof value['birthorder']!=='undefined' && value['birthorder']!==null && value['birthorder']!=="") 
					  {  $('input[name=birthorder][value=' + value['birthorder'] + ']').prop('checked',true) ;
					  } 
					 if(typeof value['babydeliveryas']!=='undefined' && value['babydeliveryas']!==null && value['babydeliveryas']!=="") 
					  {  $('input[name=babydeliveryas][value=' + value['babydeliveryas'] + ']').prop('checked',true) ;
					  } 
					 if(typeof value['sex']!=='undefined' && value['sex']!==null && value['sex']!=="") 
					  {  $('input[name=sex][value=' + value['sex'] + ']').prop('checked',true) ;
					  } 
					 if(typeof value['caste']!=='undefined' && value['caste']!==null && value['caste']!=="") 
					  {  $('input[name=caste][value=' + value['caste'] + ']').prop('checked',true) ;
					  } 
					 
					 if(typeof value['mctsno']!=='undefined' && value['mctsno']!==null && value['mctsno']!=="") 
					  { $('#mctsno').val(value['mctsno']);
					  }
					 if(typeof value['adharno']!=='undefined' && value['adharno']!==null && value['adharno']!=="") 
					  { $('#adharno').val(value['adharno']);
					  }
					 if(typeof value['mothersname']!=='undefined' && value['mothersname']!==null && value['mothersname']!=="") 
					  { $('#mothersname').val(value['mothersname']);
					  }
					 if(typeof value['mothersage']!=='undefined' && value['mothersage']!==null && value['mothersage']!=="") 
					  { $('#mothersage').val(value['mothersage']);
					  }
					 if(typeof value['fathername']!=='undefined' && value['fathername']!==null && value['fathername']!=="") 
					  { $('#fathername').val(value['fathername']);
					  }
					 if(typeof value['fathersage']!=='undefined' && value['fathersage']!==null && value['fathersage']!=="") 
					  { $('#fathersage').val(value['fathersage']);
					  }
					 if(typeof value['mobilenumber']!=='undefined' && value['mobilenumber']!==null && value['mobilenumber']!=="") 
					  { $('#mobileno').val(value['mobilenumber']);
					  }
					 
					 if(typeof value['typeofdelivery']!=='undefined' && value['typeofdelivery']!==null && value['typeofdelivery']!=="") 
					  {  $('input[name=typeofdelivery][value=' + value['typeofdelivery'] + ']').prop('checked',true) ;
					  if( value['typeofdelivery']==='ventouse' ||  value['typeofdelivery']==='forceps')
						  {
						  $('.typeofdelive').show();
						  $('.typeofdeliveothr').show(); 
						  $('input[name=typeofdeliveryassisted][value=AssistedDelivery]').prop('checked',true) ;
						  $('input[name=typeofdelivery][value=' + value['typeofdelivery'] + ']').prop('checked',true) ;
						  }
					  else
						  {
						  $('.typeofdelive').show();
						  $('.typeofdeliveothr').show(); 
						  $('input[name=typeofdeliveryother][value=typeofdeliveryother]').prop('checked',true) ;
						  $('input[name=typeofdelivery]').prop('checked',false) ; 
						  $('#typeofdeliverytxtbox').show();
						  $('#typeofdeliverytxtbox').val(value['typeofdelivery']); 
						  }
					  } 
					 if(typeof value['birthtype']!=='undefined' && value['birthtype']!==null && value['birthtype']!=="") 
					  {  $('input[name=birthtype][value=' + value['birthtype'] + ']').prop('checked',true) ;
					  } 
					 // 
					 if(typeof value['resonforcesarean']!=='undefined' && value['resonforcesarean']!==null && value['resonforcesarean']!=="") 
					  {  $('input[name=typeofdelivery][value=Cesareandelivery]').prop('checked',true) ;
					  $('#resonforcesarean').show();
					  $('#resonforcesarean').val(value['resonforcesarean']); 
					  } 
				// ***********************************Step 2-   Delivery Outcome Details Ended Here ***********************************//
					 
					// ***********************************Step 3-   Permanent address Ended Here ***********************************//
					 if(typeof value['bhouseno']!=='undefined' && value['bhouseno']!==null && value['bhouseno']!=="") {  $('#bhouseno').val(value['bhouseno']) ;  }
					 if(typeof value['bstreetname']!=='undefined' && value['bstreetname']!==null && value['bstreetname']!=="") {  $('#bstreetname').val(value['bstreetname']) ;  }
					 if(typeof value['bareaname']!=='undefined' && value['bareaname']!==null && value['bareaname']!=="") {  $('#bareaname').val(value['bareaname']) ;  }
					 if(typeof value['bpostoffice']!=='undefined' && value['bpostoffice']!==null && value['bpostoffice']!=="") {  $('#bpostoffice').val(value['bpostoffice']) ;  }
					  
					 if(typeof value['bthouseno']!=='undefined' && value['bthouseno']!==null && value['bthouseno']!=="") { 
						 $('#bthouseno').val(value['bthouseno']) ;  
						 $('input[name=residentialcheck][value=' + value['residentialNo'] + ']').prop('checked',true) ;
						 $('#residentialdiv').show();
					 }
					 if(typeof value['btstreetname']!=='undefined' && value['btstreetname']!==null && value['btstreetname']!=="") {  
						 $('#btstreetname').val(value['btstreetname']) ; 
						 $('input[name=residentialcheck][value=' + value['residentialNo'] + ']').prop('checked',true) ;
						 $('#residentialdiv').show();
						 }
					 if(typeof value['btareaname']!=='undefined' && value['btareaname']!==null && value['btareaname']!=="") {
						 $('#btareaname').val(value['btareaname']) ; 
						 $('input[name=residentialcheck][value=' + value['residentialNo'] + ']').prop('checked',true) ;
						 $('#residentialdiv').show();
						 }
					 if(typeof value['btpostoffice']!=='undefined' && value['btpostoffice']!==null && value['btpostoffice']!=="") { 
						 $('#btpostoffice').val(value['btpostoffice']) ; 
						 $('input[name=residentialcheck][value=' + value['residentialNo'] + ']').prop('checked',true) ;
						 $('#residentialdiv').show();
						 }
					 if(typeof value['btstate']!=='undefined' && value['btstate']!==null && value['btstate']!=="") {  
						 $('#btstate').val(value['btstate']) ; 
						 $('input[name=residentialcheck][value=' + value['residentialNo'] + ']').prop('checked',true) ;
						 $('#residentialdiv').show();
						 }
					 if(typeof value['btdistrict']!=='undefined' && value['btdistrict']!==null && value['btdistrict']!=="") { 
						 $('#btdistrict').val(value['btdistrict']) ;
						 $('input[name=residentialcheck][value=' + value['residentialNo'] + ']').prop('checked',true) ; 
						 $('#residentialdiv').show();
						 }
					 
					 if(typeof value['bdistrict']!=='undefined' && value['bdistrict']!==null && value['bdistrict']!=="") {  
						 $('input[name=bdistrict][value=' + value['bdistrict'] + ']').prop('checked',true) ;
					 }
					 if(typeof value['bstate']!=='undefined' && value['bstate']!==null && value['bstate']!=="") {  
						 $('input[name=bstate][value=' + value['bstate'] + ']').prop('checked',true) ;
					 }
					 
						// ***********************************Step 3-   Delivery Details Started Here ***********************************//
					 if(typeof value['gestationalageinweeks']!=='undefined' && value['gestationalageinweeks']!==null && value['gestationalageinweeks']!=="") {  $('#gestationalageinweeks').val(value['gestationalageinweeks']) ;  }
					 if(typeof value['lastmensuralperiod']!=='undefined' && value['lastmensuralperiod']!==null && value['lastmensuralperiod']!=="") {  $('#lastmensuralperiod').val(value['lastmensuralperiod']) ;  }
					 if(typeof value['birthblock']!=='undefined' && value['birthblock']!==null && value['birthblock']!=="") {  $('#birthblock').val(value['birthblock']) ;  }
					 if(typeof value['birthminicipality']!=='undefined' && value['birthminicipality']!==null && value['birthminicipality']!=="") {  $('#birthminicipality').val(value['birthminicipality']) ;  }
					 
					 if(typeof value['birthasphyxia']!=='undefined' && value['birthasphyxia']!==null && value['birthasphyxia']!=="") {  
						 $('input[name=birthasphyxia][value=' + value['birthasphyxia'] + ']').prop('checked',true) ;
					 }
					 if(typeof value['autopsyshowbirth']!=='undefined' && value['autopsyshowbirth']!==null && value['autopsyshowbirth']!=="") { 
						 $('input[name=autopsyshowbirth][value=' + value['autopsyshowbirth'] + ']').prop('checked',true) ;
					 }
					 if(typeof value['modeofdelivery']!=='undefined' && value['modeofdelivery']!==null && value['modeofdelivery']!=="") { 
						 $('input[name=modeofdelivery][value=' + value['modeofdelivery'] + ']').prop('checked',true) ;
					 }
					 if(typeof value['statusofinduction']!=='undefined' && value['statusofinduction']!==null && value['statusofinduction']!=="") { 
						 $('input[name=statusofinduction][value=' + value['statusofinduction'] + ']').prop('checked',true) ;
					 }
					 if(typeof value['birthplace']!=='undefined' && value['birthplace']!==null && value['birthplace']!=="") {
						 $('input[name=birthplace][value=' + value['birthplace'] + ']').prop('checked',true) ;
					 }
					 if(typeof value['birthstate']!=='undefined' && value['birthstate']!==null && value['birthstate']!=="") { 
						 $('input[name=birthstate][value=' + value['birthstate'] + ']').prop('checked',true) ;
					 }
					 if(typeof value['birthdistict']!=='undefined' && value['birthdistict']!==null && value['birthdistict']!=="") { 
						 $('input[name=birthdistict][value=' + value['birthdistict'] + ']').prop('checked',true) ;
					 }

						// ***********************************Step 3-   Delivery Details Ended Here ***********************************//
					 // ***********************************Step 3-   Permanent address Ended Here ***********************************//
					 

					// ***********************************Step 4-  Antenatal details Started Here ***********************************//


					 if(typeof value['folicaciddetails']!=='undefined' &&  value['folicaciddetails']!=='No' && value['folicaciddetails']!==null && value['folicaciddetails']!=="") 
					  {  $('input[name=folicaciddetails][value=' + value['folicaciddetails'] + ']').prop('checked',true) ; $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  }  

					 if(typeof value['hoseriousmetirialillness']!=='undefined' &&  value['hoseriousmetirialillness']!=='No'  && value['hoseriousmetirialillness']!==null && value['hoseriousmetirialillness']!=="") 
					  {  $('input[name=hoseriousmetirialillness][value=' + value['hoseriousmetirialillness'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 

					 if(typeof value['horadiationexposure']!=='undefined' &&  value['horadiationexposure']!=='No'  && value['horadiationexposure']!==null && value['horadiationexposure']!=="") 
					  {  $('input[name=horadiationexposure][value=' + value['horadiationexposure'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show(); 
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 
					 

					 if(typeof value['hosubstancceabuse']!=='undefined' &&  value['hosubstancceabuse']!=='No'  && value['hosubstancceabuse']!==null && value['hosubstancceabuse']!=="") 
					  {  $('input[name=hosubstancceabuse][value=' + value['hosubstancceabuse'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show(); 
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 
					  

					 if(typeof value['parentalconsanguinity']!=='undefined' &&  value['parentalconsanguinity']!=='No'  && value['parentalconsanguinity']!==null && value['parentalconsanguinity']!=="") 
					  {  $('input[name=parentalconsanguinity][value=' + value['parentalconsanguinity'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 
					  

					 if(typeof value['assistedconception']!=='undefined' &&  value['assistedconception']!=='No'  && value['assistedconception']!==null && value['assistedconception']!=="") 
					  {  $('input[name=assistedconception][value=' + value['assistedconception'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 

					 if(typeof value['immunisationhistory']!=='undefined' &&  value['immunisationhistory']!=='No'  && value['immunisationhistory']!==null && value['immunisationhistory']!=="") 
					  {  $('input[name=immunisationhistory][value=' + value['immunisationhistory'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show(); 
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 
					 

					 if(typeof value['maternaldrugs']!=='undefined' &&  value['maternaldrugs']!=='No' && value['maternaldrugs']!==null && value['maternaldrugs']!=="") 
					  {  $('input[name=maternaldrugs][value=' + value['maternaldrugs'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 

					 if(typeof value['historyofanomalies']!=='undefined' &&  value['historyofanomalies']!=='No'  && value['historyofanomalies']!==null && value['historyofanomalies']!=="") 
					  {  $('input[name=historyofanomalies][value=' + value['historyofanomalies'] + ']').prop('checked',true)  ; $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  }

					 if(typeof value['noofpreviousabortion']!=='undefined' &&  value['noofpreviousabortion']!=='Nill'  && value['noofpreviousabortion']!==null && value['noofpreviousabortion']!=="") 
					  {  $('input[name=noofpreviousabortion][value=' + value['noofpreviousabortion'] + ']').prop('checked',true)  ;
					  $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 
					  

					 if(typeof value['nofofpreviousstillbirth']!=='undefined' &&  value['nofofpreviousstillbirth']!=='Nill'  && value['nofofpreviousstillbirth']!==null && value['nofofpreviousstillbirth']!=="") 
					  {  $('input[name=nofofpreviousstillbirth][value=' + value['nofofpreviousstillbirth'] + ']').prop('checked',true)  ;
					  $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 

					 if(typeof value['maternaldrugsdesc']!=='undefined' &&  value['maternaldrugsdesc']!=='No'  && value['maternaldrugsdesc']!==null && value['maternaldrugsdesc']!=="") 
					  {  $('input[name=maternaldrugsdesc][value=' + value['maternaldrugsdesc'] + ']').prop('checked',true)  ; 
					  $('#antenataldetailsdiv').show();
					  jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES
					  } 
					 
					 if(typeof value['headcircumference']!=='undefined' && value['headcircumference']!==null && value['headcircumference']!=="")
					 {
					 $('#headcircumference').val(value['headcircumference']);
					   $('#antenataldetailsdiv').show();
					    jQuery("input[value='antYes']").attr('checked', true);  // checking defects YES  
					   }
					// ***********************************Step 4-  Antenatal details Started Here ***********************************//
					 
					 
					// ***********************************Step 5-  Birth Defects Started Here ***********************************//
					 
		if( value['defectcategory']==='Eye Related' ||  value['defectcategory'] ==='Nervous System' || value['defectcategory'] ==='Ear/Face/Neck' ||  value['defectcategory'] ==='Ora Facial' ||  value['defectcategory'] ==='Urinary Tract'  ||  value['defectcategory'] ==='Limb Related' || value['defectcategory'] ==='Abdominal Wall'  || value['defectcategory'] ==='Genital Disorders'  || value['defectcategory'] ==='Chromosomal Disorders'   || value['defectcategory'] ==='Digestive System'   || value['defectcategory'] ==='Other Anomalies' ) 
				{
						 jQuery("input[value='birthdefYes']").attr('checked', true);  // checking defects YES
						 $('#defectsdiv2').show();
						
					 if(typeof value['defectsubcategory']!=='undefined' && value['defectsubcategory']!==null && value['defectsubcategory']!=="" && typeof value['defectcategory']!=='undefined' && value['defectcategory']!==null && value['defectcategory']!==""  )
					 {
						 jQuery("input[value='"+value['defectsubcategory']+"']").attr('checked', true);    
					 }
					
			}
		if( value['defectcategory']==='Birth defects identified through instruments')
			{
			 jQuery("input[value='birthdefinstYes']").attr('checked', true);  // checking defects YES
			 $('#defectthroughinstrumentsdiv').show();
			
			 if(typeof value['defectsubcategory']!=='undefined' && value['defectsubcategory']!==null && value['defectsubcategory']!=="" && typeof value['defectcategory']!=='undefined' && value['defectcategory']!==null && value['defectcategory']!==""  )
			 {
				 jQuery("input[value='"+value['defectsubcategory']+"']").attr('checked', true);    
			 }
			 
			}
		
		if( value['defectcategory']==='Birth defects identified through blood test')
		{
		 jQuery("input[value='birthdefbloodYes']").attr('checked', true);  // checking defects YES
		 $('#defectthroughbloodtestsdiv').show();
		
		 if(typeof value['defectsubcategory']!=='undefined' && value['defectsubcategory']!==null && value['defectsubcategory']!=="" && typeof value['defectcategory']!=='undefined' && value['defectcategory']!==null && value['defectcategory']!==""  )
		 {
			 jQuery("input[value='"+value['defectsubcategory']+"']").attr('checked', true);    
		 }
		 
		}
		// ***********************************Step 5 -  Birth Defects  Ended Here ***********************************//
		
		// ***********************************Step 6 -  Congenital anomalies  Started Here ***********************************//	
		 if(typeof value['name']!=='undefined' && value['name']!==null && value['name']!=="")
			 {
			 jQuery("input[value='"+value['name']+"']").attr('checked', true);  
			 $('#congenitalanomaliesdiv1').show();
			 jQuery("input[value='congeYes']").attr('checked', true);  // checking defects YES
			 }
		
		 if(typeof value['confirmmed']!=='undefined' && value['confirmmed']!==null && value['confirmmed']!=="")
		 {
		//  jQuery("input[value='"+value['confirmmed']+"']").attr('checked', true);  
		  $('input[name=confirmmed][value=' + value['confirmmed'] + ']').prop('checked',true)  ;  
		 $('#congenitalanomaliesdiv1').show();
		 jQuery("input[value='congeYes']").attr('checked', true);  // checking defects YES
		 }
		 
		 if(typeof value['description']!=='undefined' && value['description']!==null && value['description']!=="")
		 {
		$('#description') .val(value['description']);
		$('#congenitalanomaliesdiv1').show();
		 jQuery("input[value='congeYes']").attr('checked', true);  // checking defects YES
		 }
		 if(typeof value['ageat']!=='undefined' && value['ageat']!==null && value['ageat']!=="")
		 {
			 $('#ageat') .val(value['ageat']);
			 $('#congenitalanomaliesdiv1').show();
			 jQuery("input[value='congeYes']").attr('checked', true);  // checking defects YES
		 }
		 if(typeof value['code']!=='undefined' && value['code']!==null && value['code']!=="")
		 {
			 $('#code') .val(value['code']);
			 $('#congenitalanomaliesdiv1').show();
			 jQuery("input[value='congeYes']").attr('checked', true);  // checking defects YES
		 }
			// ***********************************Step 6 -  Congenital anomalies  Ended Here ***********************************//
		 
			// ***********************************Step 7 -  Investigation details  Started Here ***********************************//
		 
		 if(typeof value['karotype']!=='undefined' && value['karotype']!=='Normal' && value['karotype']!==null && value['karotype']!=="") 
		  {  $('input[name=karotype][value=' + value['karotype'] + ']').prop('checked',true)  ; 
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 
		 if(typeof value['karotypefindings']!=='undefined' && value['karotypefindings']!==null && value['karotypefindings']!=="") 
		  {
			 $('#karotypefindings').val(value['karotypefindings']);
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 
		 if(typeof value['bera']!=='undefined' && value['bera']!=='Normal' && value['bera']!==null && value['bera']!=="") 
		  {  $('input[name=bera][value=' + value['bera'] + ']').prop('checked',true)  ; 
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 
		 if(typeof value['berafindings']!=='undefined' && value['berafindings']!==null && value['berafindings']!=="") 
		  {
			 $('#berafindings').val(value['berafindings']);
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 
		 
		 if(typeof value['bloodtestforch']!=='undefined' && value['bloodtestforch']!=='Normal'  && value['bloodtestforch']!==null && value['bloodtestforch']!=="") 
		  {  $('input[name=blodtestforch][value=' + value['bloodtestforch'] + ']').prop('checked',true)  ;  // in jsp input type name is blodtestforch and in db bloodtestforch 'o' missing
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 
		 if(typeof value['bloodtestfindings']!=='undefined' && value['bloodtestfindings']!==null && value['bloodtestfindings']!=="") 
		  {
			 $('#bloodtestfindings').val(value['bloodtestfindings']);
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 
		 if(typeof value['bloodtestforcah']!=='undefined' && value['bloodtestforcah']!=='Normal' && value['bloodtestforcah']!==null && value['bloodtestforcah']!=="") 
		  {  $('input[name=bloodtestforcah][value=' + value['bloodtestforcah'] + ']').prop('checked',true)  ; 
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 
		 if(typeof value['bloodtestforg6pd']!=='undefined'  && value['bloodtestforg6pd']!=='Normal' && value['bloodtestforg6pd']!==null && value['bloodtestforg6pd']!=="") 
		  {  $('input[name=bloodtestforg6pd][value=' + value['bloodtestforg6pd'] + ']').prop('checked',true)  ; 
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES
		  } 
		 if(typeof value['bloodtestforscd']!=='undefined'  && value['bloodtestforscd']!=='Normal'  && value['bloodtestforscd']!==null && value['bloodtestforscd']!=="") 
		  {  $('input[name=bloodtestforscd][value=' + value['bloodtestforscd'] + ']').prop('checked',true)  ; 
		  $('#investigationdiv1').show();
		  jQuery("input[value='invdetYes']").attr('checked', true);  // checking defects YES 
		  }  
		 
			// ***********************************Step 7 -  Investigation details  Ended Here ***********************************//
		
			// ***********************************Step 8 -  Diagnosis details  Started Here ***********************************//
		 if(typeof value['anomaly']!=='undefined' && value['anomaly']!==null && value['anomaly']!=="") 
		  {
			 $('#anomaly').val(value['anomaly']);
		 
		  } 
		 if(typeof value['syndrome']!=='undefined' && value['syndrome']!==null && value['syndrome']!=="") 
		  {
			 $('#syndrome').val(value['syndrome']);
		 
		  } 
		
		 if(typeof value['provisional']!=='undefined' && value['provisional']!==null && value['provisional']!=="") 
		  {
			 $('#provisional').val(value['provisional']);
		 
		  } 
		 if(typeof value['complediagnosis']!=='undefined' && value['complediagnosis']!==null && value['complediagnosis']!=="") 
		  {
			 $('#complediagnosis').val(value['complediagnosis']);
		 
		  } 
		 if(typeof value['notifyingperson']!=='undefined' && value['notifyingperson']!==null && value['notifyingperson']!=="") 
		  {
			 $('#notifyingperson').val(value['notifyingperson']);
		 
		  } 
		 if(typeof value['designationofcontact']!=='undefined' && value['designationofcontact']!==null && value['designationofcontact']!=="") 
		  {
			 $('#designationofcontact').val(value['designationofcontact']);
		 
		  } 
		 if(typeof value['facilityreferred']!=='undefined' && value['facilityreferred']!==null && value['facilityreferred']!=="") 
		  {
			 $('#facilityreferred').val(value['facilityreferred']);
		 
		  } 
		 if(typeof value['provisionaldiagstat']!=='undefined' && value['provisionaldiagstat']!==null && value['provisionaldiagstat']!=="") 
		  {
			  $('input[name=provisionaldiagstat][value=' + value['provisionaldiagstat'] + ']').prop('checked',true)  ; 
		 
		  } 
		 
		 	// ***********************************Step 8 -  Diagnosis details  Ended Here ***********************************//
		
			// ***********************************Step 9 -  Started Here ***********************************//
		 // var bid=$('#bid').val(); 
// 		 $('#photo1id').append("<font color=\"red\">Uploaded -  <a href=\"C:/FileServer/tglogo.jpg\" target=\"_blank\"> Image</a></font>"); 
		 if(typeof value['photo1']!=='undefined' && value['photo1']!==null && value['photo1']!=="" && photo1==="")  
		  {
			
			 photo1=value['photo1'].split("@_@")[0].split('/');
			 var  photo1val=photo1[photo1.length-1]; 
			  $('#babyphoto1_org').val(photo1val); 
			//  birthform.babyphoto1.value=value['photo1'] ;   
			 
			  $('#photo1id').append("<font color=\"red\">Uploaded -   <a href=\""+value['photo1']+"\"  target=\"_blank\"> "+photo1val+" </a></font>"); 
		  } 
		 if(typeof value['photo2']!=='undefined' && value['photo2']!==null && value['photo2']!=="" && photo2==="") 
		  {
			 photo2=value['photo2'].split("@_@")[0].split('/');
			 var  photo2val=photo2[photo2.length-1]; 
			 $('#babyphoto2_org').val(photo2val);  
			//  $('#babyphoto2').val(value['photo2']); 
			 $('#photo2id').append("<font color=\"red\">Uploaded -  <a href=\""+value['photo2']+"\"  target=\"_blank\"> "+photo2val+" </a></font>");
		  } 
		 if(typeof value['reportattachment1']!=='undefined' && value['reportattachment1']!==null && value['reportattachment1']!=="" && reportattachment1==="") 
		  {
			 reportattachment1=value['reportattachment1'].split("@_@")[0].split('/');  
			 var  reportattachment1val=reportattachment1[reportattachment1.length-1];  
			 $('#idproof1_org').val(reportattachment1val); 
			//  $('#idproof1').val(value['reportattachment1']);
			 $('#idproof1divid').append("<font color=\"red\">Uploaded -   <a href=\""+value['reportattachment1']+"\"  target=\"_blank\"> "+reportattachment1val+" </a></font>");
		  }  
		  
			// ***********************************Step 9 -  Ended Here ***********************************//
		
		
		
				 }); // loop ended here
				 
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				console.log("error occourred in LoadDistricts servlet call :");
			    console.log(message);
			   } //error ended here
		});// ajax call ended here
}

$('#babyphoto1').on('change', function(event) {  
	if(typeof $('#babyphoto1').val()!=='undefined' && $('#babyphoto1').val()!=="" && $('#babyphoto1').val()!==null)
		{
	var img=$('#babyphoto1').val().split("\\"); 
	$('#babyphoto1_modify').val(img[img.length-1]);
	// alert($('#babyphoto1_modify').val());
		}
});


$('#babyphoto2').on('change', function(event) { 
	if(typeof $('#babyphoto2').val()!=='undefined' && $('#babyphoto2').val()!=="" && $('#babyphoto2').val()!==null)
	{
	var img=$('#babyphoto2').val().split("\\"); 
	$('#babyphoto2_modify').val(img[img.length-1]);
	}
});

$('#idproof1').on('change', function(event) { 
	if(typeof $('#idproof1').val()!=='undefined' && $('#idproof1').val()!=="" && $('#idproof1').val()!==null)
	{
	var img=$('#idproof1').val().split("\\"); 
	$('#idproof1_modify').val(img[img.length-1]); 
	}
});

