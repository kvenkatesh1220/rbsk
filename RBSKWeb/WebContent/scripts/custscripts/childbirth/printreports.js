/**
 *  BY KV
 */


function printauditreportPDF()
{
	
	var printWindow = window.open('', '', 'height=1500,width=1800');
	 printWindow.document.write('<html>');
	 printWindow.document.write('<body >');
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/fontawesome/css/font-awesome.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/metisMenu/dist/metisMenu.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/animate.css/animate.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/bootstrap/dist/css/bootstrap.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"fonts/pe-icon-7-stroke/css/helper.css\" type=\"text/css\" media=\"print\"/>" );
	 printWindow.document.write( "<link rel=\"stylesheet\" href=\"styles/style.css\" type=\"text/css\" media=\"print\"/>" );
	 var divToPrint = document.getElementById('reporttable'); 
	 printWindow.document.write(divToPrint.innerHTML);  
	 printWindow.document.write('</body></html>');
	 printWindow.document.close();
	 printWindow.print(); 
	
}

function printauditinEXL() 
{
	// window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=reporttable]').html()));
	var filename='BirthReports'+"_"+$('#datefrom').val()+"_"+ $('#dateto').val(); 
	
	if($('#districts :selected').val()!="")
		{
		filename=filename+"_"+$('#districts :selected').val();
		}
	if( $('#clusters :selected').val()!="")
		{
		filename=filename+"_"+$('#clusters :selected').val(); 
		}
	 //getting values of current time for generating the file name
  /*  var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;*/
    //creating a temporary HTML link element (they support setting file names)
    var a = document.createElement('a');
    //getting data from our div that contains the HTML table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('reporttable');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    a.href = data_type + ', ' + table_html;
    //setting the file name
   // a.download = 'exported_table_' + postfix + '.xls';
    a.download = filename + '.xls'; 
    //triggering the function
    a.click();
    //just in case, prevent default behaviour
    if($('input:radio[name=audit]:checked').val()!=='Login_Details')
 	{
	$(".auditcls").show(); 
 	}  
    e.preventDefault();
   
}