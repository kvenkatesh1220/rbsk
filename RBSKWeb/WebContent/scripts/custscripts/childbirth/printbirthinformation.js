$(document).ready(function(e){
	getBabyInfo($('#bid').val(),e); // it should called in onload
});

function getBabyInfo(bid,e) // this function is used in Updatebirthreg.jsp 
{ 
	$.ajax({
		url: "GetBabyTotalDetails",
		type: 'POST',
		//dataType: 'json',
		data: {
			bid : bid
		},
		success: function (data) {
			console.log("success returned from servlet GetBabyTotalDetails.");
			var i=0;
			var totalstr="";
			var hospitalnamewithbr="";
			// var  bid="";
			$.each(data, function(key,value) {
				if(i<1){
					var hospitalname=convertHtmlEntity(value['hospitalname']);
					 bid=convertHtmlEntity(value['bid']);
					var dateofbirth=convertHtmlEntity(value['dateofbirth']);
					var birthplace=convertHtmlEntity(value['birthplace']);
					var fathername=convertHtmlEntity(value['fathername']);
					var fathersage=convertHtmlEntity(value['fathersage']);
					var mothersname=convertHtmlEntity(value['mothersname']);
					var mothersage=convertHtmlEntity(value['mothersage']);
					var bhouseno=convertHtmlEntity(value['bhouseno'])+" ,<br>" ;
					var bstreetname=convertHtmlEntity(value['bstreetname'])+",<br>" ;
					var bareaname=convertHtmlEntity(value['bareaname'])+",<br>" ;
					var bpostoffice=convertHtmlEntity(value['bpostoffice'])+",<br>" ;
					var bdistrict=convertHtmlEntity(value['bdistrict'])+",<br>" ;
					var bstate=convertHtmlEntity(value['bstate'])+"." ;
					var birthweightingrms=convertHtmlEntity(value['birthweightingrms']); 
					var sex=convertHtmlEntity(value['sex']); 
					var adharno=convertHtmlEntity(value['adharno']);
					var address=convertHtmlEntity(value['haddress']);
					var birthat="";
					if(birthplace=='Home')
					{
						birthat=bareaname+"  "+bdistrict+" "+bstate;
					}
				if(birthplace=='Institution')
					{
					birthat=(hospitalname+','+address).replace(',','<br>');
					}
					
				/*var tothspaddress=hospitalname+','+address;
					var hospitalnamewithcoma=tothspaddress.split(',');
					
					var j;
					for (j = 0; j < hospitalnamewithcoma.length; j++) {
					   if(j===0)
					{
						   hospitalnamewithbr=hospitalnamewithbr+'<h1><center>'+tothspaddress.split(',')[j]+'</center></h1>';
					}
					else
					{
						hospitalnamewithbr=hospitalnamewithbr+'<h5><center>'+tothspaddress.split(',')[j]+'</center></h5>';
					}
					   
					   alert(hospitalnamewithbr);
					}*/
					hospitalnamewithbr='<h1><center>'+hospitalname+'</center></h1><br><h5><center>'+address+'</center></h5>';
				if(typeof value['hospitalname']==='undefined' ||  value['hospitalname']===null ||  value['hospitalname']==="")
					{
					hospitalname="-";
					}
				if(typeof value['bid']==='undefined' ||  value['bid']===null ||  value['bid']==="")
				{
					bid="-";
				}
				
				if(typeof value['dateofbirth']==='undefined' ||  value['dateofbirth']===null ||  value['dateofbirth']==="")
				{
					dateofbirth="-";
				}
				
				if(typeof value['birthplace']==='undefined' ||  value['birthplace']===null ||  value['birthplace']==="")
				{
					birthplace="-";
				}
				
				if(typeof value['bdistrict']==='undefined' ||  value['bdistrict']===null ||  value['bdistrict']==="")
				{
					bdistrict="-";
				}
				if(typeof value['bstate']==='undefined' ||  value['bstate']===null ||  value['bstate']==="")
				{
					bstate="-";
				}
				
				// father name 
				if(typeof value['fathersage']==='undefined' ||  value['fathersage']===null ||  value['fathersage']==="")
				{
					fathersage="-";
				}
				if(typeof value['fathername']==='undefined' ||  value['fathername']===null ||  value['fathername']==="")
				{
					fathername="-";
				}
				
				// mother age
				if(typeof value['mothersname']==='undefined' ||  value['mothersname']===null ||  value['mothersname']==="")
				{
					mothersname="-";
				}
				if(typeof value['mothersage']==='undefined' ||  value['mothersage']===null ||  value['mothersage']==="")
				{
					mothersage="-";
				}
				// address
				if(typeof value['bhouseno']==='undefined' ||  value['bhouseno']===null ||  value['bhouseno']==="") 
				{
					bhouseno="-";
				}
				if(typeof value['bstreetname']==='undefined' ||  value['bstreetname']===null ||  value['bstreetname']==="")
				{
					bstreetname="-";
				}
				if(typeof value['bareaname']==='undefined' ||  value['bareaname']===null ||  value['bareaname']==="")
				{
					bareaname="-";
				}
				if(typeof value['bpostoffice']==='undefined' ||  value['bpostoffice']===null ||  value['bpostoffice']==="")
				{
					bpostoffice="-";
				}
				if(typeof value['bdistrict']==='undefined' ||  value['bdistrict']===null ||  value['bdistrict']==="")
				{
					bdistrict="-";
				} // 
				if(typeof value['bstate']==='undefined' ||  value['bstate']===null ||  value['bstate']==="")
				{
					bstate="-";
				} 
				
				// Weight & sex
				if(typeof value['birthweightingrms']==='undefined' ||  value['birthweightingrms']===null ||  value['birthweightingrms']==="")
				{
					birthweightingrms="-";
				}
				if(typeof value['sex']==='undefined' ||  value['sex']===null ||  value['sex']==="")
				{
					sex="-";
				}
				// aadharno
				if(typeof value['adharno']==='undefined' ||  value['adharno']===null ||  value['adharno']==="")
				{
					adharno="-";
				}
				var tglogo=value['logo'];
				totalstr=//"<div class=\"col-lg-12\">"+
			// 	"                <div width=\"1000\">"+
				"                <table style=\"width:80%\" align=\"center\" cellpadding=\"10\">"+
				"                    <tbody>"+
				" 						<th colspan=\"1\"> " ;
				if(typeof value['logo']!=='undefined' && value['logo']!==null && value['logo'].trim()!=="" )
					{
				totalstr=totalstr+	"<img src=\""+tglogo+"\" height=\"100\" width=\"120\" class=\"img-rounded m-b \" alt=\"logo\">" ;
					}
				totalstr=totalstr+"</th>"+
				" 						<th colspan=\"3\">"+hospitalnamewithbr+" </th>"	+
				"                    <tr> <td> </td>"+
				"                            <td colspan=\"3\"><center><font color=\"blue\"><h2> <b>INFORMATION OF BIRTH </b> </h2></font> </center></td>"+
				"                     </tr>"+  
				"                    <tr>"+
				"                            <td  colspan=\"4\"><right>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Birth id : "+bid+"</right></td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr>"+
				"                            <td align=\"center\" width=\"5%\" >1. </td>"+
				"                            <td  width=\"25%\">Date And Time Of Birth</td>"+
				"                            <td align=\"center\" width=\"5%\" >:</td>"+
				"                            <td width=\"25%\">"+dateofbirth+"</td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr >"+
				"                            <td align=\"center\" width=\"5%\">2. </td>"+
				"                            <td  width=\"25%\">Place Of Birth</td>"+
				"                            <td align=\"center\" width=\"5%\">:</td>"+
				"                            <td width=\"25%\">"+birthat+"</td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr>"+
				"                            <td align=\"center\" width=\"5%\">3. </td>"+				
				"                            <td  width=\"25%\">Father Name & Age</td>"+
				"                            <td align=\"center\" width=\"5%\">:</td>"+
				"                            <td width=\"25%\">"+fathername+" & "+fathersage+" </td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr>"+
				"                            <td align=\"center\" width=\"5%\">4. </td>"+				
				"                            <td  width=\"25%\">Mother Name & Age</td>"+
				"                            <td align=\"center\" width=\"5%\">:</td>"+
				"                            <td width=\"25%\">"+mothersname+" & "+mothersage+" </td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr>"+
				"                            <td align=\"center\" width=\"5%\">5. </td>"+				
				"                            <td  width=\"25%\">Address</td>"+
				"                            <td align=\"center\" width=\"5%\">:</td>"+
				"                            <td width=\"25%\">"+bhouseno+bstreetname+bareaname+bpostoffice+bdistrict+bstate+" </td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr>"+
				"                            <td align=\"center\" width=\"5%\">6. </td>"+				
				"                            <td  width=\"25%\">Child Weight(in gms) & Sex</td>"+
				"                            <td align=\"center\" width=\"5%\">:</td>"+
				"                            <td width=\"25%\">"+birthweightingrms+" & "+sex+" </td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr>"+
				"                            <td align=\"center\" width=\"5%\">7. </td>"+				
				"                            <td  width=\"25%\">Mother/Father Aadhar No.</td>"+
				"                            <td align=\"center\" width=\"5%\">:</td>"+
				"                            <td width=\"25%\">"+adharno+" </td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				"                    <tr>"+
				"                            <td colspan=\"4\"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I hereby declare that the details furnished above are true and correct to the best of my knowledge and belief .</td>"+
				"                     </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr>"+
				" 						<tr align=\"center\"><td></td><td> Date : </td> <td></td>  <td>Signature</td> </tr> <tr><td colspan=\"4\"> &nbsp; </td></tr> "	+
				"                    </tbody>"+
				"                </table>";
				//"			</div>";
				
				
				// "</div>" ;
						 $('#formdiv').empty();
			             $('#formdiv').append(totalstr); 
			}
				i=i+1;
				
			});
			
		},
		error: function(request, status, message) {
			console.log("error occourred in FamilyMembersSelectionDropdown servlet call :");
			console.log(message);
			console.log(request.responseText);
			toastr.error('Error - Unable to Connect Health Pinata Server');
		}			
	
});
}

function printsummary() // this function is used in Updatebirthreg.jsp 
{
	$.ajax({
		url: "InsertIntoAudit",
		type: 'POST',
		//dataType: 'json',
		data: {
			bid : $('#bid').val(),
			action : 'BirthReportPrinted'
		},
		success: function (data) {
			console.log("success returned from servlet InsertIntoAudit.");
				if(data.indexOf('Success!')!== -1){
					var printWindow = window.open('', '', 'height=1500,width=1800');
					 printWindow.document.write('<html>');
					 printWindow.document.write('<body >');
					 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/fontawesome/css/font-awesome.css\" type=\"text/css\" media=\"print\"/>" );
					 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/metisMenu/dist/metisMenu.css\" type=\"text/css\" media=\"print\"/>" );
					 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/animate.css/animate.css\" type=\"text/css\" media=\"print\"/>" );
					 printWindow.document.write( "<link rel=\"stylesheet\" href=\"vendor/bootstrap/dist/css/bootstrap.css\" type=\"text/css\" media=\"print\"/>" );
					 printWindow.document.write( "<link rel=\"stylesheet\" href=\"fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css\" type=\"text/css\" media=\"print\"/>" );
					 printWindow.document.write( "<link rel=\"stylesheet\" href=\"fonts/pe-icon-7-stroke/css/helper.css\" type=\"text/css\" media=\"print\"/>" );
					 printWindow.document.write( "<link rel=\"stylesheet\" href=\"styles/style.css\" type=\"text/css\" media=\"print\"/>" );
					 var divToPrint = document.getElementById('formdiv');
					 printWindow.document.write(divToPrint.innerHTML); 
					 printWindow.document.write('</body></html>');
					 printWindow.document.close();
					 printWindow.print(); 
				}
				if(data.indexOf('Error!')!== -1){
					toastr.error(data);
				}
			},
		error: function(request, status, message) {
			console.log("error occourred in FamilyMembersSelectionDropdown servlet call :");
			console.log(message);
			console.log(request.responseText);
			toastr.error('Error - Unable to Connect Health Pinata Server');
		}			
	
});
}
 