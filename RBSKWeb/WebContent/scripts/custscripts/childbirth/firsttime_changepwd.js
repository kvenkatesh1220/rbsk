/**
 * By Venkatesh Konduru 
 */

$("#changepwd").click(function(e){
	
	changepwd(e);
});

function changepwd(e)
{	
		if($('#confnewpwd').val().trim()!=="" && $('#newpwd').val().trim()!=="" && $('#confnewpwd').val().trim()===$('#newpwd').val().trim())
	{
		$.ajax({
       	url : "ChangePassword",
       	type : "POST",
       	data : { 
       		currpwd: $('#currpwd').val() ,
       		newpwd: $('#newpwd').val()
       		
       		    },
       	success : function(data) 
       	{
       		console.log("status from ChangePassword servlet : "+data);
       		if(data.indexOf('Success!') !== -1)
	    	{
       	/*		$('#confnewpwd').val("");
       			$('#newpwd').val("");*/
       			/*swal({
					 title: "",
				     text:data
				 });*/
       			document.location.href="/SignIn"; 
       		}
       		
       		if(data.indexOf('Error!') !== -1)
	    	{
	    	toastr.error(data);
	    	return ;
	    	}
	   	},
    	error: function(request, status, message) {
			console.log("error occourred in ChangePassword servlet call :");
			console.log(message);
			console.log(request.responseText);
			toastr.error('Error - Unable to process your request at this time.');
		}	
       	}); // ajax ended here
	}
	else{
		
		if($('#newpwd').val().trim()==="")
		{
	toastr.error('Error - Please enter New Password.');
	$('#newpwd').focus();
		}
		if($('#confnewpwd').val().trim()==="")
		{
	toastr.error('Error - Please enter Conform Password.');
	$('#currpwd').focus();
		}
		if($('#currpwd').val().trim()!==$('#newpwd').val().trim())
		{
	toastr.error('Error - Conform  Password and New Password should be same .');
		}
	
	}
	
}

//password 1 key up function started here
$('#newpwd').keyup(function()
{
		if($('#newpwd').val()!="")
			{
	$('#result').html(checkStrength($('#newpwd').val()));
			}
	
});
//password 1 key up function ended here

//checkstrength method started here
function checkStrength(password){
	var strength = 0;
	
	//if the password length is less than 6, return message.
	
	if (password.length < 6) {  
		$("#passwdstrength").width("10%");
		$('#passwdstrength').removeClass();
		$("#passwdstrength").addClass(' progress-bar progress-bar-danger');
		return "<font color=\"red\"><b>Too Short</b></font>";
	}
	
	//length is ok, lets continue.
	//if length is 8 characters or more, increase strength value
	if (password.length > 6) { strength += 1; }
	
	//if password contains both lower and uppercase characters, increase strength value
	if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  { strength += 1; }
	
	//if it has numbers and characters, increase strength value
	if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  { strength += 1; }
	
	//if it has one special character, increase strength value
	if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) { strength += 1; }
	
	//if it has two special characters, increase strength value
	if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) { strength += 1; }
	
	//now we have calculated strength value, we can return messages
	
	//if value is less than 2
	if (strength < 2 && strength > 0)
	{
		$("#passwdstrength").width("40%");
		$('#passwdstrength').removeClass();
		$("#passwdstrength").addClass(' progress-bar progress-bar-warning');
		return "<font color=\"orange\"><b>Weak	</b></font>";
	}
	else if (strength < 3 &&  strength >= 2)
	{
		$("#passwdstrength").width("75%");
		$('#passwdstrength').removeClass();
		$("#passwdstrength").addClass(' progress-bar progress-bar-info');
		return "<font color=\"#0099FF\"><b>Strong</b></font>";		
	}
	else if (strength > 3 )
	{
		$("#passwdstrength").width("100%");
		$('#passwdstrength').removeClass();
		$("#passwdstrength").addClass(' progress-bar progress-bar-success');
		return "<font color=\"green\"><b>Very Strong</b></font>";
	}
}
//checkstrength method ended here
