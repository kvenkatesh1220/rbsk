
$(document).ready(function(){
// date picker started here
$("#reportingmonth").datepicker({
       	format: "yyyy-mm" ,
       	todayBtn: "linked" ,
       	endDate: '0',
});
$('#reportingmonth').on('change', function () {
    $('.datepicker').hide();
});

$("#reportingdate").datepicker({
   	format: "yyyy-mm-dd" ,
   	todayBtn: "linked" ,
   	endDate: '0',
});
$('#reportingdate').on('change', function () {
$('.datepicker').hide();
});

$("#reportingdatef").datepicker({
   	format: "yyyy-mm-dd" ,
   	todayBtn: "linked" ,
   	endDate: '0',
});
$('#reportingdatef').on('change', function () {
$('.datepicker').hide();
});

$("#reportingdatet").datepicker({
   	format: "yyyy-mm-dd" ,
   	todayBtn: "linked" ,
   	endDate: '0',
});
$('#reportingdatet').on('change', function () {
$('.datepicker').hide();
});
//date picker ended here

// hiding divs on page load started here
$('#reportsdiv').hide();
$('#formdiv').show();
$('#monthwise').hide();
$('#yearwise').hide();
$('#datewise').hide();
$('#daterange').hide();



//hiding divs on page load ended here

// reporting type date selection started here

$("#rptdurationd").click(function(){  // date selection started here
	$('#monthwise').hide();
	$('#yearwise').hide();
	$('#datewise').show();	
	$('#daterange').hide();
	
}); // date selection ended here

$("#rptdurationm").click(function(){ // month selection started here
	$('#monthwise').show();
	$('#yearwise').hide();
	$('#datewise').hide();
	$('#daterange').hide();
	
}); // month selection ended here

$("#rptdurationy").click(function(){ // year selection started here
	$('#monthwise').hide();
	$('#yearwise').show();
	$('#datewise').hide();	
	$('#daterange').hide();
	
}); // year selection ended here

$("#rptdurationdr").click(function(){ // date range selection started here
	$('#monthwise').hide();
	$('#yearwise').hide();
	$('#datewise').hide();	
	$('#daterange').show();
	
}); // date range selection ended here

$("#rptdurationn").click(function(){ // none report type selection started here
	$('#monthwise').hide();
	$('#yearwise').hide();
	$('#datewise').hide();	
	$('#daterange').hide();
	
}); // none report type selection ended here


// reporting type date selection ended here

//form validate started here
$("#birthreport").validate({ //Member Addition Form validation started here       

	rules: {
		
		
    },
    
    submitHandler: function(form) {
    	
    	
    	if($("input[name='rptduration']:checked").val()=='Date') // if reporting date Date select started here
    	{
    		if($("#reportingdate").val()=== undefined || $("#reportingdate").val()==null || $("#reportingdate").val().trim()=="")
    		{
    		toastr.error("Please select reporting date.");
    		return false;
    		}
    	}// if reporting date Date select ended here
    	
    	if($("input[name='rptduration']:checked").val()=='Month') // if reporting date Month select started here
    	{
    		if($("#reportingmonth").val()=== undefined || $("#reportingmonth").val()==null || $("#reportingmonth").val().trim()=="")
    		{
    		toastr.error("Please select reporting Month-Year.");
    		return false;
    		}
    	}// if reporting date Month select ended here
    	
    	if($("input[name='rptduration']:checked").val()=='Year') // if reporting date year select started here
    	{
    		if($("#reportingyear").val()=== undefined || $("#reportingyear").val()==null || $("#reportingyear").val().trim()=="")
    		{
    		toastr.error("Please select reporting Year.");
    		return false;
    		}
    	}// if reporting date Year select ended here
    	
    	if($("input[name='rptduration']:checked").val()=='') // if no reporting date select started here
    	{
    		toastr.error("No reporting type selected.");
    		return false;
    	}// if no reporting date select ended here    	
    	
    	// if date range selected started here
    	var fromDate = Date.parse($("#reportingdatef").val());
    	var toDate = Date.parse($("#reportingdatet").val());
    	console.log("Is from date greater than to date"+(toDate<fromDate));
    	if($("input[name='rptduration']:checked").val()=='Date Range') 
    	{
    	if(toDate<fromDate)
    	{
    		toastr.error("From date cannot be greater than to date.");	
    		return false;
    	}
    	if($("#reportingdatef").val()=== undefined || $("#reportingdatef").val()==null || $("#reportingdatef").val().trim()=="" )
    	{
    		toastr.error("From date cannot be empty.");	
    		return false;
    	}
    	if($("#reportingdatet").val()=== undefined || $("#reportingdatet").val()==null || $("#reportingdatet").val().trim()=="" )
    	{
    		toastr.error("To date cannot be empty.");	
    		return false;
    	}
    	} // if date range selected ended here
    	$('#formdiv').hide();
    	$('#reportsdiv').show();
    	
    	// calling GetGenericdata servlet started here
    	 $.ajax({
    		   url: "/GetGenericdataServlet",
    		   type: 'POST',
    		   data:$("#birthreport").serializeArray(),
    		   success: function (data) {
    			   var i=0;
    			   var graphlabels = [];
   				   var graphdata = [];
    			   $.each(data, function(key,value) {
    				   console.log("value ::"+value);
    				   if(i==0)
    				   {
    				    $('#reportdatetd').empty();
    				    $('#reportdatetd').append(value);
    				   }
    				   
    				   if(i==1)
    				   {
    				    $('#noofbirthstd').empty();
    				    $('#noofbirthstd').append(value);
    				    var birtsresp = value.split(":");
    				    graphlabels.push(birtsresp[0]);
    				    graphdata.push(birtsresp[1]);
    				   }
    				   if(i==2)
    				   {
    				    $('#noofdefectstd').empty();
    				    $('#noofdefectstd').append(value);
    				    var birtdefectssresp = value.split(":");
    				    graphlabels.push(birtdefectssresp[0]);
    				    graphdata.push(birtdefectssresp[1]);
    				   }
    				   i=i+1;
    				   
    			   });
    			   
    			// chart initialization started here
    				
    				var sharpLineData = {
    			        labels: graphlabels,
    			        datasets: [
    			            {
    			                label: "Example dataset",
    			                fillColor: "rgba(98,203,49,0.5)",
    			                strokeColor: "rgba(98,203,49,0.7)",
    			                pointColor: "rgba(98,203,49,1)",
    			                pointStrokeColor: "#fff",
    			                pointHighlightFill: "#fff",
    			                pointHighlightStroke: "rgba(98,203,49,1)",
    			                data: graphdata
    			            }
    			        ]
    				};

    				var sharpLineOptions = {
    			        scaleShowGridLines : true,
    			        scaleGridLineColor : "rgba(0,0,0,.05)",
    			        scaleGridLineWidth : 1,
    			        bezierCurve : false,
    			        pointDot : true,
    			        pointDotRadius : 4,
    			        pointDotStrokeWidth : 1,
    			        pointHitDetectionRadius : 20,
    			        datasetStroke : true,
    			        datasetStrokeWidth : 1,
    			        datasetFill : true,
    			        responsive: true
    				};

    				var ctx = document.getElementById("sharpLineOptions").getContext("2d");
    				var myNewChart = new Chart(ctx).Line(sharpLineData, sharpLineOptions);
    				myNewChart.render();
    				$("#overallstatsgraph").hide();
    			//chart initialization ended here
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetGenericdata servlet ended here
    	
    	 // calling GetDefectsReport servlet started here
    	 $.ajax({
    		   url: "/GetDefectsReportServlet",
    		   type: 'POST',
    		   data:$("#birthreport").serializeArray(),
    		   success: function (data) {
    			   $('#defectlist'). empty();
    			   var i=0;
    			   var noofrec=0;
    			   var dgraphlabels = [""];
   				   var dgraphdata = [0];
    			   $.each(data, function(key,value) {
    				   $('#defectlist').append("<tr><td>"+value.defectname+"</td><td>"+value.count+"</td></tr>");
    				   dgraphlabels.push(value.defectname);
   				       dgraphdata.push(value.count);
    				   noofrec=noofrec+1;
    				   i=i+1;
    			   });
    			// chart initialization started here
   				
   				var dsharpLineData = {
   			        labels: dgraphlabels,
   			        datasets: [
   			            {
   			                label: "Example dataset",
   			                fillColor: "rgba(98,203,49,0.5)",
   			                strokeColor: "rgba(98,203,49,0.7)",
   			                pointColor: "rgba(98,203,49,1)",
   			                pointStrokeColor: "#fff",
   			                pointHighlightFill: "#fff",
   			                pointHighlightStroke: "rgba(98,203,49,1)",
   			                data: dgraphdata
   			            }
   			        ]
   				};

   				var dsharpLineOptions = {
   			        scaleShowGridLines : true,
   			        scaleGridLineColor : "rgba(0,0,0,.05)",
   			        scaleGridLineWidth : 1,
   			        bezierCurve : false,
   			        pointDot : true,
   			        pointDotRadius : 4,
   			        pointDotStrokeWidth : 1,
   			        pointHitDetectionRadius : 20,
   			        datasetStroke : true,
   			        datasetStrokeWidth : 1,
   			        datasetFill : true,
   			        responsive: true
   				};

   				var dctx = document.getElementById("dsharpLineOptions").getContext("2d");
   				var dmyNewChart = new Chart(dctx).Line(dsharpLineData, dsharpLineOptions);
   				dmyNewChart.render();
   				$("#defectwisegraph").hide();
   			//chart initialization ended here
    			   if(noofrec==0)
    			   {
    			      
    				   $('#defectlist').append("<tr><td>No Records Found</td><td></td></tr>");
    			   }
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetDefectsReport servlet ended here
    	 
    	// calling GetAgeWiseReport servlet started here
    	 $.ajax({
    		   url: "/GetAgeWiseReportServlet",
    		   type: 'POST',
    		   data:$("#birthreport").serializeArray(),
    		   success: function (data) {
    			   $('#agewiselist'). empty();
    			   var i=0;
    			   var noofrec=0;
    			   var agraphlabels = [""];
   				   var agraphdata = [0];
    			   $.each(data, function(key,value) {
    				   $('#agewiselist').append("<tr><td>"+value.age+"</td><td>"+value.count+"</td></tr>");
    				   agraphlabels.push(value.age);
   				       agraphdata.push(value.count);
    				   noofrec=noofrec+1;
    				   i=i+1;
    			   });
    			// chart initialization started here
      				
      				var asharpLineData = {
      			        labels: agraphlabels,
      			        datasets: [
      			            {
      			                label: "Example dataset",
      			                fillColor: "rgba(98,203,49,0.5)",
      			                strokeColor: "rgba(98,203,49,0.7)",
      			                pointColor: "rgba(98,203,49,1)",
      			                pointStrokeColor: "#fff",
      			                pointHighlightFill: "#fff",
      			                pointHighlightStroke: "rgba(98,203,49,1)",
      			                data: agraphdata
      			            }
      			        ]
      				};

      				var asharpLineOptions = {
      			        scaleShowGridLines : true,
      			        scaleGridLineColor : "rgba(0,0,0,.05)",
      			        scaleGridLineWidth : 1,
      			        bezierCurve : false,
      			        pointDot : true,
      			        pointDotRadius : 4,
      			        pointDotStrokeWidth : 1,
      			        pointHitDetectionRadius : 20,
      			        datasetStroke : true,
      			        datasetStrokeWidth : 1,
      			        datasetFill : true,
      			        responsive: true
      				};

      				var actx = document.getElementById("asharpLineOptions").getContext("2d");
      				var amyNewChart = new Chart(actx).Line(asharpLineData, asharpLineOptions);
      				amyNewChart.render();
      				$("#agewisegraph").hide();
      			//chart initialization ended here
    			   if(noofrec==0)
    			   {
    			      
    				   $('#agewiselist').append("<tr><td>No Records Found</td><td></td></tr>");
    			   }
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetAgeWiseReport servlet ended here 
    	 
    	// calling GetSourceWiseReport servlet started here
    	 $.ajax({
    		   url: "/GetSourceWiseReportServlet",
    		   type: 'POST',
    		   data:$("#birthreport").serializeArray(),
    		   success: function (data) {
    			   $('#sourcewiselist'). empty();
    			   var i=0;
    			   var noofrec=0;
    			   var sgraphlabels = [""];
   				   var sgraphdata = [0];
    			   $.each(data, function(key,value) {
    				   $('#sourcewiselist').append("<tr><td>"+value.source+"</td><td>"+value.count+"</td></tr>");
    				   sgraphlabels.push(value.source);
   				       sgraphdata.push(value.count);
    				   noofrec=noofrec+1;
    				   i=i+1;
    			   });
    			// chart initialization started here
     				
     				var ssharpLineData = {
     			        labels: sgraphlabels,
     			        datasets: [
     			            {
     			                label: "Example dataset",
     			                fillColor: "rgba(98,203,49,0.5)",
     			                strokeColor: "rgba(98,203,49,0.7)",
     			                pointColor: "rgba(98,203,49,1)",
     			                pointStrokeColor: "#fff",
     			                pointHighlightFill: "#fff",
     			                pointHighlightStroke: "rgba(98,203,49,1)",
     			                data: sgraphdata
     			            }
     			        ]
     				};

     				var ssharpLineOptions = {
     			        scaleShowGridLines : true,
     			        scaleGridLineColor : "rgba(0,0,0,.05)",
     			        scaleGridLineWidth : 1,
     			        bezierCurve : false,
     			        pointDot : true,
     			        pointDotRadius : 4,
     			        pointDotStrokeWidth : 1,
     			        pointHitDetectionRadius : 20,
     			        datasetStroke : true,
     			        datasetStrokeWidth : 1,
     			        datasetFill : true,
     			        responsive: true
     				};

     				var sctx = document.getElementById("ssharpLineOptions").getContext("2d");
     				var smyNewChart = new Chart(sctx).Line(ssharpLineData, ssharpLineOptions);
     				smyNewChart.render();
     				$("#sourcewisegraph").hide();
     			//chart initialization ended here
    			   if(noofrec==0)
    			   {
    			      
    				   $('#sourcewiselist').append("<tr><td>No Records Found</td><td></td></tr>");
    			   }
    		   }, // success ended here
    		   error: function(request, status, message) { // error started here
    			    console.log("error occourred in AddMembers servlet call :");
    			    console.log(message);
    			    toastr.error('Error - Unable to Connect Health Pinata Server');
    			    
    			   } // error ended here
    			  });
    	// calling GetSourceWiseReport servlet ended here 
    }
	
	
 });// form validate ended here

// overall graph show hide event handler started here
 $("#overallgshow").click(function(){
	$("#overallstatsgraph").show();
 });
 $("#overallhshow").click(function(){
	$("#overallstatsgraph").hide();
 });
 $("#difectgshow").click(function(){
	$("#defectwisegraph").show();
 });
 $("#difectghide").click(function(){
	$("#defectwisegraph").hide();
 });
 $("#agegshow").click(function(){
		$("#agewisegraph").show();
 });
 $("#ageghide").click(function(){
		$("#agewisegraph").hide();
 });
$("#sourcegshow").click(function(){
		$("#sourcewisegraph").show();
 });
 $("#sourceghide").click(function(){
		$("#sourcewisegraph").hide();
 });
//overall graph show hide event handler ended here
});

