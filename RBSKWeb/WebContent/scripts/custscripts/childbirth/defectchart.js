/**
 * 
 */

$(document).ready(function()
{
	$.ajax({
		   url: "/CustomSelectQueryServlet1",
		   type: 'POST',
		   success: function (data) {
			   var i=0;
			   var prev_district="";
			   var prev_cluster="";
			   var prev_source="";
			   var prev_defectcategory="";
			   var prev_defectsubcategory="";
			   var dsi=0;
			   var di=0;
			   var si=0;
			   var ci=0;
			   var disti=0;
			   
			     var districtstr="{ \"name\": \"TS\", \"children\": [";
			      $.each(data, function(key,value) {
				   
				   var state="TS";
				   var district=value.district;
				   var cluster=value.cluster;
				   var source=value.source;
				   var defectcategory=value.defectcategory;
				   var defectsubcategory=value.defectsubcategory;
				   var defectsubcat_size=value.count;
				   
				   if(district!="" && typeof district != undefined)
				   {
					   
					   if(prev_district!=district && disti!=0) // Means first record.
					   {
						   
						   districtstr=districtstr+"]}]}]}]},{ \"name\": \""+district+"\",\"children\": [";
						   disti=disti+1;
						   ci=0;
						   
					   }
					   if(disti==0) // Means first record.
					   {
						   districtstr=districtstr+"{ \"name\": \""+district+"\",\"children\": [";
						   disti=disti+1;
						   ci=0;
					   }
					   if(prev_cluster!=cluster && ci!=0) // Means first record.
					   {
						   districtstr=districtstr+"]}]}]},{ \"name\": \""+cluster+"\",\"children\": [";
						   ci=ci+1;
						   si=0;
					   }
					   if(ci==0) // Means first record.
					   {
						   districtstr=districtstr+"{ \"name\": \""+cluster+"\",\"children\": [";
						   ci=ci+1;
						   si=0;
					   }
					   
					   if(prev_source!=source && si!=0) // Means first record.
					   {
						   districtstr=districtstr+"]}]},{ \"name\": \""+source+"\",\"children\": [";
						   di=0;
						   si=si+1;
					   }
					   if(si==0) // Means first record.
					   {
						   districtstr=districtstr+"{ \"name\": \""+source+"\",\"children\": [";
						   di=0;
						   si=si+1;
					   }
					   if(prev_defectcategory!=defectcategory && di!=0) // Means first record.
					   {
						   
						   districtstr=districtstr+"]},{ \"name\": \""+defectcategory+"\",\"children\": [";
						   dsi=0;
						   di=di+1;
					   }
					   if(di==0) // Means first record.
					   {
						   districtstr=districtstr+"{ \"name\": \""+defectcategory+"\",\"children\": [";
						   di=di+1;
						   dsi=0;
					   }
					   if(prev_defectsubcategory!=defectsubcategory && dsi!=0) // Means first record.
					   {
						   districtstr=districtstr+",{ \"name\": \""+defectsubcategory+"\",\"size\": "+defectsubcat_size+"}";
						   dsi=dsi+1;
					   }
					   if(dsi==0) // Means first record.
					   {
						   districtstr=districtstr+"{ \"name\": \""+defectsubcategory+"\",\"size\": "+defectsubcat_size+"}";
						   dsi=dsi+1;
					   }
					   
				   }				 
				   
				   prev_district=district;
				   prev_cluster=cluster;
				   prev_source=source;
				   prev_defectcategory=defectcategory;
				   prev_defectsubcategory=defectsubcategory;

			   });
			   districtstr=districtstr+"]}]}]}]}]}";
			   console.log("distcitstr = "+districtstr);
			   ///
			 
							
						   
					   
			   
			   var myjson = {
					   "name": "TS",
					   "children": 
					   [
					     
					     {
					         "name": "Adilabad",
					         "children": 
					         [
					         {
					         
					             "name": "MG Road",
					             "children": 
					             [
					             {
					             	  "name": "ASHA",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					             	  ]	
					             },
					             {
					             	  "name": "PHC",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					             	  ]
					             
					              }
					             ]
					          },
					          {
					              "name": "Shanti Nagar",
					              "children": 
					              [
					                 {
					             	  "name": "ASHA",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					             	  ]	
					             },
					             {
					             	  "name": "PHC",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					                 ]
					              }
					              ]
					           }
					           ]
					       },
					       {
					         "name": "Hyderabad",
					         "children": 
					         [
					         {
					         
					             "name": "MG Road",
					             "children": 
					             [
					             {
					             	  "name": "ASHA",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					             	  ]	
					             },
					             {
					             	  "name": "PHC",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					             	  ]
					             
					              }
					             ]
					          },
					          {
					              "name": "Shanti Nagar",
					              "children": 
					              [
					                 {
					             	  "name": "ASHA",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					             	  ]	
					             },
					             {
					             	  "name": "PHC",
					             	  "children":
					             	  [
					             	  {
					             	  	  "name": "Limb Related", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "LimReduction [10]", "size": 10},
					             	  	   	 	{"name": "Others [10]", "size": 10}
					             	  	  ]
					             	  },
					             	  {
					             	  	  "name": "Abdominal Wall", 
					             	  	  "children":
					             	  	  [
					             	  	   	 	{"name": "Gastroschisis [20]", "size": 10},
					             	  	   	 	{"name": "Mcgas [10]", "size": 10}
					             	  	  ]
					             	  }
					                 ]
					              }
					              ]
					           }
					           ]
					       }
					   ]
					   };
					   //
			            console.log("myjson: "+JSON.stringify(myjson));
					   var width = 1160,
					       height = 610,
					       radius = Math.min(width, height) / 2;
					   var x = d3.scale.linear()
					       .range([0, 2 * Math.PI]);
					   var y = d3.scale.linear()
					       .range([0, radius]);
					   var color = d3.scale.category20c();
					   var svg = d3.select("body").append("svg")
					       .attr("width", width)
					       .attr("height", height)
					     .append("g")
					       .attr("transform", "translate(" + width / 2 + "," + (height / 2 + 10) + ")");
					   var partition = d3.layout.partition()
					       .value(function(d) { return d.size; });
					   var arc = d3.svg.arc()
					       .startAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x))); })
					       .endAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); })
					       .innerRadius(function(d) { return Math.max(0, y(d.y)); })
					       .outerRadius(function(d) { return Math.max(0, y(d.y + d.dy)); });
					   //d3.json("/scripts/custscripts/flaret.json", function(error, root) {
					   // root = myjson;
					   root = JSON.parse(districtstr);
					     var g = svg.selectAll("g")
					         .data(partition.nodes(root))
					       .enter().append("g");
					     var path = g.append("path")
					       .attr("d", arc)
					       .style("fill", function(d) { return color((d.children ? d : d.parent).name); })
					       .on("click", click);
					     var text = g.append("text")
					       .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
					       .attr("x", function(d) { return y(d.y); })
					       .attr("dx", "6") // margin
					       .attr("dy", ".35em") // vertical-align
					       .text(function(d) { return d.name; });
					     function click(d) {
					       // fade out all text elements
					       text.transition().attr("opacity", 0);
					       path.transition()
					         .duration(750)
					         .attrTween("d", arcTween(d))
					         .each("end", function(e, i) {
					             // check if the animated element's data e lies within the visible angle span given in d
					             if (e.x >= d.x && e.x < (d.x + d.dx)) {
					               // get a selection of the associated text element
					               var arcText = d3.select(this.parentNode).select("text");
					               // fade in the text element and recalculate positions
					               arcText.transition().duration(750)
					                 .attr("opacity", 1)
					                 .attr("transform", function() { return "rotate(" + computeTextRotation(e) + ")" })
					                 .attr("x", function(d) { return y(d.y); });
					             }
					         });
					     }
					   //});
					   d3.select(self.frameElement).style("height", height + "px");
					   // Interpolate the scales!
					   function arcTween(d) {
					     var xd = d3.interpolate(x.domain(), [d.x, d.x + d.dx]),
					         yd = d3.interpolate(y.domain(), [d.y, 1]),
					         yr = d3.interpolate(y.range(), [d.y ? 20 : 0, radius]);
					     return function(d, i) {
					       return i
					           ? function(t) { return arc(d); }
					           : function(t) { x.domain(xd(t)); y.domain(yd(t)).range(yr(t)); return arc(d); };
					     };
					   }
					   function computeTextRotation(d) {
					     return (x(d.x + d.dx / 2) - Math.PI / 2) / Math.PI * 180;
					   }
			   //
			   
		   },
		   error: function(request, status, message)
		   {  // error started here
			  console.log("error occourred in Defect Chart servlet call :");
			  console.log(message);
			  toastr.error('Error - Unable to Connect Keansa Solutions Server');
		   } // error ended here
		   });
	
	
});