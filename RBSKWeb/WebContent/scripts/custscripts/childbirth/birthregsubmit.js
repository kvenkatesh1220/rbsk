/**
 * 
 */

$(document).ready(function(){
	
	// checking mandatory fields on step 1 next button click started here
	$('#step1next').click(function(){
		
		// checking district is checked or not in reporting tab started here
		if($("input[name='district']:checked").val()===undefined || $("input[name='district']:checked").val()==null)
		{
			$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
			$('#disterrmsg').show(); // show dist error message
		}
		// checking district is checked or not in reporting tab ended here
		
		// checking source is checked or not in reporting tab started here
		if($("input[name='source']:checked").val()===undefined || $("input[name='source']:checked").val()==null)
		{
			$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
			$('#sourceerrmsg').show(); // show dist error message
		}
		// checking source is checked or not in reporting tab ended here
		
		// checking cluster is valid or not in reporting tab ended here
		if($('#cluster').val()===undefined || $('#cluster').val()==null || $('#cluster').val().trim()=="")
		{
			$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
			$('#clusterrmsg').show(); // show cluster error message
		}
		// checking cluster is valid or not in reporting tab ended here
		// checking reporting date is valid or not in reporting tab ended here
		if($('#reportingdate').val()===undefined || $('#reportingdate').val()==null || $('#reportingdate').val().trim()=="")
		{
			$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
			$('#rptdterrmsg').show(); // show reporting date error message
		}
		// checking reporting date is valid or not in reporting tab ended here
		
		
		
    });
	// checking mandatory fields on next button click ended here
	/////
	$('#noofbabiesm').click(function(){
		$('#noofbabiesc').show();
	});
	$('.noofbabopt').click(function(){
		$('#noofbabiesc').hide();
	});
	///
	
	// checking mandatory fields on step 2 next button click started here
	$('#step2next').click(function(){
		// checking date of birth is valid or not in step2 tab started here
		if($('#dateofbirth').val()===undefined || $('#dateofbirth').val()==null || $('#dateofbirth').val().trim()=="")
		{
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
			$('#doberrmsg').show(); // show cluster error message
		}
		// checking date of birth is valid or not in step2 tab ended here
		// checking adhar card is valid or not in step2 tab started here
		if($('#adharno').val()===undefined || $('#adharno').val()==null || $('#adharno').val().trim()=="")
		{
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
			$('#adharerrmsg').show(); // show cluster error message
		}
		// checking adhar card of birth is valid or not in step2 tab ended here
		
		// checking sex  is valid or not in step2 tab started here
		
		if($("input[name='sex']:checked").val()===undefined || $("input[name='sex']:checked").val()==null || $("input[name='sex']:checked").val()=="")
		{
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step2 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step2 tab
			$('#sexnonerrmsg').show(); // show sex error message
		}
		
		// checking sex  of birth is valid or not in step2 tab ended here
		// checking birth weight  is valid or not in step2 tab started here
		
		if($('#birthweightingrms').val()===undefined || $('#birthweightingrms').val()==null || $('#birthweightingrms').val()=="")
		{
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step2 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step2 tab
			$('#birthweightingrmserrmsg').show(); // show sex error message
			
		}
		
		// checking birth weight of birth is valid or not in step2 tab ended here
		// checking source is checked or not in reporting tab started here
		if(typeof $("input[name='noofbabies']:checked").val()!= undefined || $("input[name='noofbabies']:checked").val()!=null)
		{
			
			if($("input[name='noofbabies']:checked").val()=="Multiple")
			{
				if($('#noofbabiesc').val()===undefined || $('#noofbabiesc').val().trim()=="" || $('#noofbabiesc').val()<=2)
				{
					alert($("input[name='noofbabies']:checked").val());
					$('[href=#step2]').removeClass('btn-default');  // removing default blue of step1 tab
					$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
					$('#noofbabieserrmsg').show(); // show dist error message
				}
				else
				{
					$('#noofbabieserrmsg').hide();
					$('[href=#step2]').removeClass('btn-danger');  // removing default blue of step1 tab
					$('[href=#step2]').addClass('btn-default');   // adding red color to step1 tab
				}
			}	
			
		}
		// checking source is checked or not in reporting tab ended here
	});
	// checking mandatory fields on step 2 next button click ended here	
	
	
});

    $(function(){
        
    	// tabs color change started here
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('a[data-toggle="tab"]').removeClass('btn-info');
            $('a[data-toggle="tab"]').addClass('btn-default');
            $(this).removeClass('btn-default');
            $(this).addClass('btn-info');
        });
        // tabs color change ended here
        
        // tab next button on click logic started here
        $('.next').click(function(){
            var nextId = $(this).parents('.tab-pane').next().attr("id");
            $('[href=#'+nextId+']').tab('show');
        });
        // tab next button on click logic started here
        
        // tab previous button on click logic started here
        $('.prev').click(function(){
            var prevId = $(this).parents('.tab-pane').prev().attr("id");
            $('[href=#'+prevId+']').tab('show');
        });
        // tab previous button on click logic ended here
        
        
    });
