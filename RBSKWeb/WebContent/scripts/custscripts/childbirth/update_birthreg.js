var curbabyindex=1;

$(document).ready(function()
{
	// GetUserMappings();  commented in Update Baby details.
	onloadhides();
	$('#curbabyrecstat').text("You are recording details of baby : "+curbabyindex);
	$("#birthform").validate({ //Birth Form validation started here  

		rules: {
		
	    },
	    
	    
	    submitHandler: function(form) {
	    	
	    	var isvalidform = fieldvalidation();
	    	// photo 1
	    	if($('#babyphoto1_org').val()==$('#babyphoto1').val() && $('#babyphoto1').val() !=="" && $('#babyphoto1_modify').val()==="")
	    		{
	    		$('#babyphoto1_modify').val($('#babyphoto1_org').val());
	    		}
	    	if($('#babyphoto1').val()==="") 
	    		{
	    		$('#babyphoto1_modify').val($('#babyphoto1_org').val()); 
	    		}
	    	//  photo 2 
	    	
	    	if($('#babyphoto2_org').val()==$('#babyphoto2').val() && $('#babyphoto2').val() !=="" && $('#babyphoto2_modify').val()==="")
    		{
    		$('#babyphoto2_modify').val($('#babyphoto2_org').val());
    		}
    	if($('#babyphoto2').val()==="") 
    		{
    		$('#babyphoto2_modify').val($('#babyphoto2_org').val()); 
    		}
    	
    	// photo 3
    	
    	if($('#idproof1_org').val()==$('#idproof1').val() && $('#idproof1').val() !=="" && $('#idproof1_modify').val()==="")
		{
		$('#idproof1_modify').val($('#idproof1_org').val());
		}
	if($('#idproof1').val()==="") 
		{
		$('#idproof1_modify').val($('#idproof1_org').val()); 
		}
	
	//  checking all uploaded file name are same or not
	//alert('id1::'+$('#idproof1_modify').val()+':: b2'+$('#babyphoto2_modify').val()+'::     b1::'+ $('#babyphoto1_modify').val()); 
	    
	if((($('#idproof1_modify').val().trim()== $('#babyphoto2_modify').val().trim() && $('#idproof1_modify').val().trim()!=="" && $('#babyphoto2_modify').val().trim()!=="" ) ||  ($('#babyphoto2_modify').val().trim()==$('#babyphoto1_modify').val().trim() && $('#babyphoto2_modify').val().trim()!=="" && $('#babyphoto1_modify').val().trim()!=="")  || ($('#idproof1_modify').val().trim()== $('#babyphoto1_modify').val().trim() && $('#idproof1_modify').val().trim()!=="" && $('#babyphoto1_modify').val().trim()!=="" ) )  )
		{
		//alert((($('#idproof1_modify').val()== $('#babyphoto2_modify').val()) ||  ($('#babyphoto2_modify').val()==$('#babyphoto1_modify').val())  || ($('#idproof1_modify').val()== $('#babyphoto1_modify').val()) ) &&  $('#idproof1_modify').val()!=="" && $('#babyphoto2_modify').val()!=="" && $('#babyphoto1_modify').val()!=="");  
		toastr.error("uploaded file names should not be same ");
		isvalidform=false; 
		} 
	
	/***********Added by Ravindar**************/
	if($('#mothersname').val().trim()!=""){
		if(!checkalphanumeric($('#mothersname').val())){
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Mother's name.");
			isvalidform=false;
		}
	}
	if($('#fathername').val().trim()!=""){
		if(!checkalphanumeric($('#fathername').val())){
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Father name.");
			isvalidform=false;
		}
	}
	if($('#bhouseno').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#bhouseno').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in House No.");
			isvalidform=false;
		}
	}
	if($('#bstreetname').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#bstreetname').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Street name.");
			isvalidform=false;
		}
	}
	if($('#bareaname').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#bareaname').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Street Area.");
			isvalidform=false;
		}
	}
	if($('#bpostoffice').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#bpostoffice').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Post office.");
			isvalidform=false;
		}
	}
	if($('#bthouseno').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#bthouseno').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in House No.");
			isvalidform=false;
		}
	}
	if($('#btstreetname').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#btstreetname').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Street name.");
			isvalidform=false;
		}
	}
	if($('#btareaname').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#btareaname').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Street Area.");
			isvalidform=false;
		}
	}
	if($('#btpostoffice').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#btpostoffice').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Post office.");
			isvalidform=false;
		}
	}
	if($('#btdistrict').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#btdistrict').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in District.");
			isvalidform=false;
		}
	}
	if($('#btstate').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#btstate').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in State.");
			isvalidform=false;
		}
	}
	if($('#birthblock').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#birthblock').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Cluster.");
			isvalidform=false;
		}
	}
	if($('#birthminicipality').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#birthminicipality').val())){
			$('[href=#step3]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step3]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Municipality.");
			isvalidform=false;
		}
	}
	if($('#description').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#description').val())){
			$('[href=#step6]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step6]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Description of the anomaly.");
			isvalidform=false;
		}
	}
	if($('#ageat').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#ageat').val())){
			$('[href=#step6]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step6]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Age at diagnosis.");
			isvalidform=false;
		}
	}
	if($('#code').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#code').val())){
			$('[href=#step6]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step6]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Code- ICD10.");
			isvalidform=false;
		}
	}
	if($('#karotypefindings').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#karotypefindings').val())){
			$('[href=#step7]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step7]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Findings.");
			isvalidform=false;
		}
	}
	if($('#berafindings').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#berafindings').val())){
			$('[href=#step7]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step7]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Findings.");
			isvalidform=false;
		}
	}
	if($('#bloodtestfindings').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#bloodtestfindings').val())){
			$('[href=#step7]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step7]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Findings.");
			isvalidform=false;
		}
	}
	if($('#anomaly').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#anomaly').val())){
			$('[href=#step8]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step8]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Anomaly.");
			isvalidform=false;
		}
	}
	if($('#syndrome').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#syndrome').val())){
			$('[href=#step8]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step8]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Syndrome.");
			isvalidform=false;
		}
	}
	if($('#provisional').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#provisional').val())){
			$('[href=#step8]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step8]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Provisional diagnosis.");
			isvalidform=false;
		}
	}
	if($('#complediagnosis').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#complediagnosis').val())){
			$('[href=#step8]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step8]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Complete diagnosis.");
			isvalidform=false;
		}
	}
	if($('#notifyingperson').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#notifyingperson').val())){
			$('[href=#step8]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step8]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Notifying person.");
			isvalidform=false;
		}
	}
	if($('#designationofcontact').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#designationofcontact').val())){
			$('[href=#step8]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step8]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Designation and contact details.");
			isvalidform=false;
		}
	}
	if($('#facilityreferred').val().trim()!=""){
		if(!checkalphanumericwithsymbols($('#facilityreferred').val())){
			$('[href=#step8]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step8]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Special characters are not allowed in Facility referred.");
			isvalidform=false;
		}
	}
	
	
	/****file validation***/
	if( $("#babyphoto1")[0].files[0]){ 
		
		if(!validateextension('babyphoto1')){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			toastr.error("Photo 1 - We support .jpg,.gif,.png,.jpeg. ");
			isvalidform=false; 
		}else if($("#babyphoto1")[0].files[0].size > 10485760){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			
			toastr.error("Photo 1 - Max file upload size is 10MB. ");
			isvalidform=false; 
		}
	}
	if( $("#babyphoto2")[0].files[0]){
		if(!validateextension('babyphoto2')){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			toastr.error("Photo 2 - We support .jpg,.gif,.png,.jpeg. ");
			isvalidform=false; 
		}else if($("#babyphoto2")[0].files[0].size > 10485760){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Photo 2 - Max file upload size is 10MB. ");
		}
	}
	if( $("#idproof1")[0].files[0]){
		if(!validateextension('idproof1')){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			toastr.error("ID proof - We support .jpg,.gif,.png,.jpeg. ");
			isvalidform=false; 
		}else if($("#idproof1")[0].files[0].size > 10485760){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("ID proof - Max file upload size is 10MB. ");
		}
	}
	/***********Added by Ravindar END**************/
	
	    	
	if(isvalidform){
	        	
	   $('.splash').css('display', 'initial'); 	 	
	 // Birth Form submit started here
		$("#submitcustfeedback").attr('disabled',true);
		var addform = document.getElementById('birthform');
		var formdata = new FormData(addform);
		  // ajax call started here
		  $.ajax({
		   url: "/UpdateBabyDetails",
		   type: 'POST',
		   data: formdata,
	       contentType: false,
		   processData: false,
		   success: function (data) { 
			   	if(data.indexOf('Success')!== -1){
			   		$("#birthform").trigger('reset');
	 			   $('#submitcustfeedback').removeAttr('disabled');
	 			
	 			  swal({
                      title: "",
                      text: "Data has been successfully saved.",
                      type: "info",
                      showCancelButton: false,
                      confirmButtonColor: "info",
                      confirmButtonText: "OK",
                      cancelButtonText: "No, cancel plx!",
                      closeOnConfirm: false,
                      closeOnCancel: false },
                  function (isConfirm) {
                      if (isConfirm) {
                         document.location.href="/reports.jsp";
                      } else {
                          swal("Cancelled", "Your imaginary file is safe :)", "error");
                      }
                  });
	    		}
	    		if(data.indexOf('Error!') !== -1)
	    			{ 
	    			toastr.error(data);
		    		}
		   
	    		onloadhides();
	    		
	    		$('.splash').css('display', 'none'); 
		   }, // success ended here
		   error: function(request, status, message) { // error started here
			    console.log("error occourred in UpdateBabyDetails servlet call :");
			    console.log(message);
			    toastr.error('Error - Unable to Connect Keansa Solutions Server');
			    $('#submitcustfeedback').removeAttr('disabled');
			    $('.splash').css('display', 'none'); 
			    
			   } // error ended here
			  });
		   
	    } // isformvalid ended here
	       /* $('[href=#step1]').tab('show'); */  
			  return false; 
	}
//Birth Form submission Ended here
	});

 // *********************************** DATE PICKER Hide          started here ***********************************//
//reportingdate started here
$("#reportingdate").datepicker({
       	format: "yyyy-mm-dd" ,
       	endDate: '0'
});
$('#reportingdate').on('change', function () {
    $('.datepicker').hide();
});
//reportingdate ended here

//dateofidentification started here
$("#dateofidentification").datepicker({
       	format: "yyyy-mm-dd" ,
       	endDate: '0',
});
$('#dateofidentification').on('change', function () {
    $('.datepicker').hide();
});
//dateofidentification ended here

// dateofbirth started here
/*$("#dateofbirth").datepicker({
   	format: "yyyy-mm-dd" ,
   	endDate: '0',
});
$('#dateofbirth').on('change', function () {
$('.datepicker').hide();
});*/
$('#dateofbirth').datetimepicker({
	// language: 'pt-BR' ,
	format: 'DD-MMM-YYYY HH:mm:ss'
}); 

//dateofbirth ended here

//lastmensuralperiod  started here 
$("#lastmensuralperiod").datepicker({
   	format: "yyyy-mm-dd" ,
   	endDate: '0',
});
$('#lastmensuralperiod').on('change', function () {
$('.datepicker').hide();
});

//lastmensuralperiod  started here 
// *********************************** DATE PICKER Hide          ended here ***********************************//

// *********************************** Hide and Show of form Fields started here ***********************************//
// hide and show for birth defects tab started here
$('#birthdefectchoicecn').on('change', function () {
	$('#defectsdiv2').hide();
	});
$('#birthdefectchoicecy').on('change', function () {
	$('#defectsdiv2').show();
	});
//hide and show for birth defects tab ended here

//hide and show for birth defects through instruments tab started here
$('#birthdefectinstchoicen').on('change', function () {
	$('#defectthroughinstrumentsdiv').hide();
	});
$('#birthdefectinstchoicey').on('change', function () {
	$('#defectthroughinstrumentsdiv').show();
	});
//hide and show for birth defects through instruments tab ended here

//hide and show for birth defects through blood test tab started here
$('#defectbloodtestchoicen').on('change', function () {
	$('#defectthroughbloodtestsdiv').hide();
	});
$('#defectbloodtestchoicey').on('change', function () {
	$('#defectthroughbloodtestsdiv').show();
	});
//hide and show for birth defects through blood test tab ended here

//hide and show for Congenital anomalies tab started here
$('#congenitalanomalieschoicen').on('change', function () {
	$('#congenitalanomaliesdiv1').hide();
	});
$('#congenitalanomalieschoicey').on('change', function () {
	$('#congenitalanomaliesdiv1').show();
	});
//hide and show for Congenital anomalies tab ended here

//hide and show for investigation details tab started here
$('#investigationchoicen').on('change', function () {
	$('#investigationdiv1').hide();
	});
$('#investigationchoicey').on('change', function () {
	$('#investigationdiv1').show();
	});
//hide and show for or investigation details tab ended here


//hide and show for antenatal details tab started here
$('#antenatalcheckn').on('change', function () {
	$('#antenataldetailsdiv').hide();
	});
$('#antenatalchecky').on('change', function () {
	$('#antenataldetailsdiv').show();
	});
//hide and show for or antenatal details tab ended here

//hide and show for antenatal details tab started here
$('#residentialcheckn').on('change', function () {
	$('#residentialdiv').hide();
	});
$('#residentialchecky').on('change', function () {
	$('#residentialdiv').show();
	});
//hide and show for or antenatal details tab ended here

//*********************************** Hide and Show of form Fields ended here ***********************************//


//checking mandatory fields on submit button click started here	
function fieldvalidation(){
	
	 var validationstat=true;
		// checking typeofdelivery is checked or not in reporting tab started here
		if($("input[name='typeofdelivery']:checked").val()===undefined || $("input[name='typeofdelivery']:checked").val()==null)
		{  
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
			$('#typeofdeliverrmsg').show(); // show dist error message
			validationstat=false;
			if($('#typeofdeliverytxtbox').val()!="")
				{
				$('[href=#step2]').removeClass('btn-danger');  // removing default blue of step1 tab
				$('[href=#step2]').addClass('btn-info');   // adding red color to step1 tab
				$('#typeofdeliverrmsg').show(); // show dist error message
				validationstat=true;
				}
		}
		// checking typeofdelivery is checked or not in reporting tab ended here
		if($("input[name='typeofdelivery']:checked").val()==='Cesareandelivery')
		{
		if($('#resonforcesarean').val().trim()==="")
			{
			$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
			$('#typeofdeliverrmsgcesa').show(); // show dist error message 
			validationstat=false;
			}
		}
		 
	// checking district is checked or not in reporting tab started here
	if($("input[name='district']:checked").val()===undefined || $("input[name='district']:checked").val()==null)
	{
		$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
		$('#disterrmsg').show(); // show dist error message
		validationstat=false;
	}
	// checking district is checked or not in reporting tab ended here
	
	// checking source is checked or not in reporting tab started here
	if($("input[name='source']:checked").val()===undefined || $("input[name='source']:checked").val()==null)
	{
		$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
		$('#sourceerrmsg').show(); // show dist error message
		validationstat=false;
	}
	// checking source is checked or not in reporting tab ended here
	
	// checking cluster is valid or not in reporting tab ended here
	if($('#cluster').val()===undefined || $('#cluster').val()==null || $('#cluster').val().trim()=="")
	{
		$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
		$('#clusterrmsg').show(); // show cluster error message
		validationstat=false;
	}
	// checking cluster is valid or not in reporting tab ended here
	// checking reporting date is valid or not in reporting tab ended here
	if($('#reportingdate').val()===undefined || $('#reportingdate').val()==null || $('#reportingdate').val().trim()=="")
	{
		$('[href=#step1]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step1]').addClass('btn-danger');   // adding red color to step1 tab
		$('#rptdterrmsg').show(); // show reporting date error message
		validationstat=false;
	}
	// checking reporting date is valid or not in reporting tab ended here
	// checking date of birth is valid or not in step2 tab started here
	if($('#dateofbirth').val()===undefined || $('#dateofbirth').val()==null || $('#dateofbirth').val().trim()=="")
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#doberrmsg').show(); // show cluster error message
		validationstat=false;
	}
	// checking date of birth is valid or not in step2 tab ended here
	// checking sex  is valid or not in step2 tab started here
	if($("input[name='sex']:checked").val()===undefined || $("input[name='sex']:checked").val()==null || $("input[name='sex']:checked").val()=="")
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#sexnonerrmsg').show(); // show cluster error message
		validationstat=false;
	}
	// checking sex  of birth is valid or not in step2 tab ended here
	// checking adhar card is valid or not in step2 tab started here
	if($('#adharno').val()===undefined || $('#adharno').val()==null || $('#adharno').val().trim()=="")
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#adharerrmsg').show(); // show cluster error message
		validationstat=false;
	}
	// checking adhar card of birth is valid or not in step2 tab ended here
	
	// checking birth weight is valid or not in step2 tab started here
	if($('#birthweightingrms').val()===undefined || $('#birthweightingrms').val()==null || $('#birthweightingrms').val().trim()=="")
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#birthweightingrmserrmsg').show(); // show cluster error message
		validationstat=false;
	}
	// checking birth weight is valid or not in step2 tab ended here
	// check gestational age is valid or not started here
	if($('#birthweightingrms').val().trim()!="")
	{
	if(!CheckDecimal($('#birthweightingrms').val()))
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#birthweightingrmserrmsg').show();
		validationstat=false;
	}
	}
    // check gestational age is valid or not ended here
	// check gestational age is valid or not started here
	if($('#gestationalageinweeks').val().trim()!="")
	{
	if(!CheckDecimal($('#gestationalageinweeks').val()))
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#gestationalageinweekserrmsg').show();
		validationstat=false;
	}
	}
    // check gestational age is valid or not ended here
	if($('#adharno').val().trim()!="")
	{
	if(!CheckNumber($('#adharno').val()) || $('#adharno').val().length!=12)
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#adharerrmsg').show();
		validationstat=false;
	}
	}
	//
	if($('#mothersage').val().trim()!="")
	{
	if(!CheckNumber($('#mothersage').val()) || $('#mothersage').val().length!=2)
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#motherageerrmsg').show();
		validationstat=false;
	}
	}
	//
	if($('#fathersage').val().trim()!="")
	{
	if(!CheckNumber($('#fathersage').val()) || $('#fathersage').val().length!=2)
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#fatherageerrmsg').show();
		validationstat=false;
	}
	}
	//
	if($('#mobileno').val().trim()!="")
	{
	if(!CheckNumber($('#mobileno').val()) || $('#mobileno').val().length!=10)
	{
		$('[href=#step2]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step2]').addClass('btn-danger');   // adding red color to step1 tab
		$('#mobileerrmsg').show();
		validationstat=false;
	}
	}
	//
	if($('#headcircumference').val().trim()!="")
	{
	if(!CheckDecimal($('#headcircumference').val()))
	{
		$('[href=#step4]').removeClass('btn-info');  // removing default blue of step1 tab
		$('[href=#step4]').addClass('btn-danger');   // adding red color to step1 tab
		$('#headcircumferenceerrmsg').show();
		validationstat=false;
	}
	}
	/***********Added by Ravindar**************/
	if( $("#babyphoto1")[0].files[0]){ 
		
		if(!validateextension('babyphoto1')){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			toastr.error("Photo 1 - We support .jpg,.gif,.png,.jpeg. ");
			validationstat=false; 
		}else if($("#babyphoto1")[0].files[0].size > 10485760){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			
			toastr.error("Photo 1 - Max file upload size is 10MB. ");
			validationstat=false; 
		}
	} 
	if( $("#babyphoto2")[0].files[0]){
		if(!validateextension('babyphoto2')){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			toastr.error("Photo 2 - We support .jpg,.gif,.png,.jpeg. ");
			validationstat=false; 
		}else if($("#babyphoto2")[0].files[0].size > 10485760){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("Photo 2 - Max file upload size is 10MB. ");
		}
	}
	if( $("#idproof1")[0].files[0]){
		if(!validateextension('idproof1')){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			//$('babyphoto1').removeClass();  // removing default blue of step1 tab
			//$('babyphoto1').addClass('errorfield');   // adding red color to step1 tab
			toastr.error("ID proof - We support .jpg,.gif,.png,.jpeg. ");
			validationstat=false; 
		}else if($("#idproof1")[0].files[0].size > 10485760){
			$('[href=#step9]').removeClass('btn-info');  // removing default blue of step1 tab
			$('[href=#step9]').addClass('btn-danger');   // adding red color to step1 tab
			toastr.error("ID proof - Max file upload size is 10MB. ");
		}
	}
	/***********Added by Ravindar END**************/
	
return validationstat;	
}
// checking mandatory fields on submit button click ended here

function onloadhides(){
	
	// hiding the div's onload started here
	$("#defectsdiv2").hide();
	$("#defectthroughinstrumentsdiv").hide();
	$("#defectthroughbloodtestsdiv").hide();
	$("#investigationdiv1").hide();
	$("#congenitalanomaliesdiv1").hide();
	$("#antenataldetailsdiv").hide();
	
	
	AntenatalClone = $("#antenataldetailsdiv").clone();
	
	defectsdiv2Clone = $("#defectsdiv2").clone();
	defectthroughinstrumentsdivClone = $("#defectthroughinstrumentsdiv").clone();
	defectthroughbloodtestsdivClone = $("#defectthroughbloodtestsdiv").clone();
	
	congenitalanomaliesdiv1Clone = $("#congenitalanomaliesdiv1").clone();
	
	investigationdiv1Clone = $("#investigationdiv1").clone();
	diagnosisdetailsdivClone = $("#diagnosisdetailsdiv").clone();
	babyphotosdivClone = $("#babyphotosdiv").clone();
	
	
// hiding the div's onload ended here 
}


function multiplebabyformsubmitionreset(){
	
	// hiding the div's onload started here
	
	// step 1 resets started here
	$('#dateofidentification').val('');
	
	$('#ageofidentificationnone').iCheck('check');
	
	// step 1 resets ended  here
	
	// step 2 resets started here
	$('#birthweightingrms').val('');
	
	$('#babydeliveryaslb').iCheck('check');
	$('#sexnon').iCheck('check');
	
	$('#birthasphyxian').iCheck('check');
	$('#autopsyshowbirthn').iCheck('check');
	
	$('#modeofdeliveryn').iCheck('check');
	$('#statusofinductionn').iCheck('check');
	
	// step 2 resets ended here
	
	// step 4 resets started here
	
	$("#antenataldetailsdiv").replaceWith(AntenatalClone);
	// step 4 resets ended here
	
	// step 5 resets started here
	$('#birthdefectchoicecn').iCheck('check');
	$("#defectsdiv2").replaceWith(defectsdiv2Clone);
	$('#birthdefectinstchoicen').iCheck('check');
	$("#defectthroughinstrumentsdiv").replaceWith(defectthroughinstrumentsdivClone);
	
	$('#defectbloodtestchoicen').iCheck('check');
	$("#defectthroughbloodtestsdiv").replaceWith(defectthroughbloodtestsdivClone);
	
	// step 5 resets ended here
	
	// step 6 resets started here
	$('#congenitalanomalieschoicen').iCheck('check');
	$("#congenitalanomaliesdiv1").replaceWith(congenitalanomaliesdiv1Clone);
	// step 6 resets ended here
	
	// step 7 resets started here
	$('#investigationchoicen').iCheck('check');
	$("#investigationdiv1").replaceWith(investigationdiv1Clone);
	// step 7 resets ended here
	
	
	// step 8 resets started here
	$("#diagnosisdetailsdiv").replaceWith(diagnosisdetailsdivClone);
	//// step 9 resets ended here
	// step 9 resets started here
	$("#babyphotosdiv").replaceWith(babyphotosdivClone);
	// step 9 resets started here
	// hiding the div's onload ended here 
}

//
function addredonlyfornextbay()
{   
	$('#noofbabies').attr('disabled','disabled');
	$('#reportingdate').attr('disabled','disabled');
	$("input[name='district']").attr('disabled','disabled');
	$("input[name='source']").attr('disabled','disabled');
	$("#cluster").attr('disabled','disabled');
	$("#dateofbirth").attr('disabled','disabled');
	$("#adharno").attr('disabled','disabled');
	
}
//

//
function removereadonlyonfinalbaby()
{   
	$('#noofbabies').removeAttr('disabled');
	$('#reportingdate').removeAttr('disabled');
	$("input[name='district']").removeAttr('disabled');
	$("input[name='source']").removeAttr('disabled');
	$("#cluster").removeAttr('disabled');
	$("#dateofbirth").removeAttr('disabled');
	$("#adharno").removeAttr('disabled');
	
}
//

// reset all selections of antenatal fields on no check box click started here
$('#antenatalcheckdiv').on('change','#antenatalcheckn', function(event) {
	$('#antenataldetailsdiv').empty();
	$('#antenataldetailsdiv').append(AntenatalClone.html());
	$('#headcircumferenceerrmsg').hide();
	
});
//reset all selections of antenatal fields on no check box click ended here

//reset all selections of birth defects fields on no check box click started here
$('#birthdefectchoicecid').on('change','#birthdefectchoicecn', function(event) {
	$('#defectsdiv2').empty();
	$('#defectsdiv2').append(defectsdiv2Clone.html());
});
//reset all selections of birth defects fields on no check box click ended here


//reset all selections of birth defects through instruments fields on no check box click started here
$('#birthdefectinstchoicen').click(function(){
	$('#defectthroughinstrumentsdiv').empty();
	$('#defectthroughinstrumentsdiv').append(defectthroughinstrumentsdivClone.html());
});
//reset all selections of birth defects through instruments fields on no check box click ended here

//reset all selections of birth defects through blood test fields on no check box click started here
$('#defectbloodtestchoicen').click(function(){
	$('#defectthroughbloodtestsdiv').empty();
	$('#defectthroughbloodtestsdiv').append(defectthroughbloodtestsdivClone.html());
});
//reset all selections of birth defects through blood test fields on no check box click ended here

//reset all selections of congenital anomalities fields on no check box click started here
$('#congenitalanomalieschoicen').click(function(){
	$('#congenitalanomaliesdiv1').empty();
	$('#congenitalanomaliesdiv1').append(congenitalanomaliesdiv1Clone.html());
});
//reset all selections of investigation fields on no check box click ended here

//reset all selections of congenital anomalities fields on no check box click started here
$('#investigationchoicen').click(function(){
	$('#investigationdiv1').empty();
	$('#investigationdiv1').append(investigationdiv1Clone.html());
});
//reset all selections of investigation fields on no check box click ended here

/*function GetUserMappings()       commented in Update Baby details.
{
	 // getting values for Source drop down
	 $.ajax({
		 	url: "GetUserMappingsServlet", 
			type: 'POST',
			// data: {tablename : 'tg_source' },
			success: function (data) { // success started here
				//var user_id="";
				var district="";
				var cluster="";
				var hospitalname="";
				//var hospitaltype=""; 
				//var usertype="";
				 $.each(data, function(key,value) {
					// user_id=value['user_id'];
					 district=value['district'];
					 cluster=value['cluster'];
					 hospitalname=value['hospitalname'];
					// hospitaltype=value['hospitaltype']; 
					// usertype=value['usertype'];
				 }); // loop ended here
				 if(district!=="" && district!==null && typeof district!='undefined')
				 {
					 jQuery("input[value='"+district+"']").attr('checked', true); 
				 }
				 
				 if(cluster!=="" && cluster!==null && typeof cluster!='undefined')
				 {
					 $('#cluster').val(cluster);
					 $('#birthblock').val(cluster);
					 $('#cluster').attr('readonly', true); 
				 }
				
				 if(hospitalname!=="" && hospitalname!==null && typeof hospitalname!='undefined')
					 {
					 $('#hospitalname').val(hospitalname);
					 $('#hospitalname').attr('readonly', true);
					 }
				  
			},//success ended here 
			
			error: function(request, status, message) { // error started here
				
			    console.log("error occourred in GetUserMappingsServlet servlet call :");
			    console.log(message);
			    
			   } //error ended here
		});// ajax call ended here
	 
}*/

});

$('#sourcediv').on('click','.mysource', function(event) { 
	if($('input:radio[name=source]:checked').val()!=='Others')
	{
	$('#others').val('');  
	}
});

$('.onlynumberwithdot').keydown(function (e){
	allowonlynumwithdot(e); 
	 });
 
function allowonlynumwithdot(e)
{
		if (e.shiftKey || e.ctrlKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || (key == 190))) {
		e.preventDefault();
		}
		}
}

$('.onlynumber').keydown(function (e){
	allowonlynum(e); 
	 });
 
function allowonlynum(e)
{
		if (e.shiftKey || e.ctrlKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) )) {
		e.preventDefault();
		}
		}
}
