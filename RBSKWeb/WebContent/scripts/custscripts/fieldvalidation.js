/**
 * 
 */

$("#refreshcaptcha").click(function(e){
	getCaptcha("captchaimg",e);
});

$("#checkcaptha").click(function(e){
	alert($("#yemail").val());
	checkCaptCha($("#username").val());
	
});

//check number validation started here
function CheckNumber(inputtxt)   
{   
var decimal=  /^\d+$/;   
if(decimal.test(inputtxt))   
{   
return true; 
}  
else  
{   
return false;  
}  
}
//check number validation started here

//check number decimal started here
function CheckDecimal(inputtxt)   
{   
var decimal=  /((\d+)((\.\d{1,2})?))$/;   
if(decimal.test(inputtxt))   
{   
return true;  
}  
else  
{   
return false;  
}  
}
//check number validation ended here

// checking maximum length
function checkmaxlenght(inputtxt,len)
{
if(inputtxt.length>len)
	{
	return false;
	}
else
	{
	return true;
	}
}
// checking minimum length
function checkminlenght(inputtxt,len)
{
if(inputtxt.length<len)
	{
	return false;
	}
else
	{
	return true;
	}
}
// checking exact length
function checkexactlenght(inputtxt,len)
{
if(inputtxt.length!=len)
	{
	return false;
	}
else
	{
	return true;
	}
}
// checking alphabets
function checkalphabet(inputtxt)
{
	var alphabets=  /^[a-zA-Z ]*$/;   
	if(alphabets.test(inputtxt))   
	{   
	return true;  
	}  
	else  
	{   
	return false;  
	}  
}

// checking alpha Numeric

function checkalphanumeric(inputtxt)
{
	var alphabets=  /^[a-zA-Z0-9]+$/;   
	if(alphabets.test(inputtxt))   
	{   
	return true;  
	}  
	else  
	{   
	return false;  
	}  
}

// checking alphanumerics with symbols


function checkalphanumericwithsymbols(inputtxt)
{
	var alphabets= /^[ A-Za-z0-9_@.,/#&+-]*$/;   
	if(alphabets.test(inputtxt))   
	{   
	return true;  
	}  
	else  
	{   
	return false;  
	}  
}

// checking email id 

function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
	return true;
	}
	else {
	return false;
	}
}

//fieldvalisnull started here
//This method is to validate field is empty or not
//This method accepts field id and returns true if field has empty string otherwise return false.
function fieldvalisnull(fieldid)
{
	if($("#"+fieldid).val().trim()=="")
	{
		return true;
	}
	else
	{
	    return false;
	}
}
//fieldvalisnull ended here


//validatenumberfield started here
// This method is to validate whether field has only numbers or not.
// This method accepts field id 
// error message span tag should have value as fieldid followed by errmsg example : If height is field id then error message id should be heighterrmsg
function validatenumberfield(fieldid)
{
	if($("#"+fieldid).val().length <= 0 && fieldvalisnull(fieldid))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else if(!fieldvalisnull(fieldid) && !CheckNumber($("#"+fieldid).val()))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else
	{
		$("#"+fieldid+"errmsg").hide();
		return true;
		
	}
}
//validatenumberfield ended here

//validatedecimalfield started here
//This method is to validate whether field has only numbers or not.
//This method accepts field id 
//error message span tag should have value as fieldid followed by errmsg example : If height is field id then error message id should be heighterrmsg
function validatedecimalfield(fieldid)
{
	if($("#"+fieldid).val().length <= 0 && fieldvalisnull(fieldid))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else if(!fieldvalisnull(fieldid) && $("#"+fieldid).val().length <3 && !CheckNumber($("#"+fieldid).val()))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else if(!fieldvalisnull(fieldid) && $("#"+fieldid).val().length >3 && !hasvaliddecialpoint($("#"+fieldid).val()))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else
	{
		$("#"+fieldid+"errmsg").hide();
		return true;
		
	}
}
//validatedecimalfield ended here


//validatenumberfield started here
//This method is to validate whether field has only numbers or not.
//This method accepts field id 
//error message span tag should have value as fieldid followed by errmsg example : If height is field id then error message id should be heighterrmsg
function isMandatororyfield(fieldid)
{
	if($("#"+fieldid).val().length == 0)
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else
	{
		$("#"+fieldid+"errmsg").hide();
	}
}
//validatenumberfield ended here

function hasvaliddecialpoint(inputtext)
{
	if(inputtext.indexOf(".")!= -1 && inputtext.indexOf(".") < 3 && inputtext.indexOf(".") < inputtext.length-1)
	{
	 return true;
	}
	else
	{
	 return false;
	}
}

// latest method created to check value is in between min and max and maxlength
function validatenumberrangewithmaxlength(fieldid,minvalue,maxvalue,maxlength)
{
	if($("#"+fieldid).val().length <= 0 || fieldvalisnull(fieldid))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else if(!fieldvalisnull(fieldid) && ($("#"+fieldid).val() <= minvalue || $("#"+fieldid).val()> maxvalue))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else if($("#"+fieldid).val().length > maxlength)
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else
	{
		$("#"+fieldid+"errmsg").hide();
		return true;
		
	}
}


function validatenumberfieldwithnonullcheck(fieldid)
{
	if($("#"+fieldid).val().length > 0 && !fieldvalisnull(fieldid))
	{
		
	
	 if(!CheckNumber($("#"+fieldid).val()))
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else
	{
		$("#"+fieldid+"errmsg").hide();
		return true;
		
	}
	}
	else
		{
		return true;
		}
}

function validatenumberrangewithmaxlengthwithnonullcheck(fieldid,minvalue,maxvalue,maxlength)
{
	if($("#"+fieldid).val().length > 0 || !fieldvalisnull(fieldid))
	{
		
	 if($("#"+fieldid).val() <= minvalue || $("#"+fieldid).val()> maxvalue)
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else if($("#"+fieldid).val().length > maxlength)
	{
		$("#"+fieldid+"errmsg").show();
		return false;
	}
	else
	{
		$("#"+fieldid+"errmsg").hide();
		return true;
		
	}
	}
	else
	{
	return true;
	}
}

//checking alphanumerics with symbols


function checkalphawithsymbols(inputtxt)
{   
	if(inputtxt==null || inputtxt== '' || inputtxt.trim() == '')
	{
	return false;
	}
	var alphabets= /^[ A-Za-z_@./#&+-]*$/;   
	if(alphabets.test(inputtxt))   
	{   
	return true;  
	}  
	else  
	{   
	return false;  
	}
	
}
function allowonlynum(e)
{
	
		if (e.shiftKey || e.ctrlKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
		e.preventDefault();
		}
		}
		
		
}
function allowonlynumcsa(e)
{
	
		if (e.shiftKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		if (key == 67 || key == 86 || key == 88 || key == 99 || key == 118 || key == 120){
			
		}
		
		else if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
		e.preventDefault();
		}
		}
		
		
}

function allowonlyalphabets(e,fieldname)
{
	    
		var alphabets=  /^[a-zA-Z ]*$/;  
		var inputtxt= $("#"+fieldname).val();
		var key = e.keyCode;
		if((key >= 48 && key <= 57) || (key >= 96 && key <= 105))
		{
			e.preventDefault();	
		}
		if(alphabets.test(inputtxt))   
		{   
		// do nothing
		} 
		else if((key == 8) || (key == 9) || (key == 46) || (key == 16))
		{
			
		}
		else  
		{   
		e.preventDefault(); 
		}  
		
		
}
function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

function getcmsFromFeetAndInches(feet, inches,event)
{
var totalInches = parseInt(feet) * 12;
totalInches = parseInt(totalInches) + parseInt(inches);
var cm = Math.round(totalInches * 2.54);
return cm;
}



function validateLocation(fieldId,notificationFieldId){
	var isValid =true;
	$("#"+notificationFieldId).hide();
	var fieldValue = $("#"+fieldId).val();
	if(fieldValue.length > 0 || !fieldvalisnull(fieldId))
	{
		var count = fieldValue.split(",").length;
		 if(count<3){
			 $("#"+notificationFieldId).show();
			 isValid =false;
		 }else{
				$("#"+notificationFieldId).hide();
			}
			
	}else{
		$("#"+notificationFieldId).show();
		isValid =false;
	}
	return isValid;
}

function convertsqldatetobrowser(fromdate)
{
	if(fromdate!="" && fromdate!=null && typeof fromdate!=undefined)
	{
		var d = new Date(fromdate);
		var temparray = d.toString().split(" ");
		var date=temparray[2];
		var month=temparray[1];
		var year=temparray[3];
		return date+"-"+month+"-"+year;
	}
	else
	{
	return "";
	}
}






function convertsdatepickertosql(fromdate)
{
	if(fromdate!="" && fromdate!=null && typeof fromdate!=undefined)
	{
		var temparray = fromdate.toString().split("-");
		var date=temparray[0];
		var monthstr=temparray[1];
		var year=temparray[2];
		var monthnum="";
		switch(monthstr)
		{
		case 'Jan': monthnum="01"; break;
		case 'Feb': monthnum="02"; break;
		case 'Mar': monthnum="03"; break;
		case 'Apr': monthnum="04"; break;
		case 'May': monthnum="05"; break;
		case 'Jun': monthnum="06"; break;
		case 'Jul': monthnum="07"; break;
		case 'Aug': monthnum="08"; break;
		case 'Sep': monthnum="09"; break;
		case 'Oct': monthnum="10"; break;
		case 'Nov': monthnum="11"; break;
		case 'Dec': monthnum="12"; break;
		}
		return year+"-"+monthnum+"-"+date;
	}
	else
	{
	return "";
	}
}

//function dateaddition(fromdate,type,count)
//{
//	
//	Date d1 = new Date(fromdate);
//	
//	if(type=="Days")
//	{
//	Date d2 = d2.setDate(d1,getDate()+count);
//	var temparray = d2.toString().split(" ");
//	var date=temparray[2];
//	var month=temparray[1];
//	var year=temparray[3];
//	return date+"-"+month+"-"+year;
//	}
//	if(type=="Months")
//	{
//	Date d2 = d2.setMonth(d1,getMonth()+count);
//	var temparray = d2.toString().split(" ");
//	var date=temparray[2];
//	var month=temparray[1];
//	var year=temparray[3];
//	return date+"-"+month+"-"+year;
//	}
//	if(type=="Year")
//	{
//	Date d2 = d2.setFullYear(d1,getFullYear()+count);
//	var temparray = d2.toString().split(" ");
//	var date=temparray[2];
//	var month=temparray[1];
//	var year=temparray[3];
//	return date+"-"+month+"-"+year;
//	}
//	
//}

function getCaptcha(capthcaid,e)
{
	
	$.ajax({
		url: "/CaptchaServlet",
		type: "POST",
		data: {donothing:'donothing'},
		success: function(data){
			
			$("#captchaimg").removeAttr("src");
			var d = new Date();
			$("#captchaimg").attr("src","/CaptchaServlet?"+d.getTime());
			
		},
		error: function(request,status,message){
			console.log("error occourred in LoadCustVaccTable servlet call :");
			console.log(message);
		}
	
		
	});
}

function checkCaptCha(captchaterm)
{
	
	$.ajax({
		url: "/CheckCaptcha",
		type: "POST",
		data: {captchaterm:captchaterm},
		success: function(data){
			
			alert(data);
			
		},
		error: function(request,status,message){
			console.log("error occourred in LoadCustVaccTable servlet call :");
			console.log(message);
		}
	
		
	});
}

function dbdatetimetobrowsertime(dbdatetimestr)
{
	// dbdatetime must be in format of 2016-05-31 14:03:10
	var amstime =new Date(dbdatetimestr); // nethlands time with browser timezone
	var browsertzo = amstime.getTimezoneOffset(); // browser offset positve if GMT - or Negative if GMT +
	console.log("Timezoneoffset:"+browsertzo); 
	 var gmttime = new Date( amstime.getTime() - (120 * 60000));  // 120 minutes is timezone offset of server Netherlands timezone.  GMT Time with browser time zone
	 var browsertime=gmttime;
	 browsertime= new Date( gmttime.getTime() - (browsertzo * 60000));  // if GMT -3 means offset of positve 180 will be subtracted . if GMT +3 means offset of -180 will be added.
	 return browsertime;
}

function browsertimetodbdatetime(browserdatteimestr)
{
	// browserdatteimestr must be in format of 2016-05-31 14:03:10
	var browsertime =new Date(browserdatteimestr); // nethlands time with browser timezone
	var browsertzo = browsertime.getTimezoneOffset(); // browser offset positve if GMT - or Negative if GMT +
	console.log("Timezoneoffset:"+browsertzo); 
	 var gmttime = new Date( browsertime.getTime() + (browsertzo * 60000));   // if GMT -3 means offset of positve 180 will be added . if GMT +3 means offset of -180 will be subtracted.
	 var dbdatetime=gmttime;
	 dbdatetime= new Date( gmttime.getTime() + (120 * 60000));  // 120 minutes is timezone offset of server Netherlands timezone.  converting GMT to netherlands time
	 return dbdatetime;
}



/****Added By Ravindar ******/
//This function to check file extenstion of uploaded file started here
function validateextension(fieldname) {
    var allowedFiles = [".jpg", ".gif", ".png", ".jpeg", ".bmp", ".svg"];
    var filename = document.getElementById(fieldname);
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
    if (!regex.test(filename.value.toLowerCase())) {
    	/*$("#previewtitle").hide();
		$("#profilepicimage").empty();
		$("#profilepicimage").append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color=\"red\">We support .jpg,.gif,.png,.jpeg</font>");
        */
    	return false;
   }
    return true;
}

function validateFileSize(fieldname) {
    
    var filename = document.getElementById(fieldname);
 
    if (filename.files[0].size > 10485760) { //10485760 - 10mb
    	/*$("#previewtitle").hide();
		$("#profilepicimage").empty();
		$("#profilepicimage").append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color=\"red\">We support .jpg,.gif,.png,.jpeg</font>");
        */
    	return false;
   }
    return true;
}


function convertHtmlEntity(rawStr){
	if(rawStr == 'undefined' || rawStr == null){return rawStr;}
	var encodedStr = rawStr.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
	   return '&#'+i.charCodeAt(0)+';';
	});
	return encodedStr;
}