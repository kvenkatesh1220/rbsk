<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
	<!-- Page title ended here-->



    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->
    
    <!-- Health Pinata styles started here -->
      <!-- <link rel="stylesheet" type="text/css" href="styles/mycustsytles/sweetalert.css"> -->
   <!--  Health Pinata styles ended here  -->

</head>
<body>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Health Pinata</h1><p>Your Health, Your Treasure. Take Control. </p><img src="image/loading-bars.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>

 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->


<div class="boxed-wrapper">
<!-- Main Wrapper started here -->
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            
            <h4 class="font-light m-b-xs">
               Telangana Bala Swasthya Karyakram
            </h4>
            <small>Birth Defect Screening cum reporting form.</small>
        </div>
    </div>
</div>

<div class="content animate-panel">

<div class="row">
     <div class="col-md-12">
                <div class="hpanel" >
                    <div class="panel-body" >
					<form role="form" id="birthform" name="birthform" enctype="multipart/form-data" method="post" action="/BirthManagementServlet">
						
						<!-- Heading 1 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Location Of Reporting</h4>
						</div>
						<br>

                        <!-- Row Divider -->
						<div class="row">
						<div class="form-group">
                        <div class="col-md-2">
                        <label>State</label>
                        </div> 
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="state" id="state">
                		<option value="Telangana">Telangana</option>
            			</select>
                        </div>
                        </div>
                        <div class="col-md-2">
                        <label>District</label>
                        </div> 
                        <div class="col-md-4">
                        <select class="form-control m-b" name="district" id="district">
                		<option value="Hyderabad">Hyderabad</option>
                		<option value="Adilabad">Adilabad</option>
                		<option value="Karimnagar">Karimnagar</option>
                		<option value="Khammam">Khammam</option>
                		<option value="Mahbubnagar">Mahbubnagar</option>
                		<option value="Medak">Medak</option>
                		<option value="Nalgonda">Nalgonda</option>
                		<option value="Nizamabad">Nizamabad</option>
                		<option value="Ranga Reddy">Ranga Reddy</option>
                		<option value="Warangal">Warangal</option>
            			</select>
                        </div>
                        </div>
                        </div>
                        <br>
                        <!-- Row Divider -->
                        
                        <div class="row">
                        <div class="col-md-2">
                        <label>Block</label>
                        </div> 
                        <div class="col-md-4">
                        <input type="text" placeholder="Enter Block" id="block" name="block" class="form-control">
                        </div>
                        <div class="col-md-2">
                        <label>Source</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="source" id="source">
                		<option value="ASHA">ASHA</option>
                		<option value="Sub Centre">Sub Centre</option>
                		<option value="BPHC">BPHC</option>
                		<option value="CHC">CHC</option>
                		<option value="Rural Hospital">Rural Hospital</option>
                		<option value="Sub divisional Hospital">Sub divisional Hospital</option>
                		<option value="District Hospital">District Hospital</option>
                		<option value="Medical College Hospital">Medical College Hospital</option>
                		<option value="Private Nursing Home/Hospital/ tertiary centre">Private Nursing Home/Hospital/ tertiary centre</option>
                		<option value="Mobile Health Team">Mobile Health Team</option>
                		<option value="ANM">ANM</option>
                		<option value="Any other">Any other</option>
            			</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Reporting Month</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="reportingmonth" id="reportingmonth">
                		<option value="January">January</option>
                		<option value="February">February</option>
                		<option value="March">March</option>
                		<option value="April">April</option>
                		<option value="May">May</option>
                		<option value="June">June</option>
                		<option value="July">July</option>
                		<option value="August">August</option>
                		<option value="September">September</option>
                		<option value="October">October</option>
                		<option value="November">November</option>
                		<option value="December">December</option>
            			</select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Reporting Year</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                       
                        <select class="form-control m-b" name="reportingyear" id="reportingyear">
                		<option value="2015">2015</option>
                		<option value="2014">2014</option>
                		<option value="2016">2016</option>
                		<option value="2017">2017</option>
                		<option value="2018">2018</option>
                		<option value="2019">2019</option>
                		<option value="2020">2020</option>
                		<option value="2021">2021</option>
                		<option value="2022">2022</option>
                		<option value="2023">2023</option>
                		<option value="2024">2024</option>
                		<option value="2025">2025</option>
            			</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Date Of Identification</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Identification Date" id="dateofidentification" name="dateofidentification" class="form-control">
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Age of identification</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="ageofidentification" id="ageofidentification">
                		<option value="At birth">At birth</option>
                		<option value="Less Than 1 month">Less Than 1 month</option>
                		<option value="1-12 months">1-12 months</option>
                		<option value="1-6 yrs">1-6 yrs</option>
                		<option value="Above 6 years">Above 6 years</option>
                		<option value="Prenatal diagnosis">Prenatal diagnosis</option>
                		<option value="O Spont Abortion">O Spont Abortion</option>
                		<option value="Autopsy">Autopsy</option>
                		</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Heading 2 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Delivery Outcome Details</h4>
						</div>
						<br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Date of Birth</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Date Of Birth" id="dateofbirth" name="dateofbirth" class="form-control">
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Birth weight in (gms)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="number" placeholder="Enter Birth weight" id="birthweightingrms" name="birthweightingrms" class="form-control">
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth order</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthorder" id="birthorder">
                		<option value="First">First</option>
                		<option value="Second">Second</option>
                		<option value="Third">Third</option>
                		<option value="Other">Other</option>
                		</select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>No of Babies</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="noofbabies" id="noofbabies">
                		<option value="Single">Single</option>
                		<option value="Twin">Twin</option>
                		<option value="Multiple">Multiple</option>
                		</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                         <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Baby delivered as</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="babydeliveryas" id="babydeliveryas">
                		<option value="Live Birth">Live Birth</option>
                		<option value="IUD/Still birth">IUD/Still birth</option>
                		<option value="MTP following anomaly">MTP following anomaly</option>
                		<option value="Spontaneous abortion with anomaly">Spontaneous abortion with anomaly</option>
                		</select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Sex</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="sex" id="sex">
                		<option value="Male">Male</option>
                		<option value="Female">Female</option>
                		<option value="Ambiguous">Ambiguous</option>
                		<option value="Intersex">Intersex</option>
                		</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Gestational age(Weeks)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="number" placeholder="Gestational age" id="gestationalageinweeks" name="gestationalageinweeks" class="form-control">
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Last menstrual period</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Last menstrual period Date" id="lastmensuralperiod" name="lastmensuralperiod" class="form-control">
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth asphyxia</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthasphyxia" id="birthasphyxia">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
                		</select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Autopsy shows birth </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="autopsyshowbirth" id="autopsyshowbirth">
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                		</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Mode of delivery</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="modeofdelivery" id="modeofdelivery">
                        <option value=""></option>
                        <option value="MTP following anomaly">MTP following anomaly</option>
                		<option value="Spontaneous abortion with anomaly">Spontaneous abortion with anomaly</option>
                		</select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Status of induction/augmentation</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="statusofinduction" id="statusofinduction">
                        <option value="Nil">Nil</option>
                		<option value="Oxytocin">Oxytocin</option>
                		<option value="Misoprostol">Misoprostol</option>
                		</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Place of birth</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthplace" id="birthplace">
                        <option value="Home">Home</option>
                		<option value="Institution">Institution</option>
                		</select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Birth State</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthstate" id="birthstate">
                		<option value="Telangana">Telangana</option>
            			</select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth District</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthdistict" id="birthdistict">
                		<option value="Hyderabad">Hyderabad</option>
                		<option value="Adilabad">Adilabad</option>
                		<option value="Karimnagar">Karimnagar</option>
                		<option value="Khammam">Khammam</option>
                		<option value="Mahbubnagar">Mahbubnagar</option>
                		<option value="Medak">Medak</option>
                		<option value="Nalgonda">Nalgonda</option>
                		<option value="Nizamabad">Nizamabad</option>
                		<option value="Ranga Reddy">Ranga Reddy</option>
                		<option value="Warangal">Warangal</option>
            			</select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Birth Block</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Birth Block" id="birthblock" name="birthblock" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth Municipality</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Birth Municipality" id="birthminicipality" name="birthminicipality" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>MCTS No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter MCTS No" id="mctsno" name="mctsno" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Adhar Card No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Adhar Card No" id="adharno" name="adharno" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Mobile No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Mobile No" id="mobileno" name="mobileno" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Childs name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Childs name" id="childname" name="childname" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Mothers name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Mothers name" id="mothersname" name="mothersname" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Mothers age (Years)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Mothers age" id="mothersage" name="mothersage" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Father age</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Father age" id="fathersage" name="fathersage" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>caste</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="caste" id="caste">
                		<option value="SC">SC</option>
                		<option value="ST">ST</option>
                		<option value="OBC">OBC</option>
                		<option value="Others">Others</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Heading 3 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Permanent address</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>House No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter House No" id="bhouseno" name="bhouseno" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Street name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Street name" id="bstreetname" name="bstreetname" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Area</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Area" id="bareaname" name="bareaname" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Post office</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Post office" id="bpostoffice" name="bpostoffice" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>District</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="bdistrict" id="bdistrict">
                		<option value="Hyderabad">Hyderabad</option>
                		<option value="Adilabad">Adilabad</option>
                		<option value="Karimnagar">Karimnagar</option>
                		<option value="Khammam">Khammam</option>
                		<option value="Mahbubnagar">Mahbubnagar</option>
                		<option value="Medak">Medak</option>
                		<option value="Nalgonda">Nalgonda</option>
                		<option value="Nizamabad">Nizamabad</option>
                		<option value="Ranga Reddy">Ranga Reddy</option>
                		<option value="Warangal">Warangal</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>State</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="bstate" id="bstate">
                		<option value="Telangana">Telangana</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                         <!-- Heading 4 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Antenatal details</h4>
						</div>
						<br>
						
						
						<div id="Antenatal" >
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Folic acid details</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="folicaciddetails" id="folicaciddetails">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>H/O serious maternal illness</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="hoseriousmetirialillness" id="hoseriousmetirialillness">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>H/O radiation exposure (x-ray etc)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="horadiationexposure" id="horadiationexposure">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>H/O substance abuse</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="hosubstancceabuse" id="hosubstancceabuse">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Parental consanguinity</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="parentalconsanguinity" id="parentalconsanguinity">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Assisted conception IVF/ART</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="assistedconception" id="assistedconception">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Immunisation history (rubella)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="immunisationhistory" id="immunisationhistory">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>History of anomalies in previous pregnancies</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="historyofanomalies" id="historyofanomalies">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Maternal drugs</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="maternaldrugs" id="maternaldrugs">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Maternal details</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="maternaldrugsdesc" id="maternaldrugsdesc">
                		<option value="No">No</option>
                		<option value="Yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                         <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>No of previous abortion</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="noofpreviousabortion" id="noofpreviousabortion">
                		<option value="Nil">Nil</option>
                		<option value="1">1</option>
                		<option value="2">2</option>
                		<option value="3">3</option>
                		<option value="4">4</option>
                		<option value="5">5</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>No of previous still birth</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="nofofpreviousstillbirth" id="nofofpreviousstillbirth">
                        <option value="Nil">Nil</option>
                		<option value="1">1</option>
                		<option value="2">2</option>
                		<option value="3">3</option>
                		<option value="4">4</option>
                		<option value="5">5</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Heading 4 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Neonatal details</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Head circumference</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Head circumference" id="headcircumference" name="headcircumference" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
						</div>
						<!-- Heading 4 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Birth Defects</h4>
						</div>
						<br>
						
						
						<div class="row">
                        <div class="col-md-2">
                        <label>Birth Defects</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthdefectchoice" id="birthdefectchoice">
                        <option value="no">No</option>
                		<option value="yes">Yes</option>
						</select>
            			</div>
                        </div>
						</div>
						<!-- Visual Defects started here -->	
 						<div id="defectsdiv">
 						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Nervous System</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="nervoussystem" id="nervoussystem">
                        <option value=""></option>
                		<option value="Anencephaly">Anencephaly</option>
						<option value="Spina Bifida">Spina Bifida</option>
						<option value="Encephalocele">Encephalocele</option>
						<option value="Hydrocephalus">Hydrocephalus</option>
						<option value="Microcephaly">Microcephaly</option>
						<option value="Arhinencephaly/Holoprosencephaly">Arhinencephaly/Holoprosencephaly</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Eye Related</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="eyerelated" id="eyerelated">
                        <option value=""></option>
						<option value="Anophthalmos">Anophthalmos</option>
						<option value="Microphthalmos">Microphthalmos</option>
						<option value="Congenital cataract">Congenital cataract</option>
						<option value="Congenital glaucoma">Congenital glaucoma</option>
						</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Ear/Face/Neck</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="earfaceneck" id="earfaceneck">
                        <option value=""></option>
                		<option value="Microtia II">Microtia II</option>
						<option value="Microtia III">Microtia III</option>
						<option value="Microtia IV">Microtia IV</option>
						<option value="Congenital deafness">Congenital deafness</option>
						</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Ora Facial</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="orafacial" id="orafacial">
                        <option value=""></option>
                		<option value="Cleft lip with or without cleft palate">Cleft lip with or without cleft palate</option>
						<option value="Cleft palate">Cleft palate</option>
						</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Urinary Tract</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="urinarytract" id="urinarytract">
                        <option value=""></option>
                		<option value="Multicystic renal Dysplasia">Multicystic renal Dysplasia</option>
						<option value="Confenital Hydronephrosis">Confenital Hydronephrosis</option>
						<option value="Bladder exstrophy and/or epispadia">Bladder exstrophy and/or epispadia</option>
						<option value="Posterior urethral valve and/or prune belly">Posterior urethral valve and/or prune belly</option>
						<option value="Bilateral renal agenesis including Potter syndrome">Bilateral renal agenesis including Potter syndrome</option>
						</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Limb Related</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="limbrelated" id="limbrelated">
                        <option value=""></option>
                		<option value="Limb reduction">Limb reduction</option>
						<option value="Club foot talipes Equinovarus">Club foot talipes Equinovarus</option>
						<option value="Polydactyly">Polydactyly</option>
						<option value="Syndactyly">Syndactyly</option>
						<option value="Hip dislocation and /or dysplasia">Hip dislocation and /or dysplasia</option>
						</select>
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Abdominal Wall</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="abdominalwall" id="abdominalwall">
                        <option value=""></option>
                		<option value="Gastroschisis">Gastroschisis</option>
						<option value="Omphalocele">Omphalocele</option>
						</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Genital Disorders</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="genitaldisorders" id="genitaldisorders">
                        <option value=""></option>
                		<option value="Hypospadias">Hypospadias</option>
						<option value="indeterminate sex">indeterminate sex</option>
						</select>
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Chromosomal Disorders</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="chromosomaldisorders" id="chromosomaldisorders">
                        <option value=""></option>
                		<option value="Down syndrome">Down syndrome</option>
						<option value="Patau syndrome/trisomy13">Patau syndrome/trisomy13</option>
						<option value="Edwards syndrome/trisomy18">Edwards syndrome/trisomy18</option>
						<option value="Turner syndrome">Turner syndrome</option>
						<option value="Klinefelter syndrome">Klinefelter syndrome</option>
						</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Digestive System</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="digestivesystem" id="digestivesystem">
                        <option value=""></option>
                		<option value="Amo-rectal atresia and stenosis">Amo-rectal atresia and stenosis</option>
						</select>
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Other Anomalies</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="otheranomalies" id="otheranomalies">
                        <option value=""></option>
                		<option value="skeletal dysplasias">skeletal dysplasias</option>
						<option value="Craniostnostosis">Craniostnostosis</option>
						<option value="Congenital constriction bands/amniotic band">Congenital constriction bands/amniotic band</option>
						<option value="Conjoined twins">Conjoined twins</option>
						<option value="Congenital skin disorders">Congenital skin disorders</option>
						<option value="Vater/Vacterl">Vater/Vacterl</option>
						<option value="Teratogenic syndromes with malformations">Teratogenic syndromes with malformations</option>
						<option value="Fetal alcohol syndrome">Fetal alcohol syndrome</option>
						<option value="Valproate syndrome">Valproate syndrome</option>
						</select>
            			</div>
                        </div>
                        </div>
                        <br>								
						<!-- Visual Defects ended here -->
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth defects identified through instruments</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthdefectsinstumental" id="birthdefectsinstumental">
                        <option value=""></option>
                		<option value="Nil">Congenital deafness</option>
                		<option value="1">Congenital vision defects</option>
                		<option value="2">ROP</option>
                		<option value="3">CHD</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Birth defects identified through blood test</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="birthdefectsbloodtest" id="birthdefectsbloodtest">
                        <option value=""></option>
                        <option value="IEM">IEM</option>
                		<option value="CH">CH</option>
                		<option value="CAH">CAH</option>
                		<option value="G6PD">G6PD</option>
                		<option value="Others">Others</option>
                		<option value="Haemoglobinopathy">Haemoglobinopathy</option>
                		<option value="Thalassemia">Thalassemia</option>
                		<option value="SCD">SCD</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
						</div>
						<!-- Congenital anomalies  started here -->		
						
						<!-- Heading 5 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Congenital anomalies</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
						<div class="row">
                        <div class="col-md-2">
                        <label>Congenital anomalies</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="congenitalanomalieschoice" id="congenitalanomalieschoice">
						<option value="no">No</option>
                		<option value="yes">Yes</option>
						</select>
            			</div>
                        </div>
						</div>
						<br>
						
						<div id="congenitalanomaliesdiv">
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Name</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <select class="form-control m-b" name="name" id="name">
                        <option value=""></option>
                		<option value="Congenital deafness">Congenital deafness</option>
						<option value="Congenital vision defect">Congenital vision defect</option>
						<option value="ROP">ROP</option>
						<option value="Congenital Heart Disease">Congenital Heart Disease</option>
						<option value="IEM">IEM</option>
						<option value="Haemoglobinopathy">Haemoglobinopathy</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Description of the anomaly</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="description" name="description" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Age at diagnosis</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <input type="text" id="ageat" name="ageat" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Code- ICD10</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="code" name="code" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Confirmed/Suspected</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <select class="form-control m-b" name="confirmmed" id="confirmmed">
                		<option value=""></option>
                		<option value="Confirmed">Confirmed</option>
						<option value="Suspected">Suspected</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        </div>		
						<!-- Congenital anomalies  ended here -->			
						
						
						<!-- Investigation   started here -->
						
						 
						
						
						<!-- Heading 5 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Investigation details</h4>
						</div>
						<br>
						<div class="row">
                        <div class="col-md-3">
                        <label>Investigation details</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <select class="form-control m-b" name="investigationchoice" id="investigationchoice">
                		<option value="no">No</option>
                		<option value="yes">Yes</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
						<div id="investigationdiv">
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Karotype/infantogram/ECG/US abdomen/Brain MRI</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <select class="form-control m-b" name="karotype" id="karotype">
                		<option value="N">N</option>
						<option value="ABN">ABN</option>
						<option value="Pending">Pending</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Findings</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="karotypefindings" name="karotypefindings" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>BERA, Fundoscopy etc</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <select class="form-control m-b" name="bera" id="bera">
                		<option value="N">N</option>
						<option value="ABN">ABN</option>
						<option value="Pending">Pending</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Findings</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="berafindings" name="berafindings" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Blood test for O CH</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="blodtestforch" id="blodtestforch">
                		<option value="N">N</option>
						<option value="ABN">ABN</option>
						<option value="Pending">Pending</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Blood Test Findings</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="bloodtestfindings" name="bloodtestfindings" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Blood test for O CAH</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="bloodtestforcah" id="bloodtestforcah">
                		<option value="N">N</option>
						<option value="ABN">ABN</option>
						<option value="Pending">Pending</option>
            			</select>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Blood test for G6PD</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="bloodtestforg6pd" id="bloodtestforg6pd">
                		<option value="N">N</option>
						<option value="ABN">ABN</option>
						<option value="Pending">Pending</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Blood test for SCD</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="bloodtestforscd" id="bloodtestforscd">
                		<option value="N">N</option>
						<option value="ABN">ABN</option>
						<option value="Pending">Pending</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
                        </div>						
						<!-- Investigation   ended here -->
						
						<!-- Diagnosis    started here -->
						
						<!-- Heading 4 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Diagnosis  details</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>anomaly </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="anomaly" name="anomaly" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>syndrome </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="syndrome" name="syndrome" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Diagnosis    started here -->
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Provisional diagnosis </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="provisional" name="provisional" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Complete diagnosis </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="complediagnosis" name="complediagnosis" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Notifying person </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="notifyingperson" name="notifyingperson" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Designation and contact details</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="designationofcontact" name="designationofcontact" class="form-control">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Facility referred </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="facilityreferred" name="facilityreferred" class="form-control">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Provisional diagnosis status</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <select class="form-control m-b" name="provisionaldiagstat" id="provisionaldiagstat">
						<option value=""></option>
						<option value="Confirmed">Confirmed</option>
						<option value="Suspected">Suspected</option>
            			</select>
            			</div>
                        </div>
                        </div>
                        <br>
						
						<!-- Diagnosis    ended here -->
						
                        <div>
                            <button class="btn btn-sm btn-primary m-t-n-xs demo1" type="submit" name="submitcustfeedback" id="submitcustfeedback"><strong>Submit</strong></button>
                        </div>
                    </form>                    

                       
                        
                        
                        

                    </div>
                </div>
            </div>
        </div>

</div>


 	
    
    <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->

</div>
<!-- Main Wrapper ended here -->
</div>


<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<!-- Vendor scripts ended here-->

<!-- App scripts started here-->
<script src="scripts/homer.js"></script>
<!-- App scripts ended here-->

<!-- Healtha Pinata scripts started here-->
<script src="scripts/custscripts/childbirth/birth.js"></script>


<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- Healtha Pinata scripts ended here-->



</body>
</html>