<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>RBSK</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">

</head>
<body>

<!-- Simple splash screen-->
<!-- <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p>A software to upload birth defects </p><img src="image/loading-bars.svg" width="64" height="64" /> </div> </div>
[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif] -->



<!-- Header started here-->
<%
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}
%>
 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->


<!-- Main Wrapper started here -->
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            <h4 class="font-light m-b-xs">
               Telangana Bala Swasthya Karyakram
            </h4>
            <small>Birth Defect Screening statistics.</small>
        </div>
    </div>
</div>

<div class="content animate-panel">

<div class="row">
     <div class="col-md-12">
                <div class="hpanel">
                 <div class="panel-heading">
                    <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                      &nbsp;
                    </div>
                    <div class="panel-body">
                     <div class="row">
                       <div class="col-lg-4 pull-right">
                       <input type="hidden" name="bid" id="bid"  class="form-control" value="<%=request.getParameter("bid") %>" >
			<button class="btn btn-warning2 pull-right " onclick="printsummary()" type="button" id="printbtn"><i class="fa fa-paste"></i>Print Birth Info</button>
			</div> 
					</div>
				
					<div class="row"  id="formdiv">
					
                        </div>	
               
                    </div>
                </div>
            </div>
        </div>                    
</div>
    <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->

</div>
<!-- Main Wrapper ended here -->



<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
 <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script> 
<script src="vendor/toastr/build/toastr.min.js"></script>

<!-- Vendor scripts ended here-->

<!-- App scripts started here-->
<script src="scripts/homer.js"></script>
<!-- App scripts ended here-->

<!-- Healtha Pinata scripts started here-->
<script src="scripts/custscripts/childbirth/printbirthinformation.js"></script>
<script src="scripts/custscripts/fieldvalidation.js"></script> 

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>

<!-- Healtha Pinata scripts ended here-->



</body>
</html>