<!DOCTYPE html>
<html>
<head>
    <!-- ==========================
    	Meta Tags 
    =========================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- ==========================
    	Title 
    =========================== -->
    <title>RBSK</title>
    <!-- <script src="https://www.google.com/recaptcha/api.js"></script> -->
   
    
    <!-- ==========================
    	Fonts 
    =========================== -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    
    <!-- ==========================
    	CSS 
    =========================== -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <link href="css/yamm.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="css/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="css/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="css/color-switcher.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/custom4rbsk.css" rel="stylesheet" type="text/css">
    
   
    
</head>
<body class="color-blue">
	
    <div id="page-wrapper" class="">
 

    <!-- ==========================
    	Header - START 
    =========================== -->
            <div class="row" style="height:100px;background-color:#FF69B4">
            	<div class="col-md-12">
            	<img src="image/rbsk/logo.jpg" width="140px;" height="100px;"/>
            	<font size="6" color="#ffffff"  style="text-align:center;"><b> Telangana Birth Defect E-Registry</b></font>
            	<img src="image/rbsk/nhm.jpg" width="220px;" height="100px;" align="right"/>
            	</div>
            	<!-- <div class="col-md-9" style="text-align:center;">
            		
                	<font color="#ffffff"><b> Telangana Birth Defect E-Registry</b></font>
                </div> -->
				<!-- <div class="col-md-2">
            	<img src="image/rbsk/nhm.jpg" width="220px;" height="100px;" align="right"/>
            	</div> -->
            </div>
    <!-- ==========================
    	Header - END 
    =========================== --> 

    <!-- ==========================
    	JUMBOTRON - START 
    =========================== -->
    <br>
    <div class="jumbotron jumbotron6">  
        <div id="jumbotron-slider" class="owl-carousel hidden-control owl-theme">       
            <div class="item" id="slide-2">
                	<div class="row">
                        <div class="col-md-12">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                        <h3>Project was launched on 15-Dec-2015 by Honorable Health Minister Laxma Reddy.</h3>
                        </div>
                    </div>
            </div>
            <div class="item" id="slide-1">
            	<div class="container">
                	<div class="row">
                        <div class="col-md-12">
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                         <h3>Sometimes Super heros reside in the heart's of small children fighting big battles.</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item" id="slide-3">
            	<div class="container">
                	<div class="row">
                        <div class="col-md-12">
                        <h2>I Know i am Somebody. 'Cause god dont make junk. So Please save me from defects.</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <!-- ==========================
    	JUMBOTRON - END 
    =========================== -->
          
    <!-- ==========================
    	Third Row - START 
    =========================== -->
    	
            	
        <div class="row" style="height:169px;padding-top: 7px;">
        		<div class="col-md-4">
        		
        		<img src="image/rbsk/telmap.png" height="350px;" width="380px;" align="right"/>
        		<font color="blue" size="4"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Programme Implementation Plan 2015-16</b></font>
        		<font color="blue" size="2"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Health, Medical & Family Welfare Department</b></font>
        		<font color="blue" size="2"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Government of Telangana State</b></font>
        		<font color="blue" size="2"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hyderabad</b></font>
            	</div>
            	<div class="col-md-4">
            	
        		<img src="image/rbsk/rbsk3.jpg" height="160px;" width="380px;" align="left" style="padding-bottom:20px;"/>
        	<br><br><br><br><br><br><br><br>
        	<div class="features-item" id="features-owl-carousel" style="width:790px;padding-left:40px;padding-top:40px;">
                            <div id="carousel-wrapper" class="owl-carousel owl-theme">       
                                <div class="item"><img src="image/rbsk/rbsk.png" alt="" class="img-responsive"></div>
                                <div class="item"><img src="image/rbsk/rbskcarosel1.jpg" alt="" class="img-responsive"></div>
                                <div class="item"><img src="image/rbsk/rbskcarosel2.jpg" alt="" class="img-responsive"></div>
                                <div class="item"><img src="image/rbsk/rbskcarosel3.jpg" alt="" class="img-responsive"></div>
                                <div class="item"><img src="image/rbsk/rbskcarosel4.jpg" alt="" class="img-responsive"></div>
                                
                            </div>
                        </div>
        	   	</div>
            	         	<div class="col-md-3" style="background-color:#00334d;border-radius: 15px;padding-bottom:10px;">
            			<form action="/SignIn" id="loginForm" name="loginForm" method="post">
            				
                    		<div class="row">
                    		<%if(request.getAttribute("loginError")!=null){ %>
                    		<font color="red" size="3"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= request.getAttribute("loginError") %></b></font>
                    		<%} %>
                        	<br>
                        	
                        	<div class="form-group col-md-5"><label><span>Username</span></label></div>
                        	<div class="form-group col-md-6"><input type="text" name="username" id="username"  Placeholder="Enter User Name."/></div>
                    		</div>
                   			<!--  -->
                    		<div class="row">
                         	<div class="form-group col-md-5"><label><span>Password</span></label></div>
                            <div class="form-group col-md-6"><input type="password" name="password" id="password" Placeholder="Enter Password."/>
                            </div>
                            </div>
                            <div  class="row">
                            
                           <div class="col-md-5"> 
                           <div class="input-group">
                           <img src="/CaptchaServlet" height="40" width="100" id="captchaimg" name="captchaimg"> 
                           <span class="input-group-addon"><a href="#">
                           <i class="fa fa-refresh"  id="refreshcaptcha" ></i></a></span>
                           </div>
                          <!--   <button type="button" class="btn btn-xs" id="refreshcaptcha" name="refreshcaptcha">
                                <span class="glyphicon glyphicon-refresh" aria-hidden="true" ></span>
                               </button> --> 
                               </div>
                               <div class="form-group col-md-5">
                               <input type="text" name="captchatxt" id="captchatxt" Placeholder="Enter Image Text."/>
                               <br>  <button class="btn btn-warning" type="submit" id="loginbtn">&nbsp;&nbsp;&nbsp;&nbsp;Login&nbsp;</button>
							
                               </div>
                            
                            </div>
            		</form>
            	</div>
            	<div class="col-md-1">
            	 
            	</div>
            </div>
    <!-- ==========================
    	Third Row  - END 
    =========================== -->
     <!-- ==========================
    	Fourth Row  - Start 
    =========================== -->
   
     <!-- ==========================
    	Footer Start
    =========================== -->
            <div class="row" >
            	
            	
            </div>
            
    <!-- ==========================
    	Footer End 
    =========================== --> 
   
   
    </div>
   <%@ include file="footer.jsp" %>
   	<!-- ==========================
    	JS 
    =========================== -->        
	<script src="js/jquery-latest.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/creative-brands.js"></script>
    <script src="js/color-switcher.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/validator.min.js"></script>
    
    <script src="scripts/custscripts/fieldvalidation.js"></script>
    
    
    <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
</body>
</html>