/**
 * 
 */
 $("#submitcustfeedback").click(function(e){
  console.log($(this).serializeArray());
  $('#submitcustfeedback').attr('disabled',true);
  
  
  $.ajax({
   type: 'post',
   url: '/UploadFeedBackForm',
   data: $("#custfeedbackform").serializeArray(),
   success: function (data) {
    // dialog started here
    bootbox.dialog({
        message: data,
        className: "my-custom-class",
        title: "Confirmation",
        });
    // dialog ended here
    $('#custfeedbackform').trigger("reset");

   },
   error: function(request, status, message) {
     console.log("error occourred in addmember servlet call :");
     console.log(message);
     console.log(request.responseText);
     bootbox.dialog({
     message: request.responseText,
     className: "my-custom-class",
     title: "Error",
     });
   }
  });
  //ajax call ended here
  $('#submitcustfeedback').removeAttr('disabled');
  
  }); 
 //ajax call ended here