$("#leavemsgbtn").click(function(e){
	
	$("#nameerr").hide();
	$("#emailerr").hide();
	$("#megerr").hide();
	$("#successmsg").hide();
	$("#feederrormsg").hide();
	$("#custname").val("");
	$("#custemailid").val("");
	$("#rating").val("");
	$("#custfeedbackmsg").val("");
});
$("#custname").change(function(e){
	$("#successmsg").hide();
	$("#feederrormsg").hide();
});
$("#emailerr").change(function(e){
	$("#successmsg").hide();
	$("#feederrormsg").hide();
});
$("#custfeedbackmsg").change(function(e){
	$("#successmsg").hide();
	$("#feederrormsg").hide();
});

$("#submitcustfeedback").click(function(e){
	 var emailid= $("#custemailid").val();
	 var name = $("#custname").val();
	 var rating = $("#rating").val();
	 var message = $("#custfeedbackmsg").val();
	 var errfound = false;
     
     if(typeof name == undefined || name.trim()=="")
     {
    	 $("#nameerr").show();
    	 errfound=true;
     }
     if(typeof name != undefined && name.trim()!="")
     {
    	    var alphabets=  /^[a-zA-Z ]*$/; 
    	    if(alphabets.test(name))   
    		{   
    			$("#nameerr").hide();
    		}  
    	    else 
    		{   
    			$("#nameerr").show();
    			errfound=true;
    		}
    	 
     }
     
     if(typeof emailid == undefined || emailid.trim()=="")
     {
    	 $("#emailerr").show();
    	 errfound=true;
     }
     if(typeof emailid != undefined && emailid.trim()!="")
     {
    	    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    		if (filter.test(emailid)) {
    		$("#emailerr").hide();
    		}
    		else {
    		$("#emailerr").show();
    		errfound=true;
    		}
    	 
     }
     
     if((typeof rating == undefined || rating.trim()=="" || rating==0) && (typeof message == undefined || message.trim()==""))
     {
    	 $("#megerr").show();
    	 errfound=true;
     }
     if((typeof rating != undefined && rating.trim()!="" || rating!=0) || (typeof message != undefined && message.trim()!=""))
     {
    	 $("#megerr").hide();
     }
     if(errfound)
     {
    	 e.preventDefault();
    	 return false;
     }
    	 
  console.log($(this).serializeArray());
  $('#submitcustfeedback').attr('disabled','disabled');
  
  
  $.ajax({
   type: 'post',
   url: '/UploadFeedBackForm',
   data: $("#custfeedbackform").serializeArray(),
   success: function (data) {
    $("#successmsg").show();
    $("#custname").val("");
	$("#custemailid").val("");
	$("#rating").val("");
	$("#custfeedbackmsg").val("");
	 $('#submitcustfeedback').removeAttr('disabled');
   },
   error: function(request, status, message) {
     console.log("error occourred in addmember servlet call :");
     console.log(message);
     console.log(request.responseText);
     $("#feederrormsg").show();
     $('#submitcustfeedback').removeAttr('disabled');
   }
  });
  //ajax call ended here
 
  }); 
 //ajax call ended here
 



