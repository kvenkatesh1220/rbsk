/* ==============================================
Google Map
=============================================== */
function initialize() {
	
	"use strict";
	
	var mapProp = {
  		center:new google.maps.LatLng(40.758440, -73.985186), 		// <- Your LatLng
  		zoom:16,
		scrollwheel: false,
  		mapTypeId:google.maps.MapTypeId.ROADMAP
  	};
	var map = new google.maps.Map(document.getElementById("map"),mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);

/* ==============================================
jQuery
=============================================== */
$(document).ready(function () {
	
	"use strict";
	
	// ascensor initialize
	//$('#ascensor').ascensor({ascensorMap: [[1,1],[0,0],[0,1],[0,2],[1,2],[1,0],[2,0],[2,1],[2,2]]});					 // Ascensor
	//$('#ascensor').ascensor({ascensorMap: [[1,1],[0,0],[0,1],[0,2],[1,2],[1,0],[2,0],[2,1],[2,2]], queued: true});	// Ascensor Queued
	$('#ascensor').ascensor({ascensorMap: [[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],[0,9]]}); 					// Horizontal
	//$('#ascensor').ascensor({ascensorMap: [[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0]]});					// Vertical
	
	// load tweets
	$(".follow .load-tweets").load("php/twitter.php");
	
	$("[href='#']").click(function(e){
		e.preventDefault();
	});
	
	// show, hide navbar
	$(".tile").click(function(){
		$(".navbar").animate({bottom:0},"slow");
	}); 
	$(".home-link").click(function(){
		$(".navbar").animate({bottom:-50},"slow");
	}); 

	// navbar-callapse close on click
	$('.navbar li a').on('click',function(){
		if ( $('.navbar-collapse').hasClass("in") ) {
			$('.navbar-collapse').collapse('hide');
		}
	})
	
	$("#about-carousel").carousel({interval: 2000});
	
	// portfolio
	$("#grid").mixitup();
	
	// portfolio hover
	$("#grid li a ").each(function() { 
		$(this).hoverdir(); 
	});

	// center box
	function centerBox(){
		
		"use strict";
		
		var wHeight = $(window).height() ;
		$(".section .center-box").each(function() {
			var paddingTop =  $(this).height() ;
			if ( paddingTop < wHeight ) {
				paddingTop = ( wHeight - paddingTop ) / 2;
				$(this).css("padding-top",paddingTop);
			} else {
				$(this).css("padding-top","0");
			}
		});
	};
	
	$(window).resize(function(){
		centerBox();
	}).resize();

	// contact form
	$('input, textarea').placeholder();
	
	$('#contactform').submit(function(){
	
		"use strict";
		
		var action = $(this).attr('action');
		
		$("#state-message").slideUp(750,function() {
		$('#state-message').hide();
		
		$.post(action, { 
			name: $('#name').val(),
			email: $('#email').val(),
			message: $('#message').val()
		},
			function(data){
				document.getElementById('state-message').innerHTML = data;
				$('#state-message').slideDown('slow');
				$('#contactform img.loader').fadeOut('slow',function(){$(this).remove()});
				$('#submit').removeAttr('disabled'); 
				if(data.match('success') != null) $('#contactform').slideUp('slow');
			}
		);
		});
		return false; 
	});

});

/* ==============================================
Testimonials
=============================================== */

jQuery(function( $ ){
	
	"use strict";
	
	var randomnumber, quoteclass, author, timeout;
	
	startTestimonials();
	
	$('.client .photos ul li').hover( function(){

		window.clearTimeout(timeout);
		
		$('.client .photos ul li.active').removeClass('active');
		
		quoteclass = $(this).attr('class');
		
		author = $(this).find('img').attr('alt');
		author = author.split('-');
		author = author[0] + '<span> - ' + author[1] + '</span>';
		
		$('.client .quotes ul li.active').fadeOut('slow', function(){
			$(this).removeClass('active');
			$('.client .quotes ul li.' + quoteclass).fadeIn().addClass('active');
			$('.client .photos .author').html(author);
		});
		
		$(this).addClass('active');
		
	}, function(){
		timeout = window.setTimeout( startTestimonials, 5000 );
		return false;
	});
	
	function startTestimonials() {
		
		"use strict";
		
		$('.client .photos ul li.active').removeClass('active');
		
		randomnumber = Math.floor( (Math.random()*6) + 1 );
		
		author = $('.client .photos ul li.quote-1').find('img').attr('alt');
		author = author.split('-');
		author = author[0] + '<span> - ' + author[1] + '</span>';
		
		$('.client .quotes ul li.active').fadeOut('slow', function(){
			$(this).removeClass('active');
			$('.client .quotes ul li.quote-1' ).fadeIn().addClass('active');
			$('.client .photos .author').html(author);
		});
		
		$('.client .photos ul li.quote-1').addClass('active');
		
		
	}
	
});


/* ==============================================
Loading
=============================================== */
$(window).load(function(){
	"use strict";
	jQuery('#loading').fadeOut(100);
	
	$("#dropzonearea").hide(); // hiding dropzone area intially to select report type and report date
	// Adding data to Family members module started here	
	
	//// settting date fields on page load started here
    $('#dateofbirth').datepicker({
        format: "dd/mm/yyyy" ,
        todayBtn: "linked" ,
        endDate: '0'
    }); 
    //
        $('#lastdocvisit').datepicker({
            format: "dd/mm/yyyy" ,
            todayBtn: "linked" ,
            endDate: '0'
    });  
    //
        $('#insurancedate').datepicker({
            format: "dd/mm/yyyy" ,
            todayBtn: "linked" ,
    });  
    //
        $('#editdateofbirth').datepicker({
            format: "dd-M-yyyy" ,
            todayBtn: "linked" ,
            endDate: '0'
    });  
    //
        $('#editlastdocvisit').datepicker({
            format: "dd-M-yyyy" ,
            todayBtn: "linked" ,
            endDate: '0'
    });  
    //
        $('#editinsurancedate').datepicker({
            format: "dd-M-yyyy" ,
            todayBtn: "linked" ,
    });  
	//// settting date fields on page load ended here
	$.ajax({
		url: "FamilyMembers",
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		mimeType: 'application/json',
		success: function (data) {
			
			/////
			var i=1;
			
			
				  $.each(data, function(key,value) { 
					  var  profilepicsrc="http://94.228.219.195/userprofilepics/"+value['cust_id']+"_"+value['memberid']+"/"+value['imagename'];
					  var  membername=value['membername'];
				       var  dob=value['dob'];
				       var  bloodgroup=value['bloodgroup'];
				       var  healthstatus=value['healthstatus'];
				       var  emergencynumber=value['emergencynumber'];
				       var  lastdocvisit=value['lastdocvisit'];
				       var  insurancedate=value['insurancedate'];
				       
				       if(value['imagename']===undefined || value['imagename']==null)
				       {
				        profilepicsrc="image/emptyprofile.png";    
				       }
				       if(value['membername']===undefined || value['membername']==null)
				       {
				        membername="";    
				       }
				       
				       if(value['dob']===undefined || value['dob']==null)
				       {
				        dob="";    
				       }
				       
				       if(value['bloodgroup']===undefined || value['bloodgroup']==null)
				       {
				        bloodgroup="";    
				       }
				       
				       if(value['healthstatus']===undefined || value['healthstatus']==null)
				       {
				        healthstatus="";    
				       }
				       
				       if(value['emergencynumber']===undefined || value['emergencynumber']==null)
				       {
				        emergencynumber="";    
				       }
				       
				       if(value['lastdocvisit']===undefined || value['lastdocvisit']==null)
				       {
				        lastdocvisit="";    
				       }
				       
				       if(value['insurancedate']===undefined || value['insurancedate']==null)
				       {
				        insurancedate="";    
				       }
				       
				       // Variable initialization started here
				       // row one started here
				       if(i<7)
				      {
				       $("#familymembers1").append("<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\" id=\"familymemcol"+value['memberid']+"\">" +
				           "<div class=\"team-member\" id=\"familymember"+value['memberid']+"\">" +
				           "<img src=\""+profilepicsrc+"\" alt=\"team_member\">" +
				           "<h5 class=\"h5\">Name : "+membername+"</h5>" +
				           "<h5 class=\"h5\">Dob : "+dob+"</h5>" +
				           "<h5 class=\"h5\">Blood Group : "+bloodgroup+"</h5>" +
				           "<h5 class=\"h5\">Health : "+healthstatus+"</h5>" +
				           "<h5 class=\"h5\">Emergency Contact : </h5>" +
				           "<h5 class=\"h5\">"+emergencynumber+"</h5>" +
				           "<h5 class=\"h5\">Last Doctor Visit : </h5>" +
				           "<h5 class=\"h5\">"+lastdocvisit+"</h5>" +
				           "<h5 class=\"h5\">Insurance Date : </h5>" +
				           "<h5 class=\"h5\">"+insurancedate+"</h5>" +
				           "</div>" +
				           "</div>");       
				       console.log(value['membername']);
				       console.log(key);					  
					  
					 }
					  // row one ended here
					  
					  // Adding data to Delete members module started here
					  if(i!=1)
					 {
					  
					  $("#deletemembers1").append("<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\" id=\"deletememcol"+value['memberid']+"\">" +
						  		"<div class=\"delete-member\" id=\"deletemember"+value['memberid']+"\">" +
						  		"<img src=\""+profilepicsrc+"\" alt=\"team_member\">" +
						  		"<h5 class=\"h5\"><a href=\"javascript:deletefamilymember('deletelink',"+value['memberid']+")\" id=\"deletelink"+value['memberid']+"\">Delete&nbsp;"+value['membername']+"</a></h5>" +
						  		"</div>" +
						  		"</div>");						 
					  console.log(value['membername']);
					  console.log(key);					  
					  }

					 // Adding data to Delete members module ended here

					 ////// adding options to membernametoget field in edit member form started here
					 if(i<7)
					 {
					 $("#membernametoget").append( // Append an object to the inside of the select box
					            $("<option></option>") // Yes you can do this.
					                .text(value['membername'])
					                .val(value['memberid'])
					        );
					 }
					////// adding options to membernametoget field in edit member form ended here
					 if(i==1)
					 {
						 $("#myhealthdata").append("<h5>Name:"+membername+"</h5> " +
							      "  <h5>Dob:"+dob+"</h5>" +
							      "<h5>Emergency Contact:"+emergencynumber+"</h5>" +
							      "<h5>Last Doctor Visit:"+lastdocvisit+"</h5>" +
							      "<h5>Insurance Due Date:"+ insurancedate+"</h5>");
						 
						  $("#myhealthpic").append("<img src=\""+profilepicsrc+"\" style=\"max-height:100%;max-width:100%; vertical-align:bottom;\">");
					 }

					 i++;
				  });
				  
			
			////
				  // Adding data to Family members module ended here	
				  
	}
});
	$('#myhealthprevent').click();
//
	
//
});