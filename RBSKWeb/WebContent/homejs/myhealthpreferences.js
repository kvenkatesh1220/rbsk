//myhealthmenu code
// myhealthmenu,myhealthtile have same code

$("#myhealthmenu").click(function(e){
  $.ajax({
   type: 'post',
   url: '/FamilyMembers',
   dataType: 'json',
    success: function (data) {
	   console.log("success returned from servlet GetMyHealthPreferences");
	   var i=1;
	   $.each(data, function(key,value) {
		   if(i==1)
		   {		   
		   var membername=value['membername']
		   var  dob=value['dob'];
		   var  emergencynumber=value['emergencynumber'];
		   var  lastdocvisit=value['lastdocvisit'];
		   var  insurancedate=value['insurancedate'];
		   var  profilepicsrc="http://94.228.219.195/userprofilepics/"+value['cust_id']+"_"+value['memberid']+"/"+value['imagename'];
		   
		   if(value['imagename']===undefined || value['imagename']==null)
	       {
	        profilepicsrc="image/emptyprofile.png";    
	       }
		   if(value['membername']===undefined || value['membername']==null)
		   {
			   membername="";    
		   }
		   
		   if(value['dob']===undefined || value['dob']==null)
		   {
			   dob="";    
		   }
		   
		   if(value['emergencynumber']===undefined || value['emergencynumber']==null)
		   {
			   emergencynumber="";    
		   }
		   
		   if(value['lastdocvisit']===undefined || value['lastdocvisit']==null)
		   {
			   lastdocvisit="";    
		   }
		   
		   if(value['insurancedate']===undefined || value['insurancedate']==null)
		   {
			   insurancedate="";    
		   }
		   
		   if(value['imagename']===undefined || value['imagename']==null)
		   {
			   imagename="";    
		   }
		   	$("#myhealthdata").empty();
			 $("#myhealthdata").append("<h5>Name:"+membername+"</h5> " +
				      "  <h5>Dob:"+dob+"</h5>" +
				      "<h5>Emergency Contact:"+emergencynumber+"</h5>" +
				      "<h5>Last Doctor Visit:"+lastdocvisit+"</h5>" +
				      "<h5>Insurance Due Date:"+ insurancedate+"</h5>");
			 $("#myhealthpic").empty();
			  $("#myhealthpic").append("<img src=\""+profilepicsrc+"\" style=\"max-height:100%;max-width:100%; vertical-align:bottom;\">");
		   }
	   });
	  },
   error: function(request, status, message) {
     console.log("error occourred in GetMyHealthPreferences servlet call :");
     console.log(message);
     console.log(request.responseText);
     bootbox.dialog({
     message: request.responseText,
     title: "Error",
     });
   }
  });
  //ajax call ended here
    }); 
 //ajax call ended here
 
 // myhealth Tile code
 
 
 $("#myhealthtile").click(function(e){
	  $.ajax({
	   type: 'post',
	   url: '/FamilyMembers',
	   dataType: 'json',
	    success: function (data) {
		   console.log("success returned from servlet GetMyHealthPreferences");
		   var i=1;
		   $.each(data, function(key,value) {
			   if(i==1)
			   {
			   var membername=value['membername']
			   var  dob=value['dob'];
			   var  emergencynumber=value['emergencynumber'];
			   var  lastdocvisit=value['lastdocvisit'];
			   var  insurancedate=value['insurancedate'];
			   var  profilepicsrc="http://94.228.219.195/userprofilepics/"+value['cust_id']+"_"+value['memberid']+"/"+value['imagename'];
			   
			   if(value['imagename']===undefined || value['imagename']==null)
		       {
		        profilepicsrc="image/emptyprofile.png";    
		       }
			   if(value['membername']===undefined || value['membername']==null)
			   {
				   membername="";    
			   }
			   
			   if(value['dob']===undefined || value['dob']==null)
			   {
				   dob="";    
			   }
			   
			   if(value['emergencynumber']===undefined || value['emergencynumber']==null)
			   {
				   emergencynumber="";    
			   }
			   
			   if(value['lastdocvisit']===undefined || value['lastdocvisit']==null)
			   {
				   lastdocvisit="";    
			   }
			   
			   if(value['insurancedate']===undefined || value['insurancedate']==null)
			   {
				   insurancedate="";    
			   }
			   
			   if(value['imagename']===undefined || value['imagename']==null)
			   {
				   imagename="";    
			   }
				$("#myhealthdata").empty();
				 $("#myhealthdata").append("<h5>Name:"+membername+"</h5> " +
					      "  <h5>Dob:"+dob+"</h5>" +
					      "<h5>Emergency Contact:"+emergencynumber+"</h5>" +
					      "<h5>Last Doctor Visit:"+lastdocvisit+"</h5>" +
					      "<h5>Insurance Due Date:"+ insurancedate+"</h5>");
				 $("#myhealthpic").empty();
				  $("#myhealthpic").append("<img src=\""+profilepicsrc+"\" style=\"max-height:100%;max-width:100%; vertical-align:bottom;\">");
			   }
		   });
		  },
	   error: function(request, status, message) {
	     console.log("error occourred in GetMyHealthPreferences servlet call :");
	     console.log(message);
	     console.log(request.responseText);
	     bootbox.dialog({
	     message: request.responseText,
	     title: "Error",
	     });
	   }
	  });
	  //ajax call ended here
	    }); 
	 //ajax call ended here