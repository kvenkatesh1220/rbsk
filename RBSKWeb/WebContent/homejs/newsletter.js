/**
 * 
 */

$("#preventioncustomnewsletter").click(function(){
	$("#latestnewsletterslist").empty();	
	var newletterstring="";
	$.ajax({
		url: "GetLatestNewsletters", 
		type: 'POST',
		success: function (data) {
				var i=1;
			 $.each(data, function(key,value) {
				 
				 var mainhead=value['mainhead'];
				 var postdate=value['postdate'];
				 var maindesc=value['maindesc'];
				 var postid=value['postid'];
				 
				 
				 if(value['mainhead']===undefined || value['mainhead']==null)
		            {
					 mainhead="";    
		            }
				 if(value['postdate']===undefined || value['postdate']==null)
		            {
					 postdate="";    
		            }
				 if(value['maindesc']===undefined || value['maindesc']==null)
		            {
					 maindesc="";    
		            }
				 if(value['postid']===undefined || value['postid']==null)
		            {
					 postid="";    
		            }
				
				 
				 if(i==1)
				 {
				 newletterstring=newletterstring+"<div class=\"row\">";
				 }
				 newletterstring=newletterstring+"<div class=\"col-sm-6 col-md-6 col-lg-6 blog-post\">";
				 newletterstring=newletterstring+"<article class=\"article\">";
				 newletterstring=newletterstring+"<div class=\"article-media\">";
				 newletterstring=newletterstring+"<a href=\"#\"><img class=\"img-responsive\" src=\""+value['mainimage']+"\" alt=\"blog-article\"></a>";
				 newletterstring=newletterstring+"</div>";
				 newletterstring=newletterstring+"<div class=\"article-body\">";
				 newletterstring=newletterstring+"<h4 class=\"h4 article-title\"><a href=\"#\" id=\"newslettertitle1\">"+mainhead+"</a></h4>";
				 newletterstring=newletterstring+"<div class=\"article-tag\"><i class=\"fa fa-tag\"></i><a href=\"#\">Publish Date : "+postdate+"</a>";
				 newletterstring=newletterstring+"</div>";
				 newletterstring=newletterstring+"<p>"+maindesc+"</p>";
				 newletterstring=newletterstring+"<div class=\"read-more\"><a href=\"/CustomNewsLetter?newsletterpostid="+postid+"\" target=\"_blank\" >Read More</a>";
				 newletterstring=newletterstring+"</div></div></article></div>";
				 console.log(postid+"-"+mainhead+"-"+maindesc+"-"+maindesc+"-"+value['mainimage']+"-"+postdate+"-"+value['categeoryname']+"-"+value['articlename']);
				 if(i==2)
				 {
				 newletterstring=newletterstring+"</div>";
				 i=1;
				 }
				 i=i+1;
			 });
			 $("#latestnewsletterslist").append(newletterstring);
			 console.log(newletterstring);
			 
		},
		error: function(request, status, message) {
		console.log("error occourred in GetNewsLetterAlerts servlet call :");
		console.log(message);
		console.log(request.responseText);
		bootbox.dialog({
		message: request.responseText,
		className: "my-custom-class",
		title: "Error",
		});
		}
   });	
	
});

