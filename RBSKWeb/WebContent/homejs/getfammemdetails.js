/**
 * 
 */
$("#getmemberdetails").click(function(){
  
  var member_id = $("#membernametoget").val();
  $("#getmemberdetails").attr('disabled',true);
 $.ajax({
  url: "GetMemberDetails",
  type: 'POST',
  dataType: 'json',
  data: { memberid: member_id },
  success: function (data) {
   console.log("success returned from servlet GetMemberDetails");
   $.each(data, function(key,value) {
	   var  relationship=value['relationship'];
	   var  dob=value['dob'];
	   var  bloodgroup=value['bloodgroup'];
	   var  emergencynumber=value['emergencynumber'];
	   var  lastdocvisit=value['lastdocvisit'];
	   var  insurancedate=value['insurancedate'];
	   if(value['relationship']===undefined || value['relationship']==null)
	   {
	    relationship="";    
	   }
	   
	   if(value['dob']===undefined || value['dob']==null)
	   {
	    dob="";    
	   }
	   
	   if(value['bloodgroup']===undefined || value['bloodgroup']==null)
	   {
	    bloodgroup="";    
	   }
	   
	   if(value['emergencynumber']===undefined || value['emergencynumber']==null)
	   {
	    emergencynumber="";    
	   }
	   
	   if(value['lastdocvisit']===undefined || value['lastdocvisit']==null)
	   {
	    lastdocvisit="";    
	   }
	   
	   if(value['insurancedate']===undefined || value['insurancedate']==null)
	   {
	    insurancedate="";    
	   }
	   
	    $("#editrelationship").val(relationship);
	    $("#editbloodgroup").val(bloodgroup);
	    $("#editemergencynumber").val(emergencynumber);
	    $("#editdateofbirth").val(dob);
	    $("#editlastdocvisit").val(lastdocvisit);
	    $("#editinsurancedate").val(insurancedate);
   });
  },
  error: function(request, status, message) {
   console.log("error occourred in deletemember servlet call :");
   console.log(message);
   console.log(request.responseText);
   bootbox.dialog({
    message: request.responseText,
    className: "my-custom-class",
    title: "Error",
   });
  }
 });
 $('#getmemberdetails').removeAttr('disabled');
});

