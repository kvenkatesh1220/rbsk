/**
 * 
 */
///// generating family member preferences tabs started here Note familypreferences1, familypreferences2 must have same data
$("#familypreferences1").click(function(){
	$("#familymemberperf1").hide();
	$("#familymemberperf2").hide();
	$("#familymemberperf3").hide();
	$("#familymemberperf4").hide();
	$("#familymemberperf5").hide();
	$("#familymemberperf6").hide();
	//$('#familymemberprefsec').empty();
	$.ajax({
		url: "GetFamilyMemberNames",
		type: 'POST',
		dataType: 'json',
		
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			$.each(data, function(key,value) {
				console.log(value['membername']);
				if(i==1)
				{
				$("#familymemberperf1").text(value['membername']);
				$("#familymemberperf1").show();
				}
				if(i==2)
				{
				$("#familymemberperf2").text(value['membername']);
				$("#familymemberperf2").show();
				}
				if(i==3)
				{
				$("#familymemberperf3").text(value['membername']);
				$("#familymemberperf3").show();
				}
				if(i==4)
				{
				$("#familymemberperf4").text(value['membername']);
				$("#familymemberperf5").show();
				}
				if(i==5)
				{
				$("#familymemberperf5").text(value['membername']);
				$("#familymemberperf5").show();
				}
				if(i==6)
				{
				$("#familymemberperf6").text(value['membername']);
				$("#familymemberperf6").show();
				}
				i=i+1;
			});

		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}			
	
});

});
///// generating family member preferences tabs started here Note familypreferences1, familypreferences2 must have same data
$("#familypreferences2").click(function(){
	$("#familymemberperf1").hide();
	$("#familymemberperf2").hide();
	$("#familymemberperf3").hide();
	$("#familymemberperf4").hide();
	$("#familymemberperf5").hide();
	$("#familymemberperf6").hide();
	//$('#familymemberprefsec').empty();
	$.ajax({
		url: "GetFamilyMemberNames",
		type: 'POST',
		dataType: 'json',
		
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			$.each(data, function(key,value) {
				console.log(value['membername']);
				if(i==1)
				{
				$("#familymemberperf1").text(value['membername']);
				$("#familymemberperf1").show();
				}
				if(i==2)
				{
				$("#familymemberperf2").text(value['membername']);
				$("#familymemberperf2").show();
				}
				if(i==3)
				{
				$("#familymemberperf3").text(value['membername']);
				$("#familymemberperf3").show();
				}
				if(i==4)
				{
				$("#familymemberperf4").text(value['membername']);
				$("#familymemberperf5").show();
				}
				if(i==5)
				{
				$("#familymemberperf5").text(value['membername']);
				$("#familymemberperf5").show();
				}
				if(i==6)
				{
				$("#familymemberperf6").text(value['membername']);
				$("#familymemberperf6").show();
				}
				i=i+1;
			});

		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}			
	
});

});
///// generating family member preferences tabs ended here

///// generating form data for familymemberperf1 started here Note same code sud be there for familymemberperf1 to familymemberperf6
$("#familymemberperf1").click(function(){
	$('#familymemberprefsec').empty(); // empty current form data of familymemberprefsec 
	var alertcategory=null;
	var familyperfformpart="";
	
	$.ajax({
		url: "GetCustomerHealthAlertsPerf",
		type: 'POST',
		dataType: 'json',
		data:{membername:$('#familymemberperf1').text()},
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			var k=1;
			$.each(data, function(key,value) {
			console.log("alert_category :"+value['alert_category']+" alert_name :"+value['alert_name']+" alert_status:"+value['alert_status']+" memberid :"+value['memberid']);
			console.log("tempalert_category :"+alertcategory);	
			if(alertcategory!=value['alert_category'])
				{
					if(k>1 && k<=4)
					{
					familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
					}	
					console.log("entered into new alert_category :"+value['alert_category']);
					familyperfformpart=familyperfformpart+"<div class=\"row\">" +
														  "<div class=\"form-group col-lg-1\" ></div>" +
														  "<div class=\"form-group col-lg-4\" ><h2>&nbsp;"+value['alert_category']+"</h2></div>" +
														  "</div>";
					alertcategory=value['alert_category'];
					console.log("k:"+k);
					
					k=1;
				}
				if(k==1)
				{
				familyperfformpart=familyperfformpart+"<div class=\"row\"><div class=\"form-group col-lg-1\" ></div>"; //starting row of alerts
				}
				familyperfformpart=familyperfformpart+"<input type=\"hidden\" name=\"healthalertcategory"+i+"\" value=\""+value['alert_category']+"\"></input>"+
				"<input type=\"hidden\" name=\"memberid"+i+"\" value=\""+value['memberid']+"\"></input>"+				
				"<div class=\"form-group col-lg-2\" >";
				if(value['alert_status']=='Y')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\" checked>"+value['alert_name']+"</label>";
				}
				if(value['alert_status']=='N')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\">"+value['alert_name']+"</label>";
				}
				familyperfformpart=familyperfformpart+"</div>";
				if(k==4)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				k=0;
				}
				i=i+1;
				k=k+1;
				});
				if(k!=1)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				}
				familyperfformpart=familyperfformpart+"<div class=\"row\">"+
				"<div class=\"form-group col-lg-1\" ></div>"+
				"<div class=\"form-group col-lg-2\" >"+
				"<div class=\"form-group\">"+
				"&nbsp;&nbsp;<button  type=\"button\" id=\"savepreferences\" class=\"btn btn-info btn-block\">Save Preferences</button>"+
				"</div></div></div><br><br><br><br><br>";
				console.log(familyperfformpart);
				$('#familymemberprefsec').append(familyperfformpart);
		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}
	});

});

///// generating form data for familymemberperf1 started here Note same code sud be there for familymemberperf1 to familymemberperf6
$("#familymemberperf2").click(function(){
	$('#familymemberprefsec').empty(); // empty current form data of familymemberprefsec 
	var alertcategory=null;
	var familyperfformpart="";
	
	$.ajax({
		url: "GetCustomerHealthAlertsPerf",
		type: 'POST',
		dataType: 'json',
		data:{membername:$('#familymemberperf2').text()},
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			var k=1;
			$.each(data, function(key,value) {
			console.log("alert_category :"+value['alert_category']+" alert_name :"+value['alert_name']+" alert_status:"+value['alert_status']+" memberid :"+value['memberid']);
			console.log("tempalert_category :"+alertcategory);	
			if(alertcategory!=value['alert_category'])
				{
					if(k>=1 && k<=4)
					{
					familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
					}	
					console.log("entered into new alert_category :"+value['alert_category']);
					familyperfformpart=familyperfformpart+"<div class=\"row\">" +
														  "<div class=\"form-group col-lg-1\" ></div>" +
														  "<div class=\"form-group col-lg-4\" ><h2>&nbsp;"+value['alert_category']+"</h2></div>" +
														  "</div>";
					alertcategory=value['alert_category'];
					console.log("k:"+k);
					
					k=1;
				}
				if(k==1)
				{
				familyperfformpart=familyperfformpart+"<div class=\"row\"><div class=\"form-group col-lg-1\" ></div>"; //starting row of alerts
				}
				familyperfformpart=familyperfformpart+"<input type=\"hidden\" name=\"healthalertcategory"+i+"\" value=\""+value['alert_category']+"\"></input>"+
				"<input type=\"hidden\" name=\"memberid"+i+"\" value=\""+value['memberid']+"\"></input>"+				
				"<div class=\"form-group col-lg-2\" >";
				if(value['alert_status']=='Y')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\" checked>"+value['alert_name']+"</label>";
				}
				if(value['alert_status']=='N')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\">"+value['alert_name']+"</label>";
				}
				familyperfformpart=familyperfformpart+"</div>";
				if(k==4)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				k=0;
				}
				i=i+1;
				k=k+1;
				});
				if(k!=1)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				}
				familyperfformpart=familyperfformpart+"<div class=\"row\">"+
				"<div class=\"form-group col-lg-1\" ></div>"+
				"<div class=\"form-group col-lg-2\" >"+
				"<div class=\"form-group\">"+
				"&nbsp;&nbsp;<button  type=\"button\" id=\"savepreferences\" class=\"btn btn-info btn-block\">Save Preferences</button>"+
				"</div></div></div><br><br><br><br><br>";
				console.log(familyperfformpart);
				$('#familymemberprefsec').append(familyperfformpart);
		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}
	});

});

///// generating form data for familymemberperf1 started here Note same code sud be there for familymemberperf1 to familymemberperf6
$("#familymemberperf3").click(function(){
	$('#familymemberprefsec').empty(); // empty current form data of familymemberprefsec 
	var alertcategory=null;
	var familyperfformpart="";
	
	$.ajax({
		url: "GetCustomerHealthAlertsPerf",
		type: 'POST',
		dataType: 'json',
		data:{membername:$('#familymemberperf3').text()},
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			var k=1;
			$.each(data, function(key,value) {
			console.log("alert_category :"+value['alert_category']+" alert_name :"+value['alert_name']+" alert_status:"+value['alert_status']+" memberid :"+value['memberid']);
			console.log("tempalert_category :"+alertcategory);	
			if(alertcategory!=value['alert_category'])
				{
					if(k>=1 && k<=4)
					{
					familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
					}	
					console.log("entered into new alert_category :"+value['alert_category']);
					familyperfformpart=familyperfformpart+"<div class=\"row\">" +
														  "<div class=\"form-group col-lg-1\" ></div>" +
														  "<div class=\"form-group col-lg-4\" ><h2>&nbsp;"+value['alert_category']+"</h2></div>" +
														  "</div>";
					alertcategory=value['alert_category'];
					console.log("k:"+k);
					
					k=1;
				}
				if(k==1)
				{
				familyperfformpart=familyperfformpart+"<div class=\"row\"><div class=\"form-group col-lg-1\" ></div>"; //starting row of alerts
				}
				familyperfformpart=familyperfformpart+"<input type=\"hidden\" name=\"healthalertcategory"+i+"\" value=\""+value['alert_category']+"\"></input>"+
				"<input type=\"hidden\" name=\"memberid"+i+"\" value=\""+value['memberid']+"\"></input>"+				
				"<div class=\"form-group col-lg-2\" >";
				if(value['alert_status']=='Y')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\" checked>"+value['alert_name']+"</label>";
				}
				if(value['alert_status']=='N')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\">"+value['alert_name']+"</label>";
				}
				familyperfformpart=familyperfformpart+"</div>";
				if(k==4)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				k=0;
				}
				i=i+1;
				k=k+1;
				});
				if(k!=1)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				}
				familyperfformpart=familyperfformpart+"<div class=\"row\">"+
				"<div class=\"form-group col-lg-1\" ></div>"+
				"<div class=\"form-group col-lg-2\" >"+
				"<div class=\"form-group\">"+
				"&nbsp;&nbsp;<button  type=\"button\" id=\"savepreferences\" class=\"btn btn-info btn-block\">Save Preferences</button>"+
				"</div></div></div><br><br><br><br><br>";
				console.log(familyperfformpart);
				$('#familymemberprefsec').append(familyperfformpart);
		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}
	});

});
///// generating form data for familymemberperf1 started here Note same code sud be there for familymemberperf1 to familymemberperf6
$("#familymemberperf4").click(function(){
	$('#familymemberprefsec').empty(); // empty current form data of familymemberprefsec 
	var alertcategory=null;
	var familyperfformpart="";
	
	$.ajax({
		url: "GetCustomerHealthAlertsPerf",
		type: 'POST',
		dataType: 'json',
		data:{membername:$('#familymemberperf4').text()},
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			var k=1;
			$.each(data, function(key,value) {
			console.log("alert_category :"+value['alert_category']+" alert_name :"+value['alert_name']+" alert_status:"+value['alert_status']+" memberid :"+value['memberid']);
			console.log("tempalert_category :"+alertcategory);	
			if(alertcategory!=value['alert_category'])
				{
					if(k>=1 && k<=4)
					{
					familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
					}	
					console.log("entered into new alert_category :"+value['alert_category']);
					familyperfformpart=familyperfformpart+"<div class=\"row\">" +
														  "<div class=\"form-group col-lg-1\" ></div>" +
														  "<div class=\"form-group col-lg-4\" ><h2>&nbsp;"+value['alert_category']+"</h2></div>" +
														  "</div>";
					alertcategory=value['alert_category'];
					console.log("k:"+k);
					
					k=1;
				}
				if(k==1)
				{
				familyperfformpart=familyperfformpart+"<div class=\"row\"><div class=\"form-group col-lg-1\" ></div>"; //starting row of alerts
				}
				familyperfformpart=familyperfformpart+"<input type=\"hidden\" name=\"healthalertcategory"+i+"\" value=\""+value['alert_category']+"\"></input>"+
				"<input type=\"hidden\" name=\"memberid"+i+"\" value=\""+value['memberid']+"\"></input>"+				
				"<div class=\"form-group col-lg-2\" >";
				if(value['alert_status']=='Y')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\" checked>"+value['alert_name']+"</label>";
				}
				if(value['alert_status']=='N')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\">"+value['alert_name']+"</label>";
				}
				familyperfformpart=familyperfformpart+"</div>";
				if(k==4)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				k=0;
				}
				i=i+1;
				k=k+1;
				});
				if(k!=1)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				}
				familyperfformpart=familyperfformpart+"<div class=\"row\">"+
				"<div class=\"form-group col-lg-1\" ></div>"+
				"<div class=\"form-group col-lg-2\" >"+
				"<div class=\"form-group\">"+
				"&nbsp;&nbsp;<button  type=\"button\" id=\"savepreferences\" class=\"btn btn-info btn-block\">Save Preferences</button>"+
				"</div></div></div><br><br><br><br><br>";
				console.log(familyperfformpart);
				$('#familymemberprefsec').append(familyperfformpart);
		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}
	});

});
///// generating form data for familymemberperf1 started here Note same code sud be there for familymemberperf1 to familymemberperf6
$("#familymemberperf5").click(function(){
	$('#familymemberprefsec').empty(); // empty current form data of familymemberprefsec 
	var alertcategory=null;
	var familyperfformpart="";
	
	$.ajax({
		url: "GetCustomerHealthAlertsPerf",
		type: 'POST',
		dataType: 'json',
		data:{membername:$('#familymemberperf5').text()},
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			var k=1;
			$.each(data, function(key,value) {
			console.log("alert_category :"+value['alert_category']+" alert_name :"+value['alert_name']+" alert_status:"+value['alert_status']+" memberid :"+value['memberid']);
			console.log("tempalert_category :"+alertcategory);	
			if(alertcategory!=value['alert_category'])
				{
					if(k>=1 && k<=4)
					{
					familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
					}	
					console.log("entered into new alert_category :"+value['alert_category']);
					familyperfformpart=familyperfformpart+"<div class=\"row\">" +
														  "<div class=\"form-group col-lg-1\" ></div>" +
														  "<div class=\"form-group col-lg-4\" ><h2>&nbsp;"+value['alert_category']+"</h2></div>" +
														  "</div>";
					alertcategory=value['alert_category'];
					console.log("k:"+k);
					
					k=1;
				}
				if(k==1)
				{
				familyperfformpart=familyperfformpart+"<div class=\"row\"><div class=\"form-group col-lg-1\" ></div>"; //starting row of alerts
				}
				familyperfformpart=familyperfformpart+"<input type=\"hidden\" name=\"healthalertcategory"+i+"\" value=\""+value['alert_category']+"\"></input>"+
				"<input type=\"hidden\" name=\"memberid"+i+"\" value=\""+value['memberid']+"\"></input>"+				
				"<div class=\"form-group col-lg-2\" >";
				if(value['alert_status']=='Y')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\" checked>"+value['alert_name']+"</label>";
				}
				if(value['alert_status']=='N')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\">"+value['alert_name']+"</label>";
				}
				familyperfformpart=familyperfformpart+"</div>";
				if(k==4)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				k=0;
				}
				i=i+1;
				k=k+1;
				});
				if(k!=1)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				}
				familyperfformpart=familyperfformpart+"<div class=\"row\">"+
				"<div class=\"form-group col-lg-1\" ></div>"+
				"<div class=\"form-group col-lg-2\" >"+
				"<div class=\"form-group\">"+
				"&nbsp;&nbsp;<button  type=\"button\" id=\"savepreferences\" class=\"btn btn-info btn-block\">Save Preferences</button>"+
				"</div></div></div><br><br><br><br><br>";
				console.log(familyperfformpart);
				$('#familymemberprefsec').append(familyperfformpart);
		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}
	});

});
///// generating form data for familymemberperf1 started here Note same code sud be there for familymemberperf1 to familymemberperf6
$("#familymemberperf6").click(function(){
	$('#familymemberprefsec').empty(); // empty current form data of familymemberprefsec 
	var alertcategory=null;
	var familyperfformpart="";
	
	$.ajax({
		url: "GetCustomerHealthAlertsPerf",
		type: 'POST',
		dataType: 'json',
		data:{membername:$('#familymemberperf6').text()},
		success: function (data) {
			console.log("success returned from servlet GetFamilyMemberNames.");
			var i=1;
			var k=1;
			$.each(data, function(key,value) {
			console.log("alert_category :"+value['alert_category']+" alert_name :"+value['alert_name']+" alert_status:"+value['alert_status']+" memberid :"+value['memberid']);
			console.log("tempalert_category :"+alertcategory);	
			if(alertcategory!=value['alert_category'])
				{
					if(k>=1 && k<=4)
					{
					familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
					}	
					console.log("entered into new alert_category :"+value['alert_category']);
					familyperfformpart=familyperfformpart+"<div class=\"row\">" +
														  "<div class=\"form-group col-lg-1\" ></div>" +
														  "<div class=\"form-group col-lg-4\" ><h2>&nbsp;"+value['alert_category']+"</h2></div>" +
														  "</div>";
					alertcategory=value['alert_category'];
					console.log("k:"+k);
					
					k=1;
				}
				if(k==1)
				{
				familyperfformpart=familyperfformpart+"<div class=\"row\"><div class=\"form-group col-lg-1\" ></div>"; //starting row of alerts
				}
				familyperfformpart=familyperfformpart+"<input type=\"hidden\" name=\"healthalertcategory"+i+"\" value=\""+value['alert_category']+"\"></input>"+
				"<input type=\"hidden\" name=\"memberid"+i+"\" value=\""+value['memberid']+"\"></input>"+				
				"<div class=\"form-group col-lg-2\" >";
				if(value['alert_status']=='Y')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\" checked>"+value['alert_name']+"</label>";
				}
				if(value['alert_status']=='N')
				{
				familyperfformpart=familyperfformpart+"&nbsp;&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"alertname"+i+"\" value=\""+value['alert_name']+"\">"+value['alert_name']+"</label>";
				}
				familyperfformpart=familyperfformpart+"</div>";
				if(k==4)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				k=0;
				}
				i=i+1;
				k=k+1;
				});
				if(k!=1)
				{
				familyperfformpart=familyperfformpart+"</div>"; // closing row of alerts
				}
				familyperfformpart=familyperfformpart+"<div class=\"row\">"+
				"<div class=\"form-group col-lg-1\" ></div>"+
				"<div class=\"form-group col-lg-2\" >"+
				"<div class=\"form-group\">"+
				"&nbsp;&nbsp;<button  type=\"button\" id=\"savepreferences\" class=\"btn btn-info btn-block\">Save Preferences</button>"+
				"</div></div></div><br><br><br><br><br>";
				console.log(familyperfformpart);
				$('#familymemberprefsec').append(familyperfformpart);
		},
		error: function(request, status, message) {
			console.log("error occourred in GetFamilyMemberNames servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
							message: request.responseText,
							className: "my-custom-class",
							title: "Error",
						  });
		}
	});

});

$('#familymemberprefsec').on('click', '#savepreferences', function (event) {
	
	console.log($("#familymemberperfform").serializeArray());
	$('#savepreferences').attr('disabled','disabled');
	$.ajax({
		type: 'post',
		url: 'UpdateCustomerPreferences',
		data: $("#familymemberperfform").serializeArray(),
		success: function (data) {
			// dialog started here
			bootbox.dialog({
			message: data,
			className: "my-custom-class",
			title: "Success",
			});
			// dialog ended here

		},
		error: function(request, status, message) {
				console.log("error occourred in addmember servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				className: "my-custom-class",
				title: "Error",
				});
				
		}
	});
	$('#savepreferences').removeAttr('disabled');


});

