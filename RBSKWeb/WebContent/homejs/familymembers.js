/**
 * 
 */
$("#customnewsletter").click(function(){
});

$("#scanconfirm").click(function (){
	if($("#scanreporttype").val()!=null && $("#scanreporttype").val()!=undefined && $("#scanreportdate").val()!=null && $("#scanreportdate").val()!=undefined && $("#scanreporttype").val()!="" && $("#scanreportdate").val()!="")
	{
	$("#dropzonearea").show();
	$("#scanreporterrorarea").empty();
	$("#scanreporttype").attr('disabled',true);
	$("#scanreportdate").attr('disabled',true);
	$("#scanconfirm").attr('disabled',true);
	$('#uploadnewscanreport').removeAttr('disabled');	
	}
	else
	{
		$("#scanreporterrorarea").show();
		$("#scanreporterrorarea").append("Please select report date and report type to proceed.");
	}
});
$("#uploadnewscanreport").click(function (){
	$('#scanreporttype').removeAttr('disabled');
	$('#scanreportdate').removeAttr('disabled');
	$('#scanconfirm').removeAttr('disabled');
	$('#scanreporttype').val("");
	$('#scanreportdate').val("");
	$("#dropzonearea").hide();
	$("#scanreporterrorarea").empty();
	$("#uploadnewscanreport").attr('disabled',true);
});

var addmemberform = $('#addmember');
var addform = document.getElementById('addmember');
addmemberform.attr('enctype', "multipart/form-data");
addmemberform.submit(function () {
	var formdata = new FormData(addform);
	$('#addsubmit').attr('disabled',true);
	bootbox.confirm(
			{
			      "message": "Are you sure you want to Add this member.",
			      "className" : "my-custom-class",
			      "callback": function(result) {
	// confirm if condition started here	
	if(result==true)
	{	
		console.log("submit called");
		// ajax call started here 
		$.ajax({
			type: addmemberform.attr('method'),
			url: addmemberform.attr('action'),
			data: formdata,
			contentType: false,
			processData: false,
			success: function (data) {
				// dialog started here
				bootbox.dialog({
				message: data,
				className: "my-custom-class",
				title: "Success",
				});
				// dialog ended here
				$('#addmember').trigger("reset");
				getfamilymembers();
			},
			error: function(request, status, message) {
					console.log("error occourred in addmember servlet call :");
					console.log(message);
					console.log(request.responseText);
					bootbox.dialog({
					message: request.responseText,
					className: "my-custom-class",
					title: "Error",
					});
					$('#addmember').trigger("reset");
			}
		});
		//ajax call ended here
	}
	// confirm if condition ended here
}
});
$('#addsubmit').removeAttr('disabled');	

return false; // not refreshing page
});

/**
 * 
 */
///family members function started here
function getfamilymembers(){

	  // Adding data to Family members module started here	
	$.ajax({
		url: "FamilyMembers",
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		mimeType: 'application/json',
		success: function (data) {
			
			/////
			var i=1;

					$("#familymembers1").empty();
					$("#deletemembers1").empty();
					 $("#membernametoget").empty();
					 $("#membernametoget").append( // Append an object to the inside of the select box
					            $("<option></option>") // Yes you can do this.
					                .text("Select Member Name")
					                .val("")
					        );
				  $.each(data, function(key,value) { 
					  var  profilepicsrc="http://94.228.219.195/userprofilepics/"+value['cust_id']+"_"+value['memberid']+"/"+value['imagename'];
					  var  membername=value['membername'];
				       var  dob=value['dob'];
				       var  bloodgroup=value['bloodgroup'];
				       var  healthstatus=value['healthstatus'];
				       var  emergencynumber=value['emergencynumber'];
				       var  lastdocvisit=value['lastdocvisit'];
				       var  insurancedate=value['insurancedate'];
				       
				       if(value['imagename']===undefined || value['imagename']==null)
				       {
				        profilepicsrc="image/emptyprofile.png";    
				       }
				       if(value['membername']===undefined || value['membername']==null)
				       {
				        membername="";    
				       }
				       
				       if(value['dob']===undefined || value['dob']==null)
				       {
				        dob="";    
				       }
				       
				       if(value['bloodgroup']===undefined || value['bloodgroup']==null)
				       {
				        bloodgroup="";    
				       }
				       
				       if(value['healthstatus']===undefined || value['healthstatus']==null)
				       {
				        healthstatus="";    
				       }
				       
				       if(value['emergencynumber']===undefined || value['emergencynumber']==null)
				       {
				        emergencynumber="";    
				       }
				       
				       if(value['lastdocvisit']===undefined || value['lastdocvisit']==null)
				       {
				        lastdocvisit="";    
				       }
				       
				       if(value['insurancedate']===undefined || value['insurancedate']==null)
				       {
				        insurancedate="";    
				       }
				       
				       // Variable initialization started here
				       // row one started here
				       if(i<7)
				      {
				       $("#familymembers1").append("<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\" id=\"familymemcol"+value['memberid']+"\">" +
				           "<div class=\"team-member\" id=\"familymember"+value['memberid']+"\">" +
				           "<img src=\""+profilepicsrc+"\" alt=\"team_member\">" +
				           "<h5 class=\"h5\">Name : "+membername+"</h5>" +
				           "<h5 class=\"h5\">Dob : "+dob+"</h5>" +
				           "<h5 class=\"h5\">Blood Group : "+bloodgroup+"</h5>" +
				           "<h5 class=\"h5\">Health : "+healthstatus+"</h5>" +
				           "<h5 class=\"h5\">Emergency Contact : </h5>" +
				           "<h5 class=\"h5\">"+emergencynumber+"</h5>" +
				           "<h5 class=\"h5\">Last Doctor Visit : </h5>" +
				           "<h5 class=\"h5\">"+lastdocvisit+"</h5>" +
				           "<h5 class=\"h5\">Insurance Date : </h5>" +
				           "<h5 class=\"h5\">"+insurancedate+"</h5>" +
				           "</div>" +
				           "</div>");       
				       console.log(value['membername']);
				       console.log(key);					  
					 }
					  // row one ended here
					  
					  // Adding data to Delete members module started here
					    console.log("Memberid"+value['memberid']);
					    console.log("i"+i);
						 if(i!=1)
						 {
						  $("#deletemembers1").append("<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\" id=\"deletememcol"+value['memberid']+"\">" +
							  		"<div class=\"delete-member\" id=\"deletemember"+value['memberid']+"\">" +
							  		"<img src=\""+profilepicsrc+"\" alt=\"team_member\">" +
							  		"<h5 class=\"h5\"><a href=\"javascript:deletefamilymember('deletelink',"+value['memberid']+")\" id=\"deletelink"+value['memberid']+"\">Delete&nbsp;"+value['membername']+"</a></h5>" +
							  		"</div>" +
							  		"</div>");						 
						  console.log(value['membername']+value['memberid']+value['cust_id']);
						  console.log(key);
						  }

						 // Adding data to Delete members module ended here
						
						 ////// adding options to membernametoget field in edit member form started here
						 
						 if(i<7)
						 {
						 $("#membernametoget").append( // Append an object to the inside of the select box
						            $("<option></option>") // Yes you can do this.
						                .text(value['membername'])
						                .val(value['memberid'])
						        );
						 }
						////// adding options to membernametoget field in edit member form ended here

					  
					  
					 i++;
				  });
				  
			
			////
				  // Adding data to Family members module ended here	
				  
	}
});
}
///family members function ended here
var editmemberform = $('#editmember');
var editform = document.getElementById('editmember');
editmemberform.attr('enctype', "multipart/form-data");
editmemberform.submit(function () {
	var formdata = new FormData(editform);
	$('#editsubmit').attr('disabled',true);
		bootbox.confirm(
				{
				      "message": "Are you sure you want to Edit this member?",
				      "className" : "my-custom-class",
				      "callback": function(result) {		
	// confirm if condition started here	
	if(result==true)
	{	
		console.log("submit called");
		// ajax call started here 
		$.ajax({
			type: editmemberform.attr('method'),
			url: editmemberform.attr('action'),
			data: formdata,
			contentType: false,
			processData: false,
			success: function (data) {
				// dialog started here
				bootbox.dialog({
				message: data,
				className: "my-custom-class",
				title: "Success",
				height: 10,
				width: 'auto'
				});
				// dialog ended here
				$('#editmember').trigger("reset");
				getfamilymembers();
			},
			error: function(request, status, message) {
					console.log("error occourred in editmember servlet call :");
					console.log(message);
					console.log(request.responseText);
					bootbox.dialog({
					message: request.responseText,
					className: "my-custom-class",
					title: "Error",
					});
					$('#editmember').trigger("reset");
			}
		});
		//ajax call ended here
	}
	// confirm if condition ended here
}
});
$('#editsubmit').removeAttr('disabled');
return false; // not refreshing page
});

function deletefamilymember(a,b)
{
	console.log(a+b);
	console.log("deletemember"+b);
	
		bootbox.confirm(
				{
				      "message": "Are you sure you want to delete?",
				      "className" : "my-custom-class",
				      "callback": function(result) {		
		// if condition started here
		if(result==true)
		{
			// Ajax call started here	
			$.ajax({
				url: "DeleteMembers",
				type: 'POST',
				data:{memberid:b},
				success: function (data) {
					getfamilymembers();
					if(data=="true")
					{
					console.log(data);
					bootbox.dialog({
						message: "Member has been deleted successfully.",
						className: "my-custom-class",
						title: "Success",
						});

					}
					if(data=="false")
					{
					  console.log(data);
						bootbox.dialog({
							message: "Unable to delete member. Please contact Admin.",
							className: "my-custom-class",
							title: "Error",
							});

					}
					
				},
				error: function(request, status, message) {
					console.log("error occourred in deletemember servlet call :");
					console.log(message);
					console.log(request.responseText);
					bootbox.dialog({
					message: request.responseText,
					className: "my-custom-class",
					title: "Error",
					});
					
			}
				
				
			});
			// Ajax call ended here
		}
		// if condition ended here
	}		
		});
}
/////


