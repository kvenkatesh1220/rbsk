$(document).ready(function(){
	
	$("#viewalert").click(function(e){
		console.log($("#viewalert").serializeArray());
		console.log("viewalert called");
		$.ajax({
			type: 'post',
			url: 'ViewCustomerAlert',
			dataType: 'json',
			contentType: 'application/json',
			mimeType: 'application/json',
			success: function (data) {
				// dialog started here
				$("#viewalerttab").empty();
				var output="";
				var i=0;
				$.each(data, function(key,value) { 
					console.log(key+"--"+value);
					var alertname= value['alert_name'];
					var alert_category= value['alert_category'];
					var added_by= value['added_by'];
					if(added_by =="undefined" || added_by == null)
					{
					added_by="";	
					}
					if(alert_category =="undefined" || alert_category == null)
					{
					alert_category="";	
					}
					if(alertname =="undefined" || alertname == null)
					{
					alertname="";	
					}
					
					if(i==0)
					{
					output="<table class=\"table\" style=\"width:810px;\"><thead><tr><th>Alert Name</th><th>Alert Category</th><th>Added By</th></tr></thead><tbody>";
					output=output+"<tr class=\"info\"><td>"+alertname+"</td><td>"+alert_category+"</td><td>"+added_by+"</td></tr>";
					}
					else
					{
					output=output+"<tr class=\"info\"><td>"+alertname+"</td><td>"+alert_category+"</td><td>"+added_by+"</td></tr>";
					}
					i=i+1;
				 });
					output=output+"</tbody></table>";
				 $("#viewalerttab").append(output);
				// dialog ended here
				 
			},
			error: function(request, status, message) {
					console.log("error occourred in viewalerttab servlet call :");
					$("#viewalert").empty();
					console.log(message);
					console.log(request.responseText);
					bootbox.dialog({
					message: request.responseText,
					title: "Error",
					});
			}
			
		});

	});

	$("#addalertcategorysubmit").click(function(e){
	console.log($("#addalertcategory").serializeArray());
	console.log("addalertcategorysubmit called");
	$.ajax({
		type: 'post',
		url: 'AddAlertCategory',
		data: $("#addalertcategory").serializeArray(),
		success: function (data) {
			// dialog started here
			bootbox.dialog({
			message: data,
			title: "Response",
			});
			// dialog ended here
		},
		error: function(request, status, message) {
				console.log("error occourred in addalertcategory servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
		
	});

});
///////// Getting alert categories from db and load into select box of deletealertcategorytab
$("#getealertcategories").click(function(e){
	console.log("delalertcategorysubmit called");
	$.ajax({
		type: 'post',
		url: 'GetAlertCategory',
		dataType: 'json',
		contentType: 'application/json',
		mimeType: 'application/json',
		success: function (data) {
			// dialog started here
			$("#delcategoryname1").empty();
			$("#delcategoryname1").append("<option value=\"\">Select Alert Category</option>");
			$.each(data, function(key,value) { 
				console.log(key+"--"+value); 
				$("#delcategoryname1").append("<option value=\""+value+"\">"+value+"</option>");
			 });
			// dialog ended here
		},
		error: function(request, status, message) {
				$("#delcategoryname1").empty();
				$("#delcategoryname1").append("<option value=\"Select Alert Category\">"+value+"</option>");
				console.log("error occourred in addalertcategory servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
		
	});

});
/////////
/////////
$("#delalertcategorysubmit").click(function(e){
	console.log($("#deletelertcategory").serializeArray());
	console.log("delalertcategorysubmit called");
	$.ajax({
		type: 'post',
		url: 'DeleteAlertCategory',
		data: $("#deletelertcategory").serializeArray(),
		success: function (data) {
			// dialog started here
			bootbox.dialog({
			message: data,
			title: "Response",
			});
			// dialog ended here
			console.log("GetCustomerAlert called");	
			console.log($("#delalert").serializeArray());
			$.ajax({
				type: 'post',
				url: 'GetCustomerAlert',
				data: $("#delalert").serializeArray(),
				success: function (data) {
					// dialog started here
					$("#delalertname2").empty();
					$("#delalertname2").append("<option value=\"\">Select Alert</option>");
					$.each(data, function(key,value) { 
						console.log(key+"--"+value); 
						$("#delalertname2").append("<option value=\""+value+"\">"+value+"</option>");
					 });
					// dialog ended here
				},
				error: function(request, status, message) {
						$("#delalertname2").empty();
						$("#delalertname2").append("<option value=\"Select Alert\">"+value+"</option>");
						console.log("error occourred in addalertcategory servlet call :");
						console.log(message);
						console.log(request.responseText);
						bootbox.dialog({
						message: request.responseText,
						title: "Error",
						});
				}
				
			});			
			//delalertcategorysubmit started.
		},
		error: function(request, status, message) {
				console.log("error occourred in deletelertcategory servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
	});

});
/////////
///////// Getting alert categories from db and load into select box of addalerttab
$("#getaddalerts").click(function(e){
	console.log("delalertcategorysubmit called");
	$.ajax({
		type: 'post',
		url: 'GetAlertCategory',
		dataType: 'json',
		contentType: 'application/json',
		mimeType: 'application/json',
		success: function (data) {
			// dialog started here
			$("#addalertcategoryname2").empty();
			$("#addalertcategoryname2").append("<option value=\"\">Select Alert Category</option>");
			 $.each(data, function(key,value) { 
				console.log(key+"--"+value); 
				$("#addalertcategoryname2").append("<option value=\""+value+"\">"+value+"</option>");
			 });
			// dialog ended here
		},
		error: function(request, status, message) {
				console.log("error occourred in addalertcategory servlet call :");
				$("#delcategoryname2").empty();
				$("#addalertcategoryname2").append("<option value=\"Select Alert Category\">"+value+"</option>");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
		
	});

});
/////////
/////////
$("#addalert").submit(function(){
	console.log($("#addalert").serializeArray());
	console.log("addalertsubmit called");
	$.ajax({
		type: 'post',
		url: 'AddCustomerAlert',
		data: $("#addalert").serializeArray(),
		success: function (data) {
			// dialog started here
			bootbox.dialog({
			message: data,
			title: "Response",
			});
			// dialog ended here
		},
		error: function(request, status, message) {
				console.log("error occourred in addalert servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
		
	});
	event.preventDefault();
	return false;
});
/////////
$("#delalertsubmit").click(function(){
	console.log($("#delalertsubmit").serializeArray());
	console.log("delalertsubmit called");
	$.ajax({
		type: 'post',
		url: 'DeleteCustomerAlert',
		data: $("#delalert").serializeArray(),
		success: function (data) {
			// dialog started here
			bootbox.dialog({
			message: data,
			title: "Response",
			});
			// dialog ended here
			console.log("GetCustomerAlert called");	
			console.log($("#delalert").serializeArray());
			$.ajax({
				type: 'post',
				url: 'GetCustomerAlert',
				data: $("#delalert").serializeArray(),
				success: function (data) {
					// dialog started here
					$("#delalertname2").empty();
					$("#delalertname2").append("<option value=\"\">Select Alert</option>");
					$.each(data, function(key,value) { 
						console.log(key+"--"+value); 
						$("#delalertname2").append("<option value=\""+value+"\">"+value+"</option>");
					 });
					// dialog ended here
				},
				error: function(request, status, message) {
						$("#delalertname2").empty();
						$("#delalertname2").append("<option value=\"Select Alert\">"+value+"</option>");
						console.log("error occourred in addalertcategory servlet call :");
						console.log(message);
						console.log(request.responseText);
						bootbox.dialog({
						message: request.responseText,
						title: "Error",
						});
				}
				
			});
				
			

		},
		error: function(request, status, message) {
				console.log("error occourred in delalert servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
		
	});
	event.preventDefault();
	return false;
	
});

/////////
$('#delalertcategory2').change(function(e){
console.log("GetCustomerAlert called");	
console.log($("#delalert").serializeArray());
$.ajax({
	type: 'post',
	url: 'GetCustomerAlert',
	data: $("#delalert").serializeArray(),
	success: function (data) {
		// dialog started here
		$("#delalertname2").empty();
		$("#delalertname2").append("<option value=\"\">Select Alert</option>");
		$.each(data, function(key,value) { 
			console.log(key+"--"+value); 
			$("#delalertname2").append("<option value=\""+value+"\">"+value+"</option>");
		 });
		// dialog ended here
	},
	error: function(request, status, message) {
			$("#delalertname2").empty();
			$("#delalertname2").append("<option value=\"Select Alert\">"+value+"</option>");
			console.log("error occourred in addalertcategory servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
			message: request.responseText,
			title: "Error",
			});
	}
	
});
	
	
});
///////// Getting alert categories from db and load into select box of deletealerttab
$("#getcatandalers").click(function(e){
	console.log("getcatandalers called");
	$.ajax({
		type: 'post',
		url: 'GetAlertCategory',
		dataType: 'json',
		contentType: 'application/json',
		mimeType: 'application/json',
		success: function (data) {
			// dialog started here
			$("#delalertcategory2").empty();
			$("#delalertcategory2").append("<option value=\"\">Select Alert Category</option>");
			$.each(data, function(key,value) { 
				console.log(key+"--"+value); 
				$("#delalertcategory2").append("<option value=\""+value+"\">"+value+"</option>");
			 });
			// dialog ended here
		},
		error: function(request, status, message) {
				$("#delalertcategory2").empty();
				$("#delalertcategory2").append("<option value=\"Select Alert Category\">"+value+"</option>");
				console.log("error occourred in addalertcategory servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
		
	});
///////// Getting alert names from db and load into select box of deletealerttab
	console.log("getcatandalers for alertnames called");
	$.ajax({
		type: 'post',
		url: 'GetCustomerAlert',
		dataType: 'json',
		contentType: 'application/json',
		mimeType: 'application/json',
		success: function (data) {
			// dialog started here
			$("#delalertname2").empty();
			$("#delalertname2").append("<option value=\"\">Select Alert</option>");
			$.each(data, function(key,value) { 
				console.log(key+"--"+value); 
				$("#delalertcategory2").append("<option value=\""+value+"\">"+value+"</option>");
			 });
			// dialog ended here
		},
		error: function(request, status, message) {
				$("#delalertname2").empty();
				$("#delalertname2").append("<option value=\"Select Alert\">"+value+"</option>");
				console.log("error occourred in addalertcategory servlet call :");
				console.log(message);
				console.log(request.responseText);
				bootbox.dialog({
				message: request.responseText,
				title: "Error",
				});
		}
///////////////////////////	

});
/////////


});
/////////

});