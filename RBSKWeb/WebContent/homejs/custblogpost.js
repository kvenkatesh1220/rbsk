/**
 * 
 */
// Note : healthblogtile, healthblogmenu, preventionhealthblog have same code
//healthblogtile click Note : healthblogtile, healthblogmenu, preventionhealthblog have same code
$("#healthblogtile").click(function(){
	$("#latestblogslist").empty();	
	var blogpoststring="";
	$.ajax({
		url: "GetLatestCustBlogPosts", 
		type: 'POST',
		
		success: function (data) {
			var i=1;
			 $.each(data, function(key,value) { 
				 
				 var mainhead=value['mainhead'];
				 var authorname=value['authorname'];
				 var postdate=value['postdate'];
				 var maindesc=value['maindesc'];
				 var postid=value['postid'];
				 
				 if(value['mainhead']===undefined || value['mainhead']==null)
		            {
					 mainhead="";    
		            }
				 if(value['authorname']===undefined || value['authorname']==null)
		            {
					 authorname="";    
		            }
				 if(value['postdate']===undefined || value['postdate']==null)
		            {
					 postdate="";    
		            }
				 if(value['maindesc']===undefined || value['maindesc']==null)
		            {
					 maindesc="";    
		            }
				 if(value['postid']===undefined || value['postid']==null)
		            {
					 postid="";    
		            }
				 
				 
				 if(i==1)
				 {
				 blogpoststring=blogpoststring+"<div class=\"row\">";
				 }
				 blogpoststring=blogpoststring+"<div class=\"col-sm-6 col-md-6 col-lg-6 blog-post\">";
				 blogpoststring=blogpoststring+"<article class=\"article\">";
				 blogpoststring=blogpoststring+"<div class=\"article-media\">";
				 blogpoststring=blogpoststring+"<a href=\"#\"><img class=\"img-responsive\" src=\""+value['mainimage']+"\" alt=\"blog-article\"></a>";
				 blogpoststring=blogpoststring+"</div>";
				 blogpoststring=blogpoststring+"<div class=\"article-body\">";
				 blogpoststring=blogpoststring+"<h4 class=\"h4 article-title\"><a href=\"#\" id=\"newslettertitle1\">"+mainhead+"</a></h4>";
				 blogpoststring=blogpoststring+"<div class=\"article-tag\"><i class=\"fa fa-tag\"></i><a href=\"#\">Author : "+authorname+"</a>";
				 blogpoststring=blogpoststring+"<span class=\"separator\">|</span><a href=\"#\">Published On : "+postdate+"</a>";
				 blogpoststring=blogpoststring+"</div>";
				 blogpoststring=blogpoststring+"<p>"+maindesc+"</p>";
				 blogpoststring=blogpoststring+"<div class=\"read-more\"><a href=\"/CustomerBlogPost?blogpostid="+postid+"\" target=\"_blank\" >Read More</a>";
				 blogpoststring=blogpoststring+"</div></div></article></div>";
				 console.log(postid+"-"+mainhead+"-"+maindesc+"-"+maindesc+"-"+value['mainimage']+"-"+postdate+"-"+value['categeoryname']);
				 if(i==2)
				 {
				 blogpoststring=blogpoststring+"</div>";
				 i=1;
				 }
				 i=i+1;
				 
			 });
			 $("#latestblogslist").append(blogpoststring);
			 console.log(blogpoststring);
			 
		},
		error: function(request, status, message) {
		console.log("error occourred in GetLatestCustBlogPosts servlet call :");
		console.log(message);
		console.log(request.responseText);
		bootbox.dialog({
		message: request.responseText,
		className: "my-custom-class",
		title: "Error",
		});
		}
   });		
	
});
//
/// healthblogmenu click Note : healthblogtile, healthblogmenu, preventionhealthblog have same code
$("#healthblogmenu").click(function(){
	$("#latestblogslist").empty();	
	var blogpoststring="";
	$.ajax({
		url: "GetLatestCustBlogPosts", 
		type: 'POST',
		
		success: function (data) {
			var i=1;
			 $.each(data, function(key,value) { 
				 var mainhead=value['mainhead'];
				 var authorname=value['authorname'];
				 var postdate=value['postdate'];
				 var maindesc=value['maindesc'];
				 var postid=value['postid'];
				 
				 if(value['mainhead']===undefined || value['mainhead']==null)
		            {
					 mainhead="";    
		            }
				 if(value['authorname']===undefined || value['authorname']==null)
		            {
					 authorname="";    
		            }
				 if(value['postdate']===undefined || value['postdate']==null)
		            {
					 postdate="";    
		            }
				 if(value['maindesc']===undefined || value['maindesc']==null)
		            {
					 maindesc="";    
		            }
				 if(value['postid']===undefined || value['postid']==null)
		            {
					 postid="";    
		            }
				 
				 
				 if(i==1)
				 {
				 blogpoststring=blogpoststring+"<div class=\"row\">";
				 }
				 blogpoststring=blogpoststring+"<div class=\"col-sm-6 col-md-6 col-lg-6 blog-post\">";
				 blogpoststring=blogpoststring+"<article class=\"article\">";
				 blogpoststring=blogpoststring+"<div class=\"article-media\">";
				 blogpoststring=blogpoststring+"<a href=\"#\"><img class=\"img-responsive\" src=\""+value['mainimage']+"\" alt=\"blog-article\"></a>";
				 blogpoststring=blogpoststring+"</div>";
				 blogpoststring=blogpoststring+"<div class=\"article-body\">";
				 blogpoststring=blogpoststring+"<h4 class=\"h4 article-title\"><a href=\"#\" id=\"newslettertitle1\">"+mainhead+"</a></h4>";
				 blogpoststring=blogpoststring+"<div class=\"article-tag\"><i class=\"fa fa-tag\"></i><a href=\"#\">Author : "+authorname+"</a>";
				 blogpoststring=blogpoststring+"<span class=\"separator\">|</span><a href=\"#\">Published On : "+postdate+"</a>";
				 blogpoststring=blogpoststring+"</div>";
				 blogpoststring=blogpoststring+"<p>"+maindesc+"</p>";
				 blogpoststring=blogpoststring+"<div class=\"read-more\"><a href=\"/CustomerBlogPost?blogpostid="+postid+"\" target=\"_blank\" >Read More</a>";
				 blogpoststring=blogpoststring+"</div></div></article></div>";
				 console.log(postid+"-"+mainhead+"-"+maindesc+"-"+maindesc+"-"+value['mainimage']+"-"+postdate+"-"+value['categeoryname']);
				 if(i==2)
				 {
				 blogpoststring=blogpoststring+"</div>";
				 i=1;
				 }
				 i=i+1;
				 
			 });
			 $("#latestblogslist").append(blogpoststring);
			 console.log(blogpoststring);
			 
		},
		error: function(request, status, message) {
		console.log("error occourred in GetLatestCustBlogPosts servlet call :");
		console.log(message);
		console.log(request.responseText);
		bootbox.dialog({
		message: request.responseText,
		className: "my-custom-class",
		title: "Error",
		});
		}
   });		
	
});

//// preventionhealthblog click Note : healthblogtile, healthblogmenu, preventionhealthblog have same code
$("#preventionhealthblog").click(function(){
	$("#latestblogslist").empty();	
	var blogpoststring="";
	$.ajax({
		url: "GetLatestCustBlogPosts", 
		type: 'POST',
		
		success: function (data) {
			var i=1;
			 $.each(data, function(key,value) { 
				 var mainhead=value['mainhead'];
				 var authorname=value['authorname'];
				 var postdate=value['postdate'];
				 var maindesc=value['maindesc'];
				 var postid=value['postid'];
				 
				 if(value['mainhead']===undefined || value['mainhead']==null)
		            {
					 mainhead="";    
		            }
				 if(value['authorname']===undefined || value['authorname']==null)
		            {
					 authorname="";    
		            }
				 if(value['postdate']===undefined || value['postdate']==null)
		            {
					 postdate="";    
		            }
				 if(value['maindesc']===undefined || value['maindesc']==null)
		            {
					 maindesc="";    
		            }
				 if(value['postid']===undefined || value['postid']==null)
		            {
					 postid="";    
		            }
				 
				 
				 if(i==1)
				 {
				 blogpoststring=blogpoststring+"<div class=\"row\">";
				 }
				 blogpoststring=blogpoststring+"<div class=\"col-sm-6 col-md-6 col-lg-6 blog-post\">";
				 blogpoststring=blogpoststring+"<article class=\"article\">";
				 blogpoststring=blogpoststring+"<div class=\"article-media\">";
				 blogpoststring=blogpoststring+"<a href=\"#\"><img class=\"img-responsive\" src=\""+value['mainimage']+"\" alt=\"blog-article\"></a>";
				 blogpoststring=blogpoststring+"</div>";
				 blogpoststring=blogpoststring+"<div class=\"article-body\">";
				 blogpoststring=blogpoststring+"<h4 class=\"h4 article-title\"><a href=\"#\" id=\"newslettertitle1\">"+mainhead+"</a></h4>";
				 blogpoststring=blogpoststring+"<div class=\"article-tag\"><i class=\"fa fa-tag\"></i><a href=\"#\">Author : "+authorname+"</a>";
				 blogpoststring=blogpoststring+"<span class=\"separator\">|</span><a href=\"#\">Published On : "+postdate+"</a>";
				 blogpoststring=blogpoststring+"</div>";
				 blogpoststring=blogpoststring+"<p>"+maindesc+"</p>";
				 blogpoststring=blogpoststring+"<div class=\"read-more\"><a href=\"/CustomerBlogPost?blogpostid="+postid+"\" target=\"_blank\" >Read More</a>";
				 blogpoststring=blogpoststring+"</div></div></article></div>";
				 console.log(postid+"-"+mainhead+"-"+maindesc+"-"+maindesc+"-"+value['mainimage']+"-"+postdate+"-"+value['categeoryname']);
				 if(i==2)
				 {
				 blogpoststring=blogpoststring+"</div>";
				 i=1;
				 }
				 i=i+1;
				 
			 });
			 $("#latestblogslist").append(blogpoststring);
			 console.log(blogpoststring);
			 
		},
		error: function(request, status, message) {
		console.log("error occourred in GetLatestCustBlogPosts servlet call :");
		console.log(message);
		console.log(request.responseText);
		bootbox.dialog({
		message: request.responseText,
		className: "my-custom-class",
		title: "Error",
		});
		}
   });		
	
});



