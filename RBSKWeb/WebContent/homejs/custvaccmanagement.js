/**
 * 
 */
$("#preventionvaccmanagement").click(function(){
	$("#custvaccinationcardsec").empty();
	$("#updatevaccination").hide();
	$("#custvaccinationcardheader").hide();
	$.ajax({
		url: "CustomerVaccCard", 
		type: 'POST',
		success: function (data) {

			var vacccardforstr="";
			
			
			
			var i=0;
			var k=0;
			 $.each(data, function(key,value) {
					var templatename=value['templatename'];
					var templateid=value['templateid'];
					var vaccinationName=value['vaccinationName'];
					var preventionFor=value['preventionFor'];
					var age=value['age'];
					var remarks=value['remarks'];
					var due_date=value['due_date'];
					var vaccination_date=value['vaccination_date'];
					var sno=value['sno'];
					
					if(value['templatename']===undefined || value['templatename']==null)
		            {
						templatename="";    
		            }
					if(value['templateid']===undefined || value['templateid']==null)
		            {
						templateid="";    
		            }
					if(value['vaccinationName']===undefined || value['vaccinationName']==null)
		            {
						vaccinationName="";    
		            }
					if(value['preventionFor']===undefined || value['preventionFor']==null)
		            {
						preventionFor="";    
		            }
					if(value['age']===undefined || value['age']==null)
		            {
						age="";    
		            }
					if(value['remarks']===undefined || value['remarks']==null)
		            {
						remarks="";    
		            }
					if(value['due_date']===undefined || value['due_date']==null)
		            {
						due_date="";    
		            }
					if(value['vaccination_date']===undefined || value['vaccination_date']==null)
		            {
						vaccination_date="";    
		            }
					if(value['sno']===undefined || value['sno']==null)
		            {
						sno="";    
		            }

				 console.log(vaccinationName+"-"+preventionFor+"-"+age+"-"+remarks+"-"+due_date+"-"+vaccination_date+"-"+sno+"-"+remarks);
				 vacccardforstr=vacccardforstr+"<div class=\"row\" style=\"border-bottom: 1px solid #FDFDFD; padding-bottom: 10px;\">";
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"templatename"+i+"\" name=\"templatename"+i+"\" value=\""+templatename+"\"></input>";
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"templateid"+i+"\" name=\"templateid"+i+"\" value=\""+templateid+"\"></input>";
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"sno"+i+"\" name=\"sno"+i+"\" value=\""+sno+"\"></input>";
				 vacccardforstr=vacccardforstr+"<div class=\"col-sm-2\">";
				 vacccardforstr=vacccardforstr+vaccinationName;
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"vaccinationname"+i+"\" name=\"vaccinationname"+i+"\" value=\""+vaccinationName+"\"></input>";
				 vacccardforstr=vacccardforstr+"</div>";
				 vacccardforstr=vacccardforstr+"<div class=\"col-sm-2\">";
				 vacccardforstr=vacccardforstr+preventionFor;
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"preventionfor"+i+"\" name=\"preventionfor"+i+"\" value=\""+preventionFor+"\"></input>";
				 vacccardforstr=vacccardforstr+"</div>";
				 vacccardforstr=vacccardforstr+"<div class=\"col-sm-2\">";
				 vacccardforstr=vacccardforstr+age;
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"age"+i+"\" name=\"age"+i+"\" value=\""+age+"\"></input>";
				 vacccardforstr=vacccardforstr+"</div>";
				 vacccardforstr=vacccardforstr+"<div class=\"col-sm-2\">";
				 vacccardforstr=vacccardforstr+vaccination_date;
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"vaccinationdate"+i+"\" name=\"vaccinationdate"+i+"\" value=\""+vaccination_date+"\"></input>";
				 vacccardforstr=vacccardforstr+"</div>";
				 vacccardforstr=vacccardforstr+"<div class=\"col-sm-2\">";
				 if(due_date!="")
				 {
				 vacccardforstr=vacccardforstr+due_date;
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"vaccduedate"+i+"\" name=\"vaccduedate"+i+"\" value=\""+due_date+"\"></input>";
				 }
				 console.log('due date:'+due_date);
				 if(due_date=="")
				 {
				 vacccardforstr=vacccardforstr+"<input type=\"date\" name=\"vaccduedate"+i+"\" id=\"vaccduedate"+i+"\"></input>";
				 k=k+1;
				 }
				 vacccardforstr=vacccardforstr+"</div>";
				 vacccardforstr=vacccardforstr+"<div class=\"col-sm-2\">";
				 vacccardforstr=vacccardforstr+remarks;
				 vacccardforstr=vacccardforstr+"<input type=\"hidden\" id=\"remark"+i+"\" name=\"remark"+i+"\" value=\""+remarks+"\"></input>";
				 vacccardforstr=vacccardforstr+"</div>";
				 vacccardforstr=vacccardforstr+"</div>";
				    
				 i=i+1;
			 });
			 if(i==0) // no vaccination data found
			 {
				 $("#custvaccinationcardsec").append("<h1> No Vaccination Data Found</h1>");
			 }
			 if(i!=0)
			 {
				 $("#custvaccinationcardheader").show();
				 $("#custvaccinationcardsec").append(vacccardforstr);
				 
			 }
			 if(k!=0)
			 {
				 $("#updatevaccination").show(); 
			 }
			 
			 
			 
			 
		},
		error: function(request, status, message) {
			console.log("error occourred in GetNewsLetterAlerts servlet call :");
			console.log(message);
			console.log(request.responseText);
			bootbox.dialog({
			message: request.responseText,
			className: "my-custom-class",
			title: "Error",
			});
			}
	});
	
});

$("#updatevaccination").click(function() {
	console.log($("#custvacccard").serializeArray().toString());
	
	$('#portfolio-14').modal('hide');
    $.ajax({
    	   type: 'post',
    	   url: 'CustomerVaccCardUpdate',
    	   data: $("#custvacccard").serializeArray(),
    	   success: function (data) {
    	    // dialog started here
    	    bootbox.dialog({
    	    className: "my-custom-class",
    	    message: data,
    	    title: "Success",
    	    });
    	    // dialog ended here

    	   },
    	   error: function(request, status, message) {
    	     console.log("error occourred in addmember servlet call :");
    	     console.log(message);
    	     console.log(request.responseText);
    	     bootbox.dialog({
    	     message: request.responseText,
    	     className: "my-custom-class",
    	     title: "Error",
    	     });
    	   }
    	  });
    	  //ajax call ended here
    	  $('#savehealthformsubmit').removeAttr('disabled');
    	  $('#submithealthformsubmit').removeAttr('disabled');
    	  
    	  return false; // not refreshing page
    	 }); 
    	 //ajax call ended here
