/**
 * 
 */
function passwordrules()
{
    
	bootbox.dialog({
		  message: "* Password should contain minimum 8 and maximum 16 characters.<br>"+
		  "* Password should contain at least one lower case letter.<br>"+
		  "* Password should contain at least one upper letter.<br>"+
		  "* Password should contain at least one digit.<br>"+
	      "* Password should contain at least one special character. @#$% are only allowed",
		  title: "Password Rules",
		  buttons: {
		      main: {
		      label: "Ok",
		      className: "btn-primary",
		      callback: function() {
		        
		      }
		    }
		  }
		});
}
