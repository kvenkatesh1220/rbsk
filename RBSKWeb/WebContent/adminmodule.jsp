<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
    <!-- Page title ended here-->

    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->
    
     <!-- Health Pinata styles started here -->
      <link rel="stylesheet" href="/scripts/custscripts/jquery-ui.css">
   <!--  Health Pinata styles ended here  -->

</head>
<body>

<!-- Simple splash screen-->
<div class="splash" id="mysplash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p> A software to upload birth defects</p><img src="image/loading-bars.svg" width="64" height="64" /><p>Please Wait</p> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>
 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            
            <h4 class="font-light m-b-xs text-info">
               Manage Accounts
            </h4>
            <small>Add/Edit User Accounts.</small> <span class="pull-right"><font color="red">&nbsp;*&nbsp;</font>marked fields are mandatory.</span>
        </div>
    </div>
</div>

<div class="content">

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" style="overflow-y: initial !important;  height:550px; width:1200px;  margin-top: 20px; margin-left:70px;" >
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <!-- <div class="modal-header text-center">
                                    <h4 class="modal-title">Modal title</h4>
                                    <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                                </div> -->
                                <div class="modal-body">
                                   <div class="row">
                         <div class="col-md-2">
                        <label>District <span><font color="red">&nbsp;*</font></span> </label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                          <select id="model_hsp_districts" name="model_hsp_districts" class="form-control" >
                          </select>
            			 </div>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" placeholder="Enter Hospital Name" id="model_hsp_name" name="model_hsp_name" class="form-control">
            			 </div>
                        </div>
                        <div class="col-md-4">
                         <button class="btn btn-warning submitWizard mytab btn-xs"  type="submit" name="getmodelhspdetbtn" id="getmodelhspdetbtn"><strong>Get Details</strong></button>
                        </div>
                        </div>
                        <br>
                        <div id="modelrept">
                        
                        </div>
                                </div>
                               <!--  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    
                </div>
                <div class="panel-body">
                       <!-- Tabs Started here -->
                        <div class="text-left m-b-md" id="wizardControl">
                            <a class="btn btn-info mytab" href="#step1" data-toggle="tab">Create User </a>
                            <a class="btn btn-default mytab" href="#step2" data-toggle="tab">Update User </a>
                            <a class="btn btn-default mytab" href="#step3" data-toggle="tab">Reset Password </a>
                            <a class="btn btn-default mytab" href="#step4" data-toggle="tab">Add Hospital </a>
                            <a class="btn btn-default mytab" href="#step5" data-toggle="tab">Update Hospital </a>
							
                        </div>
                        
						<!-- Tabs Started here -->
						
						<!-- Tabs Content here -->
                        <div class="tab-content">
                       
                        <!-- Step 1 Started here -->
                        	<div id="step1" class="p-m tab-pane active">
                        <form role="form" id="usercreateform" name="usercreateform" method="post">	
                        <div class="row">
                        <div class="col-md-2" style="width:11%;">
                        <span><font color="red">&nbsp;*</font></span><label>User Name </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder="Enter UserName" id="username" name="username" class="form-control">
            			 </div>
                        </div>
                         <div class="col-md-2" style="width:14%;">
                        <label><span><font color="red">&nbsp;*</font></span>Password </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="password" placeholder="Enter Password" id="password" name="password" class="form-control">
            			 </div>
                        </div>
                        </div>
<br>
						<div class="row">
						<div class="col-md-2" style="width:11%;">
                        <label><span><font color="red">&nbsp;*</font></span>District </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                         <select id="districts" name="districts" class="form-control" >
                         </select>
            			 </div>
                        </div>
                        
						 <div class="col-md-2" style="width:14%;">
                        <label><span><font color="red">&nbsp;*</font></span>Hospital Name </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                          <!-- <input type="text" placeholder="Enter HospitalName" id="hospitalname" name="hospitalname" class="form-control"> --> 
                          <select class="js-source-states-2 " style="width: 100%" id="hospitalname" name="hospitalname">
                            <option value="">Select Hospital</option>
                    		</select>
                          <!-- <button class="btn btn-success btn-xs" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Search Hospital</button> -->
            			 </div>
                        </div>
                        
                        </div>
                        <br>
                        
                        <div class="row">
                         <div class="col-md-2" style="width:11%;">
                        <label><font color="red">&nbsp;*</font></span>Cluster<span> </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <select id="clusters" name="clusters" class="form-control" >
                          <option value="">Select Cluster</option>
                          </select>
            			 </div>
                        </div>
                        
                         <div class="col-md-2" style="width:14%;">
                        <label><span><font color="red">&nbsp;*</font></span>Hospital Type </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                         <select id="hospitaltype" name="hospitaltype" class="form-control" >
                       <option value="">Select Hospital Type</option>  <option value="GOVT">GOVT</option>	<option value="Pvt. Hospital">Pvt. Hospital</option>  
                       </select>
            			 </div>
                        </div>
                        </div>	
                        <br>
                        <div class="row">
                         <div class="col-md-2" style="width:11%;">
                        <label>&nbsp;&nbsp;User Type </label>
                        </div>
                         
                        <div class="col-md-6">
                        <div class="form-group">
                          <div class="radio radio-info radio-inline" id="admindivid">
                            <input type="radio" id="isadmina" name="isadmin" value="ADMIN"  ><label>Admin</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="isadminsu" name="isadmin" value="SUPERUSER" checked><label>Super User</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="isadminu" name="isadmin" value="USER" checked><label>User</label>
                        	</div>
                        </div>
                        </div>
                       
                        </div>
                       
<br>
					</form>
                        
                        <div class="row">
                        
                         
                        <div class="col-md-2">
                         <button class="btn btn-warning submitWizard mytab"  type="submit" name="createuserbtn" id="createuserbtn"><strong>Create User</strong></button>
                        </div>
                       
                        </div>
                        </div> 
                        
						<!-- Step 1 ended here -->
						
						<!-- Step 2 started here -->
						
                        <div id="step2" class="p-m tab-pane">
                       
                         <div class="row">
                        <div class="col-md-2" style="width:14%;">
                        <label><font color="red">&nbsp;*</font>UserName </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder="Enter UserName" id="update_username" name="update_username" class="form-control">
            			 </div>
                        </div>
                       <div class="col-md-4">
                         <button class="btn btn-warning submitWizard mytab btn-xs"  type="submit" name="getdetailsbtn" id="getdetailsbtn"><strong>Get Details</strong></button>
                        </div>
                        
                        </div>
<br>
 <form role="form" id="userupdateform" name="userupdateform" method="post">
						<div class="row">
						 <div class="col-md-2" style="width:14%;">
                        <label><font color="red">&nbsp;*</font>District </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                         <select id="update_districts" name="update_districts" class="form-control" >
                         </select>
            			 </div>
                        </div>
                           
                        <div class="col-md-2" style="width:11%;">
                        <label><font color="red">&nbsp;*</font>Cluster </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <select id="update_clusters" name="update_clusters" class="form-control" >
                          <option value="">Select Cluster</option>
                          </select>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <div class="row">
                     
                        
                        <div class="col-md-2" style="width:14%;">
                        <label><font color="red">&nbsp;*</font>Hospital Name </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                          <!-- <input type="text" placeholder="Enter HospitalName" id="update_hospitalname" name="update_hospitalname" class="form-control"> -->
                          <select class="js-source-states-2 " style="width: 100%" id="update_hospitalname" name="update_hospitalname">
                            <option value="">Select Hospital</option>
                    		</select>
            			 </div>
                        </div>
                        
                          <div class="col-md-2" style="width:11%;">
                        <label><font color="red">&nbsp;*</font>Hospital Type </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                         <select id="update_hospitaltype" name="update_hospitaltype" class="form-control" >
                       <option value="">Select Hospital Type</option>  <option value="GOVT">GOVT</option>	<option value="Pvt. Hospital">Pvt. Hospital</option>  
                       </select>
            			 </div>
                        </div>
                        
                        </div>	
                        <br>
                        <div class="row">
                        
                        
                        
                        <div class="col-md-2" style="width:14%;">
                        <label><font color="red">&nbsp;*</font>User Type </label>
                        </div>
                         
                        <div class="col-md-6">
                        <div class="form-group">
                          <div class="radio radio-info radio-inline" id="updateadmindivid">
                            <input type="radio" id="isadmin" name="update_isadmin" value="ADMIN"  ><label>Admin</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="isadminsu" name="update_isadmin" value="SUPERUSER" ><label>Super User</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="isadmin" name="update_isadmin" value="USER" checked><label>User</label>
                        	</div>
            			 </div>
                        </div>
                       
                        </div>
                       
<br>
				</form>

                        <div class="row">
                        
                         
                        <div class="col-md-2">
                         <button class="btn btn-warning submitWizard mytab"  type="submit" name="updateuserbtn" id="updateuserbtn"><strong>Update User</strong></button>
                        </div>
                       
    
                        </div>
                        </div> 
                        
                        
                       <!-- Step2 ended here -->
                       
                       <!-- Step3 started here -->
                       <div id="step3" class="p-m tab-pane ">
                        <div class="row">
                        <div class="col-md-2" style="width:11%;">
                        <label><font color="red">&nbsp;*</font>User Name </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder="Enter UserName" id="reset_username" name="reset_username" class="form-control">
            			 </div>
                        </div>
                        
                         <div class="col-md-2" style="width:11%;">
                        <label><font color="red">&nbsp;*</font>Password </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="password" placeholder="Enter Password" id="reset_pwd" name="reset_pwd" class="form-control">
            			 </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-2">
                         <button class="btn btn-warning submitWizard mytab"  type="submit" name="resetpwdbtn" id="resetpwdbtn"><strong>Reset</strong></button>
                        </div>
                       
                        </div>
					  </div>
					   <!-- Step3 ended here -->
                       
                       <!-- Step4 Started here -->
                        <div id="step4" class="p-m tab-pane ">
                        <form role="form" id="addhspform" name="addhspform" method="post">	
                        <div class="row">
                        <div class="col-md-2" style="width:14%;">
                        <label><font color="red">&nbsp;*</font>Hospital Name</label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder="Enter Hospital Name" id="hspname" name="hspname" class="form-control"  value="">
            			 </div>
                        </div>
                         <div class="col-md-2" style="width:11%;">
                        <label>Registration Number </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder="Enter Reg.number" id="regnum" name="regnum" class="form-control" value="">
            			 </div>
                        </div>
                        </div>
<br>
						<div class="row">
                        <div class="col-md-2" style="width:14%;">
                        <label>Phone Number </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                         <input type="text" placeholder="Enter Phone Number" id="phnum" name="phnum" class="form-control onlynumber" value="" maxlength="10">
            			 </div>
                        </div>
                         <div class="col-md-2" style="width:11%;">
                        <label>Hospital Logo </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="file" id="hsplogo" name="hsplogo" class="form-control">
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <div class="row">
                        <div class="col-md-2" style="width:14%;">
							<label>Location:</label>
							</div>
							<div class="col-md-2">
							<input type="text" name="det_location" id="det_location" class="location form-control" value="">
							<div class="geo-details" style="display:none">
			  				<input type="hidden" data-geo="country" value="" id="det_country" name="det_country">
			 				<input type="hidden" data-geo="country_short" value="" id="det_country_short" name="det_country_short">
			  				<input type="hidden" data-geo="administrative_area_level_1" value="" id="det_state" name="det_state">
			  				<input type="hidden" data-geo="administrative_area_level_1_short" value="" id="det_state_short" name="det_state_short">
			  				<input type="hidden" data-geo="administrative_area_level_2" value="" id="det_city" name="det_city">
			  				<input type="hidden" data-geo="lat" value="" id="det_latitude" name="det_latitude">
			  				<input type="hidden" data-geo="lng" value="" id="det_longitude" name="det_longitude">
							</div>
							</div>
							  <div class="col-md-2" style="width:11%;">
                        <label><font color="red">&nbsp;*</font>Districts </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                         <select id="add_hsp_districts" name="add_hsp_districts" class="form-control" >
                          </select>
            			 </div>
                        </div>
							
		                </div>	
                        <br>
                        <div class="row">
                          <div class="col-md-2" style="width:14%;">
                        <label><font color="red">&nbsp;*</font>Address</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                          <textarea rows="4" cols="50" id="address" name="address" class="form-control" placeholder="Enter Address" value=""> </textarea>
                          <span> In comma  separate with out hospital name</span>
            			 </div>
                        </div>
                        </div>
					</form>
                        
                        <div class="row">
                        <div class="col-md-2">
                         <button class="btn btn-warning submitWizard mytab"  type="submit" name="addhspbtn" id="addhspbtn"><strong>Add Hospital </strong></button>
                        </div>
                       
                        </div>
                        </div> 
                       <!--  Step 4 ednded here -->
                       <!--  Step5 Started here -->
                       
                       <div id="step5" class="p-m tab-pane ">
                       <div class="row">
                         <div class="col-md-2" style="width:14%;">
                        <label><font color="red">&nbsp;*</font>District  </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <select id="update_hsp_districts" name="update_hsp_districts" class="form-control" >
                          </select>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder=" Hospital Name" id="update_hsp_name" name="update_hsp_name" class="form-control">
            			 </div>
                        </div>
                        <div class="col-md-1">
                         <button class="btn btn-warning submitWizard mytab btn-xs"  type="submit" name="gethspdetbtn" id="gethspdetbtn"><strong>Get Details</strong></button>
                        </div>
                        </div>
                        <br>
                        <form role="form" id="updatehspform" name="updatehspform" method="post">	
                       <div class="row">
						<div class="col-md-2" style="width:14%;">
                        <label>Hospital Name </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder="Enter Hospital Name" id="update_hspname" name="update_hspname" class="form-control">
            			 </div>
                        </div>
                        
                        <div class="col-md-2" style="width:11%;">
                        <label>Phone Number </label>
                        </div>
                         
                        <div class="col-md-2">
                        <div class="form-group">
                         <input type="text" placeholder="Enter Phone Number" id="update_phnum" name="update_phnum" class="form-control onlynumber" maxlength="10">
            			 </div>
                        </div>
                         
                        </div>
                        <br>
                        
                        <div class="row">
                        <div class="col-md-2" style="width:14%;">
                        <label>Hospital Logo </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="file" id="update_hsplogo" name="update_hsplogo" class="form-control">
                          <input type="hidden" id="logo_original" name="logo_original" class="form-control">
                          <input type="hidden" id="logo_modify" name="logo_modify" class="form-control">
            			 </div>
                        </div> 
                        
                         <div class="col-md-2" style="width:11%;">
							<label>Location:</label>
							</div>
							<div class="col-md-2">
							<input type="text" name="update_det_location" id="update_det_location" class="location form-control">
							<div class="geo-details" style="display:none">
			  				<input type="hidden" data-geo="country" value="" id="update_det_country" name="update_det_country">
			 				<input type="hidden" data-geo="country_short" value="" id="update_det_country_short" name="update_det_country_short">
			  				<input type="hidden" data-geo="administrative_area_level_1" value="" id="update_det_state" name="update_det_state">
			  				<input type="hidden" data-geo="administrative_area_level_1_short" value="" id="update_det_state_short" name="update_det_state_short">
			  				<input type="hidden" data-geo="administrative_area_level_2" value="" id="update_det_city" name="update_det_city">
			  				<input type="hidden" data-geo="lat" value="" id="update_det_latitude" name="update_det_latitude">
			  				<input type="hidden" data-geo="lng" value="" id="update_det_longitude" name="update_det_longitude">
							</div>
							</div>
                        <input type="hidden" name="update_hospitalid" id="update_hospitalid" >
                        
                        </div>	
                        <div class="row">
                        
                         <div class="col-md-2" style="width:14%;">
                        <label>Registration Number </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <input type="text" placeholder="Enter Reg.number" id="update_regnum" name="update_regnum" class="form-control">
            			 </div>
                        </div>
                         <div class="col-md-2" style="width:11%;">
                        <label><font color="red">&nbsp;*</font>District  </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <select id="update_hsp_districts2" name="update_hsp_districts2" class="form-control" >
                          </select>
            			 </div>
                        </div>
                          
                        </div>
                        <div class="row">
                        <div class="col-md-2">
                        <label>Address </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <textarea rows="4" cols="50" id="update_address" name="update_address" class="form-control" placeholder="Enter Address" > </textarea>
                          <span> In comma  separate with out hospital name</span>
            			 </div>
                        </div>
                        </div>
                 	</form>
                        
                        <div class="row">
                         
                        <div class="col-md-2">
                         <button class="btn btn-warning submitWizard mytab"  type="submit" name="updatehspdetbtn" id="updatehspdetbtn"><strong>Update</strong></button>
                        </div>
                       
                        </div>
                        <br><br>
                        <div id="reporttable" > 
                     		
                     		</div>
                        </div> 
                        <!--  Step5 ended here -->
                        </div>
                        <!-- Tabs Content here -->
                  

                  

                </div>
            </div>
        </div>

    </div>
        </div>

     <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->
 
</div>



<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="scripts/custscripts/jquery.geocomplete.min.js"></script>
<!-- Vendor scripts ended here-->

<!-- App scripts -->
<script src="scripts/homer.js"></script>

<!-- RBSK scripts started here-->
 <script src="scripts/custscripts/admin/adminmodule.js"></script>
 <script src="scripts/custscripts/fieldvalidation.js"></script>
<script src="scripts/custscripts/mytoastr.js"></script>


<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<script  type="text/javascript">
//geo location auto complete started here
$("#det_location").geocomplete({
	details: ".geo-details",
	detailsAttribute: "data-geo"
});
$("#update_det_location").geocomplete({
	details: ".geo-details",
	detailsAttribute: "data-geo"
});

//  select fields filter load started here
$(".js-source-states-2").select2();
// select fields filter load ended here

</script>
<script type="text/javascript">
            $(function () {
            	
            	// tabs color change started here
    			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        		$('a[data-toggle="tab"]').removeClass('btn-info');
        		$('a[data-toggle="tab"]').addClass('btn-default');
        		$(this).removeClass('btn-default');
        		$(this).addClass('btn-info');
    			});
    			// tabs color change ended here
    
    			// tab next button on click logic started here
    			$('.next').click(function(){
        		var nextId = $(this).parents('.tab-pane').next().attr("id");
        		$('[href=#'+nextId+']').tab('show');
    			});
    			// tab next button on click logic started here
    
    			// tab previous button on click logic started here
    			$('.prev').click(function(){
        		var prevId = $(this).parents('.tab-pane').prev().attr("id");
        		$('[href=#'+prevId+']').tab('show');
    			});
    			
            	//tab previous button on click logic ended here
            });
            	</script>
<!-- Healtha Pinata scripts ended here-->

</body>
</html>