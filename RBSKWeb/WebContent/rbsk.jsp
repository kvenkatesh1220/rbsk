<html>
<head>
    <!-- ==========================
    	Meta Tags 
    =========================== -->
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- ==========================
    	Title 
    =========================== -->
    <title>RBSK | Telangana Brith Defect E-Regstry.</title>
    
    <!-- ==========================
    	Favicons 
    =========================== -->
    <link rel="shortcut icon" href="icons/favicon.ico">
	<link rel="apple-touch-icon" href="icons/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-touch-icon-114x114.png">
    
    <!-- ==========================
    	Fonts 
    =========================== -->
    <link href='homecss/custcss/opensans.css' rel='stylesheet' type='text/css'>
	<link href='homecss/custcss/roboto.css' rel='stylesheet' type='text/css'>
    
    <!-- ==========================
    	CSS 
    =========================== -->
    <link href="homecss/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="homecss/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="homecss/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="homecss/animate.css" rel="stylesheet" type="text/css">
    <link href="homecss/yamm.css" rel="stylesheet" type="text/css">
    <link href="homecss/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css">
    <link href="homecss/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="homecss/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="homecss/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="homecss/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="homecss/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="homecss/color-switcher.css" rel="stylesheet" type="text/css">
    <link href="homecss/color.css" rel="stylesheet" type="text/css">
    <link href="homecss/custom4index.css" rel="stylesheet" type="text/css">
    
    
   
    
</head>
<body class="color-blue">
   
   <!-- ==========================
    	Header - START 
    =========================== -->
   <header class="navbar yamm navbar-default navbar-static-top" style="height:140px; background-color:#ffffff; " >
    	<div class="container">
    	       <div class="row" >
    	       		<div class="col-lg-3">
                	<a href="/index.jsp" class="navbar-brand"><img src="image/rbsk/logo.jpg" alt="" style="height:130px;"></a>
                	</div>
                	<div class="col-lg-9">
               		<h1 style="color:#FF69B4; font-weight:bold; ">Telangana Birth Defect E-Registry</h>
                	</div>
              </div>
     	</div>
    </header> 
   <!-- ==========================
    	Header - Ended 
    =========================== -->   

    <!-- ==========================
    	JUMBOTRON - START 
    =========================== -->
    <div class="jumbotron jumbotron6">  
        <div id="jumbotron-slider" class="owl-carousel hidden-control owl-theme">       
            
            <div class="item" id="slide-2">
            	<div class="container">
                	<div class="row">
                        <div class="col-md-12">
                        	<h2>Welcome to Birth Defect E-Registry</h2>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="item" id="slide-2" style="background-image: url(../image/rbsk/babyneed5.png);">
            	<div class="container">
                	<div class="row">
                    </div>
                </div>
            </div>
            
            <div class="item" id="slide-2" style="background-image: url(../image/rbsk/projlaunch.png);">
            	<div class="container">
                	<div class="row">
                        <div class="col-md-12">
                        </div>
                        <div class="col-md-12">
                        </div>
                        <div class="col-md-12">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>  
    <!-- ==========================
    	JUMBOTRON - END 
    =========================== -->
          
    
    <!-- ==========================
    	 SECTION - START 
    =========================== -->
    <section  id="section-features">
    	<div class="container">
            <div class="row">
            	<!-- Telangana Map column started here -->
            	<div class="col-sm-10 col-md-4" style="height:350px;">
                	<div class="feature">
                    	<img src="image/rbsk/telmap.png" height="100%;" width="100%;" align="right"/>
                    </div>
                </div>
                <!-- Telangana Map column ended  here -->
                <!-- rbsk imanges column started here -->
				<div class="col-sm-10 col-md-4" style="height:160px;">
                	<div class="feature">
                    	<img src="image/rbsk/rbsk3.jpg" height="100%;" width="100%;" align="left" style="padding-bottom:20px;"/>
                    </div>
                </div>
                <!-- rbsk imanges column ended here -->
                <!-- Login form column started here -->
				<div class="col-sm-10 col-md-4" style="background-color:#00334d;border-radius: 15px;padding-bottom:10px;">
                    	<form action="/SignIn" id="loginForm" name="loginForm" method="post">
                    			<div class="row">
                    			<%if(request.getAttribute("loginError")!=null){ %>
                    			<font color="red" size="3"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= request.getAttribute("loginError") %></b></font>
                    			<%} %>
                        		<br>
                        	
                        		<div class="col-md-6"><label   style="color:#ffffff; padding-top:5%;font-size:16px;">Username</label></div>
                        		<div class=" col-md-6"><input type="text" name="username" id="username" class="form-control" Placeholder="Enter User Name."/></div>
                    			</div>
                    			<br>
                    			<div class="row">
                         		<div class="col-md-6"><label   style="color:#ffffff; padding-top:5%;font-size:16px;">Password</label></div>
                            	<div class="form-group col-md-6"><input type="password" name="password" id="password" class="form-control"  Placeholder="Enter Password."/>
                            	</div>
                            	</div>
                            	<br>
                            	<div  class="row">
                           		<div class="col-md-6"> 
                           		<div class="input-group">
                           		<img src="/CaptchaServlet" height="40" width="100%" id="captchaimg" name="captchaimg"> 
                           		<span class="input-group-addon"><a href="#">
                           		<i class="fa fa-refresh"  id="refreshcaptcha" ></i></a></span>
                           		</div>
                               </div>
                               <div class="form-group col-md-6">
                               <input type="text" name="captchatxt" id="captchatxt" class="form-control" Placeholder="Enter Image Text."/>
                               <br>  <button class="btn btn-warning" type="submit" id="loginbtn">&nbsp;&nbsp;&nbsp;&nbsp;Login&nbsp;</button>
                               </div>
                            	</div>
                    	</form>
                </div>
				<!-- Login Form column ended here -->
            </div>
        </div>
    </section>
    <!-- ==========================
    	SECTION - END 
    =========================== -->
    
       <!-- ==========================
    	Slides - START 
    =========================== -->
    <section class="content" id="section-partners">
        	<div id="partners-slider">       
                <div class="item" style="width:100%; height:200px;"><a ><img src="image/rbsk/rbsk.png" alt="" class="img-responsive" style="width:100%; height:100%;"></a></div>
                <div class="item" style="width:100%; height:200px;"><a ><img src="image/rbsk/rbskcarosel1.jpg" alt="" class="img-responsive" style="width:100%; height:100%;"></a></div>
                <div class="item" style="width:100%; height:200px;"><a ><img src="image/rbsk/rbskcarosel2.jpg" alt=""  class="img-responsive" style="width:100%; height:100%;"></a></div>
                <div class="item" style="width:100%; height:200px;"><a ><img src="image/rbsk/rbskcarosel3.jpg" alt="" class="img-responsive" style="width:100%; height:100%;"></a></div>
                <div class="item" style="width:100%; height:200px;"><a ><img src="image/rbsk/rbskcarosel4.jpg" alt=""  class="img-responsive" style="width:100%; height:100%;"></a></div>
                <div class="item" style="width:100%; height:200px;"><a ><img src="image/rbsk/rbskcarosel1.jpg" alt="" class="img-responsive" style="width:100%; height:100%;"></a></div>
                <div class="item" style="width:100%; height:200px;"><a ><img src="image/rbsk/rbskcarosel2.jpg" alt="" class="img-responsive" style="width:100%; height:100%;"></a></div>
            </div>
    </section>
    <!-- ==========================
    	Slides - END 
    =========================== -->
    
  <!-- ==========================
    	Message - START 
    =========================== -->
    <section class="content content-separator" style="background-color:#FF69B4;">
    		<div class="container">
        	<p class="text-lg text-center"><b >Rashtriya Bal Swasthya Karyakram (RBSK)</b> is an important initiative aiming at early identification and early intervention for children from birth to 18 years to cover defects at birth, deficiencies, diseases, development delays including disability.</p>
        	</div>
    </section>
    <!-- ==========================
    	Message - END 
    =========================== -->

    <!-- ==========================
    	Footer - Start 
    =========================== -->

      <footer class="footer">
 			<div class="row">
 				<div class="col-md-4">
 	 			<a href="http://keansa.com" target="_blank">Keansa Solutions LLP.</a>
 	 			</div>
 	 			<div class="col-md-4">
 	 			<div id="google_translate_element"></div>
 	 			</div>
 	 			<div class="col-md-4">
 	     		<span class="pull-right">
        		All Rights Reserved.
     			</span>
    			</div>
    		</div>
     </footer>
   	
   	<!-- ==========================
    	JS 
    =========================== -->        
	<script src="homejs/jquery-latest.min.js"></script>
    <script src="homejs/plugins/jquery-ui.js"></script>
    <script src="homejs/googlemaps.js"></script>
	<script src="homejs/bootstrap.min.js"></script>
    <script src="homejs/waypoints.min.js"></script>
    <script src="homejs/owl.carousel.js"></script>
    <script src="homejs/isotope.pkgd.min.js"></script>
    <script src="homejs/jquery.magnific-popup.min.js"></script>
    <script src="homejs/creative-brands.js"></script>
    <script src="homejs/color-switcher.js"></script>
    <script src="homejs/jquery.countTo.js"></script>
    <script src="homejs/jquery.countdown.js"></script>
    <script src="homejs/custom.js"></script>
    <script src="homejs/index.js"></script>
    <script src="homejs/validator.min.js"></script>
    
    <script src="scripts/custscripts/fieldvalidation.js"></script>
    
</body>

    <!-- ==========================
    	Google Analytics Statrted here
    =========================== -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-74077689-1', 'auto');
  ga('send', 'pageview');
</script>
    <!-- ==========================
    	Google Analytics Ended here
    =========================== -->
</html>
