<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
	<!-- Page title ended here-->


    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->
    
    <!-- Health Pinata styles started here -->
      <!-- <link rel="stylesheet" type="text/css" href="styles/mycustsytles/sweetalert.css"> -->
   <!--  Health Pinata styles ended here  -->


</head>
<body>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p>A software to upload birth defects </p><img src="image/loading-bars.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->



<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>
 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->

<div class="boxed-wrapper">
<!-- Main Wrapper started here -->
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            <h4 class="font-light m-b-xs text-info">
               General Reports
            </h4>
            <small>View or Edit Birth Reports.</small>
        </div>
    </div>
</div>

<div class="content animate-panel">

<div class="row">
     <div class="col-md-12">
                <div class="hpanel">
                 <div class="panel-heading">
                    <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                      &nbsp;
                    </div>
                    <div class="panel-body">
                        
					<form role="form" id="birthreport" name="birthreport" method="post">
					<div id="formdiv">
						<!-- Heading 1 -->
						<div class="row">
						<div class="col-md-4">
                        </div>
                        <div class="col-md-4">
						
						</div>
						<div  class="col-md-4 pull-right">
						<font size="4" color="#3300FF" >
						<!-- <a href="#" onclick="printauditreportPDF()"><i class="fa fa-file-pdf-o"></i>  </a>  &nbsp;&nbsp;&nbsp; -->
						<a href="#" onclick="printauditinEXL()"><i class="fa fa-file-excel-o"></i> Download</a> </font>
						</div>
						</div>
						<br><br>

                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2" style="width:15%;">
                        <br>
                        <label><font color="red">*</font>Report Duration </label>
                        
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                        From : <input type="text" placeholder="DD-MON-YYYY" id="datefrom" name="datefrom" class="form-control">
                        
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                        To : <input type="text" placeholder="DD-MON-YYYY" id="dateto" name="dateto" class="form-control">
            			 </div>
                        </div>
                       <!--  <div class="col-md-2">
                        <label>Aadhar No.</label>
                        </div> -->
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="text" placeholder="Aadhar Number" id="aadharnum" name="aadharnum" class="form-control onlynumber" maxlength="12">
                         </div>
                        </div>
                        </div>
<!--  Row Divider -->
						<div class="row">
                        <div class="col-md-2" style="width:15%;">
                        <label>Location </label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="districts" name="districts" class="form-control" >
                       <!-- <option value="">Select District</option> <option value="00">00</option>	<option value="01">01</option> -->
                        </select>
                         </div>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="clusters" name="clusters" class="form-control" >
                       <option value="">Select Cluster</option> <!-- <option value="00">00</option>	<option value="01">01</option>  -->
                       </select>
                         </div>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="sources" name="sources" class="form-control" >
                       <option value="">Select Source</option>
						<option value="ASHA">ASHA</option>
						<option value="UHC">UHC</option>
						<option value="PHC">PHC</option>
						<option value="CHC">CHC</option>
						<option value="Government_Medical_College">Government Medical College</option>
						<option value="District_Hospital">District Hospital</option>
						<option value="Private_Medical_College">Private Medical College</option>
						<option value="Private_Nursing_Home/Hospital/tertiary_centre">Private Nursing Home/Hospital/ tertiary centre</option>
						<option value="Mobile_Health_Team">Mobile Health Team</option>
						<option value="ANM">ANM</option>
						<option value="Others">Others</option>
						<option value="UPHC">UPHC</option>
						<option value="AH">AH</option>
                       </select>
                         </div>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="hospitals" name="hospitals" class="form-control" >
                       <option value="">Select Hospital</option> <!-- <option value="00">00</option>	<option value="01">01</option>  -->
                       </select>
                         </div>
                        </div>
                        </div>
                        
                         <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2" style="width:15%;">
                        <label>Defects</label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="defectcategory" name="defectcategory" class="form-control" >
                       <!-- <option value="">Select Category</option> <option value="00">00</option>	<option value="01">01</option>  -->
                       </select>
                         </div>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="defectsubcategory" name="defectsubcategory" class="form-control" >
                        <option value="">Select Sub-Category</option><!-- <option value="00">00</option>	<option value="01">01</option> -->
                        </select>
                         </div>
                        </div>
                        </div>
                         <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2" style="width:15%;">
                        <label>Age of identification</label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="ageofidentification" name="ageofidentification" class="form-control" >
                       	  
						<option value="">Select Age of identification</option>
						<option value="At_birth">At birth</option>
						<option value="Less_Than_1_month">Less Than 1 month</option>
						<option value="1-12_months">1-12 months</option>
						<option value="1-6_yrs">1-6 yrs</option>
						<option value="Above_6_years">Above 6 years</option>
						<option value="Prenatal_diagnosis">Prenatal diagnosis</option>
						<option value="O_Spont_Abortion">O Spont Abortion</option>
						<option value="Autopsy">Autopsy</option>
						</select>
                         </div>
                        </div>
                        </div>
                        
               <!-- Row Divider -->
               			<div class="row">
               			<div class="col-md-2" style="width:15%;">
                        <label>Delivery Type</label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                        <!-- Select box -->  
                        <select id="typeofdelivery" name="typeofdelivery" class="form-control" >
                       <option value="">Select Delivery Type</option> 
                       <option value="Normaldelivery">Normal Delivery</option>	
                       <option value="Cesareandelivery">Cesarean Delivery</option> </select>
                         </div>
                        </div>
               			</div>
                        <div class="row">
                        <div class="col-md-2" style="width:15%;">
                        <label>Birth Type</label>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="birthtype" name="birthtype" class="form-control" >
                       <option value="">Select Birth Type</option> 
                       <option value="Normal">Normal Birth</option>	
                       <option value="Defect">Defect Birth</option> </select>
                         </div>
                        </div>
                    </div>
                    
              <!-- Row Divider -->
                        
                        </div>	
					
                    </form>   
                    <div class="row">
                        <div class="col-md-2">
                        <div class="form-group">
                        <button class="btn btn-sm btn-info m-t-n-xs demo1" type="submit" name="getreport" id="getreport"><strong>Get Report</strong></button>
            			 </div>
                        </div>
                        </div>               
                    </div>
                </div>
            </div>
        </div>



<!--  Location  Table div started here -->
 <div class="row" id="loctablediv">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="panel-heading">
                    <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    Report
                    </div>
                    
                     <div class="panel-body">
                     <div id="noofrec"> 
                     		
                     		</div>
                     		
                     		<div id="reporttable" > 
                     		
                     		</div> <!-- // table will be loadded here  -->
                	</div>
                </div>
            </div>
            
 </div>    
 <!--  Location  Table div ended here -->
 

</div>


 	
    
    <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->

</div>
<!-- Main Wrapper ended here -->
</div>


<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<!-- Vendor scripts ended here-->

<!-- App scripts started here-->
<script src="scripts/homer.js"></script>
<!-- App scripts ended here-->

<!-- Healtha Pinata scripts started here-->
<script src="scripts/custscripts/childbirth/reports.js"></script>
<script src="scripts/custscripts/childbirth/printreports.js"></script>
<script src="scripts/custscripts/fieldvalidation.js"></script> 
<script src="scripts/custscripts/mytoastr.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- Healtha Pinata scripts ended here-->



</body>
</html>