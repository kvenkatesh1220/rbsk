<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
    <!-- Page title ended here-->

    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
	<link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
    <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
	
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->

</head>
<body>

<!-- Simple splash screen-->
<div class="splash" id="mysplash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p> A software to upload birth defects</p><img src="image/loading-bars.svg" width="64" height="64" /><p>Please Wait</p> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>
 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            
            <h4 class="font-light m-b-xs text-info">
              <b> Birth Defect E-Registry</b>
            </h4>
            <small>Birth Defect Screening cum Reporting Form.</small> <span class="pull-right"><font color="red">&nbsp;*&nbsp;</font>marked fields are mandatory.</span>
        </div>
    </div>
</div>

<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    Please complete all 9 steps to register baby details.
                </div>
                <div class="panel-body">

                    <form   id="birthform" name="birthform" enctype="multipart/form-data" method="post" action="/BirthManagementServlet">
                   
                       <!-- Tabs Started here -->
                        <div class="text-center m-b-md" id="wizardControl">
                            <a class="btn btn-info mytab" href="#step1" data-toggle="tab">Step 1 * </a>
                            <a class="btn btn-default mytab" href="#step2" data-toggle="tab">Step 2 * </a>
                            <a class="btn btn-default mytab" href="#step3" data-toggle="tab">Step 3 </a>
                            <a class="btn btn-default mytab" href="#step4" data-toggle="tab">Step 4 </a>
                            <a class="btn btn-default mytab" href="#step5" data-toggle="tab">Step 5 </a>
                            <a class="btn btn-default mytab" href="#step6" data-toggle="tab">Step 6 </a>
                            <a class="btn btn-default mytab" href="#step7" data-toggle="tab">Step 7 </a>
                            <a class="btn btn-default mytab" href="#step8" data-toggle="tab">Step 8 </a>
                            <a class="btn btn-default mytab" href="#step9" data-toggle="tab">Step 9 </a>
							<font size="2" class="text-info">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b id="curbabyrecstat"></b></font>
                        </div>
                        
						<!-- Tabs Started here -->
						
						<!-- Tabs Content here -->
                        <div class="tab-content">
                        
                        <!-- Step 1 Started here -->
                        	<div id="step1" class="p-m tab-pane active">
                        	<div class="row" id="noofbabiesdiv" >
                        	
                        	<div class="col-md-2" style="width:11%;"	>
                        	<label>No of Babies</label>
                        	</div>
                        	<div class="col-md-4">
                        	<div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofbabies1" name="noofbabies" value="1" class="noofbabopt" checked><label>Single</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofbabies2" name="noofbabies" value="2" class="noofbabopt"><label>Twin</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofbabiesm" name="noofbabies" value="Multiple" ><label>Multiple</label>
                        	</div>
                        	<input type="text" placeholder="Enter No oF Babies" id="noofbabiesc" name="noofbabiesc" class="form-control onlynumber">
                        	<input type="hidden" id="hiddenbabyfield" name="hiddenbabyfield" value="1">
                        	<div id="noofbabieserrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Invalid No of babies .</i></font></div>
            			 	</div>
                        	</div>
                        	</div>
                        	
    					 	<!-- Heading 1 -->
							<div class="row">
							<h4>&nbsp;&nbsp;&nbsp;Location Of Reporting</h4>
							</div>
							<br>

                        	<!-- Row Divider -->
							<div class="row">
							<div class="col-md-2">
                        	<label><font color="red">*</font>Reporting Date</label>
                        	</div>
                        	<div class="col-md-2">
                        	<input type="text" placeholder="Reporting Date" id="reportingdate" name="reportingdate" class="form-control">
                        	<div id="rptdterrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please select reporting date.</i></font></div>
                        	</div>
                        	</div>
                        	<br>
                        	
                        	<!-- Row Divider -->
							<div class="row">
                        	<div class="col-md-2">
                        	<label>&nbsp;State</label>
                        	</div> 
                        	<div class="col-md-4">
                        	<div class="form-group">
	            				<div class="radio radio-info radio-inline">
                            	<input type="radio" id="state" name="state" value="Telangana" checked><label>Telangana</label>
                        		</div>
                        	</div>
                        	</div>
                        	</div>
                        	<br>
                        
                        	<!-- Row Divider -->
							<div class="row">
							<div class="form-group">
                        	<div class="col-md-2">
                        	<label><font color="red">*</font>District</label>
                        	</div> 
                        	<div class="col-md-10">
	                      		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district1" name="district" value="Hyderabad"><label>Hyderabad&nbsp;&nbsp;&nbsp;</label>
                        		</div>
                        		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district2" name="district" value="Adilabad"><label>Adilabad</label>
                            	</div>
                            	<div class="radio radio-info radio-inline rptdist">
	                            <input type="radio" id="district3" name="district" value="Karimnagar"><label>Karimnagar</label>
                        		</div>
                        		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district4" name="district" value="Khammam"><label>Khammam</label>
                            	</div>
                            	<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district5" name="district" value="Mahbubnagar"><label>Mahbubnagar</label>
	                        	</div>
                        		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district6" name="district" value="Medak"><label>Medak</label>
                            	</div>
                            	<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district7" name="district" value="Nalgonda"><label>Nalgonda</label>
                        		</div>
                        		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district8" name="district" value="Nizamabad"><label>Nizamabad</label>
                            	</div>
                            	<br><br>
                            	<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district9" name="district" value="Ranga Reddy"><label>Ranga Reddy</label>
                        		</div>
                        		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="district10" name="district" value="Warangal"><label>Warangal</label>
                            	</div>
                            	<div id="disterrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please select district.</i></font></div>
                        </div>
                        </div>
                        </div>
                        <br> <br>
                        
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label><font color="red">*</font>Source</label>
                        </div>
                        <div class="col-md-10" id="sourcediv"> 
                        <div class="radio radio-info radio-inline mysource"> 
 						<input type="radio" id="sourceASHA" name="source" value="ASHA"> 
						<label> 
						ASHA&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceANM" name="source" value="ANM"> 
						<label> 
						ANM&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourcePHC" name="source" value="PHC"> 
						<label> 
						PHC&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceUPHC" name="source" value="UPHC"> 
						<label> 
						UPHC&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceUHC" name="source" value="UHC"> 
						<label> 
						UHC&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceCHC" name="source" value="CHC"> 
						<label> 
						CHC&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<br> 
						<br> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceAH" name="source" value="AH"> 
						<label> 
						AH&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceDistrict Hospital" name="source" value="District_Hospital"> 
						<label> 
						District Hospital&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourcePrivate Medical College" name="source" value="Private_Medical_College"> 
						<label> 
						Private Medical College&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceGovernment Medical College" name="source" value="Government_Medical_College"> 
						<label> 
						Government Medical College&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<br> 
						<br> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourcePrivate Nursing Home/Hospital/ tertiary centre" name="source" value="Private_Nursing_Home/Hospital/tertiary_centre"> 
						<label> 
						Private Nursing Home/Hospital/ tertiary centre&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysource"> 
						<input type="radio" id="sourceMobile Health Team" name="source" value="Mobile_Health_Team"> 
						<label> 
						Mobile Health Team&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div class="radio radio-info radio-inline mysourceo"> 
						<input type="radio" id="sourceOthers" name="source" value="Others"> 
						<label> 
						Others&nbsp;&nbsp;&nbsp;</label> 
						</div> 
						<div id="otherstextbox" style="display: none"> 
						<input type="text" placeholder="Others" id="others" name="source" /> 
						</div> 
                        
                        </div>
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10" id="sourceerrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please select reporting source.</i></font></div>
                        </div>
                        <br> <br>

                        <!-- Row Divider -->
                        
                        <div class="row">
                        <div class="col-md-2">
                        <label><font color="red">&nbsp;*</font>Cluster</label>
                        </div> 
                        <div class="col-md-4">
                        <input type="text" placeholder="Enter Cluster" id="cluster" name="cluster" class="form-control">
                        <div id="clusterrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please provie cluster name.</i></font></div>
                        </div>
                        
                        </div>
                        <br> <br>
                        
                        
                                <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a id="step1next" class="btn btn-info next mytab2" href="#">Next</a>
                            	</div>

                        </div>
						<!-- Step 1 ended here -->
						
						<!-- Step 2 started here -->
                        <div id="step2" class="p-m tab-pane">

                            <!-- Heading 2 -->
							<div class="row">
							<h4>&nbsp;&nbsp;&nbsp;Delivery Outcome Details</h4>
							</div>
							<br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label><font color="red">*</font>Date of Birth</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <div class="input-group date" id="datetimepicker1">
                        <input type="text" placeholder="Enter Date Of Birth" id="dateofbirth" name="dateofbirth" class="form-control">
                        <div id="doberrmsg" ><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please select date of birth.</i></font></div>
			</div>
            			 </div>
            			 
                        </div>
                        <div class="col-md-2">
                        <label><font color="red">*</font>Birth weight in (gms)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Birth weight (Ex:20.4)" id="birthweightingrms" name="birthweightingrms" class="form-control onlynumberwithdot" maxlength="4">
                        <div id="birthweightingrmserrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please enter valid birth weight.</i></font></div>
            			 </div>
                        </div>
                        </div>
                        <br>
                        <!--  Row Divider -->
                        <div class="row">
                        
                         <div class="col-md-2">
                        <label>MCTS No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter MCTS No" id="mctsno" name="mctsno" class="form-control onlynumber" maxlength="20">
            			</div>
                        </div>
                        
                         <div class="col-md-2">
                        <label>Hospital Name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Hospital Name" id="hospitalname" name="hospitalname" class="form-control">
                        </div>
                        </div>
                        
                        </div>
                        <br>
                        
                        
                        <!-- Row Divider -->
                        <div class="row">
                         <div class="col-md-2">
                         <label>Sex</label>
                         </div>
                         <div class="col-md-4">
                         <div class="form-group ">
                        	<div class="radio radio-info radio-inline sexv">
                            <input type="radio" id="sexm" name="sex" value="Male" class="sexclass"><label>Male</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="sexf" name="sex" value="Female" class="sexclass"><label>Female</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="sexa" name="sex" value="Ambiguous" class="sexclass"><label>Ambiguous</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="sexnon" name="sex" value="" checked><label>None</label>
                        	</div>
                        	<div id="sexnonerrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please select Valid Option.</i></font></div>
            			 </div>
                         </div>
                         <div class="col-md-2">
                            <label><font color="red">*</font>AAdhar Card No</label>
                         </div>
                         <div class="col-md-4">
                         <div class="form-group">
                         <input type="text" placeholder="Mother's Adhar Card No" id="adharno" name="adharno" class="form-control onlynumber" maxlength="12">
                         <div id="adharerrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Enter 12 digit adhar card no.</i></font></div>
            			 </div>
                         </div>
                         </div>
                         <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Mother's name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Mother's name" id="mothersname" name="mothersname" class="form-control " maxlength="50">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Mother's age (Years)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Mother's age" id="mothersage" name="mothersage" class="form-control onlynumber" maxlength="2">
                        <div id="motherageerrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Enter Valid Age.</i></font></div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                         <div class="col-md-2">
                        <label>Father name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Father name" id="fathername" name="fathername" class="form-control" maxlength="50">
            			</div>
                        </div>
                        
                        <div class="col-md-2">
                        <label>Father age</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Father age" id="fathersage" name="fathersage" class="form-control onlynumber" maxlength="2">
                        <div id="fatherageerrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Enter Valid Age.</i></font></div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                         <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth order</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                			<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthorder1" name="birthorder" value="First" checked><label>First</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthorder2" name="birthorder" value="Second"><label>Second</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthorder3" name="birthorder" value="Third"><label>Third</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthordero" name="birthorder" value="Other"><label>Other</label>
                        	</div>
            			 </div>
                        </div>
                        
                        <div class="col-md-2">
                        <label>Baby delivered as</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                			<div class="radio radio-success radio-inline">
                            <input type="radio" id="babydeliveryaslb" name="babydeliveryas" value="Live_Birth" checked><label>Live Birth&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="babydeliveryasis" name="babydeliveryas" value="IUD_Still_birth"><label>IUD/Still birth</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="babydeliveryasmf" name="babydeliveryas" value="MTP_following_anomaly"><label>MTP following anomaly</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="babydeliveryassa" name="babydeliveryas" value="Spontaneous_abortion_with_anomaly"><label>Spontaneous abortion with anomaly</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="babydeliveryassa" name="babydeliveryas" value="Spontaneous_abortion_without_anomaly"><label>Spontaneous abortion without anomaly</label>
                        	</div>
            			 </div>
                        </div>
                      
                        </div>
                        <br>
                        
                        <!--  Row Divider -->
                        <div class="row">
                          <div class="col-md-2">
                        <label>Birth Type</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthtypeN" name="birthtype" value="Normal" checked><label>Normal Birth</label>
                            </div>
                            <br>
                            <div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthtypeD" name="birthtype" value="Defect"><label>Defect Birth</label>
                            </div>
            			</div>
                        </div>
                        
                        <div class="col-md-2">
                        <label>Type of Delivery </label>
                        </div>
                        <div class="col-md-4" id="typeofdeliverydiv">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline deltype">
                            <input type="radio" id="typeofdelivery" name="typeofdelivery" value="Normaldelivery" checked><label>Normal Delivery</label>
                            </div>
                            <br>
                            <div class="radio radio-danger radio-inline deltype">
                            <input type="radio" id="typeofdelivery" name="typeofdelivery" value="Cesareandelivery"><label>Cesarean Delivery</label>
                            </div>   
                            
                            <div class="radio radio-info radio-inline ">
                            <input type="radio" id="typeofdeliveryassisted" name="typeofdeliveryassisted" value="AssistedDelivery"><label>Assisted Delivery</label>
                            </div>
                            
                            <div class="radio radio-info radio-inline typeofdelive">
                            <input    type="radio" id="typeofdeliveryfor" name="typeofdelivery" value="forceps"><label>Forceps</label>
                            </div>
                            
                            <div class="radio radio-info radio-inline typeofdelive">
                            <input    type="radio" id="typeofdeliveryven" name="typeofdelivery" value="ventouse"><label>Ventouse</label>
                            </div>
                            <div class="radio radio-info radio-inline typeofdeliveothr">
                            <input    type="radio" id="typeofdeliveryother" name="typeofdeliveryother" value="typeofdeliveryother"><label>Other</label>
                            </div>
                            <div class="form-group">
                            <input class="typeofdelivetxt form-control"  type="text" id="typeofdeliverytxtbox" name="typeofdelivery" value="" placeholder="Type Of Delivery">
                            </div>
                            <div class="form-group">
                        <input type="text" placeholder="Enter Reason For  Cesarean ." id="resonforcesarean" name="resonforcesarean" class="form-control">
                            </div>
            			</div>
            			<div id="typeofdeliverrmsgcesa"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please Enter Reason for Cesarean.</i></font></div>
            			<div id="typeofdeliverrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please Select Forceps or Ventouse or Enter the Delivery type in Text Box .</i></font></div>
                        </div>
                       
                        </div>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Date Of Identification</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Identification Date" id="dateofidentification" name="dateofidentification" class="form-control">
            			</div>
                        </div>
                         <div class="col-md-2">
                        <label>Mobile No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Mobile No (Ex:9898989898)" id="mobileno" name="mobileno" class="form-control onlynumber" maxlength="10">
                        <div id="mobileerrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Enter Valid Mobile No.</i></font></div>
            			</div>
                        </div>
                       
                        
                       
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Age of identification</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentificationat" name="ageofidentification" value="At_birth"><label>At birth&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentificationlt" name="ageofidentification" value="Less_Than_1_month"><label>Less Than 1 month</label>
                            </div>
                            <br>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentification112" name="ageofidentification" value="1-12_months"><label>1-12 months&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentification16" name="ageofidentification" value="1-6_yrs"><label>1-6 yrs</label>
                            </div>
                            <br>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentificationa6" name="ageofidentification" value="Above_6_years"><label>Above 6 years &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentificationpd" name="ageofidentification" value="Prenatal_diagnosis"><label>Prenatal diagnosis</label>
                            </div>
                            <br>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentificationos" name="ageofidentification" value="O_Spont_Abortion"><label>O Spont Abortion </label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="ageofidentificationa" name="ageofidentification" value="Autopsy"><label>Autopsy</label>
                            </div>
                            <br>
                            <div class="radio radio-success radio-inline">
                            <input type="radio" id="ageofidentificationnone" name="ageofidentification" value="" checked><label>None</label>
                            </div>
            			 </div>
            			 </div>
            			  
                        <div class="col-md-2">
                        <label>caste</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="castesc" name="caste" value="SC"><label>SC</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="castest" name="caste" value="ST"><label>ST</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="casteobc" name="caste" value="OBC"><label>OBC</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="casteothers" name="caste" value="Others" checked><label>Others</label>
                            </div>
            			</div>
                        </div>
                        </div>
                         
                        <br>
                        
                        <br>
                            <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a id="step2next" class="btn btn-info next mytab2" href="#">Next</a>
                            </div>

                        </div>
                       <!-- Step2 ended here -->
                       
                       <!-- Step3 started here -->
                       <div id="step3" class="p-m tab-pane ">
    					 
    					 <!-- Heading 3 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Permanent address</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>House No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter House No" id="bhouseno" name="bhouseno" class="form-control" maxlength="50">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Street name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Street name" id="bstreetname" name="bstreetname" class="form-control" maxlength="50">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Area</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Area" id="bareaname" name="bareaname" class="form-control" maxlength="50">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Post office</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Post office" id="bpostoffice" name="bpostoffice" class="form-control" maxlength="50">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>District</label>
                        </div>
                        <div class="col-md-10">
                        <div class="form-group">
            				<div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict1" name="bdistrict" value="Hyderabad"><label>Hyderabad&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict2" name="bdistrict" value="Adilabad"><label>Adilabad</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict3" name="bdistrict" value="Karimnagar"><label>Karimnagar</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict4" name="bdistrict" value="Khammam"><label>Khammam</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict5" name="bdistrict" value="Mahbubnagar"><label>Mahbubnagar</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict6" name="bdistrict" value="Medak"><label>Medak</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict7" name="bdistrict" value="Nalgonda"><label>Nalgonda</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict8" name="bdistrict" value="Nizamabad"><label>Nizamabad</label>
                            </div>
                            <br><br>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict9" name="bdistrict" value="Ranga Reddy"><label>Ranga Reddy</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="bdistrict10" name="bdistrict" value="Warangal"><label>Warangal</label>
                            </div>
                            <br><br>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>State</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
            				<div class="radio radio-info radio-inline">
                            <input type="radio" id="bstate" name="bstate" value="Telangana" checked><label>Telangana</label>
                            </div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Temporary Address Started here -->
                        <div class="row">
							<div class="col-md-4">
                        	<label>Is Residential Address different from Permanent Address</label>
                        	</div>
                        	<div class="col-md-4">
                        	<div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="residentialcheckn" name="residentialcheck" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="residentialchecky" name="residentialcheck" value="Yes"  ><label>Yes</label>
                        	</div>
            				</div>
                        </div>
						</div>
						<br>
                        <div id="residentialdiv">
                        <div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Residential address</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>House No</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter House No" id="bthouseno" name="bthouseno" class="form-control" maxlength="50">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Street name</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Street name" id="btstreetname" name="btstreetname" class="form-control" maxlength="50">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Area</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Area" id="btareaname" name="btareaname" class="form-control" maxlength="50">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Post office</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Post office" id="btpostoffice" name="btpostoffice" class="form-control" maxlength="50">
            			</div>
                        </div>
                        </div>
                        <br>
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>District</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter District" id="btdistrict" name="btdistrict" class="form-control" maxlength="50">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>State</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter State" id="btstate" name="btstate" class="form-control" maxlength="50">
            			</div>
                        </div>
                        </div>
                        <br>
                        </div>
                        
                        <div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Delivery Details</h4>
						</div>
						<br>
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Gestational age(Weeks)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Gestational age" id="gestationalageinweeks" name="gestationalageinweeks" class="form-control onlynumber" maxlength="2">
            			<div id="gestationalageinweekserrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Please enter valid Gestational age.</i></font></div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Last menstrual period</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Last menstrual period Date" id="lastmensuralperiod" name="lastmensuralperiod" class="form-control">
            			 </div>
                        </div>
                        </div>
                        <br>
                         <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth asphyxia</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthasphyxian" name="birthasphyxia" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthasphyxiay" name="birthasphyxia" value="Yes"><label>Yes</label>
                        	</div>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Autopsy shows birth </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="autopsyshowbirthn" name="autopsyshowbirth" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="autopsyshowbirthy" name="autopsyshowbirth" value="Yes"><label>Yes</label>
                        	</div>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Mode of delivery</label>
                        </div>
                        <div class="col-md-4">
                       <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="modeofdeliverym" name="modeofdelivery" value="MTP_following_anomaly"><label>MTP following anomaly</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="modeofdeliverys" name="modeofdelivery" value="Spontaneous_abortion_with_anomaly"><label>Spontaneous abortion with anomaly</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="modeofdeliveryn" name="modeofdelivery" value="" checked><label>None</label>
                        	</div>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>Status of induction/augmentation</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="statusofinductionn" name="statusofinduction" value="" checked><label>Nill</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="statusofinductiono" name="statusofinduction" value="Oxytocin"><label>Oxytocin</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="statusofinductionm" name="statusofinduction" value="Misoprostol"><label>Misoprostol</label>
                        	</div>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Place of birth</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthplacei" name="birthplace" value="Institution" checked><label>Institution/Hospital</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthplaceh" name="birthplace" value="Home"><label>Home</label>
                        	</div>
            			 </div>
                        </div>
                        <div class="col-md-2">
                        <label>State</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthstate" name="birthstate" value="Telangana" checked><label>Telangana</label>
                        	</div>
            			 </div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>District</label>
                        </div>
                        <div class="col-md-10">
                        <div class="form-group">
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict1" name="birthdistict" value="Hyderabad"><label>Hyderabad&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict2" name="birthdistict" value="Adilabad"><label>Adilabad</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict3" name="birthdistict" value="Karimnagar"><label>Karimnagar</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict4" name="birthdistict" value="Khammam"><label>Khammam</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict5" name="birthdistict" value="Mahbubnagar"><label>Mahbubnagar</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict6" name="birthdistict" value="Medak"><label>Medak</label>
                            </div>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict7" name="birthdistict" value="Nalgonda"><label>Nalgonda</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict8" name="birthdistict" value="Nizamabad"><label>Nizamabad</label>
                            </div>
                            <br><br>
                            <div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict9" name="birthdistict" value="Ranga Reddy"><label>Ranga Reddy</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="birthdistict10" name="birthdistict" value="Warangal"><label>Warangal</label>
                            </div>
            			 </div>
                        </div>
                        
                        </div>
                        <br>
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Cluster</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Cluster" id="birthblock" name="birthblock" class="form-control" maxlength="50">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Municipality</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Enter Birth Municipality" id="birthminicipality" name="birthminicipality" class="form-control" maxlength="50">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        
                        <!-- Temporary Address ended here -->
                     <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a class="btn btn-info next mytab2" href="#">Next</a>
                     </div>

					  </div>
					   <!-- Step3 ended here -->
                       
                       <!-- Step4 Started here -->
                       <div id="step4" class="p-m tab-pane ">
    					 
							<!-- Heading 4 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Antenatal details</h4>
						</div>
						<br>
						<!-- Row Divider -->
						<div class="row" id="antenatalcheckdiv">
						<div class="col-md-2">
                        <label>Antenatal details</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="antenatalcheckn" name="antenatalcheck" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="antenatalchecky" name="antenatalcheck" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
						</div>
						
						<div id="antenataldetailsdiv" >
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Assisted conception IVF/ART</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="assistedconceptionn" name="assistedconception" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="assistedconceptiony" name="assistedconception" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Folic acid details</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="folicaciddetailsn" name="folicaciddetails" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="folicaciddetailsy" name="folicaciddetails" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>H/O radiation exposure (x-ray etc)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="horadiationexposuren" name="horadiationexposure" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="horadiationexposurey" name="horadiationexposure" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>H/O serious maternal illness</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
            				<div class="radio radio-success radio-inline">
                            <input type="radio" id="hoseriousmetirialillnessn" name="hoseriousmetirialillness" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="hoseriousmetirialillnessy" name="hoseriousmetirialillness" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>H/O substance abuse</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="hosubstancceabusen" name="hosubstancceabuse" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="hosubstancceabusey" name="hosubstancceabuse" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>History of anomalies in previous pregnancies</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="historyofanomaliesn" name="historyofanomalies" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="historyofanomaliesy" name="historyofanomalies" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Immunisation history (rubella)</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                       		 <div class="radio radio-success radio-inline">
                            <input type="radio" id="immunisationhistoryn" name="immunisationhistory" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="immunisationhistoryy" name="immunisationhistory" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Maternal drugs</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="maternaldrugs" name="maternaldrugs" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="maternaldrugs" name="maternaldrugs" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Maternal details</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="maternaldrugsdescn" name="maternaldrugsdesc" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="maternaldrugsdescy" name="maternaldrugsdesc" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Parental consanguinity</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
            				<div class="radio radio-success radio-inline">
                            <input type="radio" id="parentalconsanguinityn" name="parentalconsanguinity" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="parentalconsanguinityy" name="parentalconsanguinity" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                         <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>No of previous abortion</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="noofpreviousabortionn" name="noofpreviousabortion" value="Nill" checked><label>Nill</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofpreviousabortion1" name="noofpreviousabortion" value="1"><label>1</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofpreviousabortion2" name="noofpreviousabortion" value="2"><label>2</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofpreviousabortion3" name="noofpreviousabortion" value="3"><label>3</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofpreviousabortion4" name="noofpreviousabortion" value="4"><label>4</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="noofpreviousabortion5" name="noofpreviousabortion" value="5"><label>5</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>No of previous still birth</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="nofofpreviousstillbirth" name="nofofpreviousstillbirth" value="Nill" checked><label>Nill</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="nofofpreviousstillbirth1" name="nofofpreviousstillbirth" value="1"><label>1</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="nofofpreviousstillbirth2" name="nofofpreviousstillbirth" value="2"><label>2</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="nofofpreviousstillbirth3" name="nofofpreviousstillbirth" value="3"><label>3</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="nofofpreviousstillbirth4" name="nofofpreviousstillbirth" value="4"><label>4</label>
                        	</div>
                        	<div class="radio radio-info radio-inline">
                            <input type="radio" id="nofofpreviousstillbirth5" name="nofofpreviousstillbirth" value="5"><label>5</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
						<!-- Heading 4 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Neonatal details</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Head circumference</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" placeholder="Head circumference" id="headcircumference" name="headcircumference" class="form-control onlynumber" maxlength="10">
                        <div id="headcircumferenceerrmsg"><br><font color="red" size="3"><i class="fa fa-exclamation-triangle">&nbsp;Must be number .</i></font></div>
            			</div>
                        </div>
                        </div>
                        <br>
						</div>
						<!-- Heading 4 -->                        
                       
                            <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a class="btn btn-info next mytab2" href="#">Next</a>
                            </div>

</div>
                       
                       <div id="step5" class="p-m tab-pane ">
    					 <div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Birth Defects</h4>
						</div>
						<br>
						
						
						<div class="row">
                        <div class="col-md-2">
                        <label>Birth Defects</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group" id="birthdefectchoicecid">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="birthdefectchoicecn" name="birthdefectchoicec" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectchoicecy" name="birthdefectchoicec" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
						</div>
						<!-- Visual Defects started here -->	
 						<div id="defectsdiv2" style="background:#E8E8E8">
 						<!-- Row Divider -->
                        <div class="row">
                        <br>
                        <div class="col-md-2">
                        <label>Nervous System</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="nervoussystem" name="nervoussystem" value="Hydrocephalus"><label>Hydrocephalus</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="nervoussystem" name="nervoussystem" value="Spina Bifida"><label>Spina Bifida</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="nervoussystem" name="nervoussystem" value="Microcephaly"><label>Microcephaly&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="nervoussystem" name="nervoussystem" value="Anencephaly"><label>Anencephaly&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="nervoussystem" name="nervoussystem" value="Encephalocele"><label>Encephalocele</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="nervoussystem" name="nervoussystem" value="" checked><label>None</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="nervoussystem" name="nervoussystem" value="Arhinencephaly/Holoprosencephaly"><label>Arhinencephaly/Holoprosencephaly</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Eye Related</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="eyerelated" name="eyerelated" value="Congenital cataract"><label>Congenital cataract</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="eyerelated" name="eyerelated" value="Microphthalmos"><label>Microphthalmos</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="eyerelated" name="eyerelated" value="Congenital glaucoma"><label>Congenital glaucoma</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="eyerelated" name="eyerelated" value="Anophthalmos"><label>Anophthalmos &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="eyerelated" name="eyerelated" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Ear/Face/Neck</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="earfaceneck" name="earfaceneck" value="Congenital deafness"><label>Congenital deafness</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="earfaceneck" name="earfaceneck" value="Microtia II"><label>Microtia II&nbsp;</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="earfaceneck" name="earfaceneck" value="Microtia III"><label>Microtia III</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="earfaceneck" name="earfaceneck" value="Microtia IV"><label>Microtia IV</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="earfaceneck" name="earfaceneck" value="" checked><label>None</label>
                        	</div>
                        </div>
                        </div>
                        <div class="col-md-2">
                        <label>Oro Facial</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="orafacial" name="orafacial" value="Cleft lip with or without cleft palate"><label>Cleft lip with or without cleft palate</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="orafacial" name="orafacial" value="Cleft palate"><label>Cleft palate</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="orafacial" name="orafacial" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Urinary Tract</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <div class="radio radio-danger radio-inline">
                            <input type="radio" id="urinarytract" name="urinarytract" value="Bladder exstrophy and/or epispadia"><label>Bladder exstrophy and/or epispadia</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="urinarytract" name="urinarytract" value="Congenital Hydronephrosis"><label>Congenital Hydronephrosis</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="urinarytract" name="urinarytract" value="Posterior urethral valve and/or prune belly"><label>Posterior urethral valve and/or prune belly</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="urinarytract" name="urinarytract" value="Multicystic renal Dysplasia"><label>Multicystic renal Dysplasia</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="urinarytract" name="urinarytract" value="Bilateral renal agenesis including Potter syndrome"><label>Bilateral renal agenesis including Potter syndrome</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="urinarytract" name="urinarytract" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Limb Related</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="limbrelated" name="limbrelated" value="Hip dislocation and /or dysplasia"><label>Hip dislocation and /or dysplasia</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="limbrelated" name="limbrelated" value="Limb reduction"><label>Limb reduction</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="limbrelated" name="limbrelated" value="Polydactyly"><label>Polydactyly</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="limbrelated" name="limbrelated" value="Syndactyly"><label>Syndactyly &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="limbrelated" name="limbrelated" value="" checked><label>None</label>
                        	</div>
                        	
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Abdominal Wall</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="abdominalwall" name="abdominalwall" value="Gastroschisis"><label>Gastroschisis</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="abdominalwall" name="abdominalwall" value="Omphalocele"><label>Omphalocele</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="abdominalwall" name="abdominalwall" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Genital Disorders</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="genitaldisorders" name="genitaldisorders" value="Hypospadias"><label>Hypospadias</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="genitaldisorders" name="genitaldisorders" value="indeterminate sex"><label>Indeterminate sex</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="genitaldisorders" name="genitaldisorders" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Chromosomal Disorders</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="chromosomaldisorders" name="chromosomaldisorders" value="Down syndrome"><label>Down syndrome &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="chromosomaldisorders" name="chromosomaldisorders" value="Turner syndrome"><label>Turner syndrome</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="chromosomaldisorders" name="chromosomaldisorders" value="Klinefelter syndrome"><label>Klinefelter syndrome</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="chromosomaldisorders" name="chromosomaldisorders" value="" checked><label>None</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="chromosomaldisorders" name="chromosomaldisorders" value="Patau syndrome/trisomy13"><label>Patau syndrome/trisomy13</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="chromosomaldisorders" name="chromosomaldisorders" value="Edwards syndrome/trisomy18"><label>Edwards syndrome/trisomy18</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Digestive System</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="digestivesystem" name="digestivesystem" value="Amo-rectal atresia and stenosis"><label>Amo-rectal atresia and stenosis</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="digestivesystem" name="digestivesystem" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Other Anomalies</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="skeletal dysplasias"><label>skeletal dysplasias &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Vater/Vacterl"><label>Vater/Vacterl</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Craniostnostosis"><label>Craniostnostosis &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Conjoined twins"><label>Conjoined twins</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Fetal alcohol syndrome"><label>Fetal alcohol syndrome</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="" checked><label>None</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Congenital constriction bands/amniotic band"><label>Congenital constriction bands/amniotic band</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Congenital skin disorders"><label>Congenital skin disorders</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Teratogenic syndromes with malformations"><label>Teratogenic syndromes with malformations</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="otheranomalies" name="otheranomalies" value="Valproate syndrome"><label>Valproate syndrome</label>
                        	</div>
                        	
            			</div>
                        </div>
                        </div>
                        </div>
                        <br><br>								
						<!-- Visual Defects ended here -->
						<div class="row">
                        <div class="col-md-2">
                        <label>Birth defects identified through instruments</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group" >
                            <div class="radio radio-success radio-inline">
                            <input type="radio" id="birthdefectinstchoicen" name="birthdefectchoice" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectinstchoicey" name="birthdefectchoice" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
						</div>
						<br>
						
						
						<div id="defectthroughinstrumentsdiv" style="background:#E8E8E8" > 
						<!-- Row Divider -->
                        <div class="row">
                        <br>
                        
                        <div class="col-md-10">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsinstumental" name="birthdefectsinstumental" value="Congenital deafness"><label>Congenital deafness &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsinstumental" name="birthdefectsinstumental" value="ROP"><label>ROP</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsinstumental" name="birthdefectsinstumental" value="Congenital vision defects"><label>Congenital vision defects </label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsinstumental" name="birthdefectsinstumental" value="CHD"><label>CHD</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="birthdefectsinstumental" name="birthdefectsinstumental" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-2">
                        <label>Birth defects identified through blood test</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                            <div class="radio radio-success radio-inline">
                            <input type="radio" id="defectbloodtestchoicen" name="defectbloodtestchoice" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="defectbloodtestchoicey" name="defectbloodtestchoice" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
						</div>
						<br>
                       
                       <div id="defectthroughbloodtestsdiv" style="background:#E8E8E8" > 
						<!-- Row Divider -->
                        <div class="row">
                        <br>
                        
                        <div class="col-md-10">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="" checked><label>None</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="IEM"><label>IEM</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="Haemoglobinopathy"><label>Haemoglobinopathy</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="CH"><label>CH</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="Thalassemia"><label>Thalassemia</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="CAH"><label>CAH</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="SCD"><label>SCD</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="birthdefectsbloodtest" name="birthdefectsbloodtest" value="G6PD"><label>G6PD</label>
                        	</div>
                        	
            			</div>
                        </div>
                       </div>
                       </div>
                        <br>
						
						
						
                       
                            <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a class="btn btn-info next mytab2" href="#">Next</a>
                            </div>

			</div>
                       
                       <div id="step6" class="p-m tab-pane ">
    					 
    					 <!-- Congenital anomalies  started here -->		
						
						<!-- Heading 5 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Congenital anomalies</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
						<div class="row">
                        <div class="col-md-2">
                        <label>Congenital anomalies</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <div class="radio radio-success radio-inline">
                            <input type="radio" id="congenitalanomalieschoicen" name="congenitalanomalieschoice" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="congenitalanomalieschoicey" name="congenitalanomalieschoice" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
						</div>
						<br>
						
						<div id="congenitalanomaliesdiv1">
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Name</label>
                        </div>
                        <div class="col-md-10">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="name" name="name" value="Congenital deafness"><label>Congenital deafness</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="name" name="name" value="ROP"><label>ROP</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="name" name="name" value="Congenital vision defect"><label>Congenital vision defect</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="name" name="name" value="IEM"><label>IEM</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="name" name="name" value="Congenital Heart Disease"><label>Congenital Heart Disease</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="name" name="name" value="Haemoglobinopathy"><label>Haemoglobinopathy</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="name" name="name" value="" checked><label>None</label>
                        	</div>
                        
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Description of the anomaly</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="description" name="description" class="form-control" maxlength="200">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Age at diagnosis</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <input type="text" id="ageat" name="ageat" class="form-control onlynumber" maxlength="20">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Code- ICD10</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="code" name="code" class="form-control" maxlength="50">
            			</div>
                        </div>
                        </div>
                        <br>	
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Confirmed/Suspected</label>
                        </div>
                        <div class="col-md-10">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="confirmmed" name="confirmmed" value="Confirmed"><label>Confirmed</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="confirmmed" name="confirmmed" value="Suspected"><label>Suspected</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="confirmmed" name="confirmmed" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        </div>		
						<!-- Congenital anomalies  ended here -->			
    					 
						
                       
                            <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a class="btn btn-info next mytab2" href="#">Next</a>
                            </div>

						</div>
						
						<div id="step7" class="p-m tab-pane ">
    					 
    					 <!-- Investigation   started here -->
						
						 
						
						
						<!-- Heading 5 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Investigation details</h4>
						</div>
						<br>
						<div class="row">
                        <div class="col-md-3">
                        <label>Investigation details</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                        	<input type="radio" id="investigationchoicen" name="investigationchoice" value="No" checked><label>No</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="investigationchoicey" name="investigationchoice" value="Yes"><label>Yes</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
						<div id="investigationdiv1">
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Karotype/infantogram/ECG/US abdomen/Brain MRI</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                        	<input type="radio" id="karotype" name="karotype" value="Normal" checked><label>Normal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="karotype" name="karotype" value="Abnormal"><label>Abnormal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-warning radio-inline">
                            <input type="radio" id="karotype" name="karotype" value="Pending"><label>Pending</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-1">
                        <label>Findings</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="karotypefindings" name="karotypefindings" class="form-control" maxlength="200">
            			</div>
                        </div>
                        </div>
                        <br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>BERA, Fundoscopy etc</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                        	<input type="radio" id="bera" name="bera" value="Normal" checked><label>Normal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="bera" name="bera" value="Abnormal"><label>Abnormal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-warning radio-inline">
                            <input type="radio" id="bera" name="bera" value="Pending"><label>Pending</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-1">
                        <label>Findings</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="berafindings" name="berafindings" class="form-control" maxlength="200">
            			</div>
                        </div>
                        </div>
                        <br>
						
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Blood test for O CH</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                        	<input type="radio" id="blodtestforch" name="blodtestforch" value="Normal" checked><label>Normal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="blodtestforch" name="blodtestforch" value="Abnormal"><label>Abnormal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-warning radio-inline">
                            <input type="radio" id="blodtestforch" name="blodtestforch" value="Pending"><label>Pending</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-1">
                        <label>Findings</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="bloodtestfindings" name="bloodtestfindings" class="form-control" maxlength="200">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Blood test for O CAH</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                        	<input type="radio" id="bloodtestforcah" name="bloodtestforcah" value="Normal" checked><label>Normal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="bloodtestforcah" name="bloodtestforcah" value="Abnormal"><label>Abnormal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-warning radio-inline">
                            <input type="radio" id="bloodtestforcah" name="bloodtestforcah" value="Pending"><label>Pending</label>
                        	</div>
            			</div>
                        </div>
                        <div class="col-md-3">
                        <label>Blood test for G6PD</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                         	<div class="radio radio-success radio-inline">
                        	<input type="radio" id="bloodtestforg6pd" name="bloodtestforg6pd" value="Normal" checked><label>Normal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="bloodtestforg6pd" name="bloodtestforg6pd" value="Abnormal"><label>Abnormal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-warning radio-inline">
                            <input type="radio" id="bloodtestforg6pd" name="bloodtestforg6pd" value="Pending"><label>Pending</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        <label>Blood test for SCD</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-success radio-inline">
                        	<input type="radio" id="bloodtestforscd" name="bloodtestforscd" value="Normal" checked><label>Normal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id=bloodtestforscd name="bloodtestforscd" value="Abnormal"><label>Abnormal</label>
                        	</div>
                        	<br>
                        	<div class="radio radio-warning radio-inline">
                            <input type="radio" id="bloodtestforscd" name="bloodtestforscd" value="Pending"><label>Pending</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        <br>
                        </div>						
						<!-- Investigation   ended here -->
						
						
                       
                            <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a class="btn btn-info next mytab2" href="#">Next</a>
                            </div>

</div>
                       
                       <div id="step8" class="p-m tab-pane ">
    					 <!-- Heading  -->
						
						<!-- Diagnosis    started here -->
						
						<!-- Heading 4 -->
						<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;Diagnosis  details</h4>
						</div>
						<br>
						
						<!-- Row Divider -->
						<div id="diagnosisdetailsdiv">
                        <div class="row">
                        <div class="col-md-2">
                        <label>anomaly </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="anomaly" name="anomaly" class="form-control" maxlength="200">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>syndrome </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="syndrome" name="syndrome" class="form-control" maxlength="200">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Diagnosis    started here -->
						<!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Provisional diagnosis </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="provisional" name="provisional" class="form-control" maxlength="200">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Complete diagnosis </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="complediagnosis" name="complediagnosis" class="form-control" maxlength="200">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Notifying person </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="notifyingperson" name="notifyingperson" class="form-control" maxlength="200">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Designation and contact details</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="designationofcontact" name="designationofcontact" class="form-control" maxlength="200">
            			</div>
                        </div>
                        </div>
                        <br>
                        
                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-2">
                        <label>Facility referred </label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        <input type="text" id="facilityreferred" name="facilityreferred" class="form-control" maxlength="200">
            			</div>
                        </div>
                        <div class="col-md-2">
                        <label>Provisional diagnosis status</label>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="provisionaldiagstat" name="provisionaldiagstat" value="Confirmed"><label>Confirmed</label>
                        	</div>
                        	<div class="radio radio-danger radio-inline">
                            <input type="radio" id="provisionaldiagstat" name="provisionaldiagstat" value="Suspected"><label>Suspected</label>
                        	</div>
                        	<div class="radio radio-success radio-inline">
                            <input type="radio" id="provisionaldiagstat" name="provisionaldiagstat" value="" checked><label>None</label>
                        	</div>
            			</div>
                        </div>
                        </div>
                        </div>
                        <br>
						
						<!-- Diagnosis    ended here -->
						
						
                       
                            <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                                <a class="btn btn-info next mytab2" href="#">Next</a>
                            </div>

						</div>
                       
                       
                        <div id="step9" class="tab-pane">
                             <!-- Row Divider -->
                        <div id="babyphotosdiv">
                        <div class="row">
                        <!-- Column started here -->
                        <div class="col-md-6">
                        <div class="col-md-4">
                        <label>Baby Photo1 </label>
                        </div>
                        <div class="col-md-8">
                        <div class="form-group">
                        <input type="file" id="babyphoto1" name="babyphoto1" class="form-control">
            			</div>
                        </div>
                        <br>
                        <div class="col-md-4">
                        <label>Baby Photo2 </label>
                        </div>
                        <div class="col-md-8">
                        <div class="form-group">
                        <input type="file" id="babyphoto2" name="babyphoto2" class="form-control">
            			</div>
                        </div>
                        </div>
                        <!-- Column ended here -->
                         <!-- Column started here -->
                        <div class="col-md-6">
                        <div class="col-md-4">
                        <label>Identify Proof </label>
                        </div>
                        <div class="col-md-8">
                        <div class="form-group">
                        <input type="file" id="idproof1" name="idproof1" class="form-control">
            			</div>
                        </div>
                        <br>
                        
                        <!-- Column ended here -->
                        </div>
                        </div>
                        </div>
                        <br>
                            <div class="text-right m-t-xs">
                                <a class="btn btn-info prev mytab2" href="#">Previous</a>
                             <button class="btn btn-warning submitWizard mytab"  type="submit" name="submitcustfeedback" id="submitcustfeedback"><strong>Submit</strong></button>
                       
                            </div>
                        
                        </div>
                        </div>
                        <!-- Tabs Content here -->
                    </form>

                  

                </div>
            </div>
        </div>

    </div>
    </div>

    <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->
 
</div>



<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="vendor/moment/moment.js"></script>
<script src="vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
<script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Vendor scripts ended here-->

<!--  Aditional Scripts -->
<script src="vendor/jquery-flot/jquery.flot.js"></script>
<script src="vendor/jquery-flot/jquery.flot.resize.js"></script>
<script src="vendor/jquery-flot/jquery.flot.pie.js"></script>
<script src="vendor/flot.curvedlines/curvedLines.js"></script>
<script src="vendor/jquery.flot.spline/index.js"></script>
<!--  Aditional Scripts -->
<!-- App scripts -->
<script src="scripts/homer.js"></script>

<!-- RBSK scripts started here-->
<script src="scripts/custscripts/childbirth/birthreg.js"></script>
<script src="scripts/custscripts/childbirth/birthregsubmit.js"></script>
<script src="scripts/custscripts/childbirth/birthregonload.js"></script>
<script src="scripts/custscripts/fieldvalidation.js"></script>
<script src="scripts/custscripts/mytoastr.js"></script>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- Healtha Pinata scripts ended here-->

</body>
</html>