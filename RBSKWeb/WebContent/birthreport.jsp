<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
	<!-- Page title ended here-->


    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->
    
    <!-- Health Pinata styles started here -->
      <!-- <link rel="stylesheet" type="text/css" href="styles/mycustsytles/sweetalert.css"> -->
   <!--  Health Pinata styles ended here  -->

</head>
<body>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p>A software to upload birth defects </p><img src="image/loading-bars.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->



<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>

 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->

<div class="boxed-wrapper">
<!-- Main Wrapper started here -->
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            <h4 class="font-light m-b-xs text-info">
               Telangana Bala Swasthya Karyakram
            </h4>
            <small>Birth Defect Screening statistics.</small>
        </div>
    </div>
</div>

<div class="content animate-panel">

<div class="row">
     <div class="col-md-12">
                <div class="hpanel">
                    <div class="panel-body">
                        
					<form role="form" id="birthreport" name="birthreport" method="post">
						<div id="formdiv">
						<!-- Heading 1 -->
						<div class="row">
						<div class="col-md-4">
                        </div>
                        <div class="col-md-4">
						<h2>&nbsp;&nbsp;&nbsp;Birth Defect Statistics</h2>
						</div>
						</div>
						<br><br>

                        <!-- Row Divider -->
                        <div class="row">
                        <div class="col-md-3">
                        
                        </div>
                        <div class="col-md-3">
                        <label>Reporting Month and Year</label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <input type="text" placeholder="YYYY-MM" id="reportingyear" name="reportingyear" class="form-control">
            			 </div>
                        </div>
                        </div>
                        <br>

                        <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        <button class="btn btn-sm btn-primary m-t-n-xs demo1" type="submit" name="getreport" id="getreport"><strong>Get Birth Defect Report</strong></button>
            			 </div>
                        </div>
                        </div>
                        </div>
                        <!--  -->
                        <div id="reportsdiv">
                         <div class="row">
                         <h1>&nbsp;&nbsp;&nbsp;Overall Statistics</h1>
                         </div>
                        <div class="row">
                        <div class="col-md-9">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                    	<thead>
                    	<tr>
                    	<th id="reportmonthtd"></th>
                    	<th id="reportyeartd"></th>
                    	<th id="noofbirthstd"></th>
                    	<th id="noofdefectstd"></th>
                    	</tr>
                    	</thead>
                    	</table>
                    	</div>
                    	</div>
                    	<br>
                    	<!-- Row Divider -->
                    	
                    	<div class="row">
                         <h1>&nbsp;&nbsp;&nbsp;Defectwise Statistics</h1>
                         </div>
                        <div class="row">
                        <div class="col-md-9">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped" id="defectlist">
                    	<thead>
                    	<tr>
                    	<th>Defect Name</th>
                    	<th>No of Defects Identified</th>
                    	</tr>
                    	</thead>
                    	</table>
                    	</div>
                    	</div>
                    	<br>
                    	
                    	<!-- Row Divider -->
                    	<div class="row">
                         <h1>&nbsp;&nbsp;&nbsp;Agewise Statistics</h1>
                         </div>
                        <div class="row">
                        <div class="col-md-9">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped" id="agewiselist">
                    	<thead>
                    	<tr>
                    	<th>Age</th>
                    	<th>No of Defects Identified</th>
                    	</tr>
                    	</thead>
                    	</table>
                    	</div>
                    	</div>
                    	<br>
                    	
                    	<!-- Row Divider -->
                    	<div class="row">
                         <h1>&nbsp;&nbsp;&nbsp;Sourcewise Statistics</h1>
                         </div>
                        <div class="row">
                        <div class="col-md-9">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped" id="sourcewiselist">
                    	<thead>
                    	<tr>
                    	<th>Source</th>
                    	<th>No of Defects Identified</th>
                    	</tr>
                    	</thead>
                    	</table>
                    	</div>
                    	</div>
                    	<br>
                    	
                    	</div>
                        <!--  -->
                    </form>                    

                       
                        
                        
                        

                    </div>
                </div>
            </div>
        </div>

</div>


 	
    
    <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->

</div>
<!-- Main Wrapper ended here -->
</div>


<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<!-- Vendor scripts ended here-->

<!-- App scripts started here-->
<script src="scripts/homer.js"></script>
<!-- App scripts ended here-->

<!-- Healtha Pinata scripts started here-->
<script src="scripts/custscripts/childbirth/birthreport.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- Healtha Pinata scripts ended here-->



</body>
</html>