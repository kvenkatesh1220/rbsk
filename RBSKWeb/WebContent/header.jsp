<!-- Header started -->
<div id="header" >
    <div class="color-line">
    </div>
    <div style="background-color:#FF69B4; vertical-align:top">
       <img src="image/rbsk/rbsk.png" width="180" height="81" align="left" /> 
    </div>
    
    <nav role="navigation" >
        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
         <div class="small-logo">
            
        </div>
        
        
        <!-- Mobile Header Started here-->        
        <div class="mobile-menu">
            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                <i class="fa fa-chevron-down"></i>
            </button>
            <div class="collapse mobile-navbar" id="mobile-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="" href="javascript:logout()">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
<!-- Mobile Ended Started here-->  
<div class="row">
<div class="col-xs-5">
<!-- <h3> <font  color="#4B0082"><b>Birth Defect E-Registry</b></font></h3> -->
</div>
<div class="col-xs-4">
<div class="navbar-right" >
            <ul class="nav navbar-nav no-borders">
                <li class="dropdown">
                    <a href="javascript:logout()">
                        <font size="2" color="#333300"><b>Logout <i class="fa fa-chevron-right"></i></b></font> 
                    </a>
                </li>
            </ul>
         </div>
</div>
</div>
       
        
        
    </nav>
</div>
<!-- Header ended -->

<!-- Navigation started  -->
<aside id="menu">
    <div id="navigation" >
        

        <ul class="nav" id="side-menu" >
        
        	<li style="background:#FF69B4;color:#ffffff; ">
                <span><font size="4" ><b><br>&nbsp;&nbsp;&nbsp;&nbsp;Menu</b></font></span>
            </li>
            
            <li>
                <a href="/birthreg.jsp"> 
                <span class="nav-label" >Reporting Form</span> 
                </a>
            </li>
            <li>
                <a href="/reports.jsp"> 
                <span class="nav-label">General Reports</span> 
                </a>
            </li>
            <%
            String usertype="";
            if (session.getAttribute("usertype") != null) { 
            	usertype=session.getAttribute("usertype")+"";	
            } 
            System.out.println("usertype in JSP ::"+usertype);
             if(usertype.equalsIgnoreCase("ADMIN") || usertype.equalsIgnoreCase("SUPERUSER")  )
            {
            %>
            <li>
                <a href="/adminmodule.jsp"> 
                <span class="nav-label">Manage Accounts</span> 
                </a>
            </li>
            <% } 
           
            if(usertype.equalsIgnoreCase("ADMIN"))
            {
            %>
             
            <li>
                <a href="/birthstats.jsp"> 
                <span class="nav-label">Overall Statistics</span> 
                </a>
            </li>
            <li>
                <a href="/defectchart.jsp"> 
                <span class="nav-label">Defect Chart</span> 
                </a>
            </li>
            <li>
                <a href="/auditreports.jsp"> 
                <span class="nav-label">Audit Trail</span> 
                </a>
            </li>
            <% } %>
            <li>
                <a href="/changepassword.jsp"> 
                <span class="nav-label">Change Password</span> 
                </a>
            </li>
            <li>
                <a href="/support.jsp"> 
                <span class="nav-label">Customer Support</span> 
                </a>
            </li>
            
            
            <li>
                <br><img src="image/rbsk/nhmlogo.png" width="100%" height="130" />
                     
                <br><br><img src="image/rbsk/operation.jpg" width="100%" height="180" />
                <br><br><img src="image/rbsk/mothering.jpg" width="100%" height="180" />
            </li>
        
        </ul>
    </div>
</aside>


<!-- Navigation ended-->

<!-- Health Pinata scripts started here-->

<script src="scripts/custscripts/childbirth/signout.js"></script>
<!-- Health Pinata scripts ended here-->