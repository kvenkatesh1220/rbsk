<!DOCTYPE html>
<html>
<head> 

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
	<!-- Page title ended here-->


    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
	<link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
    <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->
    
    <!-- Health Pinata styles started here -->
      <!-- <link rel="stylesheet" type="text/css" href="styles/mycustsytles/sweetalert.css"> -->
   <!--  Health Pinata styles ended here  -->


</head>
<body>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p>A software to upload birth defects </p><img src="image/loading-bars.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->



<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>
 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->

<div class="boxed-wrapper">
<!-- Main Wrapper started here -->
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            <h4 class="font-light m-b-xs text-info">
               Audit Trial
            </h4>
            <small>View Audit Trail Reports.</small>
        </div>
    </div>
</div>

<div class="content animate-panel">

<div class="row">
     <div class="col-md-12">
                <div class="hpanel">
                 <div class="panel-heading">
                    <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                      &nbsp;
                    </div>
                    <div class="panel-body">
                        
					<form role="form" id="birthreport" name="birthreport" method="post">
					<div id="formdiv">
						<!-- Heading 1 -->
						<div class="row">
						<div class="col-md-4">
                        </div>
                        <div class="col-md-4">
						
						</div>
						<div  class="col-md-4 pull-right">
						<font size="6" color="#3300FF" >
						<a href="#" onclick="printauditreportPDF()"><i class="fa fa-file-pdf-o"></i>  </a>  &nbsp;&nbsp;&nbsp;
						<a href="#" onclick="printauditinEXL()"><i class="fa fa-file-excel-o"></i> </a> </font>
						</div>
						</div>
						<br>

                        <!-- Row Divider -->
                        <div class="row">
                        
                        <div class="col-md-2 " style="width:14%;">
                        <span><font color="red">&nbsp;*</font></span><label>Select Duration </label>
                        </div>
                        <div class="col-md-3 " >
                        <div class="form-group">
                        From : <input type="text" placeholder="DD-MON-YYYY HH:MM:SS" id="auditdatefrom" name="auditdatefrom" class="form-control">
            			 </div>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                        To : <input type="text" placeholder="DD-MON-YYYY HH:MM:SS" id="auditdateto" name="auditdateto" class="form-control">
            			 </div>
                        </div>
                        </div>
                        <br>
<!--  Row Divider -->
<div class="row">
                        
                        <div class="col-md-2" style="width:14%;"	>
                        <label>&nbsp;Location </label>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="districts" name="districts" class="form-control" >
                       <!-- <option value="">Select District</option> <option value="00">00</option>	<option value="01">01</option> -->
                        </select>
                         </div>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
               <!-- Select box -->  
                        <select id="clusters" name="clusters" class="form-control" >
                       <option value="">Select Cluster</option> <!-- <option value="00">00</option>	<option value="01">01</option>  -->
                       </select>
                         </div>
                        </div>
                        </div>
                        <br>
                        
						<div class="row">
							<div class="form-group">
							
                        	<div class="col-md-2" style="width:14%;">
                        	<label><span><font color="red">&nbsp;*</font></span>Select Report</label>
                        	</div> 
                        	<div class="col-md-8">
	                      		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="audit1" name="audit" value="Insert"  checked><label>Entry Done By</label>
                        		</div>
                        		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="audit2" name="audit" value="Update"><label>Edit Details</label>
                            	</div>
                            	<div class="radio radio-info radio-inline rptdist">
	                            <input type="radio" id="audit3" name="audit" value="BirthReportPrinted"><label>Birth Print Out Details</label>
                        		</div>
                        		<div class="radio radio-info radio-inline rptdist">
                            	<input type="radio" id="audit4" name="audit" value="Login_Details"><label>Login Details</label>
                            	</div>
                            	
                        </div>
                        </div>
                        </div>
                        
                        </div>	
					
                    </form>   
                    <br>  
                    <div class="row">
                        <div class="col-md-3">
                        <div class="form-group">
                        <button class="btn btn-sm btn-info m-t-n-xs demo1" type="submit" name="getauditreport" id="getauditreport"><strong>Get Report</strong></button>
            			 </div>
                        </div>
                        </div>               
                    </div>
                </div>
            </div>
        </div>



<!--  Location  Table div started here -->
 <div class="row" id="loctablediv">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="panel-heading">
                    <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    Report
                    </div>
                    
                     <div class="panel-body"  id="auditreportdiv">
                     		<div id="reporttable" > 
                     		
                     		</div> <!-- // table will be loadded here  -->
                	</div>
                </div>
            </div>
            
 </div>    
 <!--  Location  Table div ended here -->
 

</div>


 	
    
    <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->

</div>
<!-- Main Wrapper ended here -->
</div>


<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="vendor/moment/moment.js"></script>
<script src="vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
<script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Vendor scripts ended here-->

<!-- App scripts started here-->
<script src="scripts/homer.js"></script>
<!-- App scripts ended here-->
<script src="scripts/custscripts/fieldvalidation.js"></script>

<!-- Healtha Pinata scripts started here-->
<script src="scripts/custscripts/admin/audit_report.js"></script>
<script src="scripts/custscripts/childbirth/reports.js"></script>
<script src="scripts/custscripts/mytoastr.js"></script>


<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- Healtha Pinata scripts ended here-->



</body>
</html>