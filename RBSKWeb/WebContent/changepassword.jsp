<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
    <!-- Page title ended here-->

    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->
    
    </head>
<body>

<!-- Simple splash screen-->
<div class="splash" id="mysplash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p> A software to upload birth defects</p><img src="image/loading-bars.svg" width="64" height="64" /><p>Please Wait</p> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>
 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <!-- <li><a href="dashboard.jsp">RBSKForm</a></li> -->
                </ol>
            </div>
            
            <h4 class="font-light m-b-xs text-info">
               Change Password
            </h4>
            <small>Change Your Password Here</small> <span class="pull-right"><font color="red">&nbsp;*&nbsp;</font>marked fields are mandatory.</span>
        </div>
    </div>
</div>

<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                		
                <div class="panel-body">
                

                    <form   id="changepwdform" name="changepwdform" method="post" >
                     <div class="row">
                        <div class="col-md-3">
                        <span><font color="red">&nbsp;*</font></span><label>Current Password </label>
                        &nbsp;&nbsp;
                        <input type="password" placeholder="Current Password" id="currpwd" name="currpwd" class="form-control">
                        <input type="hidden"  name="sessionpwd"  id="sessionpwd"  value="<%= session.getAttribute("currpwd")%>">
                        </div> 
                        <div class="col-md-8">
                        
                        </div>
                        <div class="col-md-3">
                        <span><font color="red">&nbsp;*</font></span><label>New Password </label>
                        <input type="password" placeholder="New Password" id="newpwd" name="newpwd" class="form-control">
                        </div> 
                        <div class="col-md-8">
                        
                        </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3" id="pbar">
                            <b>Password Strength :&nbsp;&nbsp;</b><span id="result"></span>
                    		<div class="progress m-t-xs full progress-small active">
                        	<div id="passwdstrength" style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class=" progress-bar progress-bar-info">
                        	</div>
                    		</div>
                			</div>
                			</div>
                			<br>
                    </form>
                    <div class="row">
                    
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-warning"  type="submit" name="changepwd" id="changepwd"><strong>Change Password</strong></button>
                   </div>

                </div>
            </div>
        </div>

    </div>
    </div>

     <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->
 
</div>



<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<!-- Vendor scripts ended here-->

<!-- App scripts -->
<script src="scripts/homer.js"></script>

<!-- Healtha Pinata scripts started here-->
<script src="scripts/custscripts/childbirth/changepwd.js"></script>
<script src="scripts/custscripts/mytoastr.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- Healtha Pinata scripts ended here-->


</body>
</html>