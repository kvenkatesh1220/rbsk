<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title started here-->
    <title>RBSK</title>
	<!-- Page title ended here-->

    <!-- Vendor styles started here -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
    <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
	<link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
	<link rel="stylesheet" href="vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css" />
    <!-- Vendor styles ended here -->

    <!-- App styles started here -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/static_custom.css">
    <!-- App styles ended here -->

</head>
<body>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Telangana Birth Defect E-Registry</h1><p>A software to upload birth defects </p><img src="image/loading-bars.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header started here-->
<% 
if (session.getAttribute("username") == null) { 
	 response.sendRedirect("rbsk.jsp");	
}

%>

 <%@ include file="header.jsp" %>
 
 <!-- Header ended here-->

<!-- Main Wrapper -->
<div id="wrapper">

<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                
            </div>
            <h4 class="font-light m-b-xs text-info">
                Customer Support
            </h4>
            <small>Administrative and Technical Support.</small>
        </div>
    </div>
</div>

<div class="content animate-panel">

<div class="row">

    <div class="col-lg-12">
    
        <div class="hpanel hgreen contact-panel">
            <div class="panel-body">
            <div class="col-lg-6">
              <div class="table-responsive">
                <table  class="table  table-striped table-condensed"  style="width:55%; border-color:#fffff;">
                <caption><h5><b>Administrative Team</b></h5></caption>
                    <tbody>
                    <tr>
                        
                        <td>Dr.G.Sree Krishna</td>
                        
                    </tr>
                    <tr>
                        
                        <td>State Programmer Officer - <b> RBSK/RKSK</b></td>
                        
                    </tr>
                    <tr>
                        
                        <td>+91 9666252624 / 7330733168</td>
                        
                    </tr>
                    <tr>
                        
                        <td>rbsk.telangana@gmail.com</td>
                        
                    </tr>
                    </tbody>
                </table>
</div>  
<br>

<div class="table-responsive">
                <table  class="table  table-striped table-condensed"  style="width:55%; border-color:#fffff;">
                <caption><h5><b>Technical Team</b></h5></caption>
                    <tbody >
                    <tr >
                        <td>Krishnam Raju B</td>
                        
                    </tr>
                    <tr>
                        <td>+91 40 6564 5666</td>
                        
                    </tr>
                    <tr>
                        
                        <td>support@keansa.com</td>
                        
                    </tr>
                    </tbody>
                </table>
</div>  
            </div>
         </div>
         </div>
    </div>
    
    
    
</div>


    </div>



    <!-- Footer started here-->
    
    <%@ include file="footer.jsp" %>
    
    <!-- Footer ended here-->

</div>

<!-- Vendor scripts Started here-->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="vendor/select2-3.5.2/select2.min.js"></script>
<script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script src="vendor/chartjs/Chart.min.js"></script>
<!-- Vendor scripts ended here-->

<!-- App scripts started here-->
<script src="scripts/homer.js"></script>
<!-- App scripts ended here-->

<!-- Healtha Pinata scripts started here-->
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-78493996-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- Healtha Pinata scripts ended here-->
<script>
    $(function () {
        // Toastr options
        toastr.options = {
 						 "closeButton": true,
  						 "debug": false,
  						 "newestOnTop": false,
  						 "progressBar": false,
  						 "positionClass": "toast-top-center",
  						 "preventDuplicates": false,
  						 "onclick": null,
  						 "showDuration": "300",
  						 "hideDuration": "1000",
  						 "timeOut": "5000",
  						 "extendedTimeOut": "1000",
  						 "showEasing": "swing",
  						 "hideEasing": "linear",
  					     "showMethod": "fadeIn",
  						 "hideMethod": "fadeOut"
						};
    });
</script>
</body>
</html>