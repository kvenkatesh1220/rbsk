/**
 * 
 */

$(document).ready(function()
{
	
	$('#insuranceerr').hide();
	$('#lastdocerr').hide();

	//Date picker setting started here
	$("#editdateofbirth").datepicker({
	       	format: "yyyy-mm-dd" ,
	       	todayBtn: "linked" ,
	       	endDate: '0'
	});

	$("#editlastdocvisit").datepicker({
	   	format: "yyyy-mm-dd" ,
	   	todayBtn: "linked" ,
	   	endDate: '0'
	});

	$("#editinsurancedate").datepicker({
	   	format: "yyyy-mm-dd" ,
	   	todayBtn: "linked" ,
	});
	//Date picker setting ended here



$("#editmember").validate({ //Member Edit Form validation started here       

	rules: {
		editmemberfname: {
            required: true,
            minlength: 1,
            maxlength: 50
        },
        editmemberlname: {
            required: true,
            minlength: 1,
            maxlength: 50
        },
        editrelationship: {
            required: true
        },
        editemergencynumber: {
            minlength: 1,
            maxlength: 50
        }
    },
    
    submitHandler: function(form) {
// insurance and last doctor visit validation started here
    	
    	var lastdocsatistied = true;
    	
    	if($("#editdateofbirth").val()!="" && $("#editlastdocvisit").val()!="" )
        { 
		var dobdate = Date.parse($("#editdateofbirth").val()); 
		var lastdocvisitdate = Date.parse($("#editlastdocvisit").val());   
		console.log(dobdate+"---"+lastdocvisitdate);
		console.log("Is lastdocsatistied "+(lastdocvisitdate<dobdate));
		if(lastdocvisitdate<dobdate)
		{
			lastdocsatistied = false;	
		}
        }
		
    	var insurancesatisfied=true;
    	if($("#editdateofbirth").val()!="" && $("#editinsurancedate").val()!="" )
        {
		var insurancedate = Date.parse($("#editinsurancedate").val());   
		var dobdate = Date.parse($("#editdateofbirth").val()); 
		console.log(dobdate+"---"+insurancesatisfied);
		console.log("Is insurancesatisfied"+(dobdate<insurancedate));
		if(insurancedate<dobdate)
		{
			insurancesatisfied = false;	
		}
        }// if both givenon date and expiry date are not empty then compare dates cond started
		
    	if(insurancesatisfied==false)
    		{
    		$('#insuranceerr').show();
    		}
    	if(insurancesatisfied==true)
		{
		$('#insuranceerr').hide();
		}
    	
    	if(lastdocsatistied==false)
		{
		$('#lastdocerr').show();
		}
    	
    	if(lastdocsatistied==true)
		{
		$('#lastdocerr').hide();
		}
    	
    	if(insurancesatisfied==true && lastdocsatistied==true)
    		{
    		$('#insuranceerr').hide();
    		$('#lastdocerr').hide();
    		submitmembereditform();
    		} // insurance and last doctor visit validation ended here
    	return false; // this will stop pag reload
    }

	
    
}); //Member Edit Form validation started here

function submitmembereditform(){ // Member Edit Form submit started here
	var editform = document.getElementById('editmember');
	var formdata = new FormData(editform);
	$("#editsubmit").attr('disabled',true);
	  // ajax call started here
	  $.ajax({
	   url: "/EditMembers",
	   type: 'POST',
	   data: formdata,
       contentType: false,
	   processData: false,
	   success: function (data) { // success started here
		   
	    console.log("success returned from servlet EditMembers");
		    		
		    		$('#editmember').removeAttr('disabled');
		    		$("#editmember").trigger('reset');
		    		if(data=='Success'){
//		    		swal({
//		                title: "",
//		                text: "Success! Member has been successfully Updated"
//		            });
		    		location.reload();
		    		}
		    		
		    		if(data=='Error'){
		    			toastr.error('Error! Unable to Proceed at this time. Please contact support');
			    		}
		    		
	   }, // success ended here
	   error: function(request, status, message) { // error started here
	    console.log("error occourred in EditMembers servlet call :");
	    console.log(message);
	    toastr.error('Error - Unable to Connect Health Pinata Server');
	   // $('#editsubmit').removeAttr('disabled');
	   } // error ended here
	   
	  });
	  
	  // ajax call ended here
	  $('#editMemberModal').modal('toggle'); // this is to open Edit Member Dialog 
	   
	  $("#previewtitle").hide(); // to close model box
		 $('#editsubmit').removeAttr('disabled');
		
} // Member Edit Form submit ended here

});

// showing image preview started here
function showimagepreview(input) {
	$("#profilepicimage").empty();
	$("#previewtitle").hide();
	if (input.files && input.files[0]) {
	//alert(input.files[0].size);
	if(input.files[0].size<=1589431)	{ // file size is less than 1.5 MB started here
	var filerdr = new FileReader();
	filerdr.onload = function(e) {
//	$('#imgprvw').attr('src', e.target.result);
//	$('#imgprvw').attr('class', 'img-circle m-b');
		$("#previewtitle").show();
		$("#profilepicimage").append("<img id=\"imgprvw\" src="+e.target.result+" class=\"img-circle m-b\" alt=\"logo\" width=\"80\" height=\"80\" >" );		
	};
	} // file size is less than 1.5 MB ended here
	if(input.files[0].size>1589431)	{ // file size is more than 1.5 MB started here
		$("#previewtitle").hide();
		$("#profilepicimage").append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color=\"red\">File size must be less than 1MB. <br>Recommended Size is : 10kb-100kb </font>");
	}
	filerdr.readAsDataURL(input.files[0]);
	}
	}
//showing image preview ended here

//
function editmem(memberid){
	 $('#editMemberModal').modal('toggle'); // this is to open Edit Member Dialog 
	 $("#previewtitle").hide();
	 //ajax started
	 $.ajax({
		  url: "GetMemberDetails",
		  type: 'POST',
		  dataType: 'json',
		  data: { memberid: memberid },
		  success: function (data) {
		   console.log("success returned from servlet GetMemberDetails");
		   $.each(data, function(key,value) {
			   var  relationship=value['relationship'];
			   var  dob=value['dob'];
			   var  bloodgroup=value['bloodgroup'];
			   var  emergencynumber=value['emergencynumber'];
			   var  lastdocvisit=value['lastdocvisit'];
			   var  insurancedate=value['insurancedate'];
			   var  firstname=value['firstname'];
			   var  lastname=value['lastname'];
			   
			   if(value['firstname']===undefined || value['firstname']==null)
			   {
				   firstname="";    
			   }
			   
			   if(value['lastname']===undefined || value['lastname']==null)
			   {
				   lastname="";    
			   }
			   
			   if(value['relationship']===undefined || value['relationship']==null)
			   {
			    relationship="";    
			   }
			   
			   if(value['dob']===undefined || value['dob']==null)
			   {
			    dob="";    
			   }
			   
			   if(value['bloodgroup']===undefined || value['bloodgroup']==null)
			   {
			    bloodgroup="";    
			   }
			   
			   if(value['emergencynumber']===undefined || value['emergencynumber']==null)
			   {
			    emergencynumber="";    
			   }
			   
			   if(value['lastdocvisit']===undefined || value['lastdocvisit']==null)
			   {
			    lastdocvisit="";    
			   }
			   
			   if(value['insurancedate']===undefined || value['insurancedate']==null)
			   {
			    insurancedate="";    
			   }
			   
			    $("#editrelationship").val(relationship);
			    $("#editbloodgroup").val(bloodgroup);
			    $("#editemergencynumber").val(emergencynumber);
			    $("#editdateofbirth").val(dob);
			    $("#editlastdocvisit").val(lastdocvisit);
			    $("#editinsurancedate").val(insurancedate);
			    $("#memberid").val(memberid);
			    
			    $("#editmemberfname").val(firstname);
			    $("#editmemberlname").val(lastname);
		   });
		  },
		  error: function(request, status, message) {
		   console.log("error occourred in deletemember servlet call :");
		   console.log(message);
		   console.log(request.responseText);
		   toastr.error('Error - Unable to Connect Health Pinata Server');
		  }
		 });
	 
	 //ajax ended
	 


}