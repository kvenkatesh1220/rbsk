/**
 * 
 */

//delmem function closed here

//delete member ajax ended here
function delmem(memberid){
		
		// swal conformation to delete started here
		swal({
	        title: "Are you sure?",
	        text: "You want to Delete!",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!"
	    },
	    function () {
	    	
	    	// Ajax call started here	
			$.ajax({
				url: "DeleteMembers",
				type: 'POST',
				data:{memberid:memberid},
				success: function (data) {
					if(data=='Success'){
//			    		swal({
//			                title: "",
//			                text: "Success! Member has been Deleted successfully "
//			            });
			    		location.reload();
			    		}
			    		
			    		if(data=='Error'){
			    			toastr.error('Error! Unable to Proceed at this time. Please contact support');
				    		}
					
				},
				error: function(request, status, message) {
					console.log("error occourred in deletemember servlet call :");
					console.log(message);
					console.log(request.responseText);
					toastr.error('Error - Unable to Connect Health Pinata Server');
					
			}
				
				
			});
			// Ajax call ended here

		
	    	
	    });
		// swal conformation to delete ended here
	}