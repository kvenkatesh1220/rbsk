/**
 * 
 */
$(document).ready(function()
{
	
	loadfamilymembers();
	
	

// memberstr represents one member 	
//var memberstr = colstart+hpanelstart+panelbodystart+memberpanelbody+panelbodyend+panelfooterstart+memberfooterbody+panelfooterend+hpanelend+colend;	

// familymemrowstr represents one row which has two members
//var familymemrow1str = rowstart+memberstr+memberstr+rowsend;
//var familymemrow2str = rowstart+memberstr+memberstr+rowsend;
//var familymemrow3str = rowstart+memberstr+memberstr+rowsend;

//familymemstr represents complete list of memebers
//var familymemstr = familymemrow1str+familymemrow2str+familymemrow3str;

//Delete members ajax started here

/**
 * 
 */


//// loading Family Members function started here
function loadfamilymembers(){
	
	
	
	// Declaring hpanel colors started here
		var hpanel1color = "hgreen";
		var hpanel2color = "hyellow";	
		var hpanel3color = "hviolet";
		var hpanel4color = "hblue";	
		var hpanel5color = "hred";	
//		Declaring hpanel colors ended here
		
	// var button colors setting started here
		var button1color = "success";
		var button2color = "warning";
		var button3color = "primary2";
		var button4color = "info";
		var button5color = "danger";
	// var button colors setting ended here	

	// Declaring html structure started here
		var rowstart = "<div class=\"row\">"; // this should end for every two members
		var colstart = "<div class=\"col-lg-6\">";
		var hpanelstart = "<div class=\"hpanel hgreen contact-panel\">";
		var panelbodystart = "<div class=\"panel-body\">";
		var memberpanelbody ="";
		var panelbodyend="</div>";
		var panelfooterstart = " <div class=\"panel-footer contact-footer\"><div class=\"row\">";
		var memberfooterbody ="";
		var panelfooterend = "</div></div>";
		var hpanelend="</div>";
		var colend="</div>";
		var rowsend = "</div>";
	// Declaring html structure ended here
		
	//Declaring Empty Profile Pics Started Here
		var emptyprofilepic1="images/client/clientgreen.png";
		var emptyprofilepic2="images/client/clientorange.png";
		var emptyprofilepic3="images/client/clientblue.png";
		var emptyprofilepic4="images/client/clientpurple.png";
		var emptyprofilepic5="images/client/clientred.png";
	//Declaring Empty Profile Pics ended Here

// ajax call started here
		
		  $.ajax({
		   url: "/FamilyMembers",
		   type: 'POST',
		   dataType: 'json',
    	   contentType: 'application/json',
		   mimeType: 'application/json',
		   success: function (data) {
			  
			   var familymemstr="";
			   var i=1;// this represents member index
			   var c=1;//This is for color index
			   $.each(data, function(key,value) { 
				   if(value['memberid']!=1){
					   // setting Profile pic src with DB Value and Default root path
				   var  profilepicsrc="http://94.228.219.195/userprofilepics/"+value['cust_id']+"_"+value['memberid']+"/"+value['imagename'];
					  var  firstname=value['firstname'];
					  var  lastname=value['lastname'];
				       var  insurancenum=value['insurancenum'];
				       var  bloodgroup=value['bloodgroup'];
				       var  nextdocvisit=value['nextdocvisit'];
				       var  emergencynumber=value['emergencynumber'];
				       var  lastdocvisit=value['lastdocvisit'];
				       var  renewaldate=value['renewaldate'];
				       var relationship=value['relationship'];
				       console.log(profilepicsrc);
				       console.log(emergencynumber);
				       console.log(relationship);
				       
				       if(value['relationship']===undefined || value['relationship']==null)
				       {
				    	   relationship="";    
				       }
				      
				       if(value['firstname']===undefined || value['firstname']==null)
				       {
				    	   firstname="";    
				       }
				       
				       if(value['lastname']===undefined || value['lastname']==null)
				       {
				    	   lastname="";    
				       }
				       
				       if(value['insurancenum']===undefined || value['insurancenum']==null)
				       {
				    	   insurancenum="";    
				       }
				       
				       if(value['bloodgroup']===undefined || value['bloodgroup']==null)
				       {
				        bloodgroup="";    
				       }
				      
				       
				       if(value['emergencynumber']===undefined || value['emergencynumber']==null)
				       {
				        emergencynumber="";    
				       }
				       
				       if(value['nextdocvisit']===undefined || value['nextdocvisit']==null)
				       {
				    	   nextdocvisit="";    
				       }
				       
				       if(value['renewaldate']===undefined || value['renewaldate']==null)
				       {
				    	   renewaldate="";    
				       }
				       
				       // Variable initialization started here

				       		
				       

				    // assigning of hpanelcolor and buttoncolor started here	
				    var hpanelcolor=hpanel1color; // default color of hpanel1
				    var buttoncolor=button1color; // default color of button
				    if(c==1)
				    {
				    	hpanelcolor=hpanel1color;
				    	buttoncolor=button1color;
				    	if(value['imagename']==null || value['imagename']===undefined){ // for setting empty profile pic based on index
				    		profilepicsrc=emptyprofilepic1;
				    	}
				    }
				    if(c==2)
				    {
				    	hpanelcolor=hpanel2color;
				    	buttoncolor=button2color;
				    	
				    	if(value['imagename']==null || value['imagename']===undefined){ // for setting empty profile pic based on index
				    		profilepicsrc=emptyprofilepic2;
				    	}
				    }
				    if(c==3)
				    {
				    	hpanelcolor=hpanel3color;
				    	buttoncolor=button3color;
				    	
				    	if(value['imagename']==null || value['imagename']===undefined){ // for setting empty profile pic based on index
				    		profilepicsrc=emptyprofilepic3;
				    	}
				    }
				    if(c==4)
				    {
				    	hpanelcolor=hpanel4color;
				    	buttoncolor=button4color;	
				    	
				    	if(value['imagename']==null || value['imagename']===undefined){ // for setting empty profile pic based on index
				    		profilepicsrc=emptyprofilepic4;
				    	}
				    }
				    if(c==5)
				    {
				    	hpanelcolor=hpanel5color;
				    	buttoncolor=button5color;
				    	
				    	if(value['imagename']==null || value['imagename']===undefined){ // for setting empty profile pic based on index
				    		profilepicsrc=emptyprofilepic5;
				    	}
				    }

				    //assigning of hpanelcolor and buttoncolor ended here



				    if(i%2==1) { // first member started here
				    	familymemstr=familymemstr+rowstart; // this is only for first member of each row to start row start tag.
				    	familymemstr=familymemstr+colstart; // member started here
				    	hpanelstart = "<div class=\"hpanel "+hpanelcolor+" contact-panel\">";
				    	familymemstr=familymemstr+hpanelstart;
				    	familymemstr=familymemstr+panelbodystart;

				    	// member panel body which has backend parameters to set started here
				    	memberpanelbody ="<div class=\"row\">" +
	    			     "<div class=\"col-md-6 border-right\">" +
	    			     "<img alt=\"logo\" class=\"img-circle m-b\" src=\""+profilepicsrc+"\">" +
	    			     "<h3><a href=\"\">"+firstname+" "+lastname+" </a></h3>" +
	    			     "<div class=\"text-muted font-bold m-b-xs\">"+relationship+"</div>" +
	    			     "</div>" +
	    			     "<div class=\"col-md-6 pull-left\">" +
	    			     "<strong>Blood Group:</strong>"+bloodgroup+"</br>" +
	    			     "<strong>Insurance Number:</strong>"+insurancenum+"</br>" +
	    			     "<strong>Renewal Date:</strong>"+renewaldate+"</strong></br>" +
	    			     "<strong>Next doctor visit:</span></strong>"+nextdocvisit+"" +
	    			     "<br><br><br><br><br>"+
	    			     "<button class=\"btn btn-"+buttoncolor+" memedit \" onclick=\"delmem("+value['memberid']+")\" type=\"button\"><i class=\"fa fa-paste\"></i> Delete</button>"+
	    			     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class=\"btn btn-"+buttoncolor+" memedit \" onclick=\"editmem("+value['memberid']+")\" type=\"button\"><i class=\"fa fa-paste\"></i> Edit</button>"+
	    			     "</div>" +
	    			     "</div>";
				    // member panel body which has backend parameters to set started here	
				    	familymemstr=familymemstr+memberpanelbody;
				    	familymemstr=familymemstr+panelbodyend;
				    	familymemstr=familymemstr+panelfooterstart;	

				    // member footer body which has backend parameters to set started here	
				    	memberfooterbody="<div class=\"col-md-3 border-right\">" +
				    					 "<div class=\"contact-stat\"><span>Weight </span> <strong>200</strong><span>Date:<strong>12 Nov 2015</strong></span>" +
				    					 "</div> </div>" +
				    					 "<div class=\"col-md-3 border-right\"> <div class=\"contact-stat\"><span>BMI </span> <strong>300</strong><span>Date:<strong>12 Nov 2015</strong></span>" +
				    					 "</div> </div>" +
				    					 "<div class=\"col-md-3 border-right\"> <div class=\"contact-stat\"><span>Blood Pressure </span> <strong>400</strong><span>Date:<strong>12 Nov 2015</strong></span>" +
				    					 "</div></div>" +
				    					 "<div class=\"col-md-3\"> <div class=\"contact-stat\"><span>Heart Rate </span> <strong>400</strong><span>Date:<strong>12 Nov 15</strong></span></div> </div>";
				    		
				    // member footer body which has backend parameters to set ended here
				    	familymemstr=familymemstr+memberfooterbody;
				    	familymemstr=familymemstr+panelfooterend;	
				    	familymemstr=familymemstr+hpanelend;
				    	familymemstr=familymemstr+colend;
				    } // first member ended here

				    if(i%2==0) { // first member started here
				    	familymemstr=familymemstr+colstart; // member started here
				    	hpanelstart = "<div class=\"hpanel "+hpanelcolor+" contact-panel\">";
				    	familymemstr=familymemstr+hpanelstart;
				    	familymemstr=familymemstr+panelbodystart;

				    	// member panel body which has backend parameters to set started here
				    	memberpanelbody = "<div class=\"row\">" +
				    			     "<div class=\"col-md-6 border-right\">" +
				    			     "<img alt=\"logo\" class=\"img-circle m-b\" src=\""+profilepicsrc+"\">" +
				    			     "<h3><a href=\"\">"+firstname+" "+lastname+" </a></h3>" +
				    			     "<div class=\"text-muted font-bold m-b-xs\">"+relationship+"</div>" +
				    			     "</div>" +
				    			     "<div class=\"col-md-6 pull-left\">" +
				    			     "<strong>Blood Group:</strong>"+bloodgroup+"</br>" +
				    			     "<strong>Insurance Number:</strong>"+insurancenum+"</br>" +
				    			     "<strong>Renewal Date:</strong>"+renewaldate+"</strong></br>" +
				    			     "<strong>Next doctor visit:</span></strong>"+nextdocvisit+"" +
				    			     "<br><br><br><br><br>"+
				    			     "<button class=\"btn btn-"+buttoncolor+" memedit \" onclick=\"delmem("+value['memberid']+")\" type=\"button\"><i class=\"fa fa-paste\"></i> Delete</button>"+
				    			     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class=\"btn btn-"+buttoncolor+" memedit \" onclick=\"editmem("+value['memberid']+")\" type=\"button\"><i class=\"fa fa-paste\"></i> Edit</button>"+
				    			     "</div>" +
				    			     "</div>";
				    	
				    	console.log(memberpanelbody);
				    // member panel body which has backend parameters to set started here	
				    	familymemstr=familymemstr+memberpanelbody;
				    	familymemstr=familymemstr+panelbodyend;
				    	familymemstr=familymemstr+panelfooterstart;	

				    // member footer body which has backend parameters to set started here	
				    	memberfooterbody="<div class=\"col-md-3 border-right\">" +
				    					 "<div class=\"contact-stat\"><span>Weight </span> <strong>200</strong><span>Date:<strong>12 Nov 2015</strong></span>" +
				    					 "</div> </div>" +
				    					 "<div class=\"col-md-3 border-right\"> <div class=\"contact-stat\"><span>BMI </span> <strong>300</strong><span>Date:<strong>12 Nov 2015</strong></span>" +
				    					 "</div> </div>" +
				    					 "<div class=\"col-md-3 border-right\"> <div class=\"contact-stat\"><span>Blood Pressure </span> <strong>400</strong><span>Date:<strong>12 Nov 2015</strong></span>" +
				    					 "</div></div>" +
				    					 "<div class=\"col-md-3\"> <div class=\"contact-stat\"><span>Heart Rate </span> <strong>400</strong><span>Date:<strong>12 Nov 15</strong></span></div> </div>";
				    		
				    // member footer body which has backend parameters to set ended here
				    	familymemstr=familymemstr+memberfooterbody;
				    	familymemstr=familymemstr+panelfooterend;	
				    	familymemstr=familymemstr+hpanelend;
				    	familymemstr=familymemstr+colend;
				    	familymemstr=familymemstr+rowsend; // this is only for second member of each row to close row start.
				    } // first member ended here
  
				   c=c+1;
				    i=i+1; 
				   }
			   });// each loop ended here
			   if(i%2==0) // adding rows end if number of members list is odd  started here
			   {	
			   familymemstr=familymemstr+rowsend;
			   } // adding rows end if number of members list is odd  ended here
			   //console.log(familymemstr);
			   $("#familymembers").empty();
			   $("#familymembers").append(familymemstr);
			   
		   }, // success ended here
		   error: function(request, status, message) { // error started here
		    console.log("error occourred in FamilyMembers servlet call :");
		    console.log(message);
		    toastr.error('Error - Unable to Connect Health Pinata Server');
		   } // error ended here
		  });
		  // ajax call ended here
}//load family members function ended here
	
		  

	
});




