/**
 * 
 */

$(document).ready(function()
{
	$('#insuranceerr').hide();
	$('#lastdocerr').hide();
	
	$("#previewtitle").hide();
//Date picker setting started here
$("#dateofbirth").datepicker({
       	format: "yyyy-mm-dd" ,
       	todayBtn: "linked" ,
       	endDate: '0'
});

$("#lastdocvisit").datepicker({
   	format: "yyyy-mm-dd" ,
   	todayBtn: "linked" ,
   	endDate: '0'
});

$("#insurancedate").datepicker({
   	format: "yyyy-mm-dd" ,
   	todayBtn: "linked" ,
});

$("#addmember").validate({ //Member Addition Form validation started here       

	rules: {
		memberfname: {
            required: true,
            minlength: 1,
            maxlength: 50
        },
        memberlname: {
            required: true,
            minlength: 1,
            maxlength: 50
        },
        relationship: {
            required: true
        },
        emergencynumber: {
            minlength: 1,
            maxlength: 50
        }
    },
    
    submitHandler: function(form) {
    	 // insurance and last doctor visit validation started here
    	
    	var lastdocsatistied = true;
    	
    	if($("#dateofbirth").val()!="" && $("#lastdocvisit").val()!="" )
        { 
		var dobdate = Date.parse($("#dateofbirth").val()); 
		var lastdocvisitdate = Date.parse($("#lastdocvisit").val());   
		console.log(dobdate+"---"+lastdocvisitdate);
		console.log("Is lastdocsatistied "+(lastdocvisitdate<dobdate));
		if(lastdocvisitdate<dobdate)
		{
			lastdocsatistied = false;	
		}
        }
		
    	var insurancesatisfied=true;
    	if($("#dateofbirth").val()!="" && $("#insurancedate").val()!="" )
        {
		var insurancedate = Date.parse($("#insurancedate").val());   
		var dobdate = Date.parse($("#dateofbirth").val()); 
		console.log(dobdate+"---"+insurancesatisfied);
		console.log("Is insurancesatisfied"+(dobdate<insurancedate));
		if(insurancedate<dobdate)
		{
			insurancesatisfied = false;	
		}
        }// if both givenon date and expiry date are not empty then compare dates cond started
		
    	if(insurancesatisfied==false)
    		{
    		$('#insuranceerr').show();
    		}
    	if(insurancesatisfied==true)
		{
		$('#insuranceerr').hide();
		}
    	
    	if(lastdocsatistied==false)
		{
		$('#lastdocerr').show();
		}
    	
    	if(lastdocsatistied==true)
		{
		$('#lastdocerr').hide();
		}
    	
    	if(insurancesatisfied==true && lastdocsatistied==true)
    		{
    		$('#insuranceerr').hide();
    		$('#lastdocerr').hide();
    		submitmemberaddform();
    		} // insurance and last doctor visit validation ended here
    	return false; // this will stop pag reload
    }
}); //Member Addition Form validation started here  


function submitmemberaddform(){ // Member Addition Form submit started here
	var addform = document.getElementById('addmember');
	var formdata = new FormData(addform);
	$("#addsubmit").attr('disabled',true);
	  // ajax call started here
	  $.ajax({
	   url: "/AddMembers",
	   type: 'POST',
	   data: formdata,
       contentType: false,
	   processData: false,
	   success: function (data) { // success started here
		   
	    console.log("success returned from servlet AddMembers");
		    		
		    		$('#addsubmit').removeAttr('disabled');
		    		$("#addmember").trigger('reset');
		    		
		    		if(data.indexOf('Success')!== -1){
		    		swal({
		                title: "",
		                text: "Success! Member has been added successfully"
		            });
		    		}
		    		
		    		if(data.indexOf('Error') !== -1)
		    			{
		    			toastr.error(data);
			    		}
	   }, // success ended here
	   error: function(request, status, message) { // error started here
	    console.log("error occourred in AddMembers servlet call :");
	    console.log(message);
	    toastr.error('Error - Unable to Connect Health Pinata Server');
	    $('#addsubmit').removeAttr('disabled');
	   } // error ended here
	  });
	  // ajax call ended here
	  $("#profilepicimage").empty();
		$("#previewtitle").hide();
} // Member Addition Form submit ended here


});

function showimagepreview(input) {
	$("#profilepicimage").empty();
	$("#previewtitle").hide();
	if (input.files && input.files[0]) {
	//alert(input.files[0].size);
	if(input.files[0].size<=1589431)	{ // file size is less than 1.5 MB started here
	var filerdr = new FileReader();
	filerdr.onload = function(e) {
//	$('#imgprvw').attr('src', e.target.result);
//	$('#imgprvw').attr('class', 'img-circle m-b');
		$("#previewtitle").show();
		$("#profilepicimage").append("<img id=\"imgprvw\" src="+e.target.result+" class=\"img-circle m-b\" alt=\"logo\" width=\"80\" height=\"80\" >" );		
	};
	} // file size is less than 1.5 MB ended here
	if(input.files[0].size>1589431)	{ // file size is more than 1.5 MB started here
		$("#previewtitle").hide();
		$("#profilepicimage").append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color=\"red\">File size must be less than 1MB. <br>Recommended Size is : 10kb-100kb </font>");
	}
	filerdr.readAsDataURL(input.files[0]);
	}
	}
