package siginin;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ejbs.SignInUserBeanRemote;
 

/**
 * Servlet implementation class SignOut
 */
@WebServlet("/SignOut")
public class SignOut extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(SignOut.class.getName());
	@EJB(name = "ejb/SignInUserBeanRemote")
	SignInUserBeanRemote sbp;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignOut() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(false);
		try
		{
		if(session!=null)
		{
			logger.info("Logout has been successfull for user :"+session.getAttribute("username"));
			
			if(session.getAttribute("username")!=null)
			{
			String username = session.getAttribute("username")+"";
			logger.info("username from session is"+username);
			boolean isLoggedOut = sbp.userLogOut(username);
			logger.info("LogoutStatus"+isLoggedOut);
			}
			session.invalidate();
			request.getRequestDispatcher(response.encodeURL("/rbsk.jsp")).forward(request, response);
		}
		else
		{
			request.getRequestDispatcher(response.encodeURL("/rbsk.jsp")).forward(request, response);
		}
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
