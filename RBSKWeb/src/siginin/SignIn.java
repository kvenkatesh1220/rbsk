package siginin;

import java.io.IOException;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.octo.captcha.module.servlet.image.SimpleImageCaptchaServlet; 




import org.apache.log4j.Logger;

import util.beans.DropDownBean;
import util.beans.UserMappingBean;
import ejbs.ChildBirthBeanRemote;
import ejbs.SignInUserBeanRemote;
// import admin.CaptchasDotNet;


/**
 * Servlet implementation class SignIn
 */
@WebServlet("/SignIn")
public class SignIn extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(SignIn.class.getName());
	@EJB(name = "ejb/SignInUserBeanRemote")
	SignInUserBeanRemote sbp;
	
	@EJB(name = "ejb/ChildBirthBeanRemote")
	ChildBirthBeanRemote childbirth;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignIn() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try
		{
			logger.info("SignIn servlet has been called");
			HttpSession session = request.getSession(false);
			 String answer=null;
				boolean verify =false;
				
				if(request.getParameter("captchatxt")!=null)
				{
					answer = request.getParameter("captchatxt");
					String captcha = (String) session.getAttribute("captcha");
					logger.info("captcha :"+captcha);
					if (captcha != null ) {
					    if (captcha.equals(answer)) {
					    	verify=true;
//					    	session.invalidate();
					    } else {
					    	verify=false;
//					    	session.invalidate();
					    }
					  }
				}

			if(session ==null )
			{
				logger.info("session not found in Signin Servlet , Creation session.");
			 session = request.getSession(true);	
			}
			String username=null;
			String password =null;
			
			
			if(session.getAttribute("username")!=null && session.getAttribute("currpwd")!=null)
			{
				 username = session.getAttribute("username")+"";
				 password = session.getAttribute("currpwd")+"";
				verify=true;
			}
			else{
			 username = request.getParameter("username");
			 password = request.getParameter("password");
			 }
			
			logger.info("Obtained parameter in Signin Servlet ::username ::"+username+":: answer :: "+answer);
			
		
			logger.info("username :"+username);
			logger.info("password :"+password);
			
			
		
			boolean isvaliduser = sbp.isValidUser(username, password);
			
			int logincount=sbp.selectFromLoginDetails(username);
			
			if(isvaliduser  && verify)
			{
				sbp.insertIntoLoginDetails(username);
			}
			else
		    {
				String error="";
				if(!isvaliduser)
				{
					error=error+"Invalid Credentials Entered.   ";
				}
				if(!verify)
				{
					error=error+"Image Text Mismatch.";
				}
		    request.setAttribute("loginError",error);
		    logger.info("Error Mss ::"+error);
		    request.getRequestDispatcher("/rbsk.jsp").forward(request, response);
			return;	
		    }
			
			if(logincount==0)
			{
				request.setAttribute("currpwd", password);
				request.setAttribute("usrname", username);
				
				session.setAttribute("currpwd", password);
		    	session.setAttribute("username", username);
				request.getRequestDispatcher("/firsttime_changepassword.jsp").forward(request, response);
				return;
			}
		    if(isvaliduser && verify)
		    {
		    	
		    	UserMappingBean usermappingbeanobj = new UserMappingBean();
		    	usermappingbeanobj=sbp.getUserMappingsObj(username);
		    	ArrayList<DropDownBean> dropdownlist = new ArrayList<DropDownBean>();
		    	dropdownlist=childbirth.selectFromApplicationProperties();
		    	session.setAttribute("applicationprop", dropdownlist);
		    	session.setAttribute("district", usermappingbeanobj.getDistrict());
		    	session.setAttribute("cluster", usermappingbeanobj.getCluster());
		    	session.setAttribute("hospitalname", usermappingbeanobj.getHospitalname());
		    	session.setAttribute("hospitaltype", usermappingbeanobj.getHospitaltype());
		    	session.setAttribute("usertype", usermappingbeanobj.getUsertype());
		    	
		    	session.setAttribute("currpwd", password);
		    	session.setAttribute("username", username);
		    	
			request.getRequestDispatcher("/birthreg.jsp").forward(request, response);
		    logger.info("SignIn servlet has been ended");
			return;
		    }
		    else
		    {
		    	String error="";
				if(!isvaliduser)
				{
				error=error+"Invalid Credentials Entered.   ";
				}
				if(!verify)
				{
					error=error+"Image Text Mismatch.";
				}
				logger.info("Error Mss ::"+error+":: Verify");
				
		    request.setAttribute("loginError",error);
		    request.getRequestDispatcher("/rbsk.jsp").forward(request, response);
			return;	 
		    }
		}
		catch(Exception e)
		{
			logger.info("Exception Occoured in : SignIn servlet");
			e.printStackTrace();
			request.setAttribute("loginError","Internal Error");
			request.getRequestDispatcher("/rbsk.jsp").forward(request, response);
			return;
		}
	}

}
