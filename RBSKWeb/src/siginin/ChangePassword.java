package siginin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ejbs.SignInUserBeanRemote;

/**
 * Servlet implementation class ChangePassword
 */
@WebServlet("/ChangePassword")
public class ChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(ChangePassword.class.getName());
	@EJB(name = "ejb/SignInUserBeanRemote")
	SignInUserBeanRemote sbp;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangePassword() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		try
		{
			String username="" ;
			HttpSession session = request.getSession(false);
			// Session validation started here
			if(session.getAttribute("username")==null )
			  {
				 out.print("Error! Invalid Session.");
				 return;
			  }
			 // Session validation ended here	
			
			logger.info("Change Password servlet Started here");
			username=session.getAttribute("username")+"" ;
			String currpassword=request.getParameter("currpwd");
			String newpassword=request.getParameter("newpwd");
			
			logger.info("Obtained parameters :: username : :"+username+":: currpassword ::"+currpassword+":: newpassword ::"+newpassword);
			
			int  updatecount =sbp.changePassword(username, currpassword, newpassword);
			
			if(updatecount>0)
			{
				session.setAttribute("username", username);
				session.setAttribute("currpwd", newpassword);
				 out.print("Success! Password updated successffully.");
				 return;
			}
			else if(updatecount==-1)
			{
				 out.print("Error! Unable to Reset the password . Please contact support.");
				 return;
			}
			else if(updatecount==-2)
			{
				out.print("Error! Current  Password mismatch .");
				 return;
			}
			else
			{
				out.print("Error! Unable to connect server .Please contact support.");
				 return;

			}
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("Change Password servlet ended here");
		}
		
	}

}
