package siginin;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.beans.UserMappingBean;
import ejbs.SignInUserBeanRemote;


/**
 * Servlet implementation class SignIn
 */
@WebServlet("/KK")
public class KK extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB(name = "ejb/SignInUserBeanRemote")
	SignInUserBeanRemote sbp;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("SignIn servlet has been called");
			HttpSession session = request.getSession(true);	
			String username = "admin"; // request.getParameter("username");
			String password = "12345" ; // request.getParameter("password");
			
			System.out.println("Obtained parameter in Signin Servlet ::username ::"+username);
			session.setAttribute("username", username);
		
			System.out.println("username :"+username);
			System.out.println("password :"+password);
		
			boolean isvaliduser = sbp.isValidUser(username, password);
		    if(isvaliduser)
		    {
		    	UserMappingBean usermappingbeanobj = new UserMappingBean();
		    	usermappingbeanobj=sbp.getUserMappingsObj(username);
		    	session.setAttribute("district", username);
		    	session.setAttribute("cluster", usermappingbeanobj.getCluster());
		    	session.setAttribute("hospitalname", usermappingbeanobj.getHospitalname());
		    	session.setAttribute("hospitaltype", usermappingbeanobj.getHospitaltype());
		    	session.setAttribute("usertype", usermappingbeanobj.getUsertype());
		    	
			request.getRequestDispatcher("/birthreg.jsp").forward(request, response);
		    System.out.println("SignIn servlet has been ended");
			return;
		    }
		    else
		    {
		    request.setAttribute("loginError","Invalid Login.");
		    request.getRequestDispatcher("/rbsk.jsp").forward(request, response);
			return;	
		    }
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured in : SignIn servlet");
			e.printStackTrace();
			request.setAttribute("loginError","Internal Error");
			request.getRequestDispatcher("/rbsk.jsp").forward(request, response);
			return;
		}
	}

}
