package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.gson.Gson;





import util.beans.UserMappingBean;
import ejbs.ChildBirthBeanRemote;

/**
 * Servlet implementation class GetDropDownValuesServlet
 */
@WebServlet("/GetUserMappingsServlet")
public class GetUserMappingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetUserMappingsServlet.class.getName());
	@EJB(name = "ejb/ChildBirthBeanRemote")
	ChildBirthBeanRemote childbirth;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUserMappingsServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("GetUserMappingsServlet servlet started here");
		try
		{
			HttpSession session = request.getSession(false);	
			String username=(String) session.getAttribute("username");
			//String username="admin";
			logger.info("user name in GetUsermapping Servlet :: "+username);
			ArrayList<UserMappingBean> usermappinglist =new ArrayList<UserMappingBean>();
			
			usermappinglist=childbirth.getUserMappings(username);
			
			Gson gson = new Gson();
			String jsonusermappingliststr = gson.toJson(usermappinglist);
			logger.info("jsonusermappingliststr = " + jsonusermappingliststr);
			response.setContentType("application/json");
			response.getWriter().write(jsonusermappingliststr);
			logger.info("End Of GetUserMappingsServlet Servlet");
			
		/*ArrayList<DropDownBean> dropdownlist =new ArrayList<DropDownBean>();
		String tablename=request.getParameter("tablename");
		dropdownlist=childbirth.GetDropDownValues(tablename);
		
		Gson gson = new Gson();
		String jsondropdownliststr = gson.toJson(dropdownlist);
		logger.info("jsondropdownliststr = " + jsondropdownliststr);
		response.setContentType("application/json");
		response.getWriter().write(jsondropdownliststr);
		logger.info("End Of GetCustAdditionalHealthInfo Servlet");*/
		return;
		
		
		}
		catch(Exception e)
		{
		logger.info("Exception occourred in GetDropDownValuesServlet Servlet");
		e.printStackTrace();
		logger.info("GetDropDownValuesServlet Servlet ended here");
		return;

		}
		
		
		
	}

}
