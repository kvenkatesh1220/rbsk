package servlets;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import util.CustIDGeneratorUtil;
import util.beans.AddressBean;
import util.beans.AntenataldetailsBean;
import util.beans.CongenitalanomaliesBean;
import util.beans.DefectsBean;
import util.beans.DeliverydetailsBean;
import util.beans.DiagnosisdetailsBean;
import util.beans.DropDownBean;
import util.beans.InvestigationdetailsBean;
import util.beans.ReportingdetailsBean;
import ejbs.ChildBirthBeanRemote;


/**
 * 
 */
@WebServlet("/BirthManagementServlet")
public class BirthManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(BirthManagementServlet.class.getName());
	@EJB(name = "ejb/ChildBirthBeanRemote")
	ChildBirthBeanRemote childbirth;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BirthManagementServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		logger.info("BirthManagement servlet started here");
		PrintWriter out = response.getWriter();
		try
		{
			String username="" ;
			HttpSession session = request.getSession(false);
			// Session validation started here
			if(session.getAttribute("username")==null )
			  {
				 out.print("Error! Invalid Session.");
				 return;
			  }
			 // Session validation ended here	
			String birthdefectspath="";
			String aadharnumber="";
			ArrayList<DropDownBean> proplist = new ArrayList<DropDownBean>();
			proplist=(ArrayList<DropDownBean>) session.getAttribute("applicationprop");
			for(DropDownBean props : proplist)
			{
				if("defects_image_uploadpath".equals(props.getName()))
				{
					birthdefectspath=props.getValue();
				}
			}
			logger.info("birthdefectspath in Birth Management Servlet::"+birthdefectspath);
			
			username=session.getAttribute("username")+"" ;
			String numofbabies=null;
			String hiddenbabyfield=null;
			int numofbabiesint=0;
			String noofbabiesintextfield=null;
			AddressBean address = new AddressBean();
			AntenataldetailsBean antenataldetails=new AntenataldetailsBean();
			CongenitalanomaliesBean congenitalanomalies= new CongenitalanomaliesBean();
			
			DeliverydetailsBean deliverydetails=new DeliverydetailsBean();
			DiagnosisdetailsBean diagnosisdetails= new DiagnosisdetailsBean();
			InvestigationdetailsBean investigationdetails=new InvestigationdetailsBean();
			ReportingdetailsBean reportingdetails= new ReportingdetailsBean();
			///
			
			
			ArrayList<DefectsBean> defectslist=new ArrayList<DefectsBean>();
			CustIDGeneratorUtil randomStringGen = new CustIDGeneratorUtil();
			String bid=randomStringGen.generateRandomString(); // +randomStringGen.generateRandomString();
			
			address.setBid(bid);
			antenataldetails.setBid(bid);
			congenitalanomalies.setBid(bid);
			deliverydetails.setBid(bid);
			diagnosisdetails.setBid(bid);
			investigationdetails.setBid(bid);
			reportingdetails.setBid(bid);
			reportingdetails.setUsername(username);
	          
			
			
			
			 if (ServletFileUpload.isMultipartContent(request)) 
		     {
		     try
		     {
		     List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	    	 logger.info(items);
	    	 logger.info(items.size());
	    	 
	    	 for(FileItem item1 : items)
    		 {
	    		 if("adharno".equals(item1.getFieldName()))
				 {
					 if(item1.getString()!=null && !"".equals(item1.getString()))
					 {
						 aadharnumber=item1.getString();
					 
					logger.info("aadharnumber  in  BirthManagement servlet ::"+aadharnumber);
					 }
				 }
    		 }
    		 
	         // **********************************************************************
	         
	       //find operating system type
    		 String operatingsystem ="";
    		 logger.info("Operating System Name :"+System.getProperty("os.name"));
    		 if(System.getProperty("os.name")!=null )
    		 {
    		 if(System.getProperty("os.name").toLowerCase().contains("win") )
    		 {
    		 operatingsystem="windows";	
    		 }
    		 else
    		 {
    		 operatingsystem="linux";		
    		 }
    		 }
    		 logger.info("Operating System :"+operatingsystem);
		
    		 // select file separator
    		 String separator="/";
    		 if("windows".equals(operatingsystem))
    		 {
    		 separator="\\";	
    		 }
    		 logger.info("separator :"+separator);
    		 
		
    		 String rootprofilepicspath=null;
    		 String birthdefectpicfolder=bid;
    		 File userprofilpicpath=null;
    		 //set userprofile rootpath
    		 if("windows".equals(operatingsystem))
    		 {
    		 rootprofilepicspath="D:\\Apache22\\htdocs\\rbsk_test\\birthdefectpics";
    		 logger.info("birthdefectspath in BirthManagement servlet  ::  "+birthdefectspath);
    		 }
    		 else
    		 {
//    		 rootprofilepicspath="/usr/local/apache/htdocs/rbsk_test/birthdefectpics";
    			 rootprofilepicspath=birthdefectspath;
    		 }
    		 logger.info("rootprofilepicspath :"+rootprofilepicspath);

    		 File currentprofilepicpath = new File(rootprofilepicspath+separator+birthdefectpicfolder);
    		 logger.info("currentuserprofilepicpath exists status ="+currentprofilepicpath.exists());
    		 if(!currentprofilepicpath.exists())
    		 {
    		 logger.info("User profile pic Root path creation status"+currentprofilepicpath.mkdirs());
    		 logger.info("current user profile pic path :"+currentprofilepicpath.getAbsolutePath());
    		 userprofilpicpath=currentprofilepicpath;
    		 }

	          
	         
	         
	         
	         //************************************************************************* 
	    	 for (FileItem item : items) {
	         logger.info(item.getFieldName()+"---"+item.getName()+"--"+item.isFormField()+"--");
	         
	         
	      // reading image file
    		 if (!item.isFormField()) 
    		 {
    		 if(item.getName()!=null && !"".equals(item.getName()))
    		 {
    			 // writing file to fileserver started here
    	         File file = new File(currentprofilepicpath, item.getName()+"@_@"+aadharnumber);
    	         item.write(file);
    	         logger.info(item.getFieldName()+"---"+item.getName()+"uploaded");
    			 // writing file to fileserver started here
    			 if ("babyphoto1".equals(item.getFieldName()))
    			 {
    				 if(item.getName()!=null && !"".equals(item.getName()))
    				 deliverydetails.setPhoto1(item.getName()+"@_@"+aadharnumber); 
    			 }
    			 if ("babyphoto2".equals(item.getFieldName()))
    			 {
    				 if(item.getName()!=null && !"".equals(item.getName()))
    				 deliverydetails.setPhoto2(item.getName()+"@_@"+aadharnumber); 
    			 }
    			 if ("idproof1".equals(item.getFieldName()))
    			 {
    				 if(item.getName()!=null && !"".equals(item.getName()))
    				 deliverydetails.setReportattachment1(item.getName()+"@_@"+aadharnumber); 
    			 }
    		 }
    		 }
    		
	          
	         if (item.isFormField()) {
	         
	         // Reading Form Fields started here
	         logger.info(item.getFieldName()+"---"+item.getString()+"--"+item.isFormField());
	         
	         //reportingdetails started here
	         
	         if("state".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setState(item.getString().toString());
	        	 logger.info("in BirthManagement servlet state : "+item.getString().toString());
	         }
	         
	         if("district".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setDistrict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet district : "+item.getString().toString());
	         }
	         
	         if("cluster".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setCluster(item.getString().toString());
	        	 logger.info("in BirthManagement servlet block : "+item.getString().toString());
	         }
	         
	         if("source".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setSource(item.getString().toString());
	        	 logger.info("in BirthManagement servlet source : "+item.getString().toString());
	         }
	         
	         
	         
	         
	         if("reportingdate".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setReportingdate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet reportingyear : "+item.getString().toString());
	         }
	         
	         if("dateofidentification".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setDateofidentification(item.getString().toString());
	        	 logger.info("in BirthManagement servlet dateofidentification : "+item.getString().toString());
	         }
	         
	         if("ageofidentification".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setAgeofidentification(item.getString().toString());
	        	 logger.info("in BirthManagement servlet ageofidentification : "+item.getString().toString());
	         } // 
	         if("hospitalname".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 reportingdetails.setHospitalname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet hospitalname : "+item.getString().toString());
	         } 
	         
	         // reportingdetails started here
	         
	         // deliverydetails started here
	         if("dateofbirth".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setDateofbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet dateofbirth : "+item.getString().toString());
	         }
	         if("birthweightingrms".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthweightingrms(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthweightingrms : "+item.getString().toString());
	         } 
	        /* if("timeofbirth".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setTimeofbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet timeofbirth : "+item.getString().toString());
	         }*/
	         
	         if("birthorder".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthorder(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthorder : "+item.getString().toString());
	         }
	         
	         if("noofbabies".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 
	        	 deliverydetails.setNoofbabies(item.getString().toString());
	        	 numofbabies=item.getString().toString();
	        	 logger.info("in BirthManagement servlet noofbabies : "+item.getString().toString()+"::::"+numofbabies);
	         }
	         
	         
	         if("noofbabiesc".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 
	        	 //deliverydetails.setNoofbabies(item.getString().toString());/
	        	 noofbabiesintextfield=item.getString().toString();
	        	 logger.info("in BirthManagement servlet noofbabiesintextfield : "+item.getString().toString()+"::::"+noofbabiesintextfield);
	         }
	         
	         
	         if("hiddenbabyfield".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 
	        	 //deliverydetails.setNoofbabies(item.getString().toString());/
	        	 hiddenbabyfield=item.getString().toString();
	        	 logger.info("in BirthManagement servlet hiddenbabyfield : "+item.getString().toString()+"::::"+hiddenbabyfield);
	         }
	         
	         if("babydeliveryas".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBabydeliveryas(item.getString().toString());
	        	 logger.info("in BirthManagement servlet babydeliveryas : "+item.getString().toString());
	         }
	         if("birthtype".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthtype(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthtype : "+item.getString().toString());
	         }
	         
	         if("sex".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setSex(item.getString().toString());
	        	 logger.info("in BirthManagement servlet sex : "+item.getString().toString());
	         }
	         if("gestationalageinweeks".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setGestationalageinweeks(item.getString().toString());
	        	 logger.info("in BirthManagement servlet gestationalageinweeks : "+item.getString().toString());
	         }
	         if("lastmensuralperiod".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setLastmensuralperiod(item.getString().toString());
	        	 logger.info("in BirthManagement servlet lastmensuralperiod : "+item.getString().toString());
	         }
	         if("birthasphyxia".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthasphyxia(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthasphyxia : "+item.getString().toString());
	         }
	         if("autopsyshowbirth".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setAutopsyshowbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet autopsyshowbirth : "+item.getString().toString());
	         }
	         if("modeofdelivery".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setModeofdelivery(item.getString().toString());
	        	 logger.info("in BirthManagement servlet modeofdelivery : "+item.getString().toString());
	         }
	         if("statusofinduction".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setStatusofinduction(item.getString().toString());
	        	 logger.info("in BirthManagement servlet statusofinduction : "+item.getString().toString());
	         }
	         if("birthplace".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthplace(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthplace : "+item.getString().toString());
	         }
	         if("birthstate".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthstate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthstate : "+item.getString().toString());
	         }
	         if("birthdistict".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthdistict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthdistict : "+item.getString().toString());
	         }
	         if("birthblock".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthblock(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthblock : "+item.getString().toString());
	         }
	         if("birthminicipality".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setBirthminicipality(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthminicipality : "+item.getString().toString());
	         }
	         if("mctsno".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setMctsno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mctsno : "+item.getString().toString());
	         }
	         if("adharno".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setAdharno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet adharno : "+item.getString().toString());
	         }
	         if("mobileno".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setMobilenumber(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mobileno : "+item.getString().toString());
	         }
	         if("typeofdelivery".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setTypeofdelivery(item.getString().toString());
	        	 logger.info("in BirthManagement servlet Typeofdelivery : "+item.getString().toString());
	         } // 
	         if("resonforcesarean".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setResonforcesarean(item.getString().toString());
	        	 logger.info("in BirthManagement servlet Resonforcesarean : "+item.getString().toString());
	         }
	         if("childname".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setChildname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet childname : "+item.getString().toString());
	         }
	         if("mothersname".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setMothersname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mothersname : "+item.getString().toString());
	         }
	         if("mothersage".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setMothersage(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mothersage : "+item.getString().toString());
	         }// 
	         if("fathername".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setFathername(item.getString().toString());
	        	 logger.info("in BirthManagement servlet fathername : "+item.getString().toString());
	         }
	         if("fathersage".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setFathersage(item.getString().toString());
	        	 logger.info("in BirthManagement servlet fathersage : "+item.getString().toString());
	         }
	         if("caste".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 deliverydetails.setCaste(item.getString().toString());
	        	 logger.info("in BirthManagement servlet caste : "+item.getString().toString());
	         }
	         
	      // deliverydetails ended here
	         
	      //Permanent address started here
	         
	         if("bhouseno".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBhouseno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bhouseno : "+item.getString().toString());
	         }
	         if("bstreetname".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBstreetname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bstreetname : "+item.getString().toString());
	         }
	         if("bareaname".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBareaname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bareaname : "+item.getString().toString());
	         }
	         if("bpostoffice".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBpostoffice(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bpostoffice : "+item.getString().toString());
	         }
	         if("bdistrict".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBdistrict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bdistrict : "+item.getString().toString());
	         }
	         if("bstate".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBstate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bstate : "+item.getString().toString());
	         }
	         
	         // temp address
	         
	         if("bthouseno".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBthouseno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bthouseno : "+item.getString().toString());
	         }
	         if("btstreetname".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBtstreetname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btstreetname : "+item.getString().toString());
	         }
	         if("btareaname".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBtareaname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btareaname : "+item.getString().toString());
	         }
	         if("btpostoffice".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBtpostoffice(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btpostoffice : "+item.getString().toString());
	         }
	         if("btdistrict".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBtdistrict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btdistrict : "+item.getString().toString());
	         }
	         if("btstate".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 address.setBtstate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btstate : "+item.getString().toString());
	         }
	         
	      ////Permanent address started here
	         
	         //antenataldetails started hrere
	         if("folicaciddetails".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setFolicaciddetails(item.getString().toString());
	        	 logger.info("in BirthManagement servlet folicaciddetails : "+item.getString().toString());
	         }
	         
	         if("hoseriousmetirialillness".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setHoseriousmetirialillness(item.getString().toString());
	        	 logger.info("in BirthManagement servlet hoseriousmetirialillness : "+item.getString().toString());
	         }
	         
	         if("horadiationexposure".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setHoradiationexposure(item.getString().toString());
	        	 logger.info("in BirthManagement servlet horadiationexposure : "+item.getString().toString());
	         }
	         
	         if("hosubstancceabuse".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setHosubstancceabuse(item.getString().toString());
	        	 logger.info("in BirthManagement servlet hosubstancceabuse : "+item.getString().toString());
	         }
	         if("parentalconsanguinity".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setParentalconsanguinity(item.getString().toString());
	        	 logger.info("in BirthManagement servlet parentalconsanguinity : "+item.getString().toString());
	         }
	         if("assistedconception".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setAssistedconception(item.getString().toString());
	        	 logger.info("in BirthManagement servlet assistedconception : "+item.getString().toString());
	         }
	         
	         if("immunisationhistory".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setImmunisationhistory(item.getString().toString());
	        	 logger.info("in BirthManagement servlet immunisationhistory : "+item.getString().toString());
	         }
	         
	         if("historyofanomalies".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setHistoryofanomalies(item.getString().toString());
	        	 logger.info("in BirthManagement servlet historyofanomalies : "+item.getString().toString());
	         }
	         if("maternaldrugs".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setMaternaldrugs(item.getString().toString());
	        	 logger.info("in BirthManagement servlet maternaldrugs : "+item.getString().toString());
	         }
	         if("maternaldrugsdesc".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setMaternaldrugsdesc(item.getString().toString());
	        	 logger.info("in BirthManagement servlet maternaldrugsdesc : "+item.getString().toString());
	         }
	         
	         if("noofpreviousabortion".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setNoofpreviousabortion(item.getString().toString());
	        	 logger.info("in BirthManagement servlet noofpreviousabortion : "+item.getString().toString());
	         }
	         
	         if("nofofpreviousstillbirth".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setNofofpreviousstillbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet nofofpreviousstillbirth : "+item.getString().toString());
	         }
	         if("headcircumference".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 antenataldetails.setHeadcircumference(item.getString().toString());
	        	 logger.info("in BirthManagement servlet headcircumference : "+item.getString().toString());
	         }
	                
	         
	        // antenataldetails ended hrere
	         
	         //Birth Defects started herre
	         
	         if("nervoussystem".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Nervous System");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet nervoussystem : "+item.getString().toString());
	         }
	         
	         if("eyerelated".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Eye Related");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet eyerelated : "+item.getString().toString());
	         }
	         
	         if("earfaceneck".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Ear/Face/Neck");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet earfaceneck : "+item.getString().toString());
	         }
	         
	         if("orafacial".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Ora Facial");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet orafacial : "+item.getString().toString());
	         }
	         
	         if("urinarytract".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Urinary Tract");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet urinarytract : "+item.getString().toString());
	         }
	         
	         if("limbrelated".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Limb Related");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet limbrelated : "+item.getString().toString());
	         }
	         
	         if("abdominalwall".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Abdominal Wall");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet abdominalwall : "+item.getString().toString());
	         }
	         
	         if("genitaldisorders".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Genital Disorders");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet genitaldisorders : "+item.getString().toString());
	         }
	         
	         if("chromosomaldisorders".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Chromosomal Disorders");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet chromosomaldisorders : "+item.getString().toString());
	         }
	         
	         if("digestivesystem".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Digestive System");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet digestivesystem : "+item.getString().toString());
	         }
	         
	         if("otheranomalies".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Other Anomalies");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet otheranomalies : "+item.getString().toString());
	         }
	         
	         if("birthdefectsinstumental".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Birth defects identified through instruments");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());

	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet birthdefectsinstumental : "+item.getString().toString());
	         }
	         
	         if("birthdefectsbloodtest".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Birth defects identified through blood test");
	        	 defects.setSource(reportingdetails.getSource());
	        	 defects.setAgeofidentification(reportingdetails.getAgeofidentification());
	        	 defects.setReportingdate(reportingdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet birthdefectsbloodtest : "+item.getString().toString());
	         }
	         
	         //Birth Defects ended here
	         
	         
	         //Congenital anomalies started here 
	         
	         if("name".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 congenitalanomalies.setName(item.getString().toString());
	        	 logger.info("in BirthManagement servlet name : "+item.getString().toString());
	        
	         }
	         
	         if("description".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 congenitalanomalies.setDescription(item.getString().toString());
	        	 logger.info("in BirthManagement servlet description : "+item.getString().toString());
	        
	         }
	         
	         if("ageat".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 congenitalanomalies.setAgeat(item.getString().toString());
	        	 logger.info("in BirthManagement servlet ageat : "+item.getString().toString());
	        
	         }
	         
	         if("code".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 congenitalanomalies.setCode(item.getString().toString());
	        	 logger.info("in BirthManagement servlet code : "+item.getString().toString());
	        
	         }
	         
	         if("confirmmed".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 congenitalanomalies.setConfirmmed(item.getString().toString());
	        	 logger.info("in BirthManagement servlet confirmmed : "+item.getString().toString());
	        
	         }
	         
	         
	       //Congenital anomalies ended here
	         
	         //Investigation details started here
	         
	         
	         if("karotype".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setKarotype(item.getString().toString());
	        	 logger.info("in BirthManagement servlet karotype : "+item.getString().toString());
	        
	         }
	         
	         if("karotypefindings".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setKarotypefindings(item.getString().toString());
	        	 logger.info("in BirthManagement servlet karotypefindings : "+item.getString().toString());
	        
	         }
	         if("bera".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setBera(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bera : "+item.getString().toString());
	        
	         }
	         if("berafindings".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setBerafindings(item.getString().toString());
	        	 logger.info("in BirthManagement servlet berafindings : "+item.getString().toString());
	        
	         }
	         
	         if("blodtestforch".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setBlodtestforch(item.getString().toString());
	        	 logger.info("in BirthManagement servlet blodtestforch : "+item.getString().toString());
	        
	         }
	         if("bloodtestfindings".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setBloodtestfindings(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestfindings : "+item.getString().toString());
	        
	         }
	         if("bloodtestforcah".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setBloodtestforcah(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestforcah : "+item.getString().toString());
	        
	         }
	         if("bloodtestforg6pd".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setBloodtestforg6pd(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestforg6pd : "+item.getString().toString());
	        
	         }
	         
	         if("bloodtestforscd".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 investigationdetails.setBloodtestforscd(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestforscd : "+item.getString().toString());
	        
	         }
	         
	         
	         //Investigation details ended here
	         
	         //Diagnosis    started here 
	         
	         if("anomaly".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setAnomaly(item.getString().toString());
	        	 logger.info("in BirthManagement servlet anomaly : "+item.getString().toString());
	        
	         }
	         if("syndrome".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setSyndrome(item.getString().toString());
	        	 logger.info("in BirthManagement servlet syndrome : "+item.getString().toString());
	        
	         }
	         if("provisional".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setProvisional(item.getString().toString());
	        	 logger.info("in BirthManagement servlet provisional : "+item.getString().toString());
	        
	         }
	         if("complediagnosis".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setComplediagnosis(item.getString().toString());
	        	 logger.info("in BirthManagement servlet complediagnosis : "+item.getString().toString());
	        
	         }
	         if("notifyingperson".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setNotifyingperson(item.getString().toString());
	        	 logger.info("in BirthManagement servlet notifyingperson : "+item.getString().toString());
	        
	         }
	         if("designationofcontact".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setDesignationofcontact(item.getString().toString());
	        	 logger.info("in BirthManagement servlet designationofcontact : "+item.getString().toString());
	        
	         }
	         if("facilityreferred".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setFacilityreferred(item.getString().toString());
	        	 logger.info("in BirthManagement servlet facilityreferred : "+item.getString().toString());
	        
	         }
	         if("provisionaldiagstat".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) && !"".equals(item.getString().trim()))
	         {
	        	 diagnosisdetails.setProvisionaldiagstat(item.getString().toString());
	        	 logger.info("in BirthManagement servlet provisionaldiagstat : "+item.getString().toString());
	        
	         }
	         
	         
	         //Diagnosis    ended  here
	         }
	         }
	    	 }
		     catch (Exception e) 
		     {
		     logger.info("upload exception occourred in CreatBlog Servlet");
		     e.printStackTrace();
		     }
	         }
			 logger.info("defectslist ::"+defectslist);
			 
			 int multipleint=0;
			 if("Multiple".equals(numofbabies))
			 {
				 
				 multipleint=Integer.parseInt(noofbabiesintextfield);
				 logger.info("numofbabies in multiple if condition :: "+multipleint);
				 deliverydetails.setNoofbabies(noofbabiesintextfield);
			 }
			 
			
			 if("2".equals(numofbabies)){
				 multipleint=Integer.parseInt(numofbabies);
				 logger.info("numofbabies in twin if condition :: "+numofbabies);
				 deliverydetails.setNoofbabies(numofbabies);
			 }
			 
			 if("1".equals(numofbabies)){
				 multipleint=Integer.parseInt(numofbabies);
				 logger.info("numofbabies in twin if condition :: "+numofbabies);
				 deliverydetails.setNoofbabies(numofbabies);
			 }
			 
			 boolean isrecordinsertedreportdatails=childbirth.insertReportingdetails(reportingdetails);
			 boolean isrecordinserteddeliverydetails=childbirth.insertDeliverydetails(deliverydetails);
			 boolean isrecordinsertedaddress=childbirth.insertAddress(address);
			 boolean isrecordinsertedantenataldetails=childbirth.insertAntenataldetails(antenataldetails);
			 if(defectslist.size()==0){
				 DefectsBean defects=new DefectsBean();
		 			defects.setBid(bid);
		 			defectslist.add(defects);
			 }
			 boolean isrecordinsertedDefects=childbirth.insertDefects(defectslist);
			 boolean isrecordinsertcongenitalanomalies=childbirth.insertCongenitalanomalies(congenitalanomalies);
			 boolean isrecordinsertedinvestigationdetails=childbirth.insertInvestigationdetails(investigationdetails);
			 boolean isrecordinsertdiagnosisdetails=childbirth.insertDiagnosisdetails(diagnosisdetails);
			 
			
			 
			 if(isrecordinsertedreportdatails)
			 {
				 logger.info(" isrecordinsertedreportdatails ::"+isrecordinsertedreportdatails);
			 }
			 if(isrecordinserteddeliverydetails)
			 {
				 logger.info(" isrecordinserteddeliverydetails ::"+isrecordinserteddeliverydetails);
			 }
	         if(isrecordinsertedaddress){
	        	 logger.info(" isrecordinsertedaddress ::"+isrecordinsertedaddress);
	         }
	         
	         if(isrecordinsertedantenataldetails){
	        	 logger.info(" isrecordinsertedantenataldetails ::"+isrecordinsertedantenataldetails);
	         }
	         
	         if(isrecordinsertedDefects){
	        	 logger.info(" isrecordinsertedDefects ::"+isrecordinsertedDefects);
	         }
	         if(isrecordinsertcongenitalanomalies){
	        	 logger.info(" isrecordinsertcongenitalanomalies ::"+isrecordinsertcongenitalanomalies);
	         }
	         if(isrecordinsertedinvestigationdetails){
	        	 logger.info(" isrecordinsertcongenitalanomalies ::"+isrecordinsertcongenitalanomalies);
	         }
	         if(isrecordinsertedinvestigationdetails){
	        	 logger.info(" isrecordinsertdiagnosisdetails ::"+isrecordinsertdiagnosisdetails);
	         }
	         
	         int hiddenbabyfieldint= Integer.parseInt(hiddenbabyfield);
			 if(hiddenbabyfieldint!=multipleint)
			 {
				 logger.info("numofbabiesint in if condition before increment ::"+hiddenbabyfieldint);
				 hiddenbabyfieldint=hiddenbabyfieldint+1; 
				 logger.info("numofbabiesint in if condition after increment::"+hiddenbabyfieldint);
				 boolean isinsertedintoaudit=childbirth.insertIntoAudit(bid, username, "Insert");
				 if(isinsertedintoaudit){
	        	 	response.setContentType("text/html");
					out.print(hiddenbabyfieldint);
					return;
				 }
			 }
	         
	         
			 if(hiddenbabyfieldint== multipleint){
				 logger.info("in final if condition "+hiddenbabyfieldint+" ::"+multipleint );
				 boolean isinsertedintoaudit=childbirth.insertIntoAudit(bid, username, "Insert");
				 if(isinsertedintoaudit){
	         	response.setContentType("text/html");
				out.print("Success");
				return;
				 }
			 }
	         }
		
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            response.setContentType("text/html");
			out.print("Error");
			return;
            
		}
		finally
		{
			out.close();
			logger.info("BirthManagementServlet ended here. ");
		}
		
	}

}
