package servlets;

import java.io.IOException;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;


import util.beans.DropDownBean;
import ejbs.ChildBirthBeanRemote;

/**
 * Servlet implementation class GetDropDownValuesServlet
 */
@WebServlet("/GetDropDownValuesServlet")
public class GetDropDownValuesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB(name = "ejb/ChildBirthBeanRemote")
	ChildBirthBeanRemote childbirth;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDropDownValuesServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("GetDropDownValuesServlet servlet started here");
		try
		{
			ArrayList<DropDownBean> dropdownlist =new ArrayList<DropDownBean>();
		String tablename=request.getParameter("tablename");
		dropdownlist=childbirth.GetDropDownValues(tablename);
		
		Gson gson = new Gson();
		String jsondropdownliststr = gson.toJson(dropdownlist);
		System.out.println("jsondropdownliststr = " + jsondropdownliststr);
		response.setContentType("application/json");
		response.getWriter().write(jsondropdownliststr);
		System.out.println("End Of GetCustAdditionalHealthInfo Servlet");
		return;
		
		
		}
		catch(Exception e)
		{
		System.out.println("Exception occourred in GetDropDownValuesServlet Servlet");
		e.printStackTrace();
		System.out.println("GetDropDownValuesServlet Servlet ended here");
		return;

		}
		
		
		
	}

}
