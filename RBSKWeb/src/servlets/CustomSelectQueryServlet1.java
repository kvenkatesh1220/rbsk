package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import custom.CustomBeanRemote;
import util.CustomHelp;
import util.CustomRowPojo;

/**
 * Servlet implementation class CustomInsertServlet1
 */
@WebServlet("/CustomSelectQueryServlet1")
public class CustomSelectQueryServlet1 extends HttpServlet {
private static final long serialVersionUID = 1L;
static Logger logger = Logger.getLogger(CustomSelectQueryServlet1.class.getName());
@EJB(name = "ejb/CustomBeanRemote")
CustomBeanRemote custbeanobj;       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomSelectQueryServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try
		{
		/* prepare select statement started here*/
			String query ="select r.state,r.district,r.cluster,r.source,d.defectcategory,d.defectsubcategory,count(d.bid) as count from reportingdetails r join defects d "
				    + "on r.bid=d.bid  "
				    + "where r.state is not null and r.district is not null and r.cluster is not null and r.source is not null and d.defectcategory is not null and d.defectsubcategory is not null  "
				    + "group by r.state,r.district,r.cluster,r.source,d.defectcategory  "
				    + "order by r.state,r.district,r.cluster,r.source,d.defectcategory,d.defectsubcategory";
		/* prepare select statement ended here*/
		
		/* calling execute select query started here*/
		ArrayList<CustomRowPojo> mycustrowsobjlist = custbeanobj.executeSelectQuery(query);
		/* calling execute select query ended here*/
		
		/* calling getJsonArrayString started here*/
		CustomHelp customhelpobj = new CustomHelp();
		String statesJsonString= customhelpobj.getJsonArrayString(mycustrowsobjlist);
		/* calling getJsonArrayString ended here*/
		
		logger.info("statesJsonString ::"+statesJsonString);
		response.setContentType("application/json");
		response.getWriter().write(statesJsonString);
		logger.info("End Of GetCities Servlet");
		return;
		}
		catch(Exception e)
		{
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		response.getWriter().write("Unable to process your request at this time. Please contact support");
		e.printStackTrace();
		}
	}
}
