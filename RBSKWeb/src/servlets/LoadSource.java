package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import ejbs.ReportsBeanRemote;
import util.beans.SelectDropDownBean;

/**
 * Servlet implementation class LoadDistricts
 */
@WebServlet("/LoadSource")
public class LoadSource extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(LoadSource.class.getName());
	@EJB(name = "ejb/ReportsBeanRemote")
	ReportsBeanRemote reportbeanobj;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadSource() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try
		{
			logger.info("LoadSource servlet started here.");
			HttpSession session = request.getSession(false);
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
		String district=request.getParameter("district");
		String cluster=request.getParameter("cluster");
		
		ArrayList<SelectDropDownBean> dropdownlist = new ArrayList<SelectDropDownBean>();
		dropdownlist=reportbeanobj.loadSource(district, cluster);
		
		Gson gson = new Gson();
		String dropdownlistJsonStr = gson.toJson(dropdownlist);
		logger.info("sourceJSON= " + dropdownlistJsonStr);
		response.setContentType("application/json");
		response.getWriter().write(dropdownlistJsonStr);
		logger.info("End Of LoadSource  Servlet");
		return;
		
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("LoadSource servlet ended here");
		}
}

}
