package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import dao.ChildBirthReportDAO;



/**
 * Servlet implementation class GetGenericdata
 */
@WebServlet("/GetGenericdataServlet")
public class GetGenericdataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetGenericdataServlet.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetGenericdataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("GetGenericdata has been started");
		ChildBirthReportDAO cbrdaoobj = new ChildBirthReportDAO();
		ArrayList<String> genericdata = new ArrayList<String>();
		logger.info("Report Duration Type :"+request.getParameter("rptduration"));
		logger.info("Reporting Date : "+request.getParameter("reportingdate"));
		logger.info("Reporting Month : "+request.getParameter("reportingmonth"));
		logger.info("Reporting Year : "+request.getParameter("reportingyear"));
		logger.info("From Date : "+request.getParameter("reportingdatef"));
		logger.info("To Date : "+request.getParameter("reportingdatet"));
		
		
		String reportdurationtype = request.getParameter("rptduration");
		String fromdate = request.getParameter("reportingdatef");
		String todate = request.getParameter("reportingdatet");
		String date="";
		String month="";
		String year="";
		String returndateval="";
		
		
		if("Date".equals(reportdurationtype)) //if reporting date is Date started here
		{
			if(request.getParameter("reportingdate")!=null && !"".equals(request.getParameter("reportingdate").trim()))
			{
				fromdate = request.getParameter("reportingdate");
				String [] tokens = request.getParameter("reportingdate").split("-");
				for(int i=0;i<tokens.length;i++)
				{
					logger.info("tokens["+i+"] ::"+tokens[i]);
					if(i==0)
					{
						year=tokens[i]; 
					}
					if(i==1)
					{
						month=tokens[i]; 
					}
					if(i==2)
					{
						date=tokens[i]; 
					}
				}
				returndateval="Date (YYYY-MM-DD): "+request.getParameter("reportingdate");
			}
			
		} //if reporting date is Date ended here.
		
		if("Month".equals(reportdurationtype)) //if reporting date is Month started here
		{
			if(request.getParameter("reportingmonth")!=null && !"".equals(request.getParameter("reportingmonth").trim()))
			{
				String [] tokens = request.getParameter("reportingmonth").split("-");
				for(int i=0;i<tokens.length;i++)
				{
					logger.info("tokens["+i+"] ::"+tokens[i]);
					if(i==0)
					{
						year=tokens[i]; 
					}
					if(i==1)
					{
						month=tokens[i]; 
					}
				}
				returndateval="Year-Month(YYYY-MM): "+request.getParameter("reportingmonth");
			}
			
		} //if reporting date is Month ended here.
		
		if("Year".equals(reportdurationtype)) //if reporting date is Year started here
		{
			year = request.getParameter("reportingyear");
			returndateval="Year (YYYY) : "+request.getParameter("reportingyear");
		}//if reporting date is Year ended here.
		
		if("Date Range".equals(reportdurationtype))
		{
			returndateval=" Date (YYYY-MM-DD) From  : "+request.getParameter("reportingdatef")+" To :"+request.getParameter("reportingdatet") ;
		}
		
		logger.info("Date:"+date);
		logger.info("Year:"+year);
		logger.info("Month:"+month);
		
		
		if(returndateval!=null && returndateval.trim()!="")
		{
			genericdata.add(returndateval);	
		}
		
		
		int birthcount = cbrdaoobj.noofbirthscount(reportdurationtype, fromdate, todate,  month,  year);
		genericdata.add("Birth's  : "+birthcount);
		
		int defectcount = cbrdaoobj.noofdefectscount(reportdurationtype, fromdate, todate,  month,  year);
		genericdata.add("Birth Defects : "+defectcount);
		
		Gson gson = new Gson();
		String genericdatalistJson = gson.toJson(genericdata);
		logger.info("genericdatalistJson :"+genericdatalistJson);
		response.setContentType("application/json");
		response.getWriter().write(genericdatalistJson);
		logger.info("GetGenericdata has been ended");
		return;
		
	}

}
