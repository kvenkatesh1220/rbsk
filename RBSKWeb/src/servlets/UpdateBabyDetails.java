package servlets;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import util.beans.BabyTotalDataBean;
import util.beans.DefectsBean;
import util.beans.DropDownBean;
import ejbs.ChildBirthBeanRemote;

import org.apache.log4j.Logger;

/**
 * 
 */
@WebServlet("/UpdateBabyDetails")
public class UpdateBabyDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
// 	private static final Logger logger = Logger.getLogger(UpdateBabyDetails.class);
	static Logger logger = Logger.getLogger(UpdateBabyDetails.class.getName());
	@EJB(name = "ejb/ChildBirthBeanRemote")
	ChildBirthBeanRemote childbirth;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateBabyDetails() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("Log4j testing");
		logger.info("BirthManagement servlet started here");
		PrintWriter out = response.getWriter();
		try
		{
			 
			HttpSession session = request.getSession(false);
			// Session validation started here
			if(session.getAttribute("username")==null )
			  {
				 out.print("Error! Invalid Session.");
				 return;
			  }
			 // Session validation ended here	
			
			String birthdefectspath="";
			ArrayList<DropDownBean> proplist = new ArrayList<DropDownBean>();
			proplist=(ArrayList<DropDownBean>) session.getAttribute("applicationprop");
			for(DropDownBean props : proplist)
			{
				if("defects_image_uploadpath".equals(props.getName()))
				{
					birthdefectspath=props.getValue();
				}
			}
			System.out.println("birthdefectspath in Birth Management Servlet::"+birthdefectspath);
			
			String numofbabies=null;
			String hiddenbabyfield=null;
			
			String noofbabiesintextfield=null;
			String bid=null;
			String babyphoto1_org =null;
			String babyphoto2_org=null;
			String idproof1_org=null;
			String aadharnumber="";
			
			String username=session.getAttribute("username")+"" ;
			
			BabyTotalDataBean babytotdetails = new BabyTotalDataBean();
			ArrayList<DefectsBean> defectslist=new ArrayList<DefectsBean>();
			 if (ServletFileUpload.isMultipartContent(request)) 
		     {
		     try
		     {
		     List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	    	 logger.error(items);
	    	 logger.info(items.size());
	    	 
	    	 for(FileItem item1 : items)
    		 {
    			 if (item1.isFormField())
    			 {
    				 logger.info("item1.getFieldName() ::"+item1.getFieldName());
    				 if("bid".equals(item1.getFieldName()))
    				 {
    					 logger.info(item1.getFieldName()+"--"+item1.getString()+"--"+item1.isFormField());
    					 logger.info("item1.getString() ::"+item1.getString());
    					 if(item1.getString()!=null && !"".equals(item1.getString()))
    					 {
    						 bid=item1.getString();
    					 
    					 logger.info("bid  in  UpdateBabyDetails servlet ::"+bid);
    					 }
    				 }	  
    				 // to delete existing image
    				 if("babyphoto1_org".equals(item1.getFieldName()))
    				 {
    					 if(item1.getString()!=null && !"".equals(item1.getString()))
    					 {
    						 babyphoto1_org=item1.getString();
    					 
    					 logger.info("babyphoto1_org  in  UpdateBabyDetails servlet ::"+babyphoto1_org);
    					 }
    				 }	 
    				 if("babyphoto2_org".equals(item1.getFieldName()))
    				 {
    					 if(item1.getString()!=null && !"".equals(item1.getString()))
    					 {
    						 babyphoto2_org=item1.getString();
    					 
    					 logger.info("babyphoto2_org  in  UpdateBabyDetails servlet ::"+babyphoto2_org);
    					 }
    				 }	 
    				 if("idproof1_org".equals(item1.getFieldName()))
    				 {
    					 if(item1.getString()!=null && !"".equals(item1.getString()))
    					 {
    						 idproof1_org=item1.getString();
    					 
    					 logger.info("idproof1_org  in  UpdateBabyDetails servlet ::"+idproof1_org);
    					 }
    				 }	  
    				 if("adharno".equals(item1.getFieldName()))
    				 {
    					 if(item1.getString()!=null && !"".equals(item1.getString()))
    					 {
    						 aadharnumber=item1.getString();
    					 
    					 logger.info("aadharnumber  in  UpdateBabyDetails servlet ::"+aadharnumber);
    					 }
    				 }
    			 }
    		 }
    		 
	         // **********************************************************************
	         
	       //find operating system type
    		 String operatingsystem ="";
    		 logger.info("Operating System Name :"+System.getProperty("os.name"));
    		 if(System.getProperty("os.name")!=null )
    		 {
    		 if(System.getProperty("os.name").toLowerCase().contains("win") )
    		 {
    		 operatingsystem="windows";	
    		 }
    		 else
    		 {
    		 operatingsystem="linux";		
    		 }
    		 }
    		 logger.info("Operating System :"+operatingsystem);
		
    		 // select file separator
    		 String separator="/";
    		 if("windows".equals(operatingsystem))
    		 {
    		 separator="\\";	
    		 }
    		 logger.info("separator :"+separator);
    		 
		
    		 String rootprofilepicspath=null;
    		 String birthdefectpicfolder=bid;
    		 File userprofilpicpath=null;
    		 //set userprofile rootpath
    		 if("windows".equals(operatingsystem))
    		 {
    		 rootprofilepicspath="D:\\Apache22\\htdocs\\rbsk_test\\birthdefectpics";
    		 }
    		 else
    		 {
    		 // rootprofilepicspath="/usr/local/apache/htdocs/birthdefectpics";
    			 rootprofilepicspath=birthdefectspath;
    		 }
    		 logger.info("rootprofilepicspath :"+rootprofilepicspath);

    		 File currentprofilepicpath = new File(rootprofilepicspath+separator+birthdefectpicfolder);
    		 logger.info("currentuserprofilepicpath exists status ="+currentprofilepicpath.exists());
    		 if(!currentprofilepicpath.exists())
    		 {
    		 logger.info("User profile pic Root path creation status"+currentprofilepicpath.mkdirs());
    		 logger.info("current user profile pic path :"+currentprofilepicpath.getAbsolutePath());
    		 userprofilpicpath=currentprofilepicpath;
    		 }
    		 System.out.println("currentprofilepicpath in UpdateBirthDetails Servlets ::"+currentprofilepicpath);
	         //************************************************************************* 
	    	 for (FileItem item : items) {
	         logger.info(item.getFieldName()+"---"+item.getName()+"--"+item.isFormField()+"--");
	         
	         
	      // reading image file
    		 if (!item.isFormField()) 
    		 {
    		 if(item.getName()!=null && !"".equals(item.getName()))
    		 {
    			 // writing file to fileserver started here
    	         File file = new File(currentprofilepicpath, item.getName()+"@_@"+aadharnumber);
    	         item.write(file);
    	         logger.info(item.getFieldName()+"---"+item.getName()+"uploaded");
    			 // writing file to fileserver started here
    			 if ("babyphoto1".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().toString().trim()))
    			 {
    				 if(item.getName()!=null && !"".equals(item.getName()))
    				 babytotdetails.setPhoto1(item.getName()+"@_@"+aadharnumber); 
    				 logger.info("photo 1 ::"+item.getName()+" photo 1 ");
    				 if(!item.getName().equalsIgnoreCase(babyphoto1_org) && !"".equals(babyphoto1_org) && babyphoto1_org!=null )
    				 {
    					 File file1 = new File(currentprofilepicpath+"/"+babyphoto1_org+"@_@"+aadharnumber);
    			       	if(file1.delete())
    			    			logger.info("File deleted ::"+babyphoto1_org+"@_@"+aadharnumber);
    			    		else
    			    			logger.info("File Not Delete ::"+babyphoto1_org+"@_@"+aadharnumber);
    				 }
    			 }
    			 if ("babyphoto2".equals(item.getFieldName()))
    			 {
    				 if(item.getName()!=null && !"".equals(item.getName().trim()))
    				 babytotdetails.setPhoto2(item.getName()+"@_@"+aadharnumber); 
    				 if(!item.getName().equalsIgnoreCase(babyphoto2_org) && !"".equals(babyphoto2_org) && babyphoto2_org!=null )
    				 {
    					 File file1 = new File(currentprofilepicpath+"/"+babyphoto2_org+"@_@"+aadharnumber);
    			       	if(file1.delete())
    			    			logger.info("File deleted ::"+babyphoto2_org+"@_@"+aadharnumber);
    			    		else
    			    			logger.info("File Not Delete ::"+babyphoto2_org+"@_@"+aadharnumber);
    				 }
    			 }
    			 if ("idproof1".equals(item.getFieldName()))
    			 {
    				 if(item.getName()!=null && !"".equals(item.getName()))
    				 babytotdetails.setReportattachment1(item.getName()+"@_@"+aadharnumber); 
    				 if(!item.getName().equalsIgnoreCase(idproof1_org) && !"".equals(idproof1_org) && idproof1_org!=null )
    				 {
    					 File file1 = new File(currentprofilepicpath+"/"+idproof1_org+"@_@"+aadharnumber);
    			       	if(file1.delete())
    			    			logger.info("File deleted ::"+idproof1_org+"@_@"+aadharnumber);
    			    		else
    			    			logger.info("File Not Delete ::"+idproof1_org+"@_@"+aadharnumber);
    				 }
    			 }
    		 }
    		 }
    		
	          
	         if (item.isFormField()) {
	         
	         // Reading Form Fields started here
	         logger.info(item.getFieldName()+"---"+item.getString()+"--"+item.isFormField());
	         
	         //reportingdetails started here
	         
	         if("state".equals(item.getFieldName()) && item.getString()!=null )
	         {
	        	 babytotdetails.setState(item.getString().toString());
	        	 logger.info("in BirthManagement servlet state : "+item.getString().toString());
	         }
	         
	         if("district".equals(item.getFieldName()) && item.getString()!=null )
	         {
	        	 babytotdetails.setDistrict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet district : "+item.getString().toString());
	         }
	         
	         if("cluster".equals(item.getFieldName()) && item.getString()!=null )
	         {
	        	 babytotdetails.setCluster(item.getString().toString());
	        	 logger.info("in BirthManagement servlet block : "+item.getString().toString());
	         }
	         
	         if("source".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().toString().trim()) )
	         {
	        	 babytotdetails.setSource(item.getString().toString());
	        	 logger.info("in BirthManagement servlet source : "+item.getString().toString());
	         }
	         
	         
	         
	         
	         if("reportingdate".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setReportingdate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet reportingyear : "+item.getString().toString());
	         }
	         
	         if("dateofidentification".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()) )
	         {
	        	 babytotdetails.setDateofidentification(item.getString().toString());
	        	 logger.info("in BirthManagement servlet dateofidentification : "+item.getString().toString());
	         }
	         
	         if("ageofidentification".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setAgeofidentification(item.getString().toString());
	        	 logger.info("in BirthManagement servlet ageofidentification : "+item.getString().toString());
	         } // 
	         if("hospitalname".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setHospitalname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet hospitalname : "+item.getString().toString());
	         } 
	         
	         // reportingdetails started here
	         
	         // deliverydetails started here
	         if("dateofbirth".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setDateofbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet dateofbirth : "+item.getString().toString());
	         }
	         if("birthweightingrms".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthweightingrms(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthweightingrms : "+item.getString().toString());
	         } 
	        /* if("timeofbirth".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setTimeofbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet timeofbirth : "+item.getString().toString());
	         }*/
	         
	         if("birthorder".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthorder(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthorder : "+item.getString().toString());
	         }
	         
	         if("noofbabies".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 
	        	 babytotdetails.setNoofbabies(item.getString().toString());
	        	 numofbabies=item.getString().toString();
	        	 logger.info("in BirthManagement servlet noofbabies : "+item.getString().toString()+"::::"+numofbabies);
	         }
	         
	         
	         if("noofbabiesc".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 
	        	 //babytotdetails.setNoofbabies(item.getString().toString());/
	        	 noofbabiesintextfield=item.getString().toString();
	        	 logger.info("in BirthManagement servlet noofbabiesintextfield : "+item.getString().toString()+"::::"+noofbabiesintextfield);
	         }
	         
	         
	         if("hiddenbabyfield".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 
	        	 //babytotdetails.setNoofbabies(item.getString().toString());/
	        	 hiddenbabyfield=item.getString().toString();
	        	 logger.info("in BirthManagement servlet hiddenbabyfield : "+item.getString().toString()+"::::"+hiddenbabyfield);
	         }
	         
	         if("babydeliveryas".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBabydeliveryas(item.getString().toString());
	        	 logger.info("in BirthManagement servlet babydeliveryas : "+item.getString().toString());
	         }
	         if("birthtype".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthtype(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthtype : "+item.getString().toString());
	         }
	         
	         if("sex".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setSex(item.getString().toString());
	        	 logger.info("in BirthManagement servlet sex : "+item.getString().toString());
	         }
	         if("gestationalageinweeks".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setGestationalageinweeks(item.getString().toString());
	        	 logger.info("in BirthManagement servlet gestationalageinweeks : "+item.getString().toString());
	         }
	         if("lastmensuralperiod".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()) )
	         {
	        	 babytotdetails.setLastmensuralperiod(item.getString().toString());
	        	 logger.info("in BirthManagement servlet lastmensuralperiod : "+item.getString().toString());
	         }
	         if("birthasphyxia".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthasphyxia(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthasphyxia : "+item.getString().toString());
	         }
	         if("autopsyshowbirth".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setAutopsyshowbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet autopsyshowbirth : "+item.getString().toString());
	         }
	         if("modeofdelivery".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setModeofdelivery(item.getString().toString());
	        	 logger.info("in BirthManagement servlet modeofdelivery : "+item.getString().toString());
	         }
	         if("statusofinduction".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setStatusofinduction(item.getString().toString());
	        	 logger.info("in BirthManagement servlet statusofinduction : "+item.getString().toString());
	         }
	         if("birthplace".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthplace(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthplace : "+item.getString().toString());
	         }
	         if("birthstate".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthstate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthstate : "+item.getString().toString());
	         }
	         if("birthdistict".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthdistict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthdistict : "+item.getString().toString());
	         }
	         if("birthblock".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthblock(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthblock : "+item.getString().toString());
	         }
	         if("birthminicipality".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBirthminicipality(item.getString().toString());
	        	 logger.info("in BirthManagement servlet birthminicipality : "+item.getString().toString());
	         }
	         if("mctsno".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setMctsno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mctsno : "+item.getString().toString());
	         }
	         if("adharno".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setAdharno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet adharno : "+item.getString().toString());
	         }
	         if("mobileno".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setMobilenumber(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mobileno : "+item.getString().toString());
	         }
	         if("typeofdelivery".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim())  )
	         {
	        	 babytotdetails.setTypeofdelivery(item.getString().toString());
	        	 logger.info("in BirthManagement servlet Typeofdelivery : "+item.getString().toString());
	         }
	         if("resonforcesarean".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()))
	         {
	        	 babytotdetails.setResonforcesarean(item.getString().toString());
	        	 System.out.println("in BirthManagement servlet Resonforcesarean : "+item.getString().toString());
	         }
	         if("childname".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setChildname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet childname : "+item.getString().toString());
	         }
	         if("mothersname".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setMothersname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mothersname : "+item.getString().toString());
	         }
	         if("mothersage".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setMothersage(item.getString().toString());
	        	 logger.info("in BirthManagement servlet mothersage : "+item.getString().toString());
	         }
	         if("fathername".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setFathername(item.getString().toString());
	        	 logger.info("in BirthManagement servlet fathername : "+item.getString().toString());
	         }
	         if("fathersage".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setFathersage(item.getString().toString());
	        	 logger.info("in BirthManagement servlet fathersage : "+item.getString().toString());
	         }
	         if("caste".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setCaste(item.getString().toString());
	        	 logger.info("in BirthManagement servlet caste : "+item.getString().toString());
	         }
	         
	      // deliverydetails ended here
	         
	      //Permanent address started here
	         
	         if("bhouseno".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBhouseno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bhouseno : "+item.getString().toString());
	         }
	         if("bstreetname".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBstreetname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bstreetname : "+item.getString().toString());
	         }
	         if("bareaname".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBareaname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bareaname : "+item.getString().toString());
	         }
	         if("bpostoffice".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBpostoffice(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bpostoffice : "+item.getString().toString());
	         }
	         if("bdistrict".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBdistrict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bdistrict : "+item.getString().toString());
	         }
	         if("bstate".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBstate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bstate : "+item.getString().toString());
	         }
	         
	         // temp babytotdetails
	         
	         if("bthouseno".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBthouseno(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bthouseno : "+item.getString().toString());
	         }
	         if("btstreetname".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBtstreetname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btstreetname : "+item.getString().toString());
	         }
	         if("btareaname".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBtareaname(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btareaname : "+item.getString().toString());
	         }
	         if("btpostoffice".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBtpostoffice(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btpostoffice : "+item.getString().toString());
	         }
	         if("btdistrict".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBtdistrict(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btdistrict : "+item.getString().toString());
	         }
	         if("btstate".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setBtstate(item.getString().toString());
	        	 logger.info("in BirthManagement servlet btstate : "+item.getString().toString());
	         }
	         
	      ////Permanent address started here
	         
	         //antenataldetails started hrere
	         if("folicaciddetails".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setFolicaciddetails(item.getString().toString());
	        	 logger.info("in BirthManagement servlet folicaciddetails : "+item.getString().toString());
	         }
	         
	         if("hoseriousmetirialillness".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setHoseriousmetirialillness(item.getString().toString());
	        	 logger.info("in BirthManagement servlet hoseriousmetirialillness : "+item.getString().toString());
	         }
	         
	         if("horadiationexposure".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setHoradiationexposure(item.getString().toString());
	        	 logger.info("in BirthManagement servlet horadiationexposure : "+item.getString().toString());
	         }
	         
	         if("hosubstancceabuse".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setHosubstancceabuse(item.getString().toString());
	        	 logger.info("in BirthManagement servlet hosubstancceabuse : "+item.getString().toString());
	         }
	         if("parentalconsanguinity".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setParentalconsanguinity(item.getString().toString());
	        	 logger.info("in BirthManagement servlet parentalconsanguinity : "+item.getString().toString());
	         }
	         if("assistedconception".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setAssistedconception(item.getString().toString());
	        	 logger.info("in BirthManagement servlet assistedconception : "+item.getString().toString());
	         }
	         
	         if("immunisationhistory".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setImmunisationhistory(item.getString().toString());
	        	 logger.info("in BirthManagement servlet immunisationhistory : "+item.getString().toString());
	         }
	         
	         if("historyofanomalies".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setHistoryofanomalies(item.getString().toString());
	        	 logger.info("in BirthManagement servlet historyofanomalies : "+item.getString().toString());
	         }
	         if("maternaldrugs".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setMaternaldrugs(item.getString().toString());
	        	 logger.info("in BirthManagement servlet maternaldrugs : "+item.getString().toString());
	         }
	         if("maternaldrugsdesc".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setMaternaldrugsdesc(item.getString().toString());
	        	 logger.info("in BirthManagement servlet maternaldrugsdesc : "+item.getString().toString());
	         }
	         
	         if("noofpreviousabortion".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setNoofpreviousabortion(item.getString().toString());
	        	 logger.info("in BirthManagement servlet noofpreviousabortion : "+item.getString().toString());
	         }
	         
	         if("nofofpreviousstillbirth".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setNofofpreviousstillbirth(item.getString().toString());
	        	 logger.info("in BirthManagement servlet nofofpreviousstillbirth : "+item.getString().toString());
	         }
	         if("headcircumference".equals(item.getFieldName()) && item.getString()!=null  )
	         {
	        	 babytotdetails.setHeadcircumference(item.getString().toString());
	        	 logger.info("in BirthManagement servlet headcircumference : "+item.getString().toString());
	         }
	                
	         
	        // antenataldetails ended hrere
	         
	         //Birth Defects started herre
	         
	         if("nervoussystem".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Nervous System");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet nervoussystem : "+item.getString().toString());
	         }
	         
	         if("eyerelated".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim())  )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Eye Related");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet eyerelated : "+item.getString().toString());
	         }
	         
	         if("earfaceneck".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Ear/Face/Neck");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet earfaceneck : "+item.getString().toString());
	         }
	         
	         if("orafacial".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Ora Facial");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet orafacial : "+item.getString().toString());
	         }
	         
	         if("urinarytract".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Urinary Tract");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet urinarytract : "+item.getString().toString());
	         }
	         
	         if("limbrelated".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Limb Related");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet limbrelated : "+item.getString().toString());
	         }
	         
	         if("abdominalwall".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Abdominal Wall");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet abdominalwall : "+item.getString().toString());
	         }
	         
	         if("genitaldisorders".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Genital Disorders");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet genitaldisorders : "+item.getString().toString());
	         }
	         
	         if("chromosomaldisorders".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim())  )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Chromosomal Disorders");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet chromosomaldisorders : "+item.getString().toString());
	         }
	         
	         if("digestivesystem".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim())  )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Digestive System");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet digestivesystem : "+item.getString().toString());
	         }
	         
	         if("otheranomalies".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Other Anomalies");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet otheranomalies : "+item.getString().toString());
	         }
	         
	         if("birthdefectsinstumental".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim()) )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Birth defects identified through instruments");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());

	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet birthdefectsinstumental : "+item.getString().toString());
	         }
	         
	         if("birthdefectsbloodtest".equals(item.getFieldName()) && item.getString()!=null  && !"".equals(item.getString().trim())  )
	         {
	 			DefectsBean defects=new DefectsBean();
	 			defects.setBid(bid);
	        	 defects.setDefectsubcategory(item.getString().toString());
	        	 defects.setDefectcategory("Birth defects identified through blood test");
	        	 defects.setSource(babytotdetails.getSource());
	        	 defects.setAgeofidentification(babytotdetails.getAgeofidentification());
	        	 defects.setReportingdate(babytotdetails.getReportingdate());
	        	 
	        	 
	        	 defectslist.add(defects);
	        	 
	        	 logger.info("in BirthManagement servlet birthdefectsbloodtest : "+item.getString().toString());
	         }
	         
	         //Birth Defects ended here
	         
	         
	         //Congenital anomalies started here 
	         
	         if("name".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setName(item.getString().toString());
	        	 logger.info("in BirthManagement servlet name : "+item.getString().toString());
	        
	         }
	         
	         if("description".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setDescription(item.getString().toString());
	        	 logger.info("in BirthManagement servlet description : "+item.getString().toString());
	        
	         }
	         
	         if("ageat".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setAgeat(item.getString().toString());
	        	 logger.info("in BirthManagement servlet ageat : "+item.getString().toString());
	        
	         }
	         
	         if("code".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setCode(item.getString().toString());
	        	 logger.info("in BirthManagement servlet code : "+item.getString().toString());
	        
	         }
	         
	         if("confirmmed".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setConfirmmed(item.getString().toString());
	        	 logger.info("in BirthManagement servlet confirmmed : "+item.getString().toString());
	        
	         }
	         
	         
	       //Congenital anomalies ended here
	         
	         //Investigation details started here
	         
	         
	         if("karotype".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setKarotype(item.getString().toString());
	        	 logger.info("in BirthManagement servlet karotype : "+item.getString().toString());
	        
	         }
	         
	         if("karotypefindings".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setKarotypefindings(item.getString().toString());
	        	 logger.info("in BirthManagement servlet karotypefindings : "+item.getString().toString());
	        
	         }
	         if("bera".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setBera(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bera : "+item.getString().toString());
	        
	         }
	         if("berafindings".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setBerafindings(item.getString().toString());
	        	 logger.info("in BirthManagement servlet berafindings : "+item.getString().toString());
	        
	         }
	         
	         if("blodtestforch".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setBloodtestforch(item.getString().toString());
	        	 logger.info("in BirthManagement servlet blodtestforch : "+item.getString().toString());
	        
	         }
	         if("bloodtestfindings".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setBloodtestfindings(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestfindings : "+item.getString().toString());
	        
	         }
	         if("bloodtestforcah".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setBloodtestforcah(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestforcah : "+item.getString().toString());
	        
	         }
	         if("bloodtestforg6pd".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setBloodtestforg6pd(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestforg6pd : "+item.getString().toString());
	        
	         }
	         
	         if("bloodtestforscd".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setBloodtestforscd(item.getString().toString());
	        	 logger.info("in BirthManagement servlet bloodtestforscd : "+item.getString().toString());
	        
	         }
	         
	         
	         //Investigation details ended here
	         
	         //Diagnosis    started here 
	         
	         if("anomaly".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setAnomaly(item.getString().toString());
	        	 logger.info("in BirthManagement servlet anomaly : "+item.getString().toString());
	        
	         }
	         if("syndrome".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setSyndrome(item.getString().toString());
	        	 logger.info("in BirthManagement servlet syndrome : "+item.getString().toString());
	        
	         }
	         if("provisional".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setProvisional(item.getString().toString());
	        	 logger.info("in BirthManagement servlet provisional : "+item.getString().toString());
	        
	         }
	         if("complediagnosis".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setComplediagnosis(item.getString().toString());
	        	 logger.info("in BirthManagement servlet complediagnosis : "+item.getString().toString());
	        
	         }
	         if("notifyingperson".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setNotifyingperson(item.getString().toString());
	        	 logger.info("in BirthManagement servlet notifyingperson : "+item.getString().toString());
	        
	         }
	         if("designationofcontact".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setDesignationofcontact(item.getString().toString());
	        	 logger.info("in BirthManagement servlet designationofcontact : "+item.getString().toString());
	        
	         }
	         if("facilityreferred".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setFacilityreferred(item.getString().toString());
	        	 logger.info("in BirthManagement servlet facilityreferred : "+item.getString().toString());
	        
	         }
	         if("provisionaldiagstat".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 babytotdetails.setProvisionaldiagstat(item.getString().toString());
	        	 logger.info("in BirthManagement servlet provisionaldiagstat : "+item.getString().toString());
	        
	         }
	         
	         babytotdetails.setBid(bid); //  setting bid .
	         //Diagnosis    ended  here
	         
	         // overriding attachiments  for update only
	         
	         if("babyphoto1_modify".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 if(item.getString()!=null && !"".equals(item.getString().trim()))
	        	 babytotdetails.setPhoto1(item.getString().toString()+"@_@"+aadharnumber);
	        	 logger.info("in BirthManagement servlet babyphoto1_modify : "+item.getString().toString());
	         }
	         
	         if("babyphoto2_modify".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 if(item.getString()!=null && !"".equals(item.getString().trim()))
	        	 babytotdetails.setPhoto2(item.getString().toString()+"@_@"+aadharnumber);
	        	 logger.info("in BirthManagement servlet babyphoto2_modify : "+item.getString().toString());
	         }
	         
	         if("idproof1_modify".equals(item.getFieldName()) && item.getString()!=null   )
	         {
	        	 if(item.getString()!=null && !"".equals(item.getString().trim()))
	        	 babytotdetails.setReportattachment1(item.getString().toString()+"@_@"+aadharnumber);
	        	 logger.info("in BirthManagement servlet idproof1_modify : "+item.getString().toString());
	         }
	         
	         }
	         }
	    	 }
		     catch (Exception e) 
		     {
		     logger.info("upload exception occourred in CreatBlog Servlet");
		     e.printStackTrace();
		     }
	         }
			 logger.info("defectslist ::"+defectslist);
			 
			 if(defectslist.size()==0){
				 DefectsBean defects=new DefectsBean();
		 			defects.setBid(bid);
		 			defectslist.add(defects);
			 }
			 
			 

				logger.info("***************************************in UPDATE******************************************************************");
				logger.info("babytotdetails.getBhouseno()  :: "+babytotdetails.getBhouseno());
				logger.info("babytotdetails.getBstreetname()  :: "+babytotdetails.getBstreetname());
				logger.info("babytotdetails.getBareaname()  :: "+babytotdetails.getBareaname());
				logger.info("babytotdetails.getBpostoffice()  :: "+babytotdetails.getBpostoffice());
				logger.info("babytotdetails.getBdistrict()  :: "+babytotdetails.getBdistrict());
				logger.info("babytotdetails.getBstate()  :: "+babytotdetails.getBstate());
				logger.info("babytotdetails.getBthouseno()  :: "+babytotdetails.getBthouseno());
				logger.info("babytotdetails.getBtstreetname()  :: "+babytotdetails.getBtstreetname());
				logger.info("babytotdetails.getBtareaname()  :: "+babytotdetails.getBtareaname());
				logger.info("babytotdetails.getBtpostoffice()  :: "+babytotdetails.getBtpostoffice());
				logger.info("babytotdetails.getBtstate()  :: "+babytotdetails.getBtstate());
				logger.info("babytotdetails.getBtdistrict()  :: "+babytotdetails.getBtdistrict());
				logger.info("babytotdetails.getFolicaciddetails()  :: "+babytotdetails.getFolicaciddetails());
				logger.info("babytotdetails.getHoseriousmetirialillness()  :: "+babytotdetails.getHoseriousmetirialillness());
				logger.info("babytotdetails.getHoradiationexposure()  :: "+babytotdetails.getHoradiationexposure());
				logger.info("babytotdetails.getHosubstancceabuse()  :: "+babytotdetails.getHosubstancceabuse());
				logger.info("babytotdetails.getParentalconsanguinity()  :: "+babytotdetails.getParentalconsanguinity());
				logger.info("babytotdetails.getAssistedconception()  :: "+babytotdetails.getAssistedconception());
				logger.info("babytotdetails.getImmunisationhistory()  :: "+babytotdetails.getImmunisationhistory());
				logger.info("babytotdetails.getMaternaldrugs()  :: "+babytotdetails.getMaternaldrugs());
				logger.info("babytotdetails.getHistoryofanomalies()  :: "+babytotdetails.getHistoryofanomalies());
				logger.info("babytotdetails.getNoofpreviousabortion()  :: "+babytotdetails.getNoofpreviousabortion());
				logger.info("babytotdetails.getNofofpreviousstillbirth()  :: "+babytotdetails.getNofofpreviousstillbirth());
				logger.info("babytotdetails.getMaternaldrugsdesc()  :: "+babytotdetails.getMaternaldrugsdesc());
				logger.info("babytotdetails.getHeadcircumference()  :: "+babytotdetails.getHeadcircumference());
				logger.info("babytotdetails.getName()  :: "+babytotdetails.getName());
				logger.info("babytotdetails.getDescription()  :: "+babytotdetails.getDescription());
				logger.info("babytotdetails.getAgeat()  :: "+babytotdetails.getAgeat());
				logger.info("babytotdetails.getCode()  :: "+babytotdetails.getCode());
				logger.info("babytotdetails.getConfirmmed()  :: "+babytotdetails.getConfirmmed());
				logger.info("babytotdetails.getDateofbirth()  :: "+babytotdetails.getDateofbirth());
				logger.info("babytotdetails.getBirthweightingrms()  :: "+babytotdetails.getBirthweightingrms());
				logger.info("babytotdetails.getBirthorder()  :: "+babytotdetails.getBirthorder());
				logger.info("babytotdetails.getBabydeliveryas()  :: "+babytotdetails.getBabydeliveryas());
				logger.info("babytotdetails.getSex()  :: "+babytotdetails.getSex());
				logger.info("babytotdetails.getGestationalageinweeks()  :: "+babytotdetails.getGestationalageinweeks());
				logger.info("babytotdetails.getLastmensuralperiod()  :: "+babytotdetails.getLastmensuralperiod());
				logger.info("babytotdetails.getBirthasphyxia()  :: "+babytotdetails.getBirthasphyxia());
				logger.info("babytotdetails.getAutopsyshowbirth()  :: "+babytotdetails.getAutopsyshowbirth());
				logger.info("babytotdetails.getModeofdelivery()  :: "+babytotdetails.getModeofdelivery());
				logger.info("babytotdetails.getStatusofinduction()  :: "+babytotdetails.getStatusofinduction());
				logger.info("babytotdetails.getBirthstate()  :: "+babytotdetails.getBirthstate());
				logger.info("babytotdetails.getBirthdistict()  :: "+babytotdetails.getBirthdistict());
				logger.info("babytotdetails.getBirthblock()  :: "+babytotdetails.getBirthblock());
				logger.info("babytotdetails.getBirthminicipality()  :: "+babytotdetails.getBirthminicipality());
				logger.info("babytotdetails.getBirthplace()  :: "+babytotdetails.getBirthplace());
				logger.info("babytotdetails.getMctsno()  :: "+babytotdetails.getMctsno());
				logger.info("babytotdetails.getAdharno()  :: "+babytotdetails.getAdharno());
				logger.info("babytotdetails.getChildname()  :: "+babytotdetails.getChildname());
				logger.info("babytotdetails.getMothersname()  :: "+babytotdetails.getMothersname());
				logger.info("babytotdetails.getMothersage()  :: "+babytotdetails.getMothersage());
				logger.info("babytotdetails.getFathersage()  :: "+babytotdetails.getFathersage());
				logger.info("babytotdetails.getMobilenumber()  :: "+babytotdetails.getMobilenumber());
				logger.info("babytotdetails.getCaste()  :: "+babytotdetails.getCaste());
				logger.info("babytotdetails.getNoofphotos()  :: "+babytotdetails.getNoofphotos());
				logger.info("babytotdetails.getNoofattachments()  :: "+babytotdetails.getNoofattachments());
				logger.info("babytotdetails.getPhoto1()  :: "+babytotdetails.getPhoto1());
				logger.info("babytotdetails.getPhoto2()  :: "+babytotdetails.getPhoto2());
				logger.info("babytotdetails.getReportattachment1()  :: "+babytotdetails.getReportattachment1());
				logger.info("babytotdetails.getReportattachment2()  :: "+babytotdetails.getReportattachment2());
				logger.info("babytotdetails.getReportattachment3()  :: "+babytotdetails.getReportattachment3());
				logger.info("babytotdetails.getReportattachment4()  :: "+babytotdetails.getReportattachment4());
				logger.info("babytotdetails.getReportattachment5()  :: "+babytotdetails.getReportattachment5());
				logger.info("babytotdetails.getReportattachment6()  :: "+babytotdetails.getReportattachment6());
				logger.info("babytotdetails.getNoofbabies()  :: "+babytotdetails.getNoofbabies());
				logger.info("babytotdetails.getTypeofdelivery()  :: "+babytotdetails.getTypeofdelivery());
				logger.info("babytotdetails.getBirthtype()  :: "+babytotdetails.getBirthtype());
				logger.info("babytotdetails.getAnomaly()  :: "+babytotdetails.getAnomaly());
				logger.info("babytotdetails.getSyndrome()  :: "+babytotdetails.getSyndrome());
				logger.info("babytotdetails.getProvisional()  :: "+babytotdetails.getProvisional());
				logger.info("babytotdetails.getComplediagnosis()  :: "+babytotdetails.getComplediagnosis());
				logger.info("babytotdetails.getNotifyingperson()  :: "+babytotdetails.getNotifyingperson());
				logger.info("babytotdetails.getDesignationofcontact()  :: "+babytotdetails.getDesignationofcontact());
				logger.info("babytotdetails.getFacilityreferred()  :: "+babytotdetails.getFacilityreferred());
				logger.info("babytotdetails.getProvisionaldiagstat()  :: "+babytotdetails.getProvisionaldiagstat());
				logger.info("babytotdetails.getKarotype()  :: "+babytotdetails.getKarotype());
				logger.info("babytotdetails.getKarotypefindings()  :: "+babytotdetails.getKarotypefindings());
				logger.info("babytotdetails.getBloodtestforch()  :: "+babytotdetails.getBloodtestforch());
				logger.info("babytotdetails.getBloodtestforcah()  :: "+babytotdetails.getBloodtestforcah());
				logger.info("babytotdetails.getBloodtestforg6Pd()  :: "+babytotdetails.getBloodtestforg6pd());
				logger.info("babytotdetails.getBloodtestforscd()  :: "+babytotdetails.getBloodtestforscd());
				logger.info("babytotdetails.getBloodtestothers()  :: "+babytotdetails.getBloodtestothers());
				logger.info("babytotdetails.getBloodtestfindings()  :: "+babytotdetails.getBloodtestfindings());
				logger.info("babytotdetails.getBera()  :: "+babytotdetails.getBera());
				logger.info("babytotdetails.getBerafindings()  :: "+babytotdetails.getBerafindings());
				logger.info("babytotdetails.getState()  :: "+babytotdetails.getState());
				logger.info("babytotdetails.getDistrict()  :: "+babytotdetails.getDistrict());
				logger.info("babytotdetails.getCluster()  :: "+babytotdetails.getCluster());
				logger.info("babytotdetails.getSource()  :: "+babytotdetails.getSource());
				logger.info("babytotdetails.getReportingdate()  :: "+babytotdetails.getReportingdate());
				logger.info("babytotdetails.getDateofidentification()  :: "+babytotdetails.getDateofidentification());
				logger.info("babytotdetails.getAgeofidentification()  :: "+babytotdetails.getAgeofidentification());
				logger.info("babytotdetails.getHospitalname()  :: "+babytotdetails.getHospitalname());
	logger.info("***************************************in UPDATE******************************************************************");
			 if(bid!=null && !"".equals(bid.trim()) )
			 {
			 boolean isbabydetailsupdated=childbirth.updateBabyTotalDetailsExceptDefects(babytotdetails);
			 boolean isdefectsdeleted=false;
			 boolean isrecordupdatedDefects=false;
			 if(isbabydetailsupdated)
			 {
				 isdefectsdeleted=childbirth.deleteFromDefects(bid);
			 }
			 
	
			 if(isdefectsdeleted)
			 {
				 isrecordupdatedDefects=childbirth.insertDefects(defectslist);
			 }
			 
	         if(isrecordupdatedDefects){
	        	 logger.info(" isrecordupdatedDefects ::"+isrecordupdatedDefects);
	         }
	         
	         if(isrecordupdatedDefects && isdefectsdeleted && isbabydetailsupdated)
	         {
	        	 boolean isinsertedintoaudit=childbirth.insertIntoAudit(bid, username, "Update");
				 if(isinsertedintoaudit){
	        	 response.setContentType("text/html");
					out.print("Success! Updated Successfully.");
					return;
				 }
	         }
			 }
			 else
			 {
				 response.setContentType("text/html");
				 boolean isinsertedintoaudit=childbirth.insertIntoAudit(bid, username, "Update");
				 if(isinsertedintoaudit){
					out.print("Error! Unable to update. Please contact Support..");
					logger.info("Bid Not found ::"+bid);
					return;
				 }
			 }
		}
		
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            response.setContentType("text/html");
			out.print("Error");
			return;
            
		}
		finally
		{
			out.close();
			logger.info("UpdateBabyDetails ended here. ");
		}
		
	}

}
