package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ejbs.ChildBirthBeanRemote;


/**
 * Servlet implementation class LoadDistricts
 */
@WebServlet("/InsertIntoAudit")
public class InsertIntoAudit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(InsertIntoAudit.class.getName());
	@EJB(name = "ejb/ChildBirthBeanRemote")
	ChildBirthBeanRemote childbirth;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertIntoAudit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try
		{
			logger.info("InsertIntoAudit Srevlet Started here ");
			HttpSession session = request.getSession(false);
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
		String bid=request.getParameter("bid");
		String action=request.getParameter("action");
		String username=session.getAttribute("username")+"";
		
		
		boolean isinserted=childbirth.insertIntoAudit(bid, username, action);
		if(isinserted)
		{
			response.setContentType("text/html");
			out.print("Success!");
			return;
		}
		else
		{
			response.setContentType("text/html");
			out.print("Error! Unable to print . Please contact Support.");
			return;
		}
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("InsertIntoAudit  servlet ended here");
		}
}

}
