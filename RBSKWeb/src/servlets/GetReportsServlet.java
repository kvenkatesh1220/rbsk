package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import util.MyDateUtil;
import util.beans.ReportBean;



import com.google.gson.Gson;

import ejbs.ReportsBeanRemote;

/**
 * Servlet implementation class GetReportsServlet
 */
@WebServlet("/GetReportsServlet")
public class GetReportsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetReportsServlet.class.getName());
	@EJB(name = "ejb/ReportsBeanRemote")
	ReportsBeanRemote reportbeanobj;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetReportsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try
		{
			logger.info("GetReportsServlet Srevlet Started here ");
			HttpSession session = request.getSession(false);
			
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
		String username=session.getAttribute("username")+"";
		String usertype=session.getAttribute("usertype")+"";
		MyDateUtil mydateutil=new MyDateUtil();
		String datefrom = request.getParameter("datefrom");		
		//java.util.Date datefromdate ;
		//java.util.Date datetodate;
		String dateto 	= request.getParameter("dateto");		
		String districts = request.getParameter("districts");								
		String clusters = request.getParameter("clusters");								
		String sources = request.getParameter("sources");								
		String hospitals = request.getParameter("hospitals");								
		String defectcategory = request.getParameter("defectcategory"); 							
		String defectsubcategory = request.getParameter("defectsubcategory");	 					
		String ageofidentification 	= request.getParameter("ageofidentification");				
		String typeofdelivery = request.getParameter("typeofdelivery");							
		String birthtype = request.getParameter("birthtype");
		String aadharnum=request.getParameter("aadharnum");
		
		String query="";
		query=query+"select  d.bid,rd.source, GROUP_CONCAT(d.defectcategory,'::',d.defectsubcategory) as defectcategory ,rd.cluster,rd.district, "
				+ " dd.sex,dd.modeofdelivery,dd.birthdistict,dd.mctsno,dd.adharno, "
				+ " dd.mothersname,dd.typeofdelivery,dd.birthtype,rd.ageofidentification ,rd.hospitalname   from reportingdetails rd "
				+ "   join defects d "
				+ " on d.bid=rd.bid "
				+ " join deliverydetails dd "
				+ " on dd.bid=rd.bid "
				+ "  where " ;
				
		if(datefrom!=null &&  !"".equals(datefrom.trim()) && dateto!=null &&  !"".equals(dateto.trim())  && aadharnum!=null &&  !"".equals(aadharnum.trim()) )
		{
			query=query + "  rd.reportingdate between '"+mydateutil.convertddmonyyyy2mysqldate(datefrom)+"' and '"+mydateutil.convertddmonyyyy2mysqldate(dateto)+"'  and dd.adharno like '%"+aadharnum+"%' ";	
		}
		if(datefrom!=null &&  !"".equals(datefrom.trim()) && dateto!=null &&  !"".equals(dateto.trim()) && (aadharnum==null ||  "".equals(aadharnum.trim())) )
		{
			query=query + "  rd.reportingdate between '"+mydateutil.convertddmonyyyy2mysqldate(datefrom)+"' and '"+mydateutil.convertddmonyyyy2mysqldate(dateto)+"'";	
		}
		if(aadharnum!=null && !"".equals(aadharnum.trim()) &&(datefrom==null ||  "".equals(datefrom.trim()) && dateto==null ||  "".equals(dateto.trim())) )
		{
			query=query		+ "    dd.adharno like '%"+aadharnum+"%' " ;
		}
		
		if(districts!=null  && !"".equals(districts.trim()) )
		{
			query=query+ "  and rd.district='"+districts+"' ";
		}
		if(clusters!=null   && !"".equals(clusters.trim()) )
		query=query+ " and rd.cluster='"+clusters+"' ";
		if(sources!=null   && !"".equals(sources.trim())  && !"Others".equals(sources))
		query=query		+ "  and rd.source='"+sources+"' ";
		if(sources!=null   && !"".equals(sources.trim())  && "Others".equals(sources))
		query=query +" and rd.source not in ('ASHA','UHC','PHC','CHC','Government_Medical_College','District_Hospital','Private_Medical_College','Private_Nursing_Home/Hospital/tertiary_centre','Mobile_Health_Team','ANM','UPHC','AH')";
		if(ageofidentification!=null   && !"".equals(ageofidentification.trim()) )
		query=query		+ "  and rd.ageofidentification='"+ageofidentification+"' ";
		if(typeofdelivery!=null   && !"".equals(typeofdelivery.trim()) )
		query=query		+ "  and dd.typeofdelivery='"+typeofdelivery+"' ";
		if(birthtype!=null   && !"".equals(birthtype.trim()) )
		query=query		+ "  and  dd.birthtype='"+birthtype+"' ";
		if(defectcategory!=null   && !"".equals(defectcategory.trim()) )
		query=query		+ "  and d.defectcategory='"+defectcategory+"'";
		if(defectsubcategory!=null   && !"".equals(defectsubcategory.trim()) )
		query=query		+ "  and d.defectsubcategory='"+defectsubcategory+"' " ;
		if(hospitals!=null   && !"".equals(hospitals.trim()) ){
			query=query		+ "  and  rd.hospitalname='"+hospitals+"' " ;
		}
		if(usertype!=null && !"".equals(usertype.trim()) && "USER".equals(usertype))
		{
			query=query		+ "  and  rd.loggedby='"+username+"' " ;
		}
		/*if(aadharnum!=null && !"".equals(aadharnum.trim()) && "USER".equals(usertype))
		{
			query=query		+ "  and  dd.adharno='"+aadharnum+"' " ;
		}*/
		
		query=query	+" group by d.bid order by d.insert_date desc  ";
		
		logger.info("Query in GetReport Servlet ::"+query);
		
		ArrayList<ReportBean> reportbeanlist = new ArrayList<ReportBean>();
		reportbeanlist=reportbeanobj.getReport(query);
		
		Gson gson = new Gson();
		String reportbeanJsonStr = gson.toJson(reportbeanlist);
		logger.info("reportbeanJsonStr= " + reportbeanJsonStr);
		response.setContentType("application/json");
		response.getWriter().write(reportbeanJsonStr);
		logger.info("End Of GetReportsServlet  Servlet");
		return;
		
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("GetReportsServlet  servlet ended here");
		}
}

}
