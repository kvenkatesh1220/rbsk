package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import util.beans.BabyTotalDataBean;
import util.beans.DropDownBean;

import com.google.gson.Gson;

import ejbs.ReportsBeanRemote;

/**
 * Servlet implementation class GetReportsServlet
 */
@WebServlet("/GetBabyTotalDetails")
public class GetBabyTotalDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetBabyTotalDetails.class.getName());    
	@EJB(name = "ejb/ReportsBeanRemote")
	ReportsBeanRemote reportbeanobj;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetBabyTotalDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try
		{
			logger.info("UpdateBabyDetails Srevlet Started here ");
			HttpSession session = request.getSession(false);
			
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
	
		String bid 	= request.getParameter("bid");		
		String file_server_url="";
		String defect_images_path="";
		String hospital_logs_path="";
		ArrayList<DropDownBean> proplist = new ArrayList<DropDownBean>();
		proplist=(ArrayList<DropDownBean>) session.getAttribute("applicationprop");
		for(DropDownBean props : proplist)
		{
			if("file_server_url".equals(props.getName()))
			{
				file_server_url=props.getValue();
			}
			 if("defect_images_path".equals(props.getName()))
			{
				 defect_images_path=props.getValue();
			}
			 if("hospital_logs_path".equals(props.getName()))
				{
				 hospital_logs_path=props.getValue();
				} 
		}
		
		
		
		ArrayList<BabyTotalDataBean> reportbeanlist = new ArrayList<BabyTotalDataBean>();
		reportbeanlist=reportbeanobj.getBabyTotalData(bid,file_server_url,defect_images_path,hospital_logs_path);
		
		Gson gson = new Gson();
		String reportbeanJsonStr = gson.toJson(reportbeanlist);
		logger.info("reportbeanJsonStr= " + reportbeanJsonStr);
		response.setContentType("application/json");
		response.getWriter().write(reportbeanJsonStr);
		logger.info("End Of UpdateBabyDetails  Servlet");
		return;
		
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("UpdateBabyDetails  servlet ended here");
		}
}

}
