package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import util.beans.DropDownBean;
import ejbs.ChildBirthBeanRemote;


/**
 * Servlet implementation class LoadDistricts
 */
@WebServlet("/CheckUpdateLimit")
public class CheckUpdateLimit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(CheckUpdateLimit.class.getName());
	@EJB(name = "ejb/ChildBirthBeanRemote")
	ChildBirthBeanRemote childbirth;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckUpdateLimit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try
		{
			logger.info("CheckUpdateLimit Srevlet Started here ");
			HttpSession session = request.getSession(false);
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
		String bid=request.getParameter("bid");
		String action=request.getParameter("action");
		String username=session.getAttribute("username")+"";
		String usertype=session.getAttribute("usertype")+"";
		
		int count=childbirth.selectFromAudit(username, action, bid);
		int prop_count=-1;
		ArrayList<DropDownBean> proplist = new ArrayList<DropDownBean>();
		proplist=(ArrayList<DropDownBean>) session.getAttribute("applicationprop");
		for(DropDownBean props : proplist)
		{
			if("user_edit_limit".equals(props.getName()))
			{
				prop_count=Integer.parseInt(props.getValue());
			}
		}
		logger.info("User Update Limit in Check Update Limit servelet ::"+prop_count+":: count ::"+count);
		if(prop_count!=-1)
		{
		if(count>=prop_count && "USER".equals(usertype) )
		{
			response.setContentType("text/html");
			out.print("Error! you Exceeded your Edit Limit. Please contact Support.");
			return;
		}
		else
		{
			response.setContentType("text/html");
			out.print("Success!");
			return;
		}
		}
		
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("CheckUpdateLimit  servlet ended here");
		}
}

}
