package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import util.beans.MonthwiseReportBean;

import com.google.gson.Gson;

import dao.ChildBirthReportDAO;



/**
 * Servlet implementation class GetDefectsReport
 */
@WebServlet("/GetSourceWiseReportServlet")
public class GetSourceWiseReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetSourceWiseReportServlet.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetSourceWiseReportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("GetDefectsReport has been started");
		ArrayList<MonthwiseReportBean> monwiserptobjlist = new ArrayList<MonthwiseReportBean>();
		ChildBirthReportDAO cbrdaoobj = new ChildBirthReportDAO();
		logger.info("Report Duration Type :"+request.getParameter("rptduration"));
		logger.info("Reporting Date : "+request.getParameter("reportingdate"));
		logger.info("Reporting Month : "+request.getParameter("reportingmonth"));
		logger.info("Reporting Year : "+request.getParameter("reportingyear"));
		logger.info("From Date : "+request.getParameter("reportingdatef"));
		logger.info("To Date : "+request.getParameter("reportingdatet"));
		
		
		String reportdurationtype = request.getParameter("rptduration");
		String fromdate = request.getParameter("reportingdatef");
		String todate = request.getParameter("reportingdatet");
		String date="";
		String month="";
		String year="";
		
		
		if("Date".equals(reportdurationtype)) //if reporting date is Date started here
		{
			if(request.getParameter("reportingdate")!=null && !"".equals(request.getParameter("reportingdate").trim()))
			{
				fromdate = request.getParameter("reportingdate");
				String [] tokens = request.getParameter("reportingdate").split("-");
				for(int i=0;i<tokens.length;i++)
				{
					logger.info("tokens["+i+"] ::"+tokens[i]);
					if(i==0)
					{
						year=tokens[i]; 
					}
					if(i==1)
					{
						month=tokens[i]; 
					}
					if(i==2)
					{
						date=tokens[i]; 
					}
				}
			}
			
		} //if reporting date is Date ended here.
		
		if("Month".equals(reportdurationtype)) //if reporting date is Month started here
		{
			if(request.getParameter("reportingmonth")!=null && !"".equals(request.getParameter("reportingmonth").trim()))
			{
				String [] tokens = request.getParameter("reportingmonth").split("-");
				for(int i=0;i<tokens.length;i++)
				{
					logger.info("tokens["+i+"] ::"+tokens[i]);
					if(i==0)
					{
						year=tokens[i]; 
					}
					if(i==1)
					{
						month=tokens[i]; 
					}
				}
			}
			
		} //if reporting date is Month ended here.
		
		if("Year".equals(reportdurationtype)) //if reporting date is Year started here
		{
			year = request.getParameter("reportingyear");
		}//if reporting date is Year ended here.
		
		logger.info("Date:"+date);
		logger.info("Year:"+year);
		logger.info("Month:"+month);
		
		monwiserptobjlist = cbrdaoobj.sourcewisecount(reportdurationtype, fromdate,todate, month, year);
		Gson gson = new Gson();
		String monwiserptobjlistJson = gson.toJson(monwiserptobjlist);
		logger.info("monwiserptobjlistJson :"+monwiserptobjlistJson);
		response.setContentType("application/json");
		response.getWriter().write(monwiserptobjlistJson);
		logger.info("GetDefectsReport has been ended");
		return;
		
	}

}
