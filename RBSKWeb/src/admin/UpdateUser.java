package admin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.AdminDAO;

/**
 * Servlet implementation class CreateUsers
 */
@WebServlet("/UpdateUser")
public class UpdateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(UpdateUser.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		try
		{
			logger.info("UpdateUser servlet started here.");
			HttpSession session = request.getSession(false);
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
		 String username=request.getParameter("username");
		//String password=request.getParameter("password");
		String districts=request.getParameter("districts");
		String clusters=request.getParameter("clusters");
		String hospitalname=request.getParameter("hospitalname");
		String hospitaltype=request.getParameter("hospitaltype");
		String isadmin=request.getParameter("isadmin");
		
		AdminDAO admindaoobj=new AdminDAO();
		int count=admindaoobj.updateUser(username , districts, clusters, hospitalname, hospitaltype, isadmin);
		if(count>0)
		{
			 	response.setContentType("text/html");
				out.print("Success! User details Updated  Successfully.");
				return;
		}
		else if(count==0)
		{
			 	response.setContentType("text/html");
				out.print("Error! Unable to Update  user details. Please try again");
				return;
		}
		else
		{
			response.setContentType("text/html");
			out.print("Error! Unable to process this time . Please contact support.");
			return;
		}
		
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("UpdateUser servlet ended here");
		}

	}

}
