package admin;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import util.CustIDGeneratorUtil;
import util.beans.DropDownBean;
import util.beans.HospitalBean;

import ejbs.ReportsBeanRemote;


/**
 * 
 */
@WebServlet("/UpdateHospitalDet")
public class UpdateHospitalDet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(UpdateHospitalDet.class.getName());
	
	@EJB(name = "ejb/ReportsBeanRemote")
	ReportsBeanRemote reportsbean;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateHospitalDet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		logger.info("AddHospital servlet started here");
		PrintWriter out = response.getWriter();
		try
		{
			String username="" ;
			HttpSession session = request.getSession(false);
			// Session validation started here
			if(session.getAttribute("username")==null )
			  {
				 out.print("Error! Invalid Session.");
				 return;
			  }
			 // Session validation ended here	

			String hospital_logs_path="";
			ArrayList<DropDownBean> proplist = new ArrayList<DropDownBean>();
			proplist=(ArrayList<DropDownBean>) session.getAttribute("applicationprop");
			for(DropDownBean props : proplist)
			{
				if("hospital_logos_upload_path".equals(props.getName()))
				{
					hospital_logs_path=props.getValue();
				}
			}
			logger.info("hospital_logs_path in Birth Management Servlet::"+hospital_logs_path);
			username=session.getAttribute("username")+"" ;
			
			CustIDGeneratorUtil randomStringGen = new CustIDGeneratorUtil();
			String hospitalid=null;
			
			HospitalBean deliverydetails=new HospitalBean();
			//deliverydetails.setHospitalid(hospitalid);
			 if (ServletFileUpload.isMultipartContent(request)) 
		     {
		     try
		     {
		     List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	    	 logger.info(items);
	    	 logger.info(items.size());
	    	 
	    	 
	    	 for(FileItem item1 : items)
    		 {
    			 if (item1.isFormField())
    			 {
    				 logger.info("item1.getFieldName() ::"+item1.getFieldName());
    				 if("update_hospitalid".equals(item1.getFieldName()))
    				 {
    					 logger.info(item1.getFieldName()+"--"+item1.getString()+"--"+item1.isFormField());
    					 logger.info("item1.getString() ::"+item1.getString());
    					 if(item1.getString()!=null && !"".equals(item1.getString()))
    					 {
    						 hospitalid=item1.getString();
    					 
    					 logger.info("bid  in  UpdateBabyDetails servlet ::"+hospitalid);
    					 }
    				 }	    			 
    			 }
    		 }

    		 
	         // **********************************************************************
	         
	       //find operating system type
    		 String operatingsystem ="";
    		 logger.info("Operating System Name :"+System.getProperty("os.name"));
    		 if(System.getProperty("os.name")!=null )
    		 {
    		 if(System.getProperty("os.name").toLowerCase().contains("win") )
    		 {
    		 operatingsystem="windows";	
    		 }
    		 else
    		 {
    		 operatingsystem="linux";		
    		 }
    		 }
    		 logger.info("Operating System :"+operatingsystem);
		
    		 // select file separator
    		 String separator="/";
    		 if("windows".equals(operatingsystem))
    		 {
    		 separator="\\";	
    		 }
    		 logger.info("separator :"+separator);
    		 
		
    		 String rootprofilepicspath=null;
    		 String birthdefectpicfolder=hospitalid;
    		 File userprofilpicpath=null;
    		 //set userprofile rootpath
    		 if("windows".equals(operatingsystem))
    		 {
    		 rootprofilepicspath="D:\\Apache22\\htdocs\\rbsk_test\\hospitallogos";
    		 logger.info("in AddHospital Servlets ::"+hospital_logs_path);
    		 }
    		 else
    		 {
    		 // rootprofilepicspath="/usr/local/apache/htdocs/hospitallogos";
    			 rootprofilepicspath= hospital_logs_path;
    		 }
    		 logger.info("rootprofilepicspath :"+rootprofilepicspath);

    		 File currentprofilepicpath = new File(rootprofilepicspath+separator+birthdefectpicfolder);
    		 logger.info("currentuserprofilepicpath exists status ="+currentprofilepicpath.exists());
    		 if(!currentprofilepicpath.exists())
    		 {
    		 logger.info("User profile pic Root path creation status"+currentprofilepicpath.mkdirs());
    		 logger.info("current user profile pic path :"+currentprofilepicpath.getAbsolutePath());
    		 userprofilpicpath=currentprofilepicpath;
    		 }

	          
	         
	         
	         
	         //************************************************************************* 
	    	 for (FileItem item : items) {
	         logger.info(item.getFieldName()+"---"+item.getName()+"--"+item.isFormField()+"--");
	         
	         
	      // reading image file
    		 if (!item.isFormField()) 
    		 {
    		 if(item.getName()!=null && !"".equals(item.getName()))
    		 {
    			 // writing file to fileserver started here
    	         File file = new File(currentprofilepicpath, item.getName());
    	         item.write(file);
    	         logger.info(item.getFieldName()+"---"+item.getName()+"uploaded");
    			 // writing file to fileserver started here
    			 if ("update_hsplogo".equals(item.getFieldName()))
    			 {
    				 deliverydetails.setLogo(item.getName()); 
    			 }
    			
    		 }
    		 }
    		
	          
	         if (item.isFormField()) {
	        	 
	        	 logger.info(item.getFieldName()+"---"+item.getString()+"--"+item.isFormField());
	        	 
	        	 if("update_hspname".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setHospitalname(item.getString().toString());
		        	 logger.info("in AddHospital servlet hspname : "+item.getString().toString());
		         }
	        	 
	        	 if("update_regnum".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setRegnum(item.getString().toString());
		        	 logger.info("in AddHospital servlet regnum : "+item.getString().toString());
		         }
	        	 
	        	 if("update_phnum".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setPhnum(item.getString().toString());
		        	 logger.info("in AddHospital servlet phnum : "+item.getString().toString());
		         }
	        	 
	        	 if("update_det_location".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setLoccation(item.getString().toString());
		        	 logger.info("in AddHospital servlet det_location : "+item.getString().toString());
		         }
	        	 
	        	 if("update_det_country".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setCountry(item.getString().toString());
		        	 logger.info("in AddHospital servlet det_country : "+item.getString().toString());
		         }
	        	 
	        	 if("update_det_state".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setState(item.getString().toString());
		        	 logger.info("in AddHospital servlet det_state : "+item.getString().toString());
		         }
	        	 
	        	 if("update_hsp_districts2".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setDistrict(item.getString().toString());
		        	 logger.info("in AddHospital servlet update_hsp_districts2 : "+item.getString().toString());
		         }
	        	 if("update_det_latitude".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setLatitude(item.getString().toString());
		        	 logger.info("in AddHospital servlet det_latitude : "+item.getString().toString());
		         }
	        	 if("update_det_longitude".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setLongitude(item.getString().toString());
		        	 logger.info("in AddHospital servlet det_longitude : "+item.getString().toString());
		         }
	        	 if("update_address".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setAddress(item.getString().toString());
		        	 logger.info("in AddHospital servlet address : "+item.getString().toString());
		         }
	        	 deliverydetails.setHospitalid(hospitalid); // setting hospital id
	        	 
	        	 if("logo_modify".equals(item.getFieldName()) && item.getString()!=null && !"".equals(item.getString().trim()))
		         {
	        		 deliverydetails.setLogo(item.getString().toString());
		        	 logger.info("in AddHospital servlet logo_modify : "+item.getString().toString());
		         }
	        	 
	         }
	         }
	    	 }
		     catch (Exception e) 
		     {
		     logger.info("upload exception occourred in AddHospital Servlet");
		     e.printStackTrace();
		     }
	         }
			 
			 boolean isinsertedintohsp=reportsbean.updateHospitalDetails(deliverydetails);
			 if(isinsertedintohsp)
			 {
				 response.setContentType("text/html");
					out.print("Success! Hospital details are Recorded.");
					return;
			 }
			 else
			 {
				 response.setContentType("text/html");
					out.print("Error! unable to proceed . Please contact support.");
					return;
			 }
	         }
		
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            response.setContentType("text/html");
			out.print("Error");
			return;
            
		}
		finally
		{
			out.close();
			logger.info("AddHospitalServlet ended here. ");
		}
		
	}

}
