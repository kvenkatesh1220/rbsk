package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import util.beans.AuditReportBean;

import com.google.gson.Gson;

import ejbs.ReportsBeanRemote;

/**
 * Servlet implementation class GetReportsServlet
 */
@WebServlet("/GetAuditReportsServlet")
public class GetAuditReportsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetAuditReportsServlet.class.getName());
       
	@EJB(name = "ejb/ReportsBeanRemote")
	ReportsBeanRemote reportbeanobj;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAuditReportsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try
		{
			logger.info("GetAuditReportsServlet Srevlet Started here ");
			HttpSession session = request.getSession(false);
			
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
	
		String auditdatefrom 	= request.getParameter("auditdatefrom");		
		String auditdateto = request.getParameter("auditdateto");								
		String clusters = request.getParameter("clusters");								
		String districts = request.getParameter("districts");								
		String audit = request.getParameter("audit");			
		
		String query="";
		if(audit.equals("Login_Details"))
		{
		query=query+"select lu.username,LOGIN_DT as date ,LOGOUT_DT,lu.district,lu.cluster from login_details ld "
				+ " join loginusers lu "
				+ " on lu.username=ld.CUST_ID  where LOGIN_DT  between str_to_date('"+auditdatefrom+"','%d-%b-%Y %T') "
				+ "and str_to_date('"+auditdateto+"','%d-%b-%Y %T') ";
		}
		
		if(!audit.equals("Login_Details"))
		{
			
			query=query+" select lu.username ,lu.district,lu.cluster , action ,audit_date as date,ld.bid  from audit ld "
					+ " join loginusers lu "
					+ " on lu.username=ld.user_name  where audit_date  between str_to_date('"+auditdatefrom+"','%d-%b-%Y %T') "
				+ "and str_to_date('"+auditdateto+"','%d-%b-%Y %T')  and action='"+audit+"'  ";
		}
		
		if(districts!=null  && !"".equals(districts.trim()) )
		{
			query=query+ "  and lu.district='"+districts+"' ";
		}
		if(clusters!=null   && !"".equals(clusters.trim()) )
		query=query+ " and lu.cluster='"+clusters+"' ";
		
		
		query=query	+" order by date  ";
		
		logger.info("Query in GetAuditReportsServlet  Servlet ::"+query);
		
		ArrayList<AuditReportBean> reportbeanlist = new ArrayList<AuditReportBean>();
		reportbeanlist=reportbeanobj.getAuditReport(query, audit);
		
		Gson gson = new Gson();
		String reportbeanJsonStr = gson.toJson(reportbeanlist);
		logger.info("AuditreportbeanJsonStr= " + reportbeanJsonStr);
		response.setContentType("application/json");
		response.getWriter().write(reportbeanJsonStr);
		logger.info("End Of GetAuditReportsServlet  Servlet");
		return;
		
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("GetAuditReportsServlet  servlet ended here");
		}
}

}
