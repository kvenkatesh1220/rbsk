package admin;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import dao.BulkUploadDAO;
import util.CustIDGeneratorUtil;
import util.beans.BulkUploadBean;
import util.beans.DropDownBean;
import ejbs.ReportsBeanRemote;

import java.io.FileInputStream;


/**
 * 
 */
@WebServlet("/BulkUpload")
public class BulkUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(BulkUpload.class.getName());
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BulkUpload() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println("BulkUpload servlet started here");
		PrintWriter out = response.getWriter();
		try
		{
			String username="" ;
			HttpSession session = request.getSession(false);
			// Session validation started here
			if(session.getAttribute("username")==null )
			  {
				 out.print("Error! Invalid Session.");
				 return;
			  }
			 // Session validation ended here	
			String rootprofilepicspath=null;
			String bulkupload_path="";
			String uploadfilename=null;
			String state="";
			String country="";
			ArrayList<DropDownBean> proplist = new ArrayList<DropDownBean>();
			proplist=(ArrayList<DropDownBean>) session.getAttribute("applicationprop");
			for(DropDownBean props : proplist)
			{
				if("bulkupload_path".equals(props.getName()))
				{
					bulkupload_path=props.getValue();
				}
				if("state".equals(props.getName()))
				{
					state=props.getValue();
				}
				if("country".equals(props.getName()))
				{
					country=props.getValue();
				}
				
			}
			System.out.println("bulkupload_path in BulkUpload Servlet::"+bulkupload_path);
			
			username=session.getAttribute("username")+"" ;
			
			CustIDGeneratorUtil randomStringGen = new CustIDGeneratorUtil();
			String bulkuploadfolderid=randomStringGen.generateRandomString()+randomStringGen.generateRandomString();
			
			BulkUploadBean  bulkuploadbean=new BulkUploadBean();
			bulkuploadbean.setUserid(username);
			 if (ServletFileUpload.isMultipartContent(request)) 
		     {
		     try
		     {
		     List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	    	 System.out.println(items);
	    	 System.out.println(items.size());
	        // **********************************************************************
	         
	       //find operating system type
    		 String operatingsystem ="";
    		 System.out.println("Operating System Name :"+System.getProperty("os.name"));
    		 if(System.getProperty("os.name")!=null )
    		 {
    		 if(System.getProperty("os.name").toLowerCase().contains("win") )
    		 {
    		 operatingsystem="windows";	
    		 }
    		 else
    		 {
    		 operatingsystem="linux";		
    		 }
    		 }
    		 System.out.println("Operating System :"+operatingsystem);
		
    		 // select file separator
    		 String separator="/";
    		 if("windows".equals(operatingsystem))
    		 {
    		 separator="\\";	
    		 }
    		 System.out.println("separator :"+separator);
    		 
		
    		 //String rootprofilepicspath=null;
    		 String birthdefectpicfolder=bulkuploadfolderid;
    		 File userprofilpicpath=null;
    		 //set userprofile rootpath
    		 if("windows".equals(operatingsystem))
    		 {
    		 rootprofilepicspath="D:\\Apache22\\htdocs\\rbsk_test\\bulkuploads\\";
    		 System.out.println("in BulkUpload Servlets ::"+bulkupload_path);
    		 }
    		 else
    		 {
    		 // rootprofilepicspath="/usr/local/apache/htdocs/hospitallogos";
    		 rootprofilepicspath= bulkupload_path;
    		 }
    		 System.out.println("rootprofilepicspath :"+rootprofilepicspath);

    		 File currentprofilepicpath = new File(rootprofilepicspath+separator+birthdefectpicfolder);
    		 System.out.println("currentuserprofilepicpath exists status ="+currentprofilepicpath.exists());
    		 if(!currentprofilepicpath.exists())
    		 {
    		 System.out.println("User profile pic Root path creation status"+currentprofilepicpath.mkdirs());
    		 System.out.println("current user profile pic path :"+currentprofilepicpath.getAbsolutePath());
    		 userprofilpicpath=currentprofilepicpath;
    		 }
	         //************************************************************************* 
	    	 for (FileItem item : items) {
	         System.out.println(item.getFieldName()+"---"+item.getName()+"--"+item.isFormField()+"--");
	         
	         
	      // reading image file
    		 if (!item.isFormField()) 
    		 {
    		 if(item.getName()!=null && !"".equals(item.getName()))
    		 {
    			 // writing file to fileserver started here
    	         File file = new File(currentprofilepicpath, item.getName());
    	         item.write(file);
    	         System.out.println(item.getFieldName()+"---"+item.getName()+"uploaded");
    			 // writing file to fileserver started here
    			 if ("uploadfilename".equals(item.getFieldName()))
    			 {
    				 bulkuploadbean.setFilename(item.getName()); 
    				 uploadfilename=item.getName();
    			 }
    		 }
    		 }
    		 
	         }
	    	 }
		     catch (Exception e) 
		     {
		     System.out.println("upload exception occourred in BulkUpload Servlet");
		     e.printStackTrace();
		     }
	         }
			 
			 System.out.println("*****************:: "+rootprofilepicspath+bulkuploadfolderid+"\\"+uploadfilename);
			 FileInputStream input = new FileInputStream(rootprofilepicspath+bulkuploadfolderid+"\\"+uploadfilename);
	            POIFSFileSystem fs = new POIFSFileSystem( input );
	            HSSFWorkbook wb = new HSSFWorkbook(fs);
	            HSSFSheet sheet = wb.getSheetAt(0);
	            Row row;
	            String distname="" ,Cluster="" ,HSPname="",hsptype="",phnum="",regnum="",address="";
	            for(int i=1; i<2; i++){
	                row = sheet.getRow(i);
	                //String Sno =  ""; //row.getCell(0).getStringCellValue();
	                 distname = row.getCell(1).getStringCellValue();
	                 Cluster = row.getCell(2).getStringCellValue();
					
					 HSPname = row.getCell(3).getStringCellValue();
					 hsptype = row.getCell(4).getStringCellValue();
					 phnum = row.getCell(5).getStringCellValue();
					 regnum = row.getCell(6).getStringCellValue();
					 address = row.getCell(7).getStringCellValue();
					System.out.println("Name of the District ::"+distname+":: Cluster ::"+Cluster+":: HSPname"+HSPname+" :: hsptype::"+hsptype+":: phnum :: "+phnum+"::  regnum ::"+regnum+" :: address ::"+address);
	                System.out.println("Import rows "+i);
	            }
	            input.close();
	            boolean isvalidexcel=false;
	            if(distname.equals("Name of the District") && Cluster.equals("Name of the Cluster") && HSPname.equals("Name of the Hospital") &&  hsptype.equals("Pvt./Govt Hospital") &  phnum.equals("Phone Number") &&  regnum.equals("Reg. Number") && address.equals("Address") )
	            {
	            	isvalidexcel=true;
	            	System.out.println("In BulkUpload Servlet isvalidexcel ::"+ isvalidexcel );
	            	
	            }
	            else
	            {
	            	System.out.println("In BulkUpload Servlet isvalidexcel ::"+ isvalidexcel );
	            	response.setContentType("text/html");
					out.print("Error! Please Upload the Proper Excel or Contact Support.");
						return;
	            }
	            BulkUploadDAO bulkuploaddao=new BulkUploadDAO();
	            boolean isinsertedintobulkupload=false;
	            if(isvalidexcel)
			  isinsertedintobulkupload=bulkuploaddao.insertIntoBulkUploads(bulkuploadfolderid,username,uploadfilename);
	            boolean isinsertedintologinuserspre=false;
	            if(isinsertedintobulkupload)
	            {
	            // 	String filepath=rootprofilepicspath+bulkuploadfolderid+"\\"+uploadfilename;
	            	isinsertedintologinuserspre=bulkuploaddao.insertIntoLoginUsersPre(sheet,country,state,username);
	            }
			 if(isinsertedintologinuserspre)
			 {
				 response.setContentType("text/html");
					out.print("Success! Hospital details are Recorded.");
					return;
			 }
			 else
			 {
				 response.setContentType("text/html");
					out.print("Error! unable to proceed . Please contact support.");
					return;
			 }
	         }
		
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            response.setContentType("text/html"); 
			out.print("Error");
			return;
            
		}
		finally
		{
			out.close();
			System.out.println("BulkUpload ended here. ");
		}
		
	}

}
