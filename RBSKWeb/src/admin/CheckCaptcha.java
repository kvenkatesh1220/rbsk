package admin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class CheckCaptcha
 */
@WebServlet("/CheckCaptcha")
public class CheckCaptcha extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(CheckCaptcha.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckCaptcha() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String term = request.getParameter("captchaterm");
		logger.info("term :"+term);
		HttpSession session = request.getSession(false);
		PrintWriter out = response.getWriter();
		String captcha = (String) session.getAttribute("captcha");
		logger.info("captcha :"+captcha);
		if (captcha != null ) {

		    if (captcha.equals(term)) {
			  out.print("Correct");
		    } else {
		          out.print("Incorrect");
		    }
		  }
		
	}

}
