package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import util.beans.UserMappingBean;
import dao.AdminDAO;

/**
 * Servlet implementation class CreateUsers
 */
@WebServlet("/GetUserDetails")
public class GetUserDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetUserDetails.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUserDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		try
		{
			logger.info("GetUserDetails servlet started here.");
			HttpSession session = request.getSession(false);
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
		 String username=request.getParameter("username");
		
		 ArrayList<UserMappingBean> usermappingsbeanlist = new ArrayList<UserMappingBean>();
		AdminDAO admindaoobj=new AdminDAO();
		usermappingsbeanlist=admindaoobj.getUserDetails(username);
		
		Gson gson = new Gson();
		String UserJsonStr = gson.toJson(usermappingsbeanlist);
		logger.info("UserJsonStr = " + UserJsonStr);
		response.setContentType("application/json");
		response.getWriter().write(UserJsonStr);
		logger.info("End Of GetUserDetails  Servlet");
		return;
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("GetUserDetails servlet ended here");
		}

	}

}
