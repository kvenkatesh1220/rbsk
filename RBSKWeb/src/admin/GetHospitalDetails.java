package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import util.beans.HospitalBean;
import dao.AdminDAO;

/**
 * Servlet implementation class CreateUsers
 */
@WebServlet("/GetHospitalDetails")
public class GetHospitalDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetHospitalDetails.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetHospitalDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		try
		{
			logger.info("GetHospitalDetails servlet started here.");
			HttpSession session = request.getSession(false);
		// Session validation started here
		if(session.getAttribute("username")==null )
		  {
			 out.print("Error! Invalid Session.");
			 return;
		  }
		 // Session validation ended here
		 String districts=request.getParameter("districts");
		 String hspname=request.getParameter("hspname");
		 String hspid=request.getParameter("hospitalid");
		
		 ArrayList<HospitalBean> hospitallist = new ArrayList<HospitalBean>();
		AdminDAO admindaoobj=new AdminDAO();
		hospitallist=admindaoobj.getHospitalDetails(districts, hspname,hspid);
		
		Gson gson = new Gson();
		String UserJsonStr = gson.toJson(hospitallist);
		logger.info("hospitalJSON = " + UserJsonStr);
		response.setContentType("application/json");
		response.getWriter().write(UserJsonStr);
		logger.info("End Of GetHospitalDetails  Servlet");
		return;
		}
		catch(Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Unable to process your request at this time. Please contact support");
            e.printStackTrace();
            return ;
		}
		finally
		{
			out.close();
			logger.info("GetHospitalDetails servlet ended here");
		}

	}

}
